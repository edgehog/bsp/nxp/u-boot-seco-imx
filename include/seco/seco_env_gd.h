#ifndef __SECO_ENV_GD__
#define __SECO_ENV_GD__



/*  __________________________________________________________________________
 * |                                                                          |
 * |                               BOOT PARAMETERS                            |
 * |__________________________________________________________________________|
*/
#define SECO_DEV_LABEL_NONE      "Not used"				/* None device to use (file will be not loaded) */
#define SECO_DEV_LABEL_EMMC      "eMMC onboard"			/* eMMC presents on board (module or SBC) */
#define SECO_DEV_LABEL_SD        "uSD onboard"			/* uSD slot presents on board (module or SBC) */
#define SECO_DEV_LABEL_SD_EXT    "uSD external"			/* uSD slot presents on carrier (only for modules) */
#define SECO_DEV_LABEL_FLASH     "SPI Flash onboard"	/* SPI Flash memory (implicity on board) */
#define SECO_DEV_LABEL_QFLASH    "QSPI Flash onboard"	/* QSPI Flash memory (implicity on board) */
#define SECO_DEV_LABEL_NAND      "NAND Flash onboard"	/* NAND Flash memory (implicity on board) */
#define SECO_DEV_LABEL_SATA      "SATA Storage"			/* SATA Storage Device */
#define SECO_DEV_LABEL_USB       "USB Storage"			/* USB Storage Device */
#define SECO_DEV_LABEL_TFTP      "TFTP"					/* Remote source */
#define SECO_DEV_LABEL_NFS       "NFS"					/* Remote mounting of a rootfs Filesystem */


typedef enum {
	SECO_DEV_TYPE_NONE,
	SECO_DEV_TYPE_EMMC,
	SECO_DEV_TYPE_SD,
	SECO_DEV_TYPE_SD_EXT,
	SECO_DEV_TYPE_NAND,
	SECO_DEV_TYPE_SPI,
	SECO_DEV_TYPE_QSPI,
	SECO_DEV_TYPE_SATA,
	SECO_DEV_TYPE_USB,
	SECO_DEV_TYPE_TFTP,
	SECO_DEV_TYPE_NFS,
} device_t;


typedef struct data_boot_dev {
	device_t dev_type;
	char     *label;
	char     *env_str;
	char     *device_id;
	char     *load_address;
	char     *def_path;
} data_boot_dev_t;


typedef struct firmware {
	char file_name[64];
	char address[64];
	char cmd_load_fw[64];
} fw_t;


#ifdef CONFIG_OF_LIBFDT_OVERLAY
typedef struct overlay_struct_mode {
	char *label;
	char *dtb_overlay;
	int  has_fw;
	fw_t fw[2];
} overlay_struct_mode_t;


typedef struct overlay_list {
	char *title;
	overlay_struct_mode_t options[20];
} overlay_list_t;
#endif  /* CONFIG_OF_LIBFDT_OVERLAY */


typedef struct source_bsp {
/* data sources */
    data_boot_dev_t *kern_dev_list;
    unsigned int    kern_dev_num;
	data_boot_dev_t *fw_dev_list;
	unsigned        fw_dev_num;
    data_boot_dev_t *fdt_dev_list;
    unsigned int    fdt_dev_num;
	data_boot_dev_t *fdt_overlay_dev_list;
    unsigned int    fdt_overlay_dev_num;
    data_boot_dev_t *ramfs_dev_list;
    unsigned int    ramfs_dev_num;
    data_boot_dev_t *filesystem_dev_list;
    unsigned int    filesystem_dev_num;
} source_bsp_t;



/*  __________________________________________________________________________
 * |                                                                          |
 * |                               VIDEO PARAMETERS                           |
 * |__________________________________________________________________________|
*/

#define SECO_VIDEO_LABEL_NONE                  "No video output"           /* None video I/F to use (file will be not loaded) */
#define SECO_VIDEO_LABEL_LVDSx1                "LVDS single channel"       /* LVDS video I/F with single channel */
#define SECO_VIDEO_LABEL_LVDSx2                "LVDS dual channel"         /* LVDS video I/F with dual channel */
#define SECO_VIDEO_LABEL_LVDS_WVGA             "LVDS 800x480"
#define SECO_VIDEO_LABEL_LVDS_1024x600         "LVDS 1024x600"             /* LVDS video I/F 1024x600 */
#define SECO_VIDEO_LABEL_LVDS_1024x768         "LVDS 1024x768"             /* LVDS video I/F 1024x768 */
#define SECO_VIDEO_LABEL_LVDS_1280x800         "LVDS 1280x800"             /* LVDS video I/F 1280x800 */
#define SECO_VIDEO_LABEL_LVDS_FHD              "LVDS 1920x1080"
#define SECO_VIDEO_LABEL_HDMI                  "HDMI"                      /* HDMI video I/F */
#define SECO_VIDEO_LABEL_HDMI_LVDS             "HDMI + LVDS"               /* HDMI + LVDS video I/F */
#define SECO_VIDEO_LABEL_EDP                   "eDP"                       /* eDP video I/F */
#define SECO_VIDEO_LABEL_HDMI_EDP              "HDMI + eDP"                /* HDMI eDP video I/F */
#define SECO_VIDEO_LABEL_DP                    "Display Port"              /* Display video I/F */
#define SECO_VIDEO_LABEL_RGB                   "RGB"                       /* RGB video I/F */
#define SECO_VIDEO_LABEL_HDMI_LVDS_EDP         "HDMI + LVDS + eDP"         /* HDMI + LVDS + eDP video I/F */
#define SECO_VIDEO_LABEL_LVDS_EDP              "LVDS + eDP"                /* LVDS + eDP video I/F */

#define SECO_VIDEO_LABEL_LVDS_DCSS             "LVDS-DCSS"                 /* LVDS video DCSS controller*/
#define SECO_VIDEO_LABEL_LVDS_LCDIF            "LVDS-LCDIF"                /* LVDS video LCDIF controller*/


/* Video I/F combinations */

typedef enum {
	NO_VIDEO,
	VIDEO_HDMI,
	VIDEO_LVDS,
	VIDEO_LVDSx2,
	VIDEO_DP,
	VIDEO_HDMI_LVDS,
	VIDEO_EDP,
	VIDEO_LVDS_EDP,
	VIDEO_HDMI_EDP,
	VIDEO_DSI,
	VIDEO_TYPEC,
	VIDEO_RGB,
	VIDEO_HDMI_LVDS_EDP,
	VIDEO_LVDS_DCSS,
	VIDEO_LVDS_LCDIF,
} video_type_t;


typedef struct panel_parameters {
	char     *label;
	char     *name;
	char     *if_map;
	char     *datamap;
	int      bpp;
	char     *opt; 
	int      num_channel;
} panel_parameters_t;


#define VIDEO_NOT_USED       -1
#define VIDEO_USED           1

typedef struct video_boot_args {
	char                *name;
	char                *buffer;
	char                *driver;
	panel_parameters_t  *panel_params;	
	int                 panel_list_size;
} video_boot_args_t;


#define NO_VIDEO_ARGS  { NULL, NULL, NULL, NULL, 0 }

#define IS_VIDEO_ARGS_NOTUSED(x) \
	( ( (x).buffer == NULL ) || ( (x).driver == NULL ) || ( (x).panel_params == NULL ) )


#define PANEL_LIST(x)  .panel_params = (x), \
						.panel_list_size = ARRAY_SIZE((x)),


typedef struct video_hw {
	int                used;
	video_type_t       type;
	video_boot_args_t  video_args;
} video_hw_t;


typedef struct video_mode {
	char        *label;
	video_hw_t  video[ENV_NUM_VIDEO_OUTPUT];
	char        *panel_name;
	int         use_bootargs;
	char        *dtbo_conf_file;
	int         has_fw;
	fw_t        fw[2];
} video_mode_t;

/*  __________________________________________________________________________
 * |                                                                          |
 * |                    TOUCH CONTROLLER PARAMETERS                           |
 * |__________________________________________________________________________|
 */

#define SECO_TOUCH_LABEL_NONE                  "No touch controller"
#define SECO_TOUCH_LABEL_I2C_EETI              "EETI I2C touch controller"
#define SECO_TOUCH_LABEL_I2C_ILITEK            "Ilitek I2C touch controller"
#define SECO_TOUCH_LABEL_I2C_MXT               "Atmel I2C maXTouch controller"

typedef enum {
	NO_TOUCH,
	TOUCH_I2C_EETI,
	TOUCH_I2C_ILITEK,
	TOUCH_I2C_MXT,
} touch_type_t;

typedef struct touch_hw {
	int               used;
	touch_type_t      type;
} touch_hw_t;

typedef struct touch_mode {
	char        *label;
	char        *touch_name;
	char        *dtbo_conf_file;
} touch_mode_t;


typedef struct boot_setup {
 	/* video output */
	video_mode_t    *video_mode_list;
	int             video_mode_num;
	/* touch controller */
	touch_mode_t    *touch_mode_list;
	int             touch_mode_num;
#ifdef CONFIG_OF_LIBFDT_OVERLAY
	data_boot_dev_t *fdt_overlay_dev_list;
	unsigned int    fdt_overlay_dev_num;
	overlay_list_t  *overlay_peripheral_list;
	unsigned int    overlay_peripheral_num;
#endif
} boot_setup_t;
#endif     /*  __SECO_ENV_GD__  */
