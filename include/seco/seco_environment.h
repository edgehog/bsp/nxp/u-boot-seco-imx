#ifndef __SECO_ENVIRONMENT_H
#define __SECO_ENVIRONMENT_H

#include <linux/stringify.h>

/*
 *
 * Variable              Scope                 Description
 * --------------------------------------------------------------------------------------------------------
 *
 * kernel_device_id      eMMC/SD/              numeric id of device
 * kernel_partition      eMMC/SD/USB/SATA      numeric id of device's partition
 * kernel_file           eMMC/SD/USB/SATA      absolute path (folder+name) of file to load
 * kernel_load2ram       all                   u-boot command to exec in order to load file to memory
 *
 * fdt_autodetect        all                   automatic detect the corret dtb file name (cpu type based)
 * fdt_device_id         eMMC/SD/              numeric id of device
 * fdt_partition         eMMC/SD/USB/SATA      numeric id of device's partition
 * fdt_file              eMMC/SD/USB/SATA      absolute path (folder+name) of file to load
 * fdt_load              all                   u-boot command to exec in order to load file to memory
 *
 * ramfs_use             all                   0 = do not use ramfs ; 1 = use ramfs
 * ramfs_device_id       eMMC/SD/              numeric id of device
 * ramfs_partition       eMMC/SD/USB/SATA      numeric id of device's partition
 * ramfs_file            eMMC/SD/USB/SATA      absolute path (folder+name) of file to load
 * ramfs_load2ram        all                   u-boot command to exec in order to load file to memory
 *
 * root_device_id        eMMC/SD/              numeric id of device
 * root_partition        eMMC/SD/USB/SATA      numeric id of device's partition
 * root_mount            all                   u-boot command to generate bootargs about FS mount point
 *
 * ipaddr                TFTP                  target ip
 * netmask               TFTP                  target netmask
 * nfs_ip_client         NFS                   static IP of client
 * nfs_netmask           NFS                   netmask for client
 * nfs_ip_server         NFS                   static IP of server
 * nfs_path              NFS                   remote path of FS folder
 * eth_if                NFS                   id of the eth inteface to use for network Filesystem
 *
 */



/* ____________________________________________________________________________
  |                                                                            |
  |                                 ENV MACROS                                 |
  |____________________________________________________________________________|
*/

/* -------------------- BOOTSCRIPT -------------------- */
#define MACRO_ENV_BOOTSCRIPT_SRC_USDHCI  \
	load mmc ${bootscript_device_id}:${bootscript_partition} ${bootscript_loadaddr} ${script}

#define MACRO_ENV_BOOTENV_SRC_USDHCI \
	load mmc ${bootscript_device_id}:${bootenv_partition} ${bootenv_loadaddr} ${envtxt}


/* -------------------- KERNEL -------------------- */

#define MACRO_ENV_KERNEL_SRC_USDHCI     \
	mmc dev ${kernel_device_id}; load mmc ${kernel_device_id}:${kernel_partition}  ${kernel_loadaddr} ${kernel_file}

#define MACRO_ENV_KERNEL_SRC_USB     \
	usb start; load usb ${kernel_device_id}:${kernel_partition} ${kernel_loadaddr} ${kernel_file}

#define MACRO_ENV_KERNEL_SRC_SATA    \
	sata init; load sata 0:${kernel_partition} ${kernel_loadaddr} ${kernel_file}

#define MACRO_ENV_KERNEL_SRC_SPI     \
	sf probe 0; sf ${kernel_loadaddr} ${kernel_spi_addr} ${kernel_spi_len}

#define MACRO_ENV_KERNEL_SRC_TFTP   \
	if test "${tftp_use_dhcp}" = "0"; then tftpboot ${kernel_loadaddr} ${kernel_file}; else dhcp ${kernel_loadaddr} ${kernel_file}; fi


/* -------------------- FDT -------------------- */
#define MACRO_ENV_FDT_SRC_USDHCI     \
	load mmc ${fdt_device_id}:${fdt_partition}  ${fdt_loadaddr} ${fdt_file}

#define MACRO_ENV_FDT_SRC_USB     \
	usb start; load usb ${fdt_device_id}:${fdt_partition} ${fdt_loadaddr} ${fdt_file}

#define MACRO_ENV_FDT_SRC_SATA    \
	sata init; load sata 0:${fdt_partition} ${fdt_loadaddr} ${fdt_file}

#define MACRO_ENV_FDT_SRC_SPI     \
	sf probe 0; sf ${fdt_loadaddr} ${fdt_spi_addr} ${fdt_spi_len}

#define MACRO_ENV_FDT_SRC_TFTP   \
	if test "${tftp_use_dhcp}" = "0"; then tftpboot ${fdt_loadaddr} ${fdt_file}; else dhcp ${fdt_loadaddr} ${fdt_file}; fi

#ifdef CONFIG_OF_LIBFDT_OVERLAY
#define MACRO_ENV_FDT_LOAD_OVERLAY_BASE \
	fdt addr ${fdt_loadaddr}; fdt resize ${fdt_resize}; \
	if test -n "${fdt_overlay_carrier_file}"; then run fdt_overlay_carrier_cmd; fi; \
	seco_config fdtoverlay ; \
	if test -n "${fdt_overlay_per_cmd}"; then run fdt_overlay_per_cmd; fi; \
	if test -n "${fdt_overlay_video_cmd}"; then run fdt_overlay_video_cmd; fi; \
	if test -n "${fdt_overlay_touch_cmd}"; then run fdt_overlay_touch_cmd; fi;


#define MACRO_ENV_FDT_OVERLAY_SRC_USDHCI     \
	load mmc ${fdt_device_id}:${fdt_partition}

#define MACRO_ENV_FDT_OVERLAY_SRC_USB     \
	usb start; load usb ${fdt_device_id}:${fdt_partition}

#define MACRO_ENV_FDT_OVERLAY_SRC_SATA    \
	sata init; load sata 0:${fdt_partition}

#define MACRO_ENV_FDT_OVERLAY_SRC_SPI     \
	sf probe 0; sf ${fdt_loadaddr} ${fdt_spi_addr} ${fdt_spi_len}

#define MACRO_ENV_FDT_OVERLAY_SRC_TFTP   \
	tftpboot

#define MACRO_ENV_FDT_OVERLAY_APPLY   \
	fdt apply
#else
#define MACRO_ENV_FDT_LOAD_OVERLAY_BASE "\0"
#endif    /* CONFIG_OF_LIBFDT_OVERLAY */


/* -------------------- FIRMWARE -------------------- */
#define MACRO_ENV_FIRMWARE_SRC_USDHCI     \
	if load mmc ${fdt_device_id}:${fdt_partition} ${fw_loadaddr} ${fw_file}; then echo load fw ${fw_file}; ${fw_cmd} ${fw_loadaddr}; fi

#define MACRO_ENV_FIRMWARE_SRC_USB     \
	usb start; if load usb ${fdt_device_id}:${fdt_partition} ${fw_loadaddr} ${fw_file}; then echo load fw ${fw_file}; ${fw_cmd} ${fw_loadaddr}; fi

#define MACRO_ENV_FIRMWARE_SRC_SATA    \
	sata init; if load sata 0:${fdt_partition} ${fdt_loadaddr} ${fdt_file}; then echo load fw ${fw_file}; ${fw_cmd} ${fw_loadaddr}; fi

#define MACRO_ENV_FIRMWARE_SRC_SPI     \
	sf probe 0; if sf ${fw_loadaddr} ${fw_spi_addr} ${fw_spi_len}; then echo load fw ${fw_file}; ${fw_cmd} ${fw_loadaddr}; fi

#define MACRO_ENV_FIRMWARE_SRC_TFTP   \
	if test "${tftp_use_dhcp}" = "0"; then tftpboot ${fw_loadaddr} ${fw_file}; else dhcp ${fw_loadaddr} ${fw_file}; fi; echo load fw ${fw_file}; ${fw_cmd} ${fw_loadaddr}
	
#define MACRO_SET_VIDEO_CMD \
	if test -n "${bootcmd}"; then run fw_video_load_cmd; fi;



/* -------------------- RAMFS -------------------- */

#define MACRO_ENV_RAMFS_SRC_USDHCI     \
	load mmc ${ramfs_device_id}:${ramfs_partition}  ${ramfs_loadaddr} ${ramfs_file}

#define MACRO_ENV_RAMFS_SRC_USB     \
	usb start; load usb ${ramfs_device_id}:${ramfs_partition} ${ramfs_loadaddr} ${ramfs_file}

#define MACRO_ENV_RAMFS_SRC_SATA    \
	sata init; load sata 0:${ramfs_partition} ${ramfs_loadaddr} ${ramfs_file}

#define MACRO_ENV_RAMFS_SRC_SPI     \
	sf probe 0; sf ${ramfs_loadaddr} ${ramfs_spi_addr} ${ramfs_spi_len}

#define MACRO_ENV_RAMFS_SRC_TFTP   \
	tftpboot ${ramfs_loadaddr} ${ramfs_file}



/* -------------------- FILESYSTEM -------------------- */

#define MACRO_ENV_FS_COMMON  rootwait rw

#define MACRO_ENV_FS_SRC_USDHCI   \
	setenv root_dev root=/dev/mmcblk${root_device_id}p${root_partition} rootwait rw

#define MACRO_ENV_FS_SRC_USB      \
	setenv root_dev root=/dev/sda${root_partition} rootwait rw

#define MACRO_ENV_FS_SRC_SATA     \
	setenv root_dev root=/dev/sda${root_partition} rootwait rw

#define MACRO_ENV_FS_SRC_NFS      \
	run set_ip; run set_remote_rootfs; setenv root_dev ${remote_fs} ip=${ip} 

#define MACRO_SET_IP \
	if test "${nfs_use_dhcp}" = "0"; then \
		run ipsetup; \
		setenv ip "${nfs_ip_client}:::${nfs_netmask}::eth0:off"; \
	else \
		setenv ip ":::::eth0:dhcp"; \
	fi; \

#define MACRO_SET_REMOTE_ROOTFS \
	if test "${nfs_use_dhcp}" = "0"; then \
		setenv remote_fs "root=/dev/nfs rootfstype=nfs nfsroot=${nfs_ip_server}:/${nfs_path},wsize=1024,rsize=1024,nfsvers=3 rootwait rw"; \
	else \
		if test "${nfs_use_dhcp_autopath}" = "0"; then \
			setenv remote_fs "root=/dev/nfs rootfstype=nfs nfsroot=${nfs_ip_server}:/${nfs_path},wsize=1024,rsize=1024,nfsvers=3 rootwait rw"; \
		else \
			setenv remote_fs "root=/dev/nfs rootfstype=nfs rootwait rw"; \
		fi; \
	fi




/* ____________________________________________________________________________
  |                                                                            |
  |                                 ENVIRONMENT                                |
  |____________________________________________________________________________|
*/

#define CONFIG_SERVERIP                          13.0.0.1
#define CONFIG_IPADDR                            13.0.0.10
#define CONFIG_NETMASK                           255.255.255.0
#define DEF_NFS_PATH                             "targetfs/"


#define ENV_BOOTARGS_BASE  setenv bootargs ${console_interface} ${memory} ${cpu_freq} ${videomode} vt.global_cursor_default=0 ${root_dev} boardname=${board_name} ${logo_args} ${custom_args}



#define ENV_COMMON                                                    \
	"bootdelay="__stringify(CONFIG_BOOTDELAY)"\0"                     \
	"stdin=serial\0"                                                  \
	"stdout=serial\0"                                                 \
	"stderr=serial\0"                                                 \
	"mmcautodetect="ENV_MMCAUTODETECT"\0"                             \
	"fdt_autodetect="ENV_FDTAUTODETECT"\0"                            \
	"mem_autodetect="ENV_MEMAUTODETECT"\0"                            \
	"run_from_tftp=0\0"                                               \
	"tftp_use_dhcp=0\0"                                               \
	"logo_args=fbcon=logo-pos:center,logo-count:1\0"


#define ENV_ARGS_BASE                                                                          \
	"console_interface='console=" __stringify(ENV_CONSOLE_DEV) "," __stringify(CONFIG_BAUDRATE)"'\0"    \
	"bootargs_base="__stringify(ENV_BOOTARGS_BASE)"\0" \
	"slot_partition=A\0"


#define ENV_BOOTSCRIPT                                                        \
	"script=seco_boot.scr\0"                                                  \
	"envtxt=/loader/uEnv.txt\0"                                                       \
	"bootscript_device_id="__stringify(ENV_SYS_MMC_ENV_DEV)"\0"               \
	"bootscript_partition="__stringify(ENV_SYS_MMC_BOOSCRIPT_PART)"\0"         \
	"bootenv_partition="__stringify(ENV_SYS_MMC_BOOATENV_PART)"\0"            \
	"bootscript_loadaddr="__stringify(ENV_BOOTSCRIPT_LOADADDR)"\0"                       \
	"bootenv_loadaddr="__stringify(ENV_RAMFS_LOADADDR)"\0"                    \
	"bootscript_load_usdhci2ram="__stringify(MACRO_ENV_BOOTSCRIPT_SRC_USDHCI)"\0"     \
	"bootenv_load_usdhci2ram="__stringify(MACRO_ENV_BOOTENV_SRC_USDHCI)"\0"


#define ENV_KERNEL                                                    \
	"kernel_device_id="__stringify(ENV_SYS_MMC_ENV_DEV)"\0"           \
	"kernel_partition="__stringify(ENV_SYS_MMC_KERNEL_PART)"\0"     \
	"kernel_loadaddr="__stringify(ENV_KERNEL_LOADADDR)"\0"                   \
	"kernel_file="__stringify(SCFG_KERNEL_FILENAME)"\0"                  \
	"kernel_load_usdhci2ram="__stringify(MACRO_ENV_KERNEL_SRC_USDHCI)"\0"     \
	"kernel_load_usb2ram="__stringify(MACRO_ENV_KERNEL_SRC_USB)"\0"           \
	"kernel_load_eth2ram="__stringify(MACRO_ENV_KERNEL_SRC_TFTP)"\0"          \
	"kernel_load2ram="__stringify(MACRO_ENV_KERNEL_SRC_USDHCI)"\0"


#ifndef CONFIG_OF_LIBFDT_OVERLAY

#define ENV_FDT                                                       \
	"fdt_device_id="__stringify(ENV_SYS_MMC_ENV_DEV)"\0"              \
	"fdt_partition="__stringify(ENV_SYS_MMC_FDT_PART)"\0"        \
	"fdt_loadaddr="__stringify(ENV_FDT_LOADADDR)"\0"                  \
	"fdt_file="__stringify(ENV_DEFAULT_FDT_FILE)"\0"                  \
	"fdt_high=0xffffffff\0"	                                          \
	"fdt_load_usdhci2ram="__stringify(MACRO_ENV_FDT_SRC_USDHCI)"\0"           \
	"fdt_load_usb2ram="__stringify(MACRO_ENV_FDT_SRC_USB)"\0"                 \
	"fdt_load_eth2ram="__stringify(MACRO_ENV_FDT_SRC_TFTP)"\0"                \
	"fdt_load2ram="__stringify(MACRO_ENV_FDT_SRC_USDHCI)"\0"

#else

#define ENV_FDT                                                       \
	"fdt_device_id="__stringify(ENV_SYS_MMC_ENV_DEV)"\0"              \
	"fdt_partition="__stringify(ENV_SYS_MMC_FDT_PART)"\0"        \
	"fdt_loadaddr="__stringify(ENV_FDT_LOADADDR)"\0"                  \
	"fdt_file="__stringify(ENV_DEFAULT_FDT_FILE)"\0"                  \
	"fdt_high=0xffffffff\0"	                                          \
	"fdt_resize="__stringify(ENV_FDT_RESIZE)"\0"                      \
	"fdt_load_usdhci2ram="__stringify(MACRO_ENV_FDT_SRC_USDHCI)"\0"           \
	"fdt_load_usb2ram="__stringify(MACRO_ENV_FDT_SRC_USB)"\0"                 \
	"fdt_load_eth2ram="__stringify(MACRO_ENV_FDT_SRC_TFTP)"\0"                \
	"fdt_load2ram="__stringify(MACRO_ENV_FDT_SRC_USDHCI)"\0"                  \
	"fdt_overlay_carrier_file=" CONFIG_DEFAULT_FDT_OVERLAY_CARRIER_FILE "\0" \
	"fdt_overlay_carrier_cmd= load mmc ${fdt_device_id}:${fdt_partition} ${fdt_overlay_base_addr} ${fdt_overlay_carrier_file}; fdt apply ${fdt_overlay_base_addr};\0" \
	"fdt_overlay_base_addr="__stringify(ENV_FDT_OVERLAY_BASEADDR)"\0"       \
	"fdt_overlay_load2ram="__stringify(MACRO_ENV_FDT_LOAD_OVERLAY_BASE)"\0"

#endif  /* CONFIG_OF_LIBFDT_OVERLAY */

/* This is just a placeholder to insert the related environment variables
   individually for each board */
#ifndef ENV_DEVICE_CONFIG
#define ENV_DEVICE_CONFIG
#endif

#define ENV_RAMFS                                                     \
	"ramfs_use=0\0"                                                   \
	"ramfs_device_id="__stringify(ENV_SYS_MMC_ENV_DEV)"\0"            \
	"ramfs_partition="__stringify(ENV_SYS_MMC_RAMFS_PART)"\0"      \
	"ramfs_loadaddr="__stringify(ENV_RAMFS_LOADADDR)"\0"              \
	"ramfs_file="__stringify(SCFG_RAMFS_FILENAME)"\0"                                          \
	"ramfs_load_usdhci2ram="__stringify(MACRO_ENV_RAMFS_SRC_USDHCI)"\0"       \
	"ramfs_load_usb2ram="__stringify(MACRO_ENV_RAMFS_SRC_USB)"\0"             \
	"ramfs_load_eth2ram="__stringify(MACRO_ENV_RAMFS_SRC_TFTP)"\0"            \
	"ramfs_load2ram="__stringify(MACRO_ENV_RAMFS_SRC_USDHCI)"\0"


#define ENV_ROOT                                                      \
	"root_device_id="__stringify(ENV_ROOT_DEV_ID)"\0"                 \
	"root_partition="__stringify(ENV_SYS_MMC_ROOTFS_PART)"\0"              \
	"root_load_usdhci="__stringify(MACRO_ENV_FS_SRC_USDHCI)"\0"               \
	"root_load_usb="__stringify(MACRO_ENV_FS_SRC_USB)"\0"                     \
	"root_load_eth="__stringify(MACRO_ENV_FS_SRC_NFS)"\0"                     \
	"run_from_nfs=0\0"  \
	"root_mount="__stringify(MACRO_ENV_FS_SRC_USDHCI)"\0"


#define ENV_NETWORK                                                   \
	"ipaddr="__stringify(CONFIG_IPADDR)"\0"                           \
	"netmask=255.255.255.0\0"                                         \
	"eth_if=0\0"                                                      \
	"nfs_use_dhcp=0\0"                                                \
	"nfs_use_dhcp_autopath=0\0"                                       \
	"ipsetup=setenv ipaddr ${ipaddr}; setenv serverip ${serverip};\0" \
	"set_remote_rootfs="__stringify(MACRO_SET_REMOTE_ROOTFS)"\0"      \
	"set_ip="__stringify(MACRO_SET_IP)"\0"                            \
	"nfs_ip_client="__stringify(CONFIG_IPADDR)"\0"                    \
	"nfs_ip_server="__stringify(CONFIG_SERVERIP)"\0"                  \
	"nfs_netmask="__stringify(CONFIG_NETMASK)"\0"                     \
	"nfs_path="__stringify(DEF_NFS_PATH)"\0"


#ifdef CONFIG_OF_LIBFDT_OVERLAY
#define	FDT_OVERLAY_LOAD2RAM "run fdt_overlay_load2ram; "
#else
#define	FDT_OVERLAY_LOAD2RAM "echo No overlay will be used; "
#endif 

#define ENV_CMD_SET_BOOT                                              \
	"if test -n \"${fw_video_load_cmd}\"; then " \
	"run fw_video_load_cmd; fi;" \
  	"if test \"${bootusb}\" = \"true\"; then " \
  	"  for fstype in fat ext2; do " \
  	"   if ${fstype}load usb 0:1 0x00500000 " __stringify(ENV_UENV_FILE_NAME)"; then " \
  	"     env import -t 0x00500000 600; run usb_boot; fi; " \
  	"  done; " \
  	"fi; " \
	"if mmc rescan; then "                                           	\
		"mmc dev ${bootscript_device_id}; "                             \
		"if run bootscript_load_usdhci2ram; then "                    	\
			"echo ==> Running bootscript mode... ; "		  	      	\
			" source ${bootscript_loadaddr}; "                          \
		"fi; "                                                        	\
	"fi; "                                                            	\
	"echo ==> Running in Normal Mode... ; "							  	\
	"run bootargs_base; "                                          	  	\
	"run kernel_load2ram; "                                           	\
	"run fdt_load2ram; "                                              	\
	FDT_OVERLAY_LOAD2RAM                                              	\
	"run root_mount; "                                                	\
	"run bootargs_base; "                                             	\
	"if test ${ramfs_use} = 1; then "                                 	\
		" run ramfs_load2ram; "                                       	\
		__stringify(ENV_BOOT_TYPE)" ${kernel_loadaddr} ${ramfs_loadaddr} ${fdt_loadaddr}; "\
	"else "                                                           \
		__stringify(ENV_BOOT_TYPE)" ${kernel_loadaddr} - ${fdt_loadaddr}; "                  \
	"fi\0"

#ifndef CONFIG_PREBOOT
#define CONFIG_PREBOOT
#endif


#define ENV_PREBOOT "preboot=echo Start boot...\0"


#ifdef CONFIG_SPLASH_SCREEN
#ifdef SPLASHIMAGE_ADDR
#define ENV_SPLASHADDR  												\
	"splashimage="__stringify(SPLASHIMAGE_ADDR)"\0"						\
	"splashpos=m,m\0"								\
	"splashsource=mmc_fs\0"
#else   /* SPLASHIMAGE_ADDR */
#define ENV_SPLASHADDR  ""
#endif  /* SPLASHIMAGE_ADDR */
#else  /* CONFIG_SPLASH_SCREEN */
#define ENV_SPLASHADDR  ""
#endif  /* CONFIG_SPLASH_SCREEN */


#ifndef ENV_CUSTOM
#define ENV_CUSTOM
#endif


#undef CONFIG_EXTRA_ENV_SETTINGS
#define CONFIG_EXTRA_ENV_SETTINGS                                     \
	ENV_COMMON                                                        \
	ENV_ARGS_BASE                                                     \
	ENV_NETWORK                                                       \
	ENV_BOOTSCRIPT                                                    \
	ENV_KERNEL                                                        \
	ENV_FDT                                                           \
	ENV_DEVICE_CONFIG                                                 \
	ENV_RAMFS                                                         \
	ENV_ROOT                                                          \
	ENV_PREBOOT                                                       \
	ENV_FW_SECO                                                       \
	ENV_SPLASHADDR                                                    \
	ENV_CUSTOM                                                        \
	"bootcmd=" ENV_CMD_SET_BOOT "\0"


#endif     /*  __SECO_ENVIRONMENT_H  */
