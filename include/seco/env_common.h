#ifndef __SECO_ENV_COMMON_H
#define __SECO_ENV_COMMON_H

#include <seco/seco_env_gd.h>


#define MIN_PARTITION_ID 1
#define MAX_PARTITION_ID 9

/*  __________________________________________________________________________
 * |                                                                          |
 * |                                   KERNEL                                 |
 * |__________________________________________________________________________|
*/
#define GET_KERNEL_PATH                   env_get ("kernel_file")

#define SAVE_KERNEL_DEVICE_ID(x)          env_set ("kernel_device_id", (x))
#define GET_KERNEL_DEVICE_ID              env_get ("kernel_device_id")
#define SAVE_KERNEL_PARTITION(x)          env_set ("kernel_partition", (x))
#define SAVE_KERNEL_PATH(x)               env_set ("kernel_file", (x))
#define SAVE_KERNEL_SPI_ADDR(x)           env_set ("kernel_spi_addr", (x))
#define SAVE_KERNEL_LOADADDR(x)           env_set ("kernel_loadaddr", (x))
#define SAVE_KERNEL_SPI_LEN(x)            env_set ("kernel_spi_len", (x))
#define GET_ENV_KERNEL_SRC(srv)           ( ENV_KERNEL_SRC_ ## (src) )
#define SAVE_KERNEL_BOOT_LOAD(x)          env_set ("kernel_load2ram", (x))


/*  __________________________________________________________________________
 * |                                                                          |
 * |                                    FDT                                   |
 * |__________________________________________________________________________|
*/
#define GET_FDT_PATH                   env_get ("fdt_file")

#define SAVE_FDT_DEVICE_ID(x)          env_set ("fdt_device_id", (x))
#define SAVE_FDT_PARTITION(x)          env_set ("fdt_partition", (x))
#define SAVE_FDT_PATH(x)               env_set ("fdt_file", (x))
#define SAVE_FDT_LOADADDR(x)           env_set ("fdt_loadaddr", (x))
#define SAVE_FDT_SPI_ADDR(x)           env_set ("fdt_spi_addr", (x))
#define SAVE_FDT_SPI_LEN(x)            env_set ("fdt_spi_len", (x))
#define GET_ENV_FDT_SRC(srv)           ( ENV_FDT_SRC_ ## (src) )
#define SAVE_FDT_BOOT_LOAD(x)          env_set ("fdt_load2ram", (x))

#define GET_FDT_BOOT_LOAD              env_get ("fdt_load2ram")

#ifdef CONFIG_OF_LIBFDT_OVERLAY
#define SAVE_FDT_OVERLAY_VIDEO_LIST(x)  env_set ("fdt_overlay_video_list", (x))
#define GET_FDT_OVERLAY_VIDEO_LIST      env_get ("fdt_overlay_video_list")

#define SAVE_FDT_OVERLAY_VIDEO_CMD(x)   env_set ("fdt_overlay_video_cmd", (x))
#define GET_FDT_OVERLAY_VIDEO_CMD       env_get ("fdt_overlay_video_cmd")

#define SAVE_FDT_OVERLAY_TOUCH_LIST(x)  env_set ("fdt_overlay_touch_list", (x))
#define GET_FDT_OVERLAY_TOUCH_LIST      env_get ("fdt_overlay_touch_list")

#define SAVE_FDT_OVERLAY_TOUCH_CMD(x)   env_set ("fdt_overlay_touch_cmd", (x))
#define GET_FDT_OVERLAY_TOUCH_CMD       env_get ("fdt_overlay_touch_cmd")

#define SAVE_FDT_OVERLAY_PER_LIST(x)    env_set ("fdt_overlay_per_list", (x))
#define GET_FDT_OVERLAY_PER_LIST        env_get ("fdt_overlay_per_list")

#define SAVE_FDT_OVERLAY_PER_CMD(x)     env_set ("fdt_overlay_per_cmd", (x))
#define GET_FDT_OVERLAY_PER_CMD         env_get ("fdt_overlay_per_cmd")

#define SAVE_FDT_OVERLAY_LOAD(x)        env_set ("fdt_overlay_per_cmd", (x))
#endif  /* CONFIG_OF_LIBFDT_OVERLAY */


/*  __________________________________________________________________________
 * |                                                                          |
 * |                                 FIRMWARE                                 |
 * |__________________________________________________________________________|
*/
#define SAVE_FW_VIDEO_LIST(x)             env_set ("fw_video_list", (x))
#define GET_FW_VIDEO_LIST                 env_get ("fw_video_list")
#define SAVE_FW_VIDEO_CMD(x)              env_set ("fw_video_load_cmd", (x))
#define GET_FW_VIDEO_CMD                  env_get ("fw_video_load_cmd")


/*  __________________________________________________________________________
 * |                                                                          |
 * |                                   RAMFS                                  |
 * |__________________________________________________________________________|
*/
#define GET_RAMFS_PATH                    env_get ("ramfs_file")

#define SAVE_RAMFS_DEVICE_ID(x)           env_set ("ramfs_device_id", (x))
#define SAVE_RAMFS_PARTITION(x)           env_set ("ramfs_partition", (x))
#define SAVE_RAMFS_PATH(x)                env_set ("ramfs_file", (x))
#define SAVE_RAMFS_LOADADDR(x)            env_set ("ramfs_loadaddr", (x))
#define SAVE_RAMFS_SPI_ADDR(x)            env_set ("ramfs_spi_addr", (x))
#define SAVE_RAMFS_SPI_LEN(x)             env_set ("ramfs_spi_len", (x))
#define GET_ENV_RAMFS_SRC(srv)            ( ENV_RAMFS_SRC_ ## (src) )
#define SAVE_RAMFS_BOOT_LOAD(x)           env_set ("ramfs_load2ram", (x))


/*  __________________________________________________________________________
 * |                                                                          |
 * |                                BOOTSCRIPT                                |
 * |__________________________________________________________________________|
*/
#define GET_BOOTSCRIPT_PATH              env_get ("ramfs_file")

#define SAVE_BOOTSCRIPT_DEVICE_ID(x)     env_set ("bootscript_device_id", (x))
#define SAVE_BOOTSCRIPT_PARTITION(x)     env_set ("bootscript_partition", (x))
#define SAVE_BOOTSCRIPT_PATH(x)          env_set ("script", (x))
#define SAVE_BOOTSCRIPT_LOADADDR(x)      env_set ("bootscript_loadaddr", (x))
#define SAVE_BOOTSCRIPT_SPI_ADDR(x)      env_set ("bootscript_spi_addr", (x))
#define SAVE_BOOTSCRIPT_SPI_LEN(x)       env_set ("bootscript_spi_len", (x))
#define GET_ENV_BOOTSCRIPT_SRC(srv)      ( ENV_BOOTSCRIPT_SRC_ ## (src) )
#define SAVE_BOOTSCRIPT_BOOT_LOAD(x)     env_set ("bootscript_load2ram", (x))


/*  __________________________________________________________________________
 * |                                                                          |
 * |                                 FILE SYSTEM                              |
 * |__________________________________________________________________________|
*/
#define SAVE_FILESYSTEM_DEVICE_ID(x)       env_set ("root_device_id", (x))
#define SAVE_FILESYSTEM_PARTITION(x)       env_set ("root_partition", (x))
#define SAVE_FILESYSTEM_ROOT(x)            env_set ("root_mount", (x))
#define GET_ENV_FS_SRC(srv)                ( ENV_FS_SRC_ ## src )



/*  __________________________________________________________________________
 * |                                                                          |
 * |                               NFS PARAMETERS                             |
 * |__________________________________________________________________________|
*/

#define GET_NFS_IP_CLIENT         env_get ("nfs_ip_client")
#define GET_NFS_IP_SERVER         env_get ("nfs_ip_server")
#define GET_NFS_NETMASK           env_get ("nfs_netmask")
#define GET_NFS_PATH              env_get ("nfs_path")

#define SAVE_NFS_USE(x)           env_set ("run_from_nfs", (x))
#define SAVE_NFS_IP_CLIENT(x)     env_set ("nfs_ip_client", (x))
#define SAVE_NFS_IP_SERVER(x)     env_set ("nfs_ip_server", (x))
#define SAVE_NFS_NETMASK(x)       env_set ("nfs_netmask", (x))
#define SAVE_NFS_PATH(x)          env_set ("nfs_path", (x))
#define SAVE_NFS_USE_DHCP(x)      env_set ("nfs_use_dhcp", (x))
#define SAVE_NFS_USE_DHCP_AUTO(x) env_set( "nfs_use_dhcp_autopath", (x))



/*  __________________________________________________________________________
 * |                                                                          |
 * |                               VIDEO PARAMETERS                           |
 * |__________________________________________________________________________|
*/
#define GET_VIDEO_PANEL          env_get("panel")
#define SET_VIDEO_PANEL(x)       env_set("panel", (x))
#define GET_VIDEOMODE            env_get("videomode")
#define SET_VIDEOMODE(x)         env_set("videomode", (x))

#endif    /*  __SECO_ENV_COMMON_H  */
