#ifndef __SECO_MX8_DTBO_H
#define __SECO_MX8_DTBO_H

#define ENV_DTBO_C26_HDMI               seco-imx8qm-c26-hdmi.dtbo
#define ENV_DTBO_C26_DP                 seco-imx8qm-c26-dp.dtbo
#define ENV_DTBO_C26_LVDS_SINGLE        seco-imx8qm-c26-lvds-single.dtbo
#define ENV_DTBO_C26_LVDS_DUAL          seco-imx8qm-c26-lvds-dual.dtbo
#define ENV_DTBO_C26_OV5640_CSI0        seco-imx8qm-c26-ov5640-csi0.dtbo
#define ENV_DTBO_C26_OV5640_CSI1        seco-imx8qm-c26-ov5640-csi1.dtbo
#define ENV_DTBO_C26_HDMIIN             seco-imx8qm-c26-hdmiin.dtbo
#define ENV_DTBO_C26_LVDS_1024x768      seco-imx8qm-c26-lvds-1024x768.dtbo

#define ENV_DTBO_C20_HDMI               seco-imx8mq-c20-hdmi.dtbo
#define ENV_DTBO_C20_EDP                seco-imx8mq-c20-edp.dtbo
#define ENV_DTBO_C20_LVDS_DUAL          seco-imx8mq-c20-lvds-dual.dtbo
#define ENV_DTBO_C20_PCIE               seco-imx8mq-c20-pcie.dtbo

#define ENV_DTBO_C43_HDMI               seco-imx8qm-c43-hdmi.dtbo
#define ENV_DTBO_C43_DP                 seco-imx8qm-c43-sn65dsi86.dtbo
#define ENV_DTBO_C43_LVDS_SINGLE        seco-imx8qm-c43-lvds-single.dtbo
#define ENV_DTBO_C43_LVDS_DUAL          seco-imx8qm-c43-lvds-dual.dtbo
#define ENV_DTBO_C43_LVDS_EDP           seco-imx8qm-c43-lvds-sn65dsi86.dtbo
#define ENV_DTBO_C43_HDMI_LVDS_DUAL     seco-imx8qm-c43-hdmi-lvds.dtbo
#define ENV_DTBO_C43_HDMI_EDP           seco-imx8qm-c43-hdmi-sn65dsi86.dtbo
#define ENV_DTBO_C43_HDMI_LVDS_EDP      seco-imx8qm-c43-hdmi-sn65dsi86-lvds.dtbo
#define ENV_DTBO_C43_HDMIIN             seco-imx8qm-c43-hdmiin.dtbo

#define ENV_DTBO_C12_HDMI               seco-imx8mq-c12-hdmi.dtbo
#define ENV_DTBO_C12_WILINK             seco-imx8mq-c12-wilink.dtbo
#define ENV_DTBO_C12_CAN_RTC            seco-imx8mq-c12-can-rtc.dtbo
#define ENV_DTBO_C12_LVDS_DUAL          seco-imx8mq-c12-lvds-dual.dtbo
#define ENV_DTBO_C12_LVDS_SINGLE_DCSS   seco-imx8mq-c12-dcss-sn65dsi84.dtbo
#define ENV_DTBO_C12_LVDS_SINGLE_LCDIF  seco-imx8mq-c12-lcdif-sn65dsi84.dtbo

#define ENV_DTBO_D18_HDMI               seco-imx8mp-d18-hdmi.dtbo
#define ENV_DTBO_D18_EDP                seco-imx8mp-d18-edp.dtbo
#define ENV_DTBO_D18_LVDS_FHD           seco-imx8mp-d18-lvds-fhd.dtbo
#define ENV_DTBO_D18_OV5640_CSI0_REVA   seco-imx8mp-d18-ov5640-csi0-revA.dtbo
#define ENV_DTBO_D18_OV5640_CSI1_REVA   seco-imx8mp-d18-ov5640-csi1-revA.dtbo
#define ENV_DTBO_D18_OV5640_CSI0        seco-imx8mp-d18-ov5640-csi0.dtbo
#define ENV_DTBO_D18_OV5640_CSI1        seco-imx8mp-d18-ov5640-csi1.dtbo
#define ENV_DTBO_D18_BTSCO              seco-imx8mp-d18-audio-btsco.dtbo
#define ENV_DTBO_D18_TOUCH_EETI         seco-imx8mp-d18-ts-eeti.dtbo
#define ENV_DTBO_D18_TOUCH_ILITEK       seco-imx8mp-d18-ts-ilitek.dtbo
#define ENV_DTBO_D18_TOUCH_MXT          seco-imx8mp-d18-ts-mxt.dtbo

#define ENV_DTBO_C72_EDP                seco-imx8m-c72-edp.dtbo
#define ENV_DTBO_C72_LVDS_215           seco-imx8m-c72-lvds-dual-215.dtbo
#define ENV_DTBO_C72_LVDS_156           seco-imx8m-c72-lvds-dual-156.dtbo
#define ENV_DTBO_C72_LVDS_7             seco-imx8m-c72-lvds-wvga.dtbo

#define ENV_DTBO_C61_EDP                seco-imx8mm-c61-video-sn65dsi86.dtbo
#define ENV_DTBO_C61_LVDS               seco-imx8mm-c61-video-sn65dsi84.dtbo
#define ENV_DTBO_C61_MODEM              seco-imx8mm-c61-modem.dtbo
#define ENV_DTBO_C61_CAMERA             seco-imx8mm-c61-ov5640.dtbo
#define ENV_DTBO_C61_P1_CAN             seco-imx8mm-c61-port1-can.dtbo
#define ENV_DTBO_C61_P1_GPIOS           seco-imx8mm-c61-port1-gpios.dtbo
#define ENV_DTBO_C61_P1_RS232           seco-imx8mm-c61-port1-rs232.dtbo
#define ENV_DTBO_C61_P1_RS485           seco-imx8mm-c61-port1-rs485.dtbo
#define ENV_DTBO_C61_P2_CAN             seco-imx8mm-c61-port2-can.dtbo
#define ENV_DTBO_C61_P2_GPIOS           seco-imx8mm-c61-port2-gpios.dtbo
#define ENV_DTBO_C61_P2_RS232           seco-imx8mm-c61-port2-rs232.dtbo
#define ENV_DTBO_C61_P2_RS485           seco-imx8mm-c61-port2-rs485.dtbo

#define ENV_DTBO_C57_LVDS_800x480       seco-imx8qxp-c57-lvds-800x480.dtbo
#define ENV_DTBO_C57_LVDS_1024x600      seco-imx8qxp-c57-lvds-1024x600.dtbo
#define ENV_DTBO_C57_LVDS_1280x800      seco-imx8qxp-c57-lvds-1280x800.dtbo
#define ENV_DTBO_C57_LVDS_DUAL_1920x1080 seco-imx8qxp-c57-lvds-dual-1920x1080.dtbo
#define ENV_DTBO_C57_EDP                seco-imx8qxp-c57-sn65dsi86-edp.dtbo

#define ENV_DTBO_ATM0700L61             seco-imx8mm-myon2-atm0700l61ct.dtbo
#define ENV_DTBO_CONXM_HDMI             seco-imx8mm-myon2-conxm-hdmi.dtbo
#define ENV_DTBO_MYON2_IMX8MM_V1R4      seco-imx8mm-myon2-v1r4.dtbo
#define ENV_DTBO_MYON2_IMX8MM_V2R1      seco-imx8mm-myon2-v2r1.dtbo
#endif   /* __SECO_MX8_DTBO_H  */
