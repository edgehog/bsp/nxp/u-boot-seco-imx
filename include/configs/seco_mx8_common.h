#ifndef __SECO_MX8M_COMMON_H
#define __SECO_MX8M_COMMON_H

#include <linux/sizes.h>
#include <asm/arch/imx-regs.h>
#include "imx_env.h"
#include "seco_common.h"


#ifdef CONFIG_SPL_BUILD
#define CONFIG_SPL_MAX_SIZE                                (192 * 1024)
#define CONFIG_SYS_MONITOR_LEN                             (1024 * 1024)
#define CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_USE_SECTOR
#define CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR            0x1040 /* (flash.bin_offset + 2Mb)/sector_size */

/*
 * 0x08081000 - 0x08180FFF is for m4_0 xip image,
 * 0x08181000 - 0x008280FFF is for m4_1 xip image
  * So 3rd container image may start from 0x8281000
 */
#define CONFIG_SYS_UBOOT_BASE                              0x08281000
         
#define CONFIG_SPL_LDSCRIPT                                "arch/arm/cpu/armv8/u-boot-spl.lds"
#define CONFIG_SPL_STACK                                   0x013fff0
#define CONFIG_SPL_BSS_START_ADDR                          0x00130000
#define CONFIG_SPL_BSS_MAX_SIZE                            0x1000	/* 4 KB */
#ifdef CONFIG_TARGET_IMX8QM_MEK_A72_ONLY          
#define CONFIG_SERIAL_LPUART_BASE                          0x5a080000	/* use UART2 */
#define CONFIG_SYS_SPL_MALLOC_START                        0xC2200000
#else
#define CONFIG_SERIAL_LPUART_BASE                          0x5a060000
#define CONFIG_SYS_SPL_MALLOC_START                        0x82200000
#endif
#define CONFIG_SYS_SPL_MALLOC_SIZE                         0x80000	/* 512 KB */
#define CONFIG_MALLOC_F_ADDR                               0x00138000

#define CONFIG_SPL_RAW_IMAGE_ARM_TRUSTED_FIRMWARE

#define CONFIG_SPL_ABORT_ON_RAW_IMAGE

#endif

#define CONFIG_REMAKE_ELF

#define CONFIG_CMD_READ

#define SPLASHIMAGE_ADDR	                               0x9e000000

/* Link Definitions */
#define CONFIG_LOADADDR                                    0x80280000
#define CONFIG_SYS_LOAD_ADDR                               CONFIG_LOADADDR
#define CONFIG_SYS_INIT_SP_ADDR                            0x80200000

/* Size of malloc() pool */
#define CONFIG_SYS_MALLOC_LEN                              ((CONFIG_ENV_SIZE + (2*1024)) * 1024)

#define CONFIG_SYS_SDRAM_BASE                              0x80000000
#define PHYS_SDRAM_1                                       0x80000000
#define PHYS_SDRAM_2			                           0x880000000
#define PHYS_SDRAM_1_SIZE_1GB                              0x40000000
#define PHYS_SDRAM_1_SIZE                                  0x80000000	    /* 2 GB */
#define PHYS_SDRAM_2_SIZE                                  0x100000000	    /* 4 GB */

/* Generic Timer Definitions */
#define COUNTER_FREQUENCY                                  8000000	/* 8MHz */

#define CONFIG_BAUDRATE                                    115200

#define CONFIG_SERIAL_TAG


/* SECO COMMON ENVIRONMENT FOR SECO_CONFIG */

#define SCFG_KERNEL_LOADADDR                               CONFIG_LOADADDR
#define SCFG_KERNEL_FILENAME                               "Image"
#define SCFG_RAMFS_FILENAME                                "ramfs.img"

#define ENV_KERNEL_LOADADDR                                SCFG_KERNEL_LOADADDR

#define ENV_FDT_LOADADDR                                   0x83000000
#define ENV_FDT_OVERLAY_BASEADDR                           0x87000000
#define ENV_FDT_OVERLAY_BASEADDR_OFFSET                    0x01000000
#define ENV_FDT_RESIZE                                        0x80000

#define ENV_BOOTSCRIPT_LOADADDR                            0x9d000000

#define ENV_RAMFS_LOADADDR                                 0xA6000000


#define LOAD_ADDR_KERNEL_LOCAL_DEV                         __stringify(CONFIG_LOADADDR)"\0"
#define LOAD_ADDR_KERNEL_REMOTE_DEV                        __stringify(CONFIG_LOADADDR)"\0"

#define LOAD_ADDR_FDT_LOCAL_DEV                            __stringify(ENV_FDT_LOADADDR)"\0"
#define LOAD_ADDR_FDT_REMOTE_DEV                           __stringify(ENV_FDT_LOADADDR)"\0"

#define LOAD_ADDR_FDT_OVERLAY_LOCAL_DEV                    __stringify(ENV_FDT_OVERLAY_BASEADDR)"\0"
#define LOAD_ADDR_FDT_OVERLAY_REMOTE_DEV                   __stringify(ENV_FDT_OVERLAY_BASEADDR)"\0"

#define LOAD_ADDR_RAMFS_LOCAL_DEV                          __stringify(ENV_RAMFS_LOADADDR)"\0"
#define LOAD_ADDR_RAMFS_REMOTE_DEV                         __stringify(ENV_RAMFS_LOADADDR)"\0"


#define ENV_BOOT_TYPE                                      booti


#endif  /*  __SECO_MX8M_COMMON_H  */
