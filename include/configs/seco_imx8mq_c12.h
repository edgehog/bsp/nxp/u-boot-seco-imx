/* SPDX-License-Identifier:	GPL-2.0+ */

#ifndef __SECO_IMX8M_C12_H
#define __SECO_IMX8M_C12_H

#include "seco_mx8mq_common.h"
#include <linux/stringify.h>


/* ____________________________________________________________________________
  |                                                                            |
  |                                   WATCHDOG                                 |
  |____________________________________________________________________________|
 */

#define CONFIG_REMAKE_ELF

/* ENET Config */
/* ENET1 */
#if defined(CONFIG_FEC_MXC)
#define CONFIG_ETHPRIME                                    "FEC"
#define PHY_ANEG_TIMEOUT                                   20000
#define FEC_QUIRK_ENET_MAC
#define IMX_FEC_BASE                                       0x30BE0000
#define CONFIG_FEC_XCV_TYPE                                RGMII
#define CONFIG_FEC_MXC_PHYADDR                             9

#endif

#define CONFIG_SYS_MMC_ENV_DEV                             1   /* USDHC2 */
#define CONFIG_MMCROOT                                     "/dev/mmcblk1p2"  /* USDHC2 */

#define CONFIG_BAUDRATE                                    115200

#define CONFIG_MXC_UART_BASE                               UART2_BASE_ADDR


#define CONFIG_SYS_FSL_USDHC_NUM                           2
#define CONFIG_SYS_FSL_ESDHC_ADDR                          0

#define CONFIG_SYS_MMC_IMG_LOAD_PART                       1

/* I2C Configs */
#define CONFIG_SYS_I2C_SPEED                               100000

/* USB configs */
#ifndef CONFIG_SPL_BUILD
#define CONFIG_USB_MAX_CONTROLLER_COUNT                    2

#define CONFIG_CMD_USB
#define CONFIG_CMD_READ

#define CONFIG_USBD_HS

#define CONFIG_CMD_USB_MASS_STORAGE
#define CONFIG_USB_GADGET_MASS_STORAGE
#define CONFIG_USB_GADGET_VBUS_DRAW                        2
#define CONFIG_USB_FUNCTION_MASS_STORAGE

#endif

#define CONFIG_SERIAL_TAG
#define CONFIG_FASTBOOT_USB_DEV 0

#ifndef CONFIG_SPL_BUILD
#define CONFIG_DM_PMIC
#endif

#ifdef CONFIG_DM_VIDEO
#define SPLASHIMAGE_ADDR                                   0x50000000
#define CONFIG_VIDEO_LOGO
#define CONFIG_BMP_16BPP
#define CONFIG_BMP_24BPP
#define CONFIG_BMP_32BPP
#define CONFIG_VIDEO_BMP_RLE8
#define CONFIG_VIDEO_BMP_LOGO
#endif


/* ____________________________________________________________________________
  |                                                                            |
  |                                 ENVIRONMENT                                |
  |____________________________________________________________________________|
*/

#define ENV_MMCAUTODETECT                                 "yes"
#define ENV_FDTAUTODETECT                                 "yes"
#define ENV_MEMAUTODETECT                                 "yes"


#define SCFG_DEFAULT_FDT_IMX8_FILE                        "seco-imx8mq-c12.dtb"
#define ENV_DEFAULT_FDT_FILE                              "seco-imx8mq-c12.dtb"

#define ENV_CONSOLE_DEV                                    "ttymxc1"

#define SECO_NUM_BOOT_DEV                                  2  // eMMC + uSD

/*  SCFG = SECO CONFIG  */
#define BOARD_BOOT_ID_EMMC                                 0
#define BOARD_BOOT_ID_SD                                   1
#define BOARD_BOOT_ID_SD_EXT                               2


#define BOARD_ROOT_ID_EMMC                                 0
#define BOARD_ROOT_ID_SD                                   1
#define BOARD_ROOT_ID_SD_EXT                               2


#define SCFG_BOOT_DEV_ID_EMMC                              __stringify(BOARD_BOOT_ID_EMMC)"\0"
#define SCFG_BOOT_DEV_ID_SD                                __stringify(BOARD_BOOT_ID_SD)"\0"
#define SCFG_BOOT_DEV_ID_SD_EXT                            __stringify(BOARD_BOOT_ID_SD_EXT)"\0"
#define SCFG_BOOT_DEV_ID_SPI                               "0"
#define SCFG_BOOT_DEV_ID_SATA                              "0"
#define SCFG_BOOT_DEV_ID_USB                               "0"

#define SCFG_ROOT_DEV_ID_EMMC                              __stringify(BOARD_ROOT_ID_EMMC)"\0"
#define SCFG_ROOT_DEV_ID_SD                                __stringify(BOARD_ROOT_ID_SD)"\0"
#define SCFG_ROOT_DEV_ID_SD_EXT                            __stringify(BOARD_BOOT_ID_SD_EXT)"\0"
#define SCFG_ROOT_DEV_ID_SATA                              "0"
#define SCFG_ROOT_DEV_ID_USB                               "0"


#define SCFG_SET_VIDEOMODE                                 1 /* if 0 not video setting in seco_config */

/* Defaults devices and partitions sources */

/*  boot file partition location  */
#define ENV_SYS_MMC_ENV_DEV                                0
#define ENV_SYS_MMC_KERNEL_PART                            1
#define ENV_SYS_MMC_FDT_PART                               1
#define ENV_SYS_MMC_RAMFS_PART                             1
/*  boot additional file partition location  */
#define ENV_SYS_MMC_BOOSCRIPT_PART                         1
#define ENV_SYS_MMC_BOOATENV_PART                          1
/*  rootfs file  */
#define ENV_SYS_MMC_ROOTFS_PART                            2
#define ENV_ROOT_DEV_ID                                    0

#define ENV_NUM_VIDEO_OUTPUT                               1   /* configurable video output  */

#include "seco_mx8_env.h"

#endif
