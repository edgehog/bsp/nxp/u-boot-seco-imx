#ifndef __SECO_COMMON_H
#define __SECO_COMMON_H

#undef CONFIG_SYS_PROMPT
#define CONFIG_SYS_PROMPT                                  "MX SECO U-Boot > "
#define CONFIG_SYS_PROMPT_HUSH_PS2                         "> "

#endif   /*  __SECO_COMMON_H  */
