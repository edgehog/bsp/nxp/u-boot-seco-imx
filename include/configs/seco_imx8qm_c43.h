#ifndef __SECO_MX8_C43_CONFIG_H
#define __SECO_MX8_C43_CONFIG_H

#include "seco_mx8_common.h"
#include <linux/stringify.h>


/* Flat Device Tree Definitions */
#define CONFIG_OF_BOARD_SETUP

#define CONFIG_SYS_FSL_ESDHC_ADDR       0
#define USDHC1_BASE_ADDR                0x5B010000
#define USDHC2_BASE_ADDR                0x5B020000


#define CONFIG_FEC_XCV_TYPE             RGMII
#define FEC_QUIRK_ENET_MAC
#define PHY_ANEG_TIMEOUT 20000

#define CONFIG_FEC_ENET_DEV 0

#if (CONFIG_FEC_ENET_DEV == 0)
#define IMX_FEC_BASE			0x5B040000
#define CONFIG_FEC_MXC_PHYADDR          0x9
#define CONFIG_ETHPRIME                 "eth0"
#elif (CONFIG_FEC_ENET_DEV == 1)
#define IMX_FEC_BASE			0x5B050000
#define CONFIG_FEC_MXC_PHYADDR          0x1
#define CONFIG_ETHPRIME                 "eth1"
#endif



#ifdef CONFIG_QSPI_BOOT
#define CONFIG_ENV_SECT_SIZE	(128 * 1024)
#define CONFIG_ENV_SPI_BUS	CONFIG_SF_DEFAULT_BUS
#define CONFIG_ENV_SPI_CS	CONFIG_SF_DEFAULT_CS
#define CONFIG_ENV_SPI_MODE	CONFIG_SF_DEFAULT_MODE
#define CONFIG_ENV_SPI_MAX_HZ	CONFIG_SF_DEFAULT_SPEED
#endif



#define CONFIG_NR_DRAM_BANKS		4




/* USB Config */
#ifndef CONFIG_SPL_BUILD
#define CONFIG_CMD_USB
#define CONFIG_USB_STORAGE
#define CONFIG_USBD_HS

#define CONFIG_CMD_USB_MASS_STORAGE
#define CONFIG_USB_GADGET_MASS_STORAGE
#define CONFIG_USB_FUNCTION_MASS_STORAGE

#endif

#define CONFIG_USB_MAX_CONTROLLER_COUNT 2

/* USB OTG controller configs */
#ifdef CONFIG_USB_EHCI_HCD
#define CONFIG_USB_HOST_ETHER
#define CONFIG_USB_ETHER_ASIX
#define CONFIG_MXC_USB_PORTSC		(PORT_PTS_UTMI | PORT_PTS_PTW)
#endif

#ifdef CONFIG_DM_VIDEO
#define CONFIG_VIDEO_LOGO
#define CONFIG_BMP_16BPP
#define CONFIG_BMP_24BPP
#define CONFIG_BMP_32BPP
#define CONFIG_VIDEO_BMP_RLE8
#define CONFIG_VIDEO_BMP_LOGO
#endif





/* ____________________________________________________________________________
  |                                                                            |
  |                                 ENVIRONMENT                                |
  |____________________________________________________________________________|
*/

#define ENV_MMCAUTODETECT                        "yes"
#define ENV_FDTAUTODETECT                        "yes"
#define ENV_MEMAUTODETECT                        "yes"


#define SCFG_DEFAULT_FDT_IMX8_FILE              "seco-imx8qm-c43.dtb"
#define ENV_DEFAULT_FDT_FILE                    "seco-imx8qm-c43.dtb"

#define ENV_CONSOLE_DEV                         "ttyLP0"

#define SECO_NUM_BOOT_DEV                        2  // eMMC, uSD on board

/*  SCFG = SECO CONFIG  */
#define BOARD_BOOT_ID_EMMC                       0
#define BOARD_BOOT_ID_SD                         1
#define BOARD_BOOT_ID_SD_EXT                     2


#define BOARD_ROOT_ID_EMMC                       0
#define BOARD_ROOT_ID_SD                         1
#define BOARD_ROOT_ID_SD_EXT                     2


#define SCFG_BOOT_DEV_ID_EMMC                    __stringify(BOARD_BOOT_ID_EMMC)"\0"
#define SCFG_BOOT_DEV_ID_SD                      __stringify(BOARD_BOOT_ID_SD)"\0"
#define SCFG_BOOT_DEV_ID_SD_EXT                  __stringify(BOARD_BOOT_ID_SD_EXT)"\0"
#define SCFG_BOOT_DEV_ID_SPI                     "0"
#define SCFG_BOOT_DEV_ID_SATA                    "0"
#define SCFG_BOOT_DEV_ID_USB                     "0"

#define SCFG_ROOT_DEV_ID_EMMC                    __stringify(BOARD_ROOT_ID_EMMC)"\0"
#define SCFG_ROOT_DEV_ID_SD                      __stringify(BOARD_ROOT_ID_SD)"\0"
#define SCFG_ROOT_DEV_ID_SD_EXT                  __stringify(BOARD_ROOT_ID_SD_EXT)"\0"
#define SCFG_ROOT_DEV_ID_SATA                    "0"
#define SCFG_ROOT_DEV_ID_USB                     "0"


#define SCFG_SET_VIDEOMODE                       1 /* if 0 not video setting in seco_config */

/* Defaults devices and partitions sources */

/*  boot file partition location  */
#define ENV_SYS_MMC_ENV_DEV                      0
#define ENV_SYS_MMC_KERNEL_PART                  1
#define ENV_SYS_MMC_FDT_PART                     1
#define ENV_SYS_MMC_RAMFS_PART                   1
/*  boot additional file partition location  */
#define ENV_SYS_MMC_BOOSCRIPT_PART               1
#define ENV_SYS_MMC_BOOATENV_PART                1
/*  rootfs file  */
#define ENV_SYS_MMC_ROOTFS_PART                  2
#define ENV_ROOT_DEV_ID                          0

#define ENV_NUM_VIDEO_OUTPUT                     1   /* configurable video output  */


#define ENV_CUSTOM                               \
	"custom_args=iommu.passthrough=1\0"


#include "seco_mx8_env.h"

#endif /* __SECO_MX8_C43_CONFIG_H */
