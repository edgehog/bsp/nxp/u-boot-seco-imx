
#ifndef __SECO_MX6_COMMON_H
#define __SECO_MX6_COMMON_H

#include "mx6_common.h"
#include "imx_env.h"
#include "seco_common.h"

#ifdef CONFIG_SPL

	#include "imx6_spl.h"

	#define CONFIG_SYS_SPI_U_BOOT_OFFS     0x12000

#endif

#ifdef CONFIG_SPL_BUILD
	#define CONFIG_SPL_DRIVERS_MISC_SUPPORT
#endif

#ifdef CONFIG_MX6S
#define SYS_NOSMP "nosmp"
#else
#define SYS_NOSMP
#endif


/* ____________________________________________________________________________
  |                                                                            |
  |                                   MEMORY                                   |
  |____________________________________________________________________________|
*/
/* Physical Memory Map */

#define PHYS_SDRAM                               MMDC0_ARB_BASE_ADDR

#define CONFIG_SYS_SDRAM_BASE                    PHYS_SDRAM
#define CONFIG_SYS_INIT_RAM_ADDR                 IRAM_BASE_ADDR
#define CONFIG_SYS_INIT_RAM_SIZE                 IRAM_SIZE

#define DEFAULT_CMA_VALUE                        396


#define CONFIG_SYS_INIT_SP_OFFSET \
	(CONFIG_SYS_INIT_RAM_SIZE - GENERATED_GBL_DATA_SIZE)
#define CONFIG_SYS_INIT_SP_ADDR \
	(CONFIG_SYS_INIT_RAM_ADDR + CONFIG_SYS_INIT_SP_OFFSET)


#if defined CONFIG_SECOMX6_256MB_1x256

	#define CONFIG_DDR_MB 256
	#define CONFIG_DDR_32BIT
	#define PHYS_SDRAM_SIZE        (256u * 1024 * 1024)

#elif defined CONFIG_SECOMX6_512MB_2x256

	#define CONFIG_DDR_MB 512
	#define CONFIG_DDR_32BIT
	#define PHYS_SDRAM_SIZE        (512u * 1024 * 1024)

#elif defined CONFIG_SECOMX6_1GB_2x512

	#define CONFIG_DDR_MB 1024
	#define CONFIG_DDR_64BIT
	#define PHYS_SDRAM_SIZE        (1u * 1024 * 1024 * 1024)

#elif defined CONFIG_SECOMX6_1GB_4x256

	#define CONFIG_DDR_MB 1024
	#define CONFIG_DDR_64BIT
	#define PHYS_SDRAM_SIZE        (1u * 1024 * 1024 * 1024)

#elif defined CONFIG_SECOMX6_2GB_4x512

	#define CONFIG_DDR_MB 2048
	#define CONFIG_DDR_64BIT
	#define PHYS_SDRAM_SIZE        (2u * 1024 * 1024 * 1024)

#elif defined CONFIG_SECOMX6_4GB_8x512

	#define CONFIG_DDR_MB 3800
	#define CONFIG_DDR_64BIT
	#define PHYS_SDRAM_SIZE        (4u * 1024 * 1024 * 1024 - 256u * 1024 * 1024)

#elif defined CONFIG_SECOMX6_DDR_DYNAMIC_SIZE

	#define CONFIG_DDR_MB_DYNAMIC

#endif


/* Size of malloc() pool */
#define CONFIG_SYS_MALLOC_LEN                    (16 * SZ_1M)






/* ____________________________________________________________________________
  |                                                                            |
  |                                     UART                                   |
  |____________________________________________________________________________|
*/
#define CONFIG_MXC_UART
#define CONFIG_CONS_INDEX                        1
#define CONFIG_BAUDRATE                          115200


/* ____________________________________________________________________________
  |                                                                            |
  |                                       SATA                                 |
  |____________________________________________________________________________|
*/
#define CONFIG_SYS_SATA_MAX_DEVICE               1


/* ____________________________________________________________________________
  |                                                                            |
  |                              DISPLAY (FRAMEBUFFER)                         |
  |____________________________________________________________________________|
*/
#ifdef CONFIG_SECO_VIDEO
        #define CONFIG_VIDEO_BMP_RLE8
        #define CONFIG_HIDE_LOGO_VERSION
        #define CONFIG_BMP_16BPP
        #define CONFIG_VIDEO_LOGO
        #define CONFIG_VIDEO_BMP_LOGO
        #define CONFIG_IMX_HDMI
        #define CONFIG_IMX_VIDEO_SKIP
#endif


/* ____________________________________________________________________________
  |                                                                            |
  |                             ENV MEM SIZE/OFFSET                            |
  |____________________________________________________________________________|
*/
/* #define CONFIG_ENV_IS_IN_MMC */
/* #define CONFIG_SYS_NO_FLASH  */

#if defined(CONFIG_ENV_IS_IN_MMC)

#elif defined(CONFIG_ENV_IS_IN_SPI_FLASH)

	#define CONFIG_ENV_SPI_BUS               CONFIG_SF_DEFAULT_BUS
	#define CONFIG_ENV_SPI_CS                CONFIG_SF_DEFAULT_CS
	#define CONFIG_ENV_SPI_MODE              CONFIG_SF_DEFAULT_MODE
	#define CONFIG_ENV_SPI_MAX_HZ            CONFIG_SF_DEFAULT_SPEED

#elif defined(CONFIG_ENV_IS_IN_FLASH)

#elif defined(CONFIG_ENV_IS_IN_NAND)

#elif defined(CONFIG_ENV_IS_IN_SATA)

	#define CONFIG_SATA_ENV_DEV              0
	#define CONFIG_SYS_DCACHE_OFF /* remove when sata driver support cache */

#endif


/* Falcon Mode */
#define CONFIG_SPL_FS_LOAD_ARGS_NAME	"args"
#define CONFIG_SPL_FS_LOAD_KERNEL_NAME	"uImage"
#define CONFIG_SYS_SPL_ARGS_ADDR       0x18000000

/* Falcon Mode - MMC support: args@1MB kernel@2MB */
#define CONFIG_SYS_MMCSD_RAW_MODE_ARGS_SECTOR  0x800   /* 1MB */
#define CONFIG_SYS_MMCSD_RAW_MODE_ARGS_SECTORS (0x20000 / 512)
#define CONFIG_SYS_MMCSD_RAW_MODE_KERNEL_SECTOR        0x1000  /* 2MB */


#define SPLASHIMAGE_ADDR                     10000000


/* SECO COMMON ENVIRONMENT FOR SECO_CONFIG */

#define SCFG_KERNEL_LOADADDR                CONFIG_LOADADDR
#define SCFG_KERNEL_FILENAME                "zImage"
#define SCFG_RAMFS_FILENAME                 "ramfs.img"

#define ENV_KERNEL_LOADADDR                 SCFG_KERNEL_LOADADDR


#if defined(CONFIG_MX6SL) || defined(CONFIG_MX6SLL) || \
	defined(CONFIG_MX6SX) || \
	defined(CONFIG_MX6UL) || defined(CONFIG_MX6ULL)
#define ENV_FDT_LOADADDR                   0x85700000
#define ENV_FDT_OVERLAY_BASEADDR           0x8a000000
#define ENV_RAMFS_LOADADDR                 0x86800000
#define ENV_BOOTSCRIPT_LOADADDR            0x84700000
#else
#define ENV_FDT_LOADADDR                   0x18000000
#define ENV_FDT_OVERLAY_BASEADDR           0x1a000000
#define ENV_RAMFS_LOADADDR                 0x12C00000
#define ENV_BOOTSCRIPT_LOADADDR            0x17000000
#endif
#define ENV_FDT_OVERLAY_BASEADDR_OFFSET    0x01000000
#define ENV_FDT_RESIZE                         0x4000


#define LOAD_ADDR_KERNEL_LOCAL_DEV         __stringify(CONFIG_LOADADDR)"\0"
#define LOAD_ADDR_KERNEL_REMOTE_DEV        __stringify(CONFIG_LOADADDR)"\0"

#define LOAD_ADDR_FDT_LOCAL_DEV            __stringify(ENV_FDT_LOADADDR)"\0"
#define LOAD_ADDR_FDT_REMOTE_DEV           __stringify(ENV_FDT_LOADADDR)"\0"

#define LOAD_ADDR_FDT_OVERLAY_LOCAL_DEV    __stringify(ENV_FDT_OVERLAY_BASEADDR)"\0"
#define LOAD_ADDR_FDT_OVERLAY_REMOTE_DEV   __stringify(ENV_FDT_OVERLAY_BASEADDR)"\0"

#define LOAD_ADDR_RAMFS_LOCAL_DEV          __stringify(ENV_RAMFS_LOADADDR)"\0"
#define LOAD_ADDR_RAMFS_REMOTE_DEV         __stringify(ENV_RAMFS_LOADADDR)"\0"


#define ENV_BOOT_TYPE                      bootz



#endif    /*  __SECO_MX6_COMMON_H  */
