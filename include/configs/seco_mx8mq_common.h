#ifndef __SECO_MX8MQ_COMMON_H
#define __SECO_MX8MQ_COMMON_H

#include <linux/sizes.h>
#include <asm/arch/imx-regs.h>
#include "imx_env.h"
#include "seco_common.h"

#define CONFIG_SYS_BOOTM_LEN	                           (32 * SZ_1M)

#define CONFIG_SPL_MAX_SIZE                                (148 * 1024)
#define CONFIG_SYS_MONITOR_LEN                             (512 * 1024)
#define CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_USE_SECTOR
#define CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR	           (0x300 + CONFIG_SECONDARY_BOOT_SECTOR_OFFSET)

#ifdef CONFIG_SPL_BUILD
/*#define CONFIG_ENABLE_DDR_TRAINING_DEBUG*/
#define CONFIG_SPL_LDSCRIPT	                               "arch/arm/cpu/armv8/u-boot-spl.lds"
#define CONFIG_SPL_STACK                                   0x187FF0
#define CONFIG_SPL_BSS_START_ADDR                          0x00180000
#define CONFIG_SPL_BSS_MAX_SIZE                            0x2000	/* 8 KB */
#define CONFIG_SYS_SPL_MALLOC_START                        0x42200000
#define CONFIG_SYS_SPL_MALLOC_SIZE                         0x80000	/* 512 KB */

#define CONFIG_MALLOC_F_ADDR                               0x182000 /* malloc f used before GD_FLG_FULL_MALLOC_INIT set */

#define CONFIG_SPL_ABORT_ON_RAW_IMAGE /* For RAW image gives a error info not panic */

#undef CONFIG_DM_MMC
#undef CONFIG_DM_PMIC
#undef CONFIG_DM_PMIC_PFUZE100

#define CONFIG_SYS_I2C

#define CONFIG_POWER
#define CONFIG_POWER_I2C
#define CONFIG_POWER_PFUZE100
#define CONFIG_POWER_PFUZE100_I2C_ADDR                     0x08

#if defined(CONFIG_NAND_BOOT)
#define CONFIG_SPL_NAND_SUPPORT
#define CONFIG_SPL_DMA
#define CONFIG_SPL_NAND_MXS
#define CONFIG_SPL_NAND_BASE
#define CONFIG_SPL_NAND_IDENT
#define CONFIG_SYS_NAND_U_BOOT_OFFS                        0x4000000 /* Put the FIT out of first 64MB boot area */

/* Set a redundant offset in nand FIT mtdpart. The new uuu will burn full boot image (not only FIT part) to the mtdpart, so we check both two offsets */
#define CONFIG_SYS_NAND_U_BOOT_OFFS_REDUND \
	(CONFIG_SYS_NAND_U_BOOT_OFFS + CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR * 512 - 0x8400)
#endif

#endif /* CONFIG_SPL_BUILD*/


#define SPLASHIMAGE_ADDR	                           0x50000000

/* Link Definitions */
#define CONFIG_LOADADDR	                                   0x40480000

#define CONFIG_SYS_LOAD_ADDR                               CONFIG_LOADADDR

#define CONFIG_SYS_INIT_RAM_ADDR                           0x40000000
#define CONFIG_SYS_INIT_RAM_SIZE                           0x80000
#define CONFIG_SYS_INIT_SP_OFFSET \
        (CONFIG_SYS_INIT_RAM_SIZE - GENERATED_GBL_DATA_SIZE)
#define CONFIG_SYS_INIT_SP_ADDR \
        (CONFIG_SYS_INIT_RAM_ADDR + CONFIG_SYS_INIT_SP_OFFSET)


/* Size of malloc() pool */
#define CONFIG_SYS_MALLOC_LEN                              ((CONFIG_ENV_SIZE + (2*1024)) * 1024)

#define CONFIG_SYS_SDRAM_BASE                              0x40000000
#define PHYS_SDRAM                                         0x40000000

#define PHYS_SDRAM_SIZE	                                   0x80000000 /* 2GB DDR3L for two rank */
#define PHYS_DRAM_IS_1GB                                   0x40000000
#define PHYS_DRAM_IS_2GB                                   0x80000000
#define PHYS_DRAM_IS_3GB                                   0xc0000000
#define PHYS_DRAM_IS_4GB                                   0x100000000
#define PHYS_SDRAM_2                                       0x100000000
#define PHYS_SDRAM_2_SIZE                                  0x40000000 /* 1GB */



/* SECO COMMON ENVIRONMENT FOR SECO_CONFIG */

#define SCFG_KERNEL_LOADADDR                               CONFIG_LOADADDR
#define SCFG_KERNEL_FILENAME                               "Image"
#define SCFG_RAMFS_FILENAME                                "ramfs.img"

#define ENV_KERNEL_LOADADDR                                SCFG_KERNEL_LOADADDR

#define ENV_FDT_LOADADDR                                   0x43000000
#define ENV_FDT_OVERLAY_BASEADDR                           0x45000000
#define ENV_FDT_OVERLAY_BASEADDR_OFFSET                    0x01000000
#define ENV_FDT_RESIZE                                        0x80000

#define ENV_BOOTSCRIPT_LOADADDR                            0x45600000

#define ENV_RAMFS_LOADADDR                                 0x46000000


#define LOAD_ADDR_KERNEL_LOCAL_DEV                         __stringify(CONFIG_LOADADDR)"\0"
#define LOAD_ADDR_KERNEL_REMOTE_DEV                        __stringify(CONFIG_LOADADDR)"\0"

#define LOAD_ADDR_FDT_LOCAL_DEV                            __stringify(ENV_FDT_LOADADDR)"\0"
#define LOAD_ADDR_FDT_REMOTE_DEV                           __stringify(ENV_FDT_LOADADDR)"\0"

#define LOAD_ADDR_FDT_OVERLAY_LOCAL_DEV                    __stringify(ENV_FDT_OVERLAY_BASEADDR)"\0"
#define LOAD_ADDR_FDT_OVERLAY_REMOTE_DEV                   __stringify(ENV_FDT_OVERLAY_BASEADDR)"\0"

#define LOAD_ADDR_RAMFS_LOCAL_DEV                          __stringify(ENV_RAMFS_LOADADDR)"\0"
#define LOAD_ADDR_RAMFS_REMOTE_DEV                         __stringify(ENV_RAMFS_LOADADDR)"\0"


#define ENV_BOOT_TYPE                                      booti

#endif   /*  __SECO_MX8MQ_COMMON_H  */