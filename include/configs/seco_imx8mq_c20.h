

#ifndef __SECO_MX8MQ_C20_CONFIG_H
#define __SECO_MX8MQ_C20_CONFIG_H

#include "seco_mx8mq_common.h"
#include <linux/stringify.h>


/* ____________________________________________________________________________
  |                                                                            |
  |                                   WATCHDOG                                 |
  |____________________________________________________________________________|
 */

#define APX_WDT_TRIGGER_BASE                               GPIO4_BASE_ADDR
#define APX_WDT_TRIGGER_NUM                                19

#define APX_WDT_ENABLE_BASE                                GPIO4_BASE_ADDR
#define APX_WDT_ENABLE_NUM                                 18

//#define APX_WDT_TRIGGER_PAD_CTL			   0x303301a8

#define APX_WDT_EN_HIGH

#define CONFIG_REMAKE_ELF

/* ENET Config */
/* ENET1 */
#if defined(CONFIG_FEC_MXC)
#define CONFIG_ETHPRIME                                    "FEC"
#define PHY_ANEG_TIMEOUT                                   20000
#define FEC_QUIRK_ENET_MAC
#define IMX_FEC_BASE                                       0x30BE0000
#define CONFIG_FEC_XCV_TYPE                                RGMII
#define CONFIG_FEC_MXC_PHYADDR                             9

#endif


#define CONFIG_SYS_MMC_ENV_DEV                             1   /* USDHC2 */
#define CONFIG_MMCROOT                                     "/dev/mmcblk1p2"  /* USDHC2 */

#define CONFIG_BAUDRATE                                    115200

#define CONFIG_MXC_UART_BASE                               UART2_BASE_ADDR


#define CONFIG_SYS_FSL_USDHC_NUM                           2
#define CONFIG_SYS_FSL_ESDHC_ADDR                          0

#define CONFIG_SYS_MMC_IMG_LOAD_PART                       1

/* I2C Configs */
#define CONFIG_SYS_I2C_SPEED                               100000

#ifdef CONFIG_NAND_MXS
#define CONFIG_CMD_NAND_TRIMFFS

/* NAND stuff */
#define CONFIG_SYS_MAX_NAND_DEVICE                         1
#define CONFIG_SYS_NAND_BASE                               0x20000000
#define CONFIG_SYS_NAND_5_ADDR_CYCLE
#define CONFIG_SYS_NAND_ONFI_DETECTION
#define CONFIG_SYS_NAND_USE_FLASH_BBT
#endif /* CONFIG_NAND_MXS */

/* USB configs */
#ifndef CONFIG_SPL_BUILD
#define CONFIG_USB_MAX_CONTROLLER_COUNT                    1

#define CONFIG_CMD_USB

#define CONFIG_USBD_HS

#define CONFIG_CMD_USB_MASS_STORAGE
#define CONFIG_USB_GADGET_MASS_STORAGE
#define CONFIG_USB_GADGET_VBUS_DRAW                        2
#define CONFIG_USB_FUNCTION_MASS_STORAGE

#endif

#define CONFIG_SERIAL_TAG


/* ____________________________________________________________________________
  |                                                                            |
  |                                 ENVIRONMENT                                |
  |____________________________________________________________________________|
*/

#define ENV_MMCAUTODETECT                                 "yes"
#define ENV_FDTAUTODETECT                                 "yes"
#define ENV_MEMAUTODETECT                                 "yes"


#define SCFG_DEFAULT_FDT_IMX8_FILE                        "seco-imx8mq-c20.dtb"
#define ENV_DEFAULT_FDT_FILE                              "seco-imx8mq-c20.dtb"

#define ENV_CONSOLE_DEV                                    "ttymxc1"

#define SECO_NUM_BOOT_DEV                                  1  // eMMC

/*  SCFG = SECO CONFIG  */
#define BOARD_BOOT_ID_EMMC                                 0
#define BOARD_BOOT_ID_SD                                   1
#define BOARD_BOOT_ID_SD_EXT                               2


#define BOARD_ROOT_ID_EMMC                                 0
#define BOARD_ROOT_ID_SD                                   1
#define BOARD_ROOT_ID_SD_EXT                               2


#define SCFG_BOOT_DEV_ID_EMMC                              __stringify(BOARD_BOOT_ID_EMMC)"\0"
#define SCFG_BOOT_DEV_ID_SD                                __stringify(BOARD_BOOT_ID_SD)"\0"
#define SCFG_BOOT_DEV_ID_SD_EXT                            __stringify(BOARD_BOOT_ID_SD_EXT)"\0"
#define SCFG_BOOT_DEV_ID_SPI                               "0"
#define SCFG_BOOT_DEV_ID_SATA                              "0"
#define SCFG_BOOT_DEV_ID_USB                               "0"

#define SCFG_ROOT_DEV_ID_EMMC                              __stringify(BOARD_ROOT_ID_EMMC)"\0"
#define SCFG_ROOT_DEV_ID_SD                                __stringify(BOARD_ROOT_ID_SD)"\0"
#define SCFG_ROOT_DEV_ID_SD_EXT                            __stringify(BOARD_BOOT_ID_SD_EXT)"\0"
#define SCFG_ROOT_DEV_ID_SATA                              "0"
#define SCFG_ROOT_DEV_ID_USB                               "0"


#define SCFG_SET_VIDEOMODE                                 1 /* if 0 not video setting in seco_config */

/* Defaults devices and partitions sources */

/*  boot file partition location  */
#define ENV_SYS_MMC_ENV_DEV                                0
#define ENV_SYS_MMC_KERNEL_PART                            1
#define ENV_SYS_MMC_FDT_PART                               1
#define ENV_SYS_MMC_RAMFS_PART                             1
/*  boot additional file partition location  */
#define ENV_SYS_MMC_BOOSCRIPT_PART                         1
#define ENV_SYS_MMC_BOOATENV_PART                          1
/*  rootfs file  */          
#define ENV_SYS_MMC_ROOTFS_PART                            2
#define ENV_ROOT_DEV_ID                                    0

#define ENV_NUM_VIDEO_OUTPUT                               1   /* configurable video output  */


#include "seco_mx8_env.h"

#endif
