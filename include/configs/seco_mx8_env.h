#ifndef __SECO_MX8_ENVIRONMENT_H
#define __SECO_MX8_ENVIRONMENT_H

#include <linux/stringify.h>

#ifdef CONFIG_ARCH_IMX8

#define FW_HDP_ADDRESS           0x9c000000
#define FW_HDMIRX_ADDRESS        0x9c800000

#define FW_HDMI_FILE             hdmitxfw.bin
#define FW_DP_FILE               dpfw.bin
#define FW_HDMIRX_FILE           hdmirxfw.bin

#define ENV_FW_SECO    \
    "fw_hdp_addr="__stringify(FW_HDP_ADDRESS)"\0" \
    "fw_hdmirx_addr="__stringify(FW_HDMIRX_ADDRESS)"\0" \
    "fw_hdmi_file="__stringify(FW_HDMI_FILE)"\0" \
    "fw_dp_file="__stringify(FW_DP_FILE)"\0" \
    "fw_hdmirx_file="__stringify(FW_HDMIRX_FILE)"\0" \
    "fw_hdp_cmd_load=hdp load\0" \
    "fw_hdprx_cmd_load=hdprx load\0"

#define ENV_FW_HDP_ADDRESS           "${fw_hdp_addr}"
#define ENV_FW_HDMIRX_ADDRESS        "${fw_hdmirx_addr}"

#define ENV_FW_HDMI_FILE             "${fw_hdmi_file}"
#define ENV_FW_DP_FILE               "${fw_dp_file}"
#define ENV_FW_HDMIRX_FILE           "${fw_hdmirx_file}"

#define ENV_FW_HDP_CMD_LOAD          "${fw_hdp_cmd_load}"
#define ENV_FW_HDPRX_CMD_LOAD        "${fw_hdprx_cmd_load}"

#else

#define ENV_FW_SECO ""

#endif

#include <seco/seco_environment.h>

#endif   /*  __SECO_MX8_ENVIRONMENT_H  */
