/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Copyright 2019 NXP
 */

#ifndef __SECO_IMX8MM_MYON2_H
#define __SECO_IMX8MM_MYON2_H

#include <linux/sizes.h>
#include <asm/arch/imx-regs.h>
#include "seco_common.h"

#define CONFIG_SPL_MAX_SIZE			(148 * 1024)
#define CONFIG_SYS_MONITOR_LEN			SZ_512K
#define CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_USE_SECTOR
#define CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR	(0x300 + CONFIG_SECONDARY_BOOT_SECTOR_OFFSET)
#define CONFIG_SYS_MMCSD_FS_BOOT_PARTITION	1
#define CONFIG_SYS_UBOOT_BASE	\
	(QSPI0_AMBA_BASE + CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR * 512)

/* eMMC uSD Configuration */
#define BOOT_ID_EMMC	2
#define ROOT_ID_EMMC	2
#define BOOT_ID_USD	1
#define ROOT_ID_USD	1

/* SPL */
#ifdef CONFIG_SPL_BUILD
#define CONFIG_SPL_STACK		0x920000
#define CONFIG_SPL_BSS_START_ADDR	0x910000
#define CONFIG_SPL_BSS_MAX_SIZE		SZ_8K
#define CONFIG_SYS_SPL_MALLOC_START	0x42200000
#define CONFIG_SYS_SPL_MALLOC_SIZE	SZ_512K

#ifndef CONFIG_SPL_POWER_SUPPORT
#define CONFIG_SPL_POWER_SUPPORT
#endif
#ifndef CONFIG_SPL_GPIO_SUPPORT
#define CONFIG_SPL_GPIO_SUPPORT
#endif

#undef CONFIG_DM_PMIC
#undef CONFIG_DM_PMIC_PFUZE100

#define CONFIG_POWER
#define CONFIG_POWER_I2C
#define CONFIG_POWER_BD71837

#ifndef CONFIG_SPL_I2C_SUPPORT
#define CONFIG_SPL_I2C_SUPPORT
#endif
#define CONFIG_SYS_I2C
#ifndef CONFIG_SYS_I2C_MXC_I2C1
#define CONFIG_SYS_I2C_MXC_I2C1
#endif
#ifndef CONFIG_SYS_I2C_MXC_I2C2
#define CONFIG_SYS_I2C_MXC_I2C2
#endif
#ifndef  CONFIG_SYS_I2C_MXC_I2C3
#define CONFIG_SYS_I2C_MXC_I2C3
#endif
#endif /* CONFIG_SPL_BUILD */

#define CONFIG_SYS_I2C_SPEED		100000

#define CONFIG_CMD_READ
#define CONFIG_SERIAL_TAG

#define CONFIG_REMAKE_ELF

/* ENET Config */
/* ENET1 */
#if defined(CONFIG_CMD_NET)

#ifndef CONFIG_CMD_MII
#define CONFIG_CMD_MII
#endif

#ifndef CONFIG_MII
#define CONFIG_MII
#endif

#define CONFIG_ETHPRIME			"FEC"

#ifndef CONFIG_FEC_MXC
#define CONFIG_FEC_MXC
#endif

#define CONFIG_FEC_XCV_TYPE		RGMII
#ifdef CONFIG_TANARO
#define CONFIG_FEC_MXC_PHYADDR		0
#else
#define CONFIG_FEC_MXC_PHYADDR		4
#endif
#define FEC_QUIRK_ENET_MAC

#ifndef CONFIG_PHY_GIGE
#define CONFIG_PHY_GIGE
#endif

#define IMX_FEC_BASE			0x30BE0000
#endif /* CONFIG_CMD_NET */

/* Link Definitions */
#ifndef  CONFIG_LOADADDR
#define CONFIG_LOADADDR			0x40280000
#endif

#define CONFIG_SYS_LOAD_ADDR		CONFIG_LOADADDR
#define SCRIPT_ADDR			0x40400000
#define FDT_ADDR			0x40300000

#ifndef  CONFIG_APPEND_BOOTARGS
#define CONFIG_APPEND_BOOTARGS		1
#endif

#define CONFIG_SYS_INIT_RAM_ADDR	0x40000000
#define CONFIG_SYS_INIT_RAM_SIZE	0x00080000
#define CONFIG_SYS_INIT_SP_OFFSET \
	(CONFIG_SYS_INIT_RAM_SIZE - GENERATED_GBL_DATA_SIZE)
#define CONFIG_SYS_INIT_SP_ADDR \
	(CONFIG_SYS_INIT_RAM_ADDR + CONFIG_SYS_INIT_SP_OFFSET)

#define CONFIG_ENV_OVERWRITE
#if defined(CONFIG_ENV_IS_IN_SPI_FLASH)
#define CONFIG_ENV_OFFSET		(4 * 1024 * 1024)
#define CONFIG_ENV_SECT_SIZE		(64 * 1024)
#define CONFIG_ENV_SPI_BUS		CONFIG_SF_DEFAULT_BUS
#define CONFIG_ENV_SPI_CS		CONFIG_SF_DEFAULT_CS
#define CONFIG_ENV_SPI_MODE		CONFIG_SF_DEFAULT_MODE
#define CONFIG_ENV_SPI_MAX_HZ		CONFIG_SF_DEFAULT_SPEED
#endif
#define CONFIG_SYS_MMC_ENV_DEV		0		  /* USDHC2 */
#define CONFIG_MMCROOT			"/dev/mmcblk0p2"  /* USDHC2 */

/* Size of malloc() pool */
#define CONFIG_SYS_MALLOC_LEN		SZ_32M

#define CONFIG_SYS_SDRAM_BASE		0x40000000
#define PHYS_SDRAM			0x40000000
#define PHYS_SDRAM_SIZE			PHYS_DRAM_IS_3GB /* 3GB DDR */
#define PHYS_DRAM_IS_1GB		0x40000000
#define PHYS_DRAM_IS_2GB		0x80000000
#define PHYS_DRAM_IS_3GB		0xc0000000
#define PHYS_DRAM_IS_4GB		0x100000000
#define PHYS_SDRAM_2			0x100000000
#define PHYS_SDRAM_2_SIZE		0x40000000 /* 1GB */

#define CONSOLE_DEV			"ttymxc0"
#define DEFAULT_FDT_FILE		"seco-imx8mm-myon2-conxm-hdmi.dtb"

#define CONFIG_BAUDRATE			115200

#ifndef CONFIG_MXC_UART
#define CONFIG_MXC_UART
#endif

#ifndef CONFIG_MXC_UART_BASE
#define CONFIG_MXC_UART_BASE		UART1_BASE_ADDR
#endif
/* Monitor Command Prompt */
#define CONFIG_SYS_PROMPT_HUSH_PS2	"> "
#define CONFIG_SYS_CBSIZE		2048
#define CONFIG_SYS_MAXARGS		64
#define CONFIG_SYS_BARGSIZE		CONFIG_SYS_CBSIZE
#define CONFIG_SYS_PBSIZE		(CONFIG_SYS_CBSIZE + \
					sizeof(CONFIG_SYS_PROMPT) + 16)

#define CONFIG_IMX_BOOTAUX

/* USDHC */
#ifndef CONFIG_CMD_MMC
#define CONFIG_CMD_MMC
#endif

#define CONFIG_FSL_USDHC

#ifdef CONFIG_TARGET_SECO_IMX8MM_DDR4
#define CONFIG_SYS_FSL_USDHC_NUM	1
#else
#define CONFIG_SYS_FSL_USDHC_NUM	2
#endif
#define CONFIG_SYS_FSL_ESDHC_ADDR	0
#define CONFIG_SYS_MMC_IMG_LOAD_PART	1

#ifdef CONFIG_FSL_FSPI
#define FSL_FSPI_FLASH_SIZE		SZ_32M
#define FSL_FSPI_FLASH_NUM		1
#define FSPI0_BASE_ADDR			0x30bb0000
#define FSPI0_AMBA_BASE			0x0
#define CONFIG_FSPI_QUAD_SUPPORT

#define CONFIG_SYS_FSL_FSPI_AHB
#endif

#ifdef CONFIG_NAND_MXS
#define CONFIG_CMD_NAND_TRIMFFS

/* NAND stuff */
#define CONFIG_SYS_MAX_NAND_DEVICE	1
#define CONFIG_SYS_NAND_BASE		0x20000000
#define CONFIG_SYS_NAND_5_ADDR_CYCLE
#define CONFIG_SYS_NAND_ONFI_DETECTION
#define CONFIG_SYS_NAND_USE_FLASH_BBT
#endif /* CONFIG_NAND_MXS */

/* USB configs */
#ifndef CONFIG_SPL_BUILD
#define CONFIG_CMD_USB
#define CONFIG_USB_STORAGE

#define CONFIG_CMD_USB_MASS_STORAGE
#define CONFIG_USB_GADGET_MASS_STORAGE
#define CONFIG_USB_FUNCTION_MASS_STORAGE
#endif

#define CONFIG_CI_UDC
#define CONFIG_USB_GADGET_DUALSPEED
#define CONFIG_USBD_HS
#define CONFIG_USB_GADGET_VBUS_DRAW	2
#define CONFIG_USB_MAX_CONTROLLER_COUNT	2


#define CONFIG_MXC_USB_PORTSC  (PORT_PTS_UTMI | PORT_PTS_PTW)

#ifdef CONFIG_DM_VIDEO
#define CONFIG_VIDEO_MXS
#define CONFIG_VIDEO_LOGO
#define CONFIG_SPLASH_SCREEN
#define CONFIG_SPLASH_SCREEN_ALIGN
#define CONFIG_CMD_BMP
#define CONFIG_BMP_16BPP
#define CONFIG_BMP_24BPP
#define CONFIG_BMP_32BPP
#define CONFIG_VIDEO_BMP_RLE8
#define CONFIG_VIDEO_BMP_LOGO
#endif

/* ____________________________________________________________________________
  |                                                                            |
  |                                 ENVIRONMENT                                |
  |____________________________________________________________________________|
*/

#define ENV_MMCAUTODETECT			"yes"
#define ENV_FDTAUTODETECT			"yes"
#define ENV_MEMAUTODETECT			"yes"


#define SCFG_DEFAULT_FDT_IMX8_FILE		"seco-imx8mm-myon2-conxm-hdmi.dtb"
#define ENV_DEFAULT_FDT_FILE			"seco-imx8mm-myon2-conxm-hdmi.dtb"

#define ENV_CONSOLE_DEV				"ttymxc0"

#define SECO_NUM_BOOT_DEV			2  // eMMC, uSD

/* boot device id  */
#define BOARD_BOOT_ID_EMMC			0
#define BOARD_BOOT_ID_SD_EXT			1

#define BOARD_ROOT_ID_EMMC			0
#define BOARD_ROOT_ID_SD_EXT			1


#define SCFG_BOOT_DEV_ID_EMMC			__stringify(BOARD_BOOT_ID_EMMC)"\0"
#define SCFG_BOOT_DEV_ID_U_SD			"0"
#define SCFG_BOOT_DEV_ID_SD_EXT			__stringify(BOARD_BOOT_ID_SD_EXT)"\0"
#define SCFG_BOOT_DEV_ID_SPI			"0"
#define SCFG_BOOT_DEV_ID_SATA			"0"
#define SCFG_BOOT_DEV_ID_USB			"0"

#define SCFG_ROOT_DEV_ID_EMMC			__stringify(BOARD_ROOT_ID_EMMC)"\0"
#define SCFG_ROOT_DEV_ID_U_SD			"0"
#define SCFG_ROOT_DEV_ID_SD_EXT			__stringify(BOARD_ROOT_ID_SD_EXT)"\0"
#define SCFG_ROOT_DEV_ID_SATA			"0"
#define SCFG_ROOT_DEV_ID_USB			"0"


#define SCFG_SET_VIDEOMODE			0 /* if 0 not video setting in seco_config */

/* Defaults devices and partitions sources */

/*  boot file partition location  */
#define ENV_SYS_MMC_ENV_DEV			0
#define ENV_SYS_MMC_KERNEL_PART			1
#define ENV_SYS_MMC_FDT_PART			1
#define ENV_SYS_MMC_RAMFS_PART			1
/*  boot additional file partition location  */
#define ENV_SYS_MMC_BOOSCRIPT_PART		1
#define ENV_SYS_MMC_BOOATENV_PART		1
/*  rootfs file  */
#define ENV_SYS_MMC_ROOTFS_PART			2
#define ENV_ROOT_DEV_ID				0

#define ENV_NUM_VIDEO_OUTPUT			1   /* configurable video output  */

/* SECO COMMON ENVIRONMENT FOR SECO_CONFIG */

#define SCFG_KERNEL_LOADADDR			CONFIG_LOADADDR
#define SCFG_KERNEL_FILENAME			"Image"
#define SCFG_RAMFS_FILENAME			"ramfs.img"

#define ENV_KERNEL_LOADADDR			SCFG_KERNEL_LOADADDR

#define ENV_FDT_LOADADDR			0x43000000
#define ENV_FDT_OVERLAY_BASEADDR		0x45000000
#define ENV_FDT_OVERLAY_BASEADDR_OFFSET		0x01000000
#define ENV_FDT_RESIZE				0x80000

#define ENV_BOOTSCRIPT_LOADADDR			0x45600000

#define ENV_RAMFS_LOADADDR			0x46000000

#define LOAD_ADDR_KERNEL_LOCAL_DEV		__stringify(CONFIG_LOADADDR)"\0"
#define LOAD_ADDR_KERNEL_REMOTE_DEV		__stringify(CONFIG_LOADADDR)"\0"

#define LOAD_ADDR_FDT_LOCAL_DEV			__stringify(ENV_FDT_LOADADDR)"\0"
#define LOAD_ADDR_FDT_REMOTE_DEV		__stringify(ENV_FDT_LOADADDR)"\0"

#define LOAD_ADDR_FDT_OVERLAY_LOCAL_DEV		__stringify(ENV_FDT_OVERLAY_BASEADDR)"\0"
#define LOAD_ADDR_FDT_OVERLAY_REMOTE_DEV	__stringify(ENV_FDT_OVERLAY_BASEADDR)"\0"

#define LOAD_ADDR_RAMFS_LOCAL_DEV		__stringify(ENV_RAMFS_LOADADDR)"\0"
#define LOAD_ADDR_RAMFS_REMOTE_DEV		__stringify(ENV_RAMFS_LOADADDR)"\0"

#define ENV_BOOT_TYPE				booti

#include "seco_mx8_env.h"

#endif /* __SECO_IMX8MM_MYON2_H */
