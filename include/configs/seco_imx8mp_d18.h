/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Copyright 2019 NXP
 */

#ifndef __SECO_IMX8MP_D18_H
#define __SECO_IMX8MP_D18_H

#include <linux/sizes.h>
#include <linux/stringify.h>
#include <asm/arch/imx-regs.h>
#include "seco_common.h"

#define CONFIG_SYS_BOOTM_LEN		(32 * SZ_1M)

#define CONFIG_SPL_MAX_SIZE		(152 * 1024)
#define CONFIG_SYS_MONITOR_LEN		(512 * 1024)
#define CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_USE_SECTOR
#define CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR	0x300
#define CONFIG_SYS_MMCSD_FS_BOOT_PARTITION	1
#define CONFIG_SYS_UBOOT_BASE	(QSPI0_AMBA_BASE + CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR * 512)

#ifdef CONFIG_SPL_BUILD
#define CONFIG_SPL_STACK		0x96dff0
#define CONFIG_SPL_BSS_START_ADDR      0x96e000
#define CONFIG_SPL_BSS_MAX_SIZE		SZ_8K	/* 8 KB */
#define CONFIG_SYS_SPL_MALLOC_START	0x42200000
#define CONFIG_SYS_SPL_MALLOC_SIZE	SZ_512K	/* 512 KB */

/* For RAW image gives a error info not panic */
#define CONFIG_SPL_ABORT_ON_RAW_IMAGE

#if defined(CONFIG_NAND_BOOT)
#define CONFIG_SPL_NAND_SUPPORT
#define CONFIG_SPL_DMA
#define CONFIG_SPL_NAND_MXS
#define CONFIG_SPL_NAND_BASE
#define CONFIG_SPL_NAND_IDENT
#define CONFIG_SYS_NAND_U_BOOT_OFFS 	0x4000000 /* Put the FIT out of first 64MB boot area */

/* Set a redundant offset in nand FIT mtdpart. The new uuu will burn full boot image (not only FIT part) to the mtdpart, so we check both two offsets */
#define CONFIG_SYS_NAND_U_BOOT_OFFS_REDUND \
	(CONFIG_SYS_NAND_U_BOOT_OFFS + CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR * 512 - 0x8400)
#endif

#endif

#define CONFIG_CMD_READ
#define CONFIG_SERIAL_TAG
#define CONFIG_FASTBOOT_USB_DEV 0

#define CONFIG_REMAKE_ELF
/* ENET Config */
/* ENET1 */

#if defined(CONFIG_CMD_NET)
#define CONFIG_ETHPRIME                 "eth1" /* Set eqos to primary since we use its MDIO */

#define CONFIG_FEC_XCV_TYPE             RGMII
#define CONFIG_FEC_MXC_PHYADDR          2

#define DWC_NET_PHYADDR			1

#define PHY_ANEG_TIMEOUT 20000

#endif

/* Link Definitions */
#define CONFIG_LOADADDR			0x40480000

#define CONFIG_SYS_LOAD_ADDR		CONFIG_LOADADDR

#define CONFIG_SYS_INIT_RAM_ADDR	0x40000000
#define CONFIG_SYS_INIT_RAM_SIZE	0x80000
#define CONFIG_SYS_INIT_SP_OFFSET \
	(CONFIG_SYS_INIT_RAM_SIZE - GENERATED_GBL_DATA_SIZE)
#define CONFIG_SYS_INIT_SP_ADDR \
	(CONFIG_SYS_INIT_RAM_ADDR + CONFIG_SYS_INIT_SP_OFFSET)

#define CONFIG_ENV_SPI_BUS		CONFIG_SF_DEFAULT_BUS
#define CONFIG_ENV_SPI_CS		CONFIG_SF_DEFAULT_CS
#define CONFIG_ENV_SPI_MODE		CONFIG_SF_DEFAULT_MODE
#define CONFIG_ENV_SPI_MAX_HZ		CONFIG_SF_DEFAULT_SPEED

#define CONFIG_MMCROOT			"/dev/mmcblk1p2"  /* USDHC2 */

/* Size of malloc() pool */
#define CONFIG_SYS_MALLOC_LEN		SZ_32M

/* Totally 6GB DDR */
#define CONFIG_SYS_SDRAM_BASE		0x40000000
#define PHYS_SDRAM			0x40000000
#define PHYS_SDRAM_SIZE			0xC0000000	/* 3 GB */
#define PHYS_SDRAM_2			0x100000000
#define PHYS_SDRAM_2_SIZE		0xC0000000	/* 3 GB */

#define PHYS_DRAM_IS_1GB                0x40000000
#define PHYS_DRAM_IS_2GB                0x80000000
#define PHYS_DRAM_IS_3GB                0xc0000000
#define PHYS_DRAM_IS_4GB                0x100000000
#define PHYS_DRAM_IS_5GB                0x140000000

#define CONFIG_MXC_UART_BASE		UART2_BASE_ADDR

/* Monitor Command Prompt */
#define CONFIG_SYS_CBSIZE		2048
#define CONFIG_SYS_MAXARGS		64
#define CONFIG_SYS_BARGSIZE CONFIG_SYS_CBSIZE
#define CONFIG_SYS_PBSIZE		(CONFIG_SYS_CBSIZE + \
					sizeof(CONFIG_SYS_PROMPT) + 16)

#define CONFIG_IMX_BOOTAUX
#define CONFIG_FSL_USDHC

#define CONFIG_SYS_FSL_USDHC_NUM	2
#define CONFIG_SYS_FSL_ESDHC_ADDR	0

#define CONFIG_SYS_MMC_IMG_LOAD_PART	1

#ifdef CONFIG_NAND_MXS
#define CONFIG_CMD_NAND_TRIMFFS

/* NAND stuff */
#define CONFIG_SYS_MAX_NAND_DEVICE     1
#define CONFIG_SYS_NAND_BASE           0x20000000
#define CONFIG_SYS_NAND_5_ADDR_CYCLE
#define CONFIG_SYS_NAND_ONFI_DETECTION
#define CONFIG_SYS_NAND_USE_FLASH_BBT
#endif /* CONFIG_NAND_MXS */

#define CONFIG_SYS_I2C_SPEED		100000

/* USB configs */
#ifndef CONFIG_SPL_BUILD

#define CONFIG_CMD_USB_MASS_STORAGE
#define CONFIG_USB_GADGET_MASS_STORAGE
#define CONFIG_USB_FUNCTION_MASS_STORAGE
#endif

#define CONFIG_USB_MAX_CONTROLLER_COUNT         2
#define CONFIG_USBD_HS
#define CONFIG_USB_GADGET_VBUS_DRAW 2

#ifdef CONFIG_DM_VIDEO
#define SPLASHIMAGE_ADDR    0x50000000
#define CONFIG_VIDEO_LOGO
#define CONFIG_BMP_16BPP
#define CONFIG_BMP_24BPP
#define CONFIG_BMP_32BPP
#define CONFIG_VIDEO_BMP_RLE8
#define CONFIG_VIDEO_BMP_LOGO
#endif

#ifdef CONFIG_ANDROID_SUPPORT
#include "imx8mp_evk_android.h"
#endif

/* ____________________________________________________________________________
  |                                                                            |
  |                                 ENVIRONMENT                                |
  |____________________________________________________________________________|
*/

#define ENV_MMCAUTODETECT                        "yes"
#define ENV_FDTAUTODETECT                        "yes"
#define ENV_MEMAUTODETECT                        "yes"


#define SCFG_DEFAULT_FDT_IMX8_FILE               "seco-imx8mp-d18.dtb"
#define ENV_DEFAULT_FDT_FILE                     "seco-imx8mp-d18.dtb"

#define ENV_CONSOLE_DEV                          "ttymxc1"

#define SECO_NUM_BOOT_DEV                        2  // eMMC, uSD

/* boot device id  */
#define BOARD_BOOT_ID_EMMC                       0
#define BOARD_BOOT_ID_SD_EXT                     1

#define BOARD_ROOT_ID_EMMC                       0
#define BOARD_ROOT_ID_SD_EXT                     1


#define SCFG_BOOT_DEV_ID_EMMC                    __stringify(BOARD_BOOT_ID_EMMC)"\0"
#define SCFG_BOOT_DEV_ID_U_SD                    "0"
#define SCFG_BOOT_DEV_ID_SD_EXT                  __stringify(BOARD_BOOT_ID_SD_EXT)"\0"
#define SCFG_BOOT_DEV_ID_SPI                     "0"
#define SCFG_BOOT_DEV_ID_SATA                    "0"
#define SCFG_BOOT_DEV_ID_USB                     "0"

#define SCFG_ROOT_DEV_ID_EMMC                    __stringify(BOARD_ROOT_ID_EMMC)"\0"
#define SCFG_ROOT_DEV_ID_U_SD                    "0"
#define SCFG_ROOT_DEV_ID_SD_EXT                  __stringify(BOARD_ROOT_ID_SD_EXT)"\0"
#define SCFG_ROOT_DEV_ID_SATA                    "0"
#define SCFG_ROOT_DEV_ID_USB                     "0"


#define SCFG_SET_VIDEOMODE                       1 /* if 0 not video setting in seco_config */

/* Defaults devices and partitions sources */

/*  boot file partition location  */
#define ENV_SYS_MMC_ENV_DEV                      0
#define ENV_SYS_MMC_KERNEL_PART                  1
#define ENV_SYS_MMC_FDT_PART                     1
#define ENV_SYS_MMC_RAMFS_PART                   1
/*  boot additional file partition location  */
#define ENV_SYS_MMC_BOOSCRIPT_PART               1
#define ENV_SYS_MMC_BOOATENV_PART                1
/*  rootfs file  */
#define ENV_SYS_MMC_ROOTFS_PART                  2
#define ENV_ROOT_DEV_ID                          1

#define ENV_NUM_VIDEO_OUTPUT                     1   /* configurable video output  */
#define ENV_NUM_TOUCH                            1

/* SECO COMMON ENVIRONMENT FOR SECO_CONFIG */

#define SCFG_KERNEL_LOADADDR               CONFIG_LOADADDR
#define SCFG_KERNEL_FILENAME               "Image"
#define SCFG_RAMFS_FILENAME                "ramfs.img"

#define ENV_KERNEL_LOADADDR                SCFG_KERNEL_LOADADDR

#define ENV_FDT_LOADADDR                   0x42480000
#define ENV_FDT_OVERLAY_BASEADDR           0x43480000
#define ENV_RAMFS_LOADADDR                 0x45480000
#define ENV_BOOTSCRIPT_LOADADDR            0x47480000
#define ENV_FDT_OVERLAY_BASEADDR_OFFSET    0x01000000
#define ENV_FDT_RESIZE                         0x4000


#define LOAD_ADDR_KERNEL_LOCAL_DEV         __stringify(CONFIG_LOADADDR)"\0"
#define LOAD_ADDR_KERNEL_REMOTE_DEV        __stringify(CONFIG_LOADADDR)"\0"

#define LOAD_ADDR_FDT_LOCAL_DEV            __stringify(ENV_FDT_LOADADDR)"\0"
#define LOAD_ADDR_FDT_REMOTE_DEV           __stringify(ENV_FDT_LOADADDR)"\0"

#define LOAD_ADDR_FDT_OVERLAY_LOCAL_DEV    __stringify(ENV_FDT_OVERLAY_BASEADDR)"\0"
#define LOAD_ADDR_FDT_OVERLAY_REMOTE_DEV   __stringify(ENV_FDT_OVERLAY_BASEADDR)"\0"

#define LOAD_ADDR_RAMFS_LOCAL_DEV          __stringify(ENV_RAMFS_LOADADDR)"\0"
#define LOAD_ADDR_RAMFS_REMOTE_DEV         __stringify(ENV_RAMFS_LOADADDR)"\0"


#define ENV_BOOT_TYPE                      booti

#define SECO_EEPROM_I2C_BUS                    0x04
#define SECO_EEPROM_I2C_ADDR                   0x50

#ifdef CONFIG_OF_LIBFDT_OVERLAY
#define ENV_DEVICE_CONFIG \
	"fdt_overlay_func=panel touch\0" \
	"fdt_overlay_conf=seco_config devconf default\0" \
	"fdt_overlay_apply_all="\
	    "if test -n \"${devconf_panel_id}${devconf_touch_id}\";"   \
	        "then; run fdt_find_seco_devconf; fi; "     \
	    "for ff in $fdt_overlay_func;do;"               \
	        "run fdt_overlay_inner;"                     \
	    "done;\0" \
	"fdt_find_seco_devconf=fdt get size cnt / ;" \
	    "env set i 0; while test $i -lt $cnt;do;"   \
	        "fdt get name n / $i;"                     \
	        "fdt get value c /${n} compatible;"\
	        "if test \"$c\" = \"seco,devconf\";then "\
	            "env set seco_devconf_node /$n;"     \
	            "exit 0;"                                          \
	        "fi;"                                                  \
	        "setexpr i $i + 1;"                            \
	    "done\0"                                                   \
	"fdt_overlay_inner= for cc in $fdt_overlay_conf;do;" \
	    "env set fe \"fdt_overlay_${cc}_${ff}\";" \
	    "if env exists ${fe}; then;" \
	        "if run ${fe}; then; exit; fi;" \
	    "fi; done;\0" \
	"fdt_overlay_apply_one= if test -n ${overlay}; then;  "\
	    "if load mmc ${fdt_device_id}:${fdt_partition} ${fdt_overlay_base_addr} ${overlay}; then; " \
	        "if fdt apply ${fdt_overlay_base_addr}; then true; else false; fi;" \
	        "else false; fi; " \
	    "else false; fi;\0" \
	"fdt_overlay_apply_by_id= "\
	    "if fdt get value overlay ${seco_devconf_node}/overlays \"${overlay_type}-id-${overlay_id}\"; then; " \
	        "echo \"Overlay for ${overlay_type} id ${overlay_id}: ${overlay}\"; run fdt_overlay_apply_one; " \
	    "else echo \"No overlay for ${overlay_type} id ${overlay_id} found\"; false; fi;\0" \
	"fdt_overlay_seco_config_panel= if test -n \"${fdt_overlay_video_list}\";then run fdt_overlay_video_cmd; else; false; fi;\0" \
	"fdt_overlay_seco_config_touch= if test -n \"${fdt_overlay_touch_list}\";then run fdt_overlay_touch_cmd; else; false; fi;\0" \
	"fdt_overlay_default_panel= if test -n \"${fdt_overlay_panel_file}\";then env set overlay ${fdt_overlay_panel_file};run fdt_overlay_apply_one; fi;\0" \
	"fdt_overlay_default_touch= if test -n \"${fdt_overlay_touch_file}\";then env set overlay ${fdt_overlay_touch_file};run fdt_overlay_apply_one; fi;\0" \
	"fdt_overlay_devconf_panel= if test -n \"${devconf_panel_id}\";then env set overlay_type panel; env set overlay_id ${devconf_panel_id};run fdt_overlay_apply_by_id;else false; fi;\0" \
	"fdt_overlay_devconf_touch= if test -n \"${devconf_touch_id}\";then env set overlay_type touch; env set overlay_id ${devconf_touch_id};run fdt_overlay_apply_by_id;else false; fi;\0" \
	"fdt_overlay_panel_file=\0" \
	"fdt_overlay_touch_file=\0" \

#endif

#include "seco_mx8_env.h"


#ifdef CONFIG_OF_LIBFDT_OVERLAY
#undef MACRO_ENV_FDT_LOAD_OVERLAY_BASE
/* Here we overwrite the fdt_overlay_load2ram command, to take the
   seco_device_config (from eeprom) into account.
   As this is currently only used for D18-MV this is implemented in the D18
   header for now.
   The fdt_overlay_load2ram is called in the default boot_cmd and also in the
   seco_boot.scr used to boot clea-os-things.
*/

#define MACRO_ENV_FDT_LOAD_OVERLAY_BASE \
	fdt addr ${fdt_loadaddr}; fdt resize ${fdt_resize}; \
	if test -n "${fdt_overlay_carrier_file}"; then run fdt_overlay_carrier_cmd; fi; \
	seco_config fdtoverlay ; \
	seco_eeprom_manager env SECO_EEPROM_I2C_BUS SECO_EEPROM_I2C_ADDR; \
	run fdt_overlay_apply_all; \
	if test -n "${fdt_overlay_per_cmd}"; then run fdt_overlay_per_cmd; fi; \


#endif

#endif
