#ifndef __SECO_MX6_DTBO_H
#define __SECO_MX6_DTBO_H

#define ENV_DTBO_A62_HDMI                     seco-imx6qdl-a62-video-hdmi.dtbo
#define ENV_DTBO_A62_LVDS                     seco-imx6qdl-a62-video-lvds.dtbo
#define ENV_DTBO_A62_LDB_CLONE                seco-imx6qdl-a62-video-lvdsx2-clone.dtbo
#define ENV_DTBO_A62_LDB_DUAL                 seco-imx6qdl-a62-video-lvdsx2-dual.dtbo
#define ENV_DTBO_A62_HDMI_LVDS                seco-imx6qdl-a62-video-hdmi-lvds.dtbo
#define ENV_DTBO_A62_LVDS_HDMI                seco-imx6qdl-a62-video-lvds-hdmi.dtbo
#define ENV_DTBO_A62_TOUCH_ST1232             seco-imx6qdl-a62-touch-st1232.dtbo
#define ENV_DTBO_A62_TOUCH_GT928              seco-imx6qdl-a62-touch-gt928.dtbo
#define ENV_DTBO_A62_CONN_J8                  seco-imx6qdl-a62-conn_j8.dtbo

#define ENV_DTBO_928_HDMI                     seco-imx6qdl-928-video-hdmi.dtbo
#define ENV_DTBO_928_LVDS                     seco-imx6qdl-928-video-lvds.dtbo
#define ENV_DTBO_928_LDB_CLONE                seco-imx6qdl-928-video-lvdsx2-clone.dtbo
#define ENV_DTBO_928_LDB_DUAL                 seco-imx6qdl-928-video-lvdsx2-dual.dtbo
#define ENV_DTBO_928_HDMI_LVDS                seco-imx6qdl-928-video-hdmi-lvds.dtbo
#define ENV_DTBO_928_LVDS_HDMI                seco-imx6qdl-928-video-lvds-hdmi.dtbo
#define ENV_DTBO_928_UART4                    seco-imx6qdl-928-uart4.dtbo
#define ENV_DTBO_928_AC97                     seco-imx6qdl-928-ac97.dtbo
#define ENV_DTBO_928_CPLD_MAXLINEAR           seco-imx6qdl-928-cpld-maxlinear.dtbo
#define ENV_DTBO_928_CPLD_NUVOTON             seco-imx6qdl-928-cpld-nuvoton.dtbo
#define ENV_DTBO_928_CPLD_GPIO_PWM            seco-imx6qdl-928-cpld-gpio-pwm.dtbo

#define ENV_DTBO_962_HDMI                     seco-imx6qdl-962-video-hdmi.dtbo
#define ENV_DTBO_962_LVDS                     seco-imx6qdl-962-video-lvds.dtbo
#define ENV_DTBO_962_LDB_CLONE                seco-imx6qdl-962-video-lvdsx2-clone.dtbo
#define ENV_DTBO_962_LDB_DUAL                 seco-imx6qdl-962-video-lvdsx2-dual.dtbo
#define ENV_DTBO_962_HDMI_LVDS                seco-imx6qdl-962-video-hdmi-lvds.dtbo
#define ENV_DTBO_962_LVDS_HDMI                seco-imx6qdl-962-video-lvds-hdmi.dtbo
#define ENV_DTBO_962_UART4                    seco-imx6qdl-962-uart4.dtbo
#define ENV_DTBO_962_AC97                     seco-imx6qdl-962-ac97.dtbo

#define ENV_DTBO_A75_HDMI                     seco-imx6qdl-a75-video-hdmi.dtbo
#define ENV_DTBO_A75_LVDS                     seco-imx6qdl-a75-video-lvds.dtbo
#define ENV_DTBO_A75_LDB_CLONE                seco-imx6qdl-a75-video-lvdsx2-clone.dtbo
#define ENV_DTBO_A75_LDB_DUAL                 seco-imx6qdl-a75-video-lvdsx2-dual.dtbo
#define ENV_DTBO_A75_HDMI_LVDS                seco-imx6qdl-a75-video-hdmi-lvds.dtbo
#define ENV_DTBO_A75_LVDS_HDMI                seco-imx6qdl-a75-video-lvds-hdmi.dtbo
#define ENV_DTBO_A75_UART1                    seco-imx6qdl-a75-uart1.dtbo
#define ENV_DTBO_A75_AC97                     seco-imx6qdl-a75-ac97.dtbo

#define ENV_DTBO_SANTINO_RGB                  seco-imx6dl-santino-video-rgb-touch-ad7879.dtbo

#endif   /* __SECO_MX6_DTBO_H  */
