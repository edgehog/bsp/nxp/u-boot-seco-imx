#ifndef __SECO_MX6_962_CONFIG_H
#define __SECO_MX6_962_CONFIG_H

#include "seco_mx6_common.h"

/* ____________________________________________________________________________
  |                                                                            |
  |                                     UART                                   |
  |____________________________________________________________________________|
*/
#define CONFIG_MXC_UART_BASE                     UART2_BASE
#define ENV_CONSOLE_DEV                          "ttymxc1"


/* ____________________________________________________________________________
  |                                                                            |
  |                                     USDHC                                  |
  |____________________________________________________________________________|
*/
#define CONFIG_SYS_FSL_USDHC_NUM                 2
#define CONFIG_SYS_MMC_ENV_DEV                   0                 /*USDHC2*/
#define CONFIG_SYS_MMC_ENV_PART                  0	               /* user area */
#define CONFIG_MMCROOT	                         "/dev/mmcblk0p2"  /* USDHC2 */

#define CONFIG_SYS_FSL_ESDHC_ADDR                0 //USDHC2_BASE_ADDR



/* ____________________________________________________________________________
  |                                                                            |
  |                                       USB                                  |
  |____________________________________________________________________________|
*/
#define CONFIG_MXC_USB_PORTSC                    (PORT_PTS_UTMI | PORT_PTS_PTW)
#define CONFIG_MXC_USB_FLAGS                     0
#define CONFIG_USB_MAX_CONTROLLER_COUNT          7



/* ____________________________________________________________________________
  |                                                                            |
  |                                   DISPLAY                                  |
  |____________________________________________________________________________|
*/
#ifdef CONFIG_VIDEO
#define SECO_COMMON_DISPLAY
#define SECO_COMMON_HDMI
#define SECO_COMMON_LVDS
#undef  SECO_COMMON_RGB
#endif


/* ____________________________________________________________________________
  |                                                                            |
  |                                 ENVIRONMENT                                |
  |____________________________________________________________________________|
*/

#define ENV_MMCAUTODETECT                        "yes"
#define ENV_FDTAUTODETECT                        "yes"
#define ENV_MEMAUTODETECT                        "yes"


#define SCFG_DEFAULT_FDT_IMX6Q_FILENAME          "seco-imx6q-962.dtb"
#define SCFG_DEFAULT_FDT_IMX6DL_FILENAME         "seco-imx6dl-962.dtb"
#define ENV_DEFAULT_FDT_FILE                     "seco-imx6dl-962.dtb"

#define SECO_NUM_BOOT_DEV                        3  // eMMC, uSD, SPI

/*  SCFG = SECO CONFIG  */
#define BOARD_BOOT_ID_EMMC                       0
#define BOARD_BOOT_ID_SD_EXT                     1

#define BOARD_ROOT_ID_EMMC                       0
#define BOARD_ROOT_ID_SD_EXT                     1

#define SCFG_KERNEL_BOOT_DEV_IMX6Q_NUM           6
#define SCFG_KERNEL_BOOT_DEV_IMX6DL_NUM          5

#define SCFG_FDT_BOOT_DEV_IMX6Q_NUM              6
#define SCFG_FDT_BOOT_DEV_IMX6DL_NUM             5

#define SCFG_RAMFS_BOOT_DEV_IMX6Q_NUM            7
#define SCFG_RAMFS_BOOT_DEV_IMX6DL_NUM           6

#define SCFG_ROOTFS_BOOT_DEV_IMX6Q_NUM           5
#define SCFG_ROOTFS_BOOT_DEV_IMX6DL_NUM          4



#define SCFG_BOOT_DEV_ID_EMMC                    __stringify(BOARD_BOOT_ID_EMMC)"\0"
#define SCFG_BOOT_DEV_ID_SD_EXT                  __stringify(BOARD_BOOT_ID_SD_EXT)"\0"
#define SCFG_BOOT_DEV_ID_SPI                     "0"
#define SCFG_BOOT_DEV_ID_SATA                    "0"
#define SCFG_BOOT_DEV_ID_USB                     "0"

#define SCFG_ROOT_DEV_ID_EMMC                    __stringify(BOARD_ROOT_ID_EMMC)"\0"
#define SCFG_ROOT_DEV_ID_SD_EXT                  __stringify(BOARD_ROOT_ID_SD_EXT)"\0"
#define SCFG_ROOT_DEV_ID_SATA                    "0"
#define SCFG_ROOT_DEV_ID_USB                     "0"


#define SCFG_SET_VIDEOMODE                       1
#define SCFG_VIDEO_MODE_NUM                      7

#ifdef CONFIG_OF_LIBFDT_OVERLAY
#define SCFG_FDT_OVERLAY_PERIPHERAL_NUM          3
#endif  /* CONFIG_OF_LIBFDT_OVERLAY */


/*  boot file  */
#define ENV_SYS_MMC_ENV_DEV                      0
#define ENV_SYS_MMC_KERNEL_PART                  1
#define ENV_SYS_MMC_FDT_PART                     1
#define ENV_SYS_MMC_RAMFS_PART                   1
/*  boot additional file  */
#define ENV_SYS_MMC_BOOSCRIPT_PART               1
#define ENV_SYS_MMC_BOOATENV_PART                1
/*  rootfs file  */
#define ENV_SYS_MMC_ROOTFS_PART                  2

#define ENV_NUM_VIDEO_OUTPUT                     3   /* lvds0, lvds1, hdmi */

#ifdef CONFIG_SECO_ENV_MANAGER

#include "seco_mx6_env.h"

#endif  /* CONFIG_SECO_ENV_MANAGE  */


#endif    /*  __SECO_MX6_962_CONFIG_H  */
