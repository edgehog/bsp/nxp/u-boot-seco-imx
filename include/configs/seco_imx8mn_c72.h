/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Copyright 2018 NXP
 */

#ifndef __IMX8MN_C72_H
#define __IMX8MN_C72_H

#include <linux/sizes.h>
#include <asm/arch/imx-regs.h>

#include "imx_env.h"

/* ____________________________________________________________________________
  |                                                                            |
  |                                   WATCHDOG                                 |
  |____________________________________________________________________________|
 */

#define APX_WDT_TRIGGER_BASE                     GPIO4_BASE_ADDR
#define APX_WDT_TRIGGER_NUM                      22

#define APX_WDT_ENABLE_BASE                      GPIO4_BASE_ADDR
#define APX_WDT_ENABLE_NUM                       21

#define APX_WDT_EN_LOW				1

#define CONFIG_SPL_MAX_SIZE		(208 * 1024)
#define CONFIG_SYS_MONITOR_LEN		(512 * 1024)
#define CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_USE_SECTOR
#define CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR	0x300
#define CONFIG_SYS_MMCSD_FS_BOOT_PARTITION	1
#define CONFIG_SYS_UBOOT_BASE	\
	(QSPI0_AMBA_BASE + CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR * 512)

#ifdef CONFIG_SPL_BUILD
#define CONFIG_SPL_STACK		0x187FF0
#define CONFIG_SPL_BSS_START_ADDR      0x0095e000
#define CONFIG_SPL_BSS_MAX_SIZE		SZ_8K	/* 8 KB */
#define CONFIG_SYS_SPL_MALLOC_START	0x42200000
#define CONFIG_SYS_SPL_MALLOC_SIZE	SZ_64K	/* 64 KB */

/* malloc f used before GD_FLG_FULL_MALLOC_INIT set */
#define CONFIG_MALLOC_F_ADDR		0x184000

/* For RAW image gives a error info not panic */
#define CONFIG_SPL_ABORT_ON_RAW_IMAGE


#define CONFIG_POWER
#define CONFIG_POWER_I2C
#ifdef CONFIG_IMX8M_DDR4
#define CONFIG_POWER_BD71837
#else
#define CONFIG_POWER_PCA9450
#endif

#define CONFIG_SYS_I2C

#if defined(CONFIG_NAND_BOOT)
#define CONFIG_SPL_NAND_SUPPORT
#define CONFIG_SPL_DMA
#define CONFIG_SPL_NAND_MXS
#define CONFIG_SPL_NAND_BASE
#define CONFIG_SPL_NAND_IDENT
#define CONFIG_SYS_NAND_U_BOOT_OFFS 	0x4000000 /* Put the FIT out of first 64MB boot area */

/* Set a redundant offset in nand FIT mtdpart. The new uuu will burn full boot image (not only FIT part) to the mtdpart, so we check both two offsets */
#define CONFIG_SYS_NAND_U_BOOT_OFFS_REDUND \
	(CONFIG_SYS_NAND_U_BOOT_OFFS + CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR * 512 - 0x8400)
#endif

#endif

#define CONFIG_CMD_READ
#define CONFIG_SERIAL_TAG
#define CONFIG_FASTBOOT_USB_DEV 0

#define CONFIG_REMAKE_ELF


/* eMMC uSD Configuration */
#define BOOT_ID_EMMC 0
#define ROOT_ID_EMMC 0
#define BOOT_ID_USD  1
#define ROOT_ID_USD  1

/* ENET Config */
/* ENET1 */
#if defined(CONFIG_FEC_MXC)
#define CONFIG_ETHPRIME                 "FEC"
#define PHY_ANEG_TIMEOUT 		20000

#define CONFIG_FEC_XCV_TYPE             RGMII
#define CONFIG_FEC_MXC_PHYADDR          9
#define FEC_QUIRK_ENET_MAC

#define IMX_FEC_BASE			0x30BE0000
#endif

#ifdef CONFIG_NAND_BOOT
#define MFG_NAND_PARTITION "mtdparts=gpmi-nand:64m(nandboot),16m(nandfit),32m(nandkernel),16m(nanddtb),8m(nandtee),-(nandrootfs)"
#endif


/* Link Definitions */
#define CONFIG_LOADADDR			0x40480000

#define CONFIG_SYS_LOAD_ADDR           CONFIG_LOADADDR

#define CONFIG_SYS_INIT_RAM_ADDR        0x40000000
#define CONFIG_SYS_INIT_RAM_SIZE        0x80000
#define CONFIG_SYS_INIT_SP_OFFSET \
        (CONFIG_SYS_INIT_RAM_SIZE - GENERATED_GBL_DATA_SIZE)
#define CONFIG_SYS_INIT_SP_ADDR \
        (CONFIG_SYS_INIT_RAM_ADDR + CONFIG_SYS_INIT_SP_OFFSET)

#define CONFIG_ENV_OVERWRITE
#if defined(CONFIG_ENV_IS_IN_SPI_FLASH)
#define CONFIG_ENV_SPI_BUS		CONFIG_SF_DEFAULT_BUS
#define CONFIG_ENV_SPI_CS		CONFIG_SF_DEFAULT_CS
#define CONFIG_ENV_SPI_MODE		CONFIG_SF_DEFAULT_MODE
#define CONFIG_ENV_SPI_MAX_HZ		CONFIG_SF_DEFAULT_SPEED
#endif

#define CONFIG_SYS_MMC_ENV_DEV		1   /* USDHC2 */
#define CONFIG_MMCROOT			"/dev/mmcblk1p2"  /* USDHC2 */

/* Size of malloc() pool */
#define CONFIG_SYS_MALLOC_LEN		SZ_32M

#define CONFIG_SYS_SDRAM_BASE           0x40000000
#define PHYS_SDRAM                      0x40000000
#define PHYS_SDRAM_SIZE			PHYS_DRAM_IS_3GB /* 2GB DDR */
#define PHYS_DRAM_IS_512MB              0x20000000
#define PHYS_DRAM_IS_1GB                0x40000000
#define PHYS_DRAM_IS_2GB                0x80000000
#define PHYS_DRAM_IS_3GB                0xc0000000
#define PHYS_DRAM_IS_4GB                0x100000000
#define PHYS_SDRAM_2                    0x100000000
#define PHYS_SDRAM_2_SIZE               0x40000000 /* 1GB */

#define CONFIG_SYS_MEMTEST_START	0x60000000
#define CONFIG_SYS_MEMTEST_END      	(CONFIG_SYS_MEMTEST_START + (PHYS_SDRAM_SIZE >> 2)	)

#define CONFIG_MXC_UART_BASE		UART2_BASE_ADDR

/* Monitor Command Prompt */
#undef CONFIG_SYS_PROMPT
#define CONFIG_SYS_PROMPT               "u-boot seco c72=> "
#define CONFIG_SYS_PROMPT_HUSH_PS2     "> "
#define CONFIG_SYS_CBSIZE              2048
#define CONFIG_SYS_MAXARGS             64
#define CONFIG_SYS_BARGSIZE CONFIG_SYS_CBSIZE
#define CONFIG_SYS_PBSIZE		(CONFIG_SYS_CBSIZE + \
					sizeof(CONFIG_SYS_PROMPT) + 16)

#define CONSOLE_DEV			"ttymxc1"
#define DEFAULT_FDT_FILE	 	"seco-imx8mn-c72.dtb"

#define CONFIG_IMX_BOOTAUX

/* USDHC */
#define CONFIG_FSL_USDHC

#define CONFIG_SYS_FSL_USDHC_NUM	2
#define CONFIG_SYS_FSL_ESDHC_ADDR       0

#define CONFIG_SYS_MMC_IMG_LOAD_PART	1

#ifdef CONFIG_FSL_FSPI
#define FSL_FSPI_FLASH_SIZE		SZ_32M
#define FSL_FSPI_FLASH_NUM		1
#define FSPI0_BASE_ADDR			0x30bb0000
#define FSPI0_AMBA_BASE			0x0
#define CONFIG_FSPI_QUAD_SUPPORT

#define CONFIG_SYS_FSL_FSPI_AHB
#endif

#ifdef CONFIG_NAND_MXS
#define CONFIG_CMD_NAND_TRIMFFS

/* NAND stuff */
#define CONFIG_SYS_MAX_NAND_DEVICE     1
#define CONFIG_SYS_NAND_BASE           0x20000000
#define CONFIG_SYS_NAND_5_ADDR_CYCLE
#define CONFIG_SYS_NAND_ONFI_DETECTION
#define CONFIG_SYS_NAND_USE_FLASH_BBT
#endif /* CONFIG_NAND_MXS */

#define CONFIG_SYS_I2C_SPEED		100000

/* USB configs */
#ifndef CONFIG_SPL_BUILD
#define CONFIG_CMD_USB
#define CONFIG_USB_STORAGE
#define CONFIG_USBD_HS

#define CONFIG_CMD_USB_MASS_STORAGE
#define CONFIG_USB_GADGET_MASS_STORAGE
#define CONFIG_USB_FUNCTION_MASS_STORAGE

#endif

#define CONFIG_USB_GADGET_VBUS_DRAW 2

#define CONFIG_MXC_USB_PORTSC  (PORT_PTS_UTMI | PORT_PTS_PTW)
#define CONFIG_USB_MAX_CONTROLLER_COUNT         1

#if defined(CONFIG_ANDROID_SUPPORT)
#include "imx8mn_evk_android.h"
#endif
#endif

/* ____________________________________________________________________________
  |                                                                            |
  |                                 ENVIRONMENT                                |
  |____________________________________________________________________________|
*/

#define ENV_MMCAUTODETECT                        "no"
#define ENV_FDTAUTODETECT                        "no"
#define ENV_MEMAUTODETECT                        "no"


#define SCFG_KERNEL_LOADADDR                               CONFIG_LOADADDR
#define SCFG_KERNEL_FILENAME                               "Image"
#define SCFG_RAMFS_FILENAME                                "ramfs.img"

#define ENV_KERNEL_LOADADDR                                SCFG_KERNEL_LOADADDR

#define SCFG_DEFAULT_FDT_IMX8_FILE              "seco-imx8mn-c72.dtb"
#define ENV_DEFAULT_FDT_FILE                    "seco-imx8mn-c72.dtb"

#define ENV_CONSOLE_DEV                         "ttymxc1"

#define SECO_NUM_BOOT_DEV                        2  // eMMC, uSD

/* boot device id  */
#define BOARD_BOOT_ID_EMMC                       0
#define BOARD_BOOT_ID_EXT_SD                     1

#define BOARD_ROOT_ID_EMMC                       0
#define BOARD_ROOT_ID_EXT_SD                     1


#define SCFG_BOOT_DEV_ID_EMMC                    __stringify(BOARD_BOOT_ID_EMMC)"\0"
#define SCFG_BOOT_DEV_ID_U_SD                    "0"
#define SCFG_BOOT_DEV_ID_EXT_SD                  __stringify(BOARD_BOOT_ID_EXT_SD)"\0"
#define SCFG_BOOT_DEV_ID_SPI                     "0"
#define SCFG_BOOT_DEV_ID_SATA                    "0"
#define SCFG_BOOT_DEV_ID_USB                     "0"

#define SCFG_ROOT_DEV_ID_EMMC                    __stringify(BOARD_ROOT_ID_EMMC)"\0"
#define SCFG_ROOT_DEV_ID_U_SD                    "0"
#define SCFG_ROOT_DEV_ID_EXT_SD                  __stringify(BOARD_ROOT_ID_EXT_SD)"\0"
#define SCFG_ROOT_DEV_ID_SATA                    "0"
#define SCFG_ROOT_DEV_ID_USB                     "0"


#define SCFG_SET_VIDEOMODE                       1 /* if 0 not video setting in seco_config */

/* Defaults devices and partitions sources */

/*  boot file partition location  */
#define ENV_SYS_MMC_ENV_DEV                      0
#define ENV_SYS_MMC_KERNEL_PART                  1
#define ENV_SYS_MMC_FDT_PART                     1
#define ENV_SYS_MMC_RAMFS_PART                   1
/*  boot additional file partition location  */
#define ENV_SYS_MMC_BOOSCRIPT_PART               1
#define ENV_SYS_MMC_BOOATENV_PART                1
/*  rootfs file  */
#define ENV_SYS_MMC_ROOTFS_PART                  2
#define ENV_ROOT_DEV_ID                          0

#define ENV_NUM_VIDEO_OUTPUT                     1   /* configurable video output  */

/* SECO COMMON ENVIRONMENT FOR SECO_CONFIG */

#define ENV_KERNEL_LOADADDR                CONFIG_LOADADDR
#define ENV_KERNEL_FILENAME                "Image"
#define ENV_RAMFS_FILENAME                 "ramfs.img"

#define ENV_FDT_LOADADDR                   0x42480000
#define ENV_FDT_OVERLAY_BASEADDR           0x43480000
#define ENV_RAMFS_LOADADDR                 0x45480000
#define ENV_BOOTSCRIPT_LOADADDR            0x47480000
#define ENV_FDT_OVERLAY_BASEADDR_OFFSET    0x01000000
#define ENV_FDT_RESIZE                         0x4000


#define LOAD_ADDR_KERNEL_LOCAL_DEV         __stringify(CONFIG_LOADADDR)"\0"
#define LOAD_ADDR_KERNEL_REMOTE_DEV        __stringify(CONFIG_LOADADDR)"\0"

#define LOAD_ADDR_FDT_LOCAL_DEV            __stringify(ENV_FDT_LOADADDR)"\0"
#define LOAD_ADDR_FDT_REMOTE_DEV           __stringify(ENV_FDT_LOADADDR)"\0"

#define LOAD_ADDR_FDT_OVERLAY_LOCAL_DEV    __stringify(ENV_FDT_OVERLAY_BASEADDR)"\0"
#define LOAD_ADDR_FDT_OVERLAY_REMOTE_DEV   __stringify(ENV_FDT_OVERLAY_BASEADDR)"\0"

#define LOAD_ADDR_RAMFS_LOCAL_DEV          __stringify(ENV_RAMFS_LOADADDR)"\0"
#define LOAD_ADDR_RAMFS_REMOTE_DEV         __stringify(ENV_RAMFS_LOADADDR)"\0"


#define ENV_BOOT_TYPE                      booti

#include "seco_mx8_env.h"

