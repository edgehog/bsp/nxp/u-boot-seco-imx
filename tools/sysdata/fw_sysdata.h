
#ifndef _FW_SYSTEMDATA_H_
#define _FW_SYSTEMDATA_H_

#ifdef MTD_OLD
# include <stdint.h>
# include <linux/mtd/mtd.h>
#else
# define  __user	/* nothing */
# include <mtd/mtd-user.h>
#endif

typedef struct sysdev {
	const char *devname;		/* Device name */
	long long  devoff;		    /* Device offset */
	ulong      env_size;		/* environment size */
	ulong      erase_size;		/* device erase size */
	ulong      env_sectors;		/* number of environment sectors */
	uint8_t    mtd_type;		/* type of the MTD device */
	int        is_ubi;			/* set if we use UBI volume */
} sysdev_t;

#define SZ_2K				0x00000800
#define DIV_ROUND_UP(n, d)	(((n) + (d) - 1) / (d))

#define min(x, y) ({				\
	typeof(x) _min1 = (x);			\
	typeof(y) _min2 = (y);			\
	(void) (&_min1 == &_min2);		\
	_min1 < _min2 ? _min1 : _min2; })

extern int dev_current;

static sysdev_t sysdevice[3] = {
	{
		.devname = "/dev/mmcblk0",
		.devoff  = CONFIG_ENV_OFFSET - SZ_2K,
		.env_size = SYSTEMDATA_SIZE,
		.erase_size = 0,
		.env_sectors = 0,
		.mtd_type = MTD_ABSENT,
		.is_ubi = 0,
	}, {
		.devname = "/dev/mmcblk1",
		.devoff  = CONFIG_ENV_OFFSET - SZ_2K,
		.env_size = SYSTEMDATA_SIZE,
		.erase_size = 0,
		.env_sectors = 0,
		.mtd_type = MTD_ABSENT,
		.is_ubi = 0,
	}, {
		.devname = "/dev/mmcblk2",
		.devoff  = CONFIG_ENV_OFFSET - SZ_2K,
		.env_size = SYSTEMDATA_SIZE,
		.erase_size = 0,
		.env_sectors = 0,
		.mtd_type = MTD_ABSENT,
		.is_ubi = 0,
	},
};


#define DEVNAME(i)     sysdevice[(i)].devname
#define DEVOFFSET(i)   sysdevice[(i)].devoff
#define DATASIZE(i)    sysdevice[(i)].env_size
#define DEVESIZE(i)    sysdevice[(i)].erase_size
#define DATASECTORS(i) sysdevice[(i)].env_sectors
#define DEVTYPE(i)     sysdevice[(i)].mtd_type
#define IS_UBI(i)      sysdevice[(i)].is_ubi

#define CUR_SYSDATASIZE DATASIZE(dev_current)

static int is_device_valid( const char * device_name ) {
	int num = sizeof( sysdevice ) / sizeof ( sysdev_t );
	int device_id = -1;
	int i;
	for ( i = 0 ; i < num ; i++ ) {
		if ( strcmp( device_name, DEVNAME(i) ) == 0 ) {
			device_id = i;
			break;
		}
	}
	return device_id;
}


#endif     /*  _FW_SYSTEMDATA_H_  */
