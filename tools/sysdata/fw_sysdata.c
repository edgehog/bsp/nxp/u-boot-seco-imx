#define _GNU_SOURCE
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/file.h>
#include <unistd.h>
#include <version.h>
#include <stdint.h>

#include <sysdata.h>
#include "fw_sysdata.h"

#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)


/*  _____________________________________________________________________________
 * |                                                                             |
 * |                               ARGUMENT MANAGEMENT                           |
 * |_____________________________________________________________________________|
 */
typedef enum cmd_target {
    CMD_NONE = 0,
    CMD_TABLE,
    CMD_SWITCHSLOT,
} cmd_target_t;

char *device_name = NULL;
static char p_name[254];

typedef enum cmd_table {
    CMD_TABLE_GET_VERSION = 0,
    CMD_TABLE_INITIALIZE,
} cmd_table_t;
typedef enum cmd_switchslot {
    CMD_SWITCHSLOT_NONE = 0,
    CMD_SWITCHSLOT_INIT,
    CMD_SWITCHSLOT_STATUS,
    CMD_SWITCHSLOT_GET_MAX_FAIL_BOOT,
    CMD_SWITCHSLOT_GLOBAL_COUNTER,
    CMD_SWITCHSLOT_FAIL_COUNTER,
    CMD_SWITCHSLOT_SET_PRIMARY,
    CMD_SWITCHSLOT_REMOVE_SLOT,
    CMD_SWITCHSLOT_INVALIDATE_SLOT,
    CMD_SWITCHSLOT_BOOTOK,
    CMD_SWITCHSLOT_GET_CURRENT_SLOT,
    CMD_SWITCHSLOT_GET_PREVIOUS_SLOT,
    CMD_SWITCHSLOT_GET_BOOT_ORDER,
    CMD_SWITCHSLOT_GET_GLOBAL_BOOT_COUNT,
    CMD_SWITCHSLOT_GET_FAIL_BOOT_COUNT,
    CMD_SWITCHSLOT_RESET_FAIL_BOOT_COUNT,
} cmd_switchslot_t;


static cmd_target_t _cmd = CMD_NONE;

static cmd_switchslot_t _cmd_swichpart = CMD_SWITCHSLOT_NONE;
static char c_switchslot = ' ';
static int flag_switchslot = 0;

static cmd_table_t _cmd_table = CMD_TABLE_GET_VERSION;

static struct option common_options[] = {
    {"device", required_argument, NULL, 'd'},
	{"target", required_argument, NULL, 't'},
	{"help", no_argument, NULL, 'h'},
	{"version", no_argument, NULL, 'v'},
    {"initialize", no_argument, NULL, 'i'},
	{NULL, 0, NULL, 0}
};

static struct option switchslot_options[] = {
	{"reset", no_argument, NULL, 'z'},
    {"status", no_argument, NULL, 's'},
    {"get_max_fail_boot", no_argument, NULL, 'm'},
    {"global_counter", required_argument, NULL, 'g'},
    {"fail_counter", required_argument, NULL, 'f'},
    {"set_primary", required_argument, NULL, 'p'},
    {"remove_slot", required_argument, NULL, 'r'},
    {"invalidate_slot", required_argument, NULL, 'k'},
    {"set_boot_ok", no_argument, NULL, 'b'},
    {"get_boot_order", no_argument, NULL, 'o'},
    {"get_current_slot", no_argument, NULL, 'c'},
    {"get_previous_slot", no_argument, NULL, 'l'},
    {"get_global_boot_count", required_argument, NULL, 'u'},
    {"get_fail_boot_count", required_argument, NULL, 'j'},
    {"reset_fail_boot_count", required_argument, NULL, 'q'},
	{NULL, 0, NULL, 0}
};

#define ARRAY_SIZE(x)  (sizeof((x)) / sizeof((x)[0]))

static void print_usage( void ) {
    fprintf( stdout, "==== For operations who do not require access to the memory table:\n" );
    fprintf( stdout, "usage: %s <command>\n", basename( p_name ) );
    fprintf( stdout, "available commands:\n" );
    fprintf( stdout, "   -h | --help                        :  Show this help\n" );
    fprintf( stdout, "   -V | --tool_version                :  Show version of this tool\n" );
    fprintf( stdout, "\n" );
    fprintf( stdout, "==== For operations with access to the memory table:\n" );  
    fprintf( stdout, "usage: %s -d <device> [<command> | [-t | --target] <target> [<subcommand>]]\n", basename( p_name ) );
    fprintf( stdout, "available commands:\n" );
    fprintf( stdout, "   -v | --version                     :  Show SysData table's version.\n" );
    fprintf( stdout, "   -i | --initialize                  :  Initialize the whole SysData memory\n" );  
    fprintf( stdout, "available target (with relative subcommand list):\n" );
    fprintf( stdout, " = switchslot <subcommand>\n" );
    fprintf( stdout, "   -z | --reset                       :  Initialize the only Switchslot part of SysData memory.\n" );
    fprintf( stdout, "   -s | --status                      :  Show the whole data set in readable way.\n" );
    fprintf( stdout, "   -m | --get_max_fail_boot           :  Show maximum number of allowed consecutive fail boot.\n" );
    fprintf( stdout, "   -g | --global_counter <arg>        :  Enable/Disable the global boot counter.\n" );
    fprintf( stdout, "                                         arg = [1:enable | 0:disable]\n" );
    fprintf( stdout, "   -f | --fail_counter <arg>          :  Enable/Disable the fail boot counter.\n" );
    fprintf( stdout, "                                         arg = [1:enable | 0:disable]\n" );
    fprintf( stdout, "   -p | --set_primary <arg>           :  Set a slot as primary slot in boot order list.\n" );
    fprintf( stdout, "                                         arg = slot ID (e.g. A or B)\n" );
    fprintf( stdout, "   -r | --remove_slot <arg>           :  Remove a slot from boot order list.\n" );
    fprintf( stdout, "                                         arg = slot ID (e.g. A or B)\n" );
    fprintf( stdout, "   -k | --invalidate_slot <arg>       :  Invalidate a slot (removes it from boot order list and puts its\n" );
    fprintf( stdout, "                                         fail boot counter over the maximum allowed).\n" );
    fprintf( stdout, "                                         arg = slot ID (e.g. A or B)\n" );
    fprintf( stdout, "   -b | --set_boot_ok                 :  Set current boot as ok (reset of fail boot counter for current slot).\n" );
    fprintf( stdout, "   -o | --get_bootorder               :  Show in readable way the boot order list.\n" );
    fprintf( stdout, "   -c | --get_current_slot            :  Show the ID of the current used slot.\n" );
    fprintf( stdout, "   -u | --get_global_boot_count <arg> :  Get the number of global boot counter for passed slot.\n" );
    fprintf( stdout, "                                         arg = slot ID (e.g. A or B)\n" );
    fprintf( stdout, "   -j | --get_fail_boot_count <arg>   :  Get the number of global fail counter for passed slot.\n" );
    fprintf( stdout, "                                         arg = slot ID (e.g. A or B)\n" );
    fprintf( stdout, "   -q | --reset_fail_boot_count <arg> :  Reset the fail boot counter of a given slot.\n" );
    fprintf( stdout, "                                         arg = slot ID (e.g. A or B)\n" );
    fprintf( stdout, "\n" );
}


static void parse_common_args( int args, char *argv[] ) {
    int c;

    _cmd = CMD_SWITCHSLOT_NONE;
    while ( ( c = getopt_long( args, argv, ":d:t:hvi", common_options, NULL ) ) != EOF ) {
		switch ( c ) {
            case 'd':
                device_name = malloc( sizeof( char ) * 256 );
                if ( device_name == NULL ) {
                    fprintf( stderr, "Memory allocation error!!!\n" );
                    exit(EXIT_FAILURE);
                }
                strcpy( device_name, optarg );
                break;
            case 't':
                if ( strcmp( optarg, "switchslot") == 0 ) {
                    _cmd = CMD_SWITCHSLOT;
                } else {
                    fprintf( stderr, "Error: invalid command type!!!\n" );
                    print_usage( );
                    exit(EXIT_FAILURE);
                }
                break;
            case 'h':
                print_usage( );
                exit(EXIT_SUCCESS);
                break;
            case 'v':
                _cmd = CMD_TABLE;
                _cmd_table = CMD_TABLE_GET_VERSION;
                break;
            case 'i':
                _cmd = CMD_TABLE;
                _cmd_table = CMD_TABLE_INITIALIZE;
                break;
            default:
                /* ignore unknown options */
                break;
		}
	}

    /* Reset getopt for the next pass. */
	opterr = 1;
	optind = 1;
}


static void parse_switchpart_args( int args, char *argv[] ) {
    int c;

    while ( ( c = getopt_long( args, argv, ":zsg:f:p:r:k:bou:j:q:m", switchslot_options, NULL ) ) != EOF ) {

        if ( _cmd_swichpart != CMD_SWITCHSLOT_NONE )
            break;
       
		switch ( c ) {
            case 'z':
                _cmd_swichpart = CMD_SWITCHSLOT_INIT;
                break;
            case 's':
                _cmd_swichpart = CMD_SWITCHSLOT_STATUS;
                break;
            case 'g':
                _cmd_swichpart = CMD_SWITCHSLOT_GLOBAL_COUNTER;
                if ( strcmp( optarg, "1") == 0 ) {
                    flag_switchslot = 1;
                } else if ( strcmp( optarg, "0") == 0 ) {
                    flag_switchslot = 0;
                } else {
                    fprintf( stderr, "Error: invalid command args!!!\n" );
                    print_usage( );
                    exit(EXIT_FAILURE);
                }
                break;
            case 'f':
                _cmd_swichpart = CMD_SWITCHSLOT_FAIL_COUNTER;
                if ( strcmp( optarg, "1") == 0 ) {
                    flag_switchslot = 1;
                } else if ( strcmp( optarg, "0") == 0 ) {
                    flag_switchslot = 0;
                } else {
                    fprintf( stderr, "Error: invalid command args!!!\n" );
                    print_usage( );
                    exit(EXIT_FAILURE);
                }        
                break;
            case 'p':
                _cmd_swichpart = CMD_SWITCHSLOT_SET_PRIMARY;
                c_switchslot = (char)optarg[0];
                if ( ! SLOT_IS_VALID(SLOT_ID2NUM(c_switchslot)) ) {
                    fprintf( stderr, "Error: invalid command args (slot ID invalid)!!!\n" );
                    print_usage( );
                    exit(EXIT_FAILURE);
                }
                break;
            case 'r':
                _cmd_swichpart = CMD_SWITCHSLOT_REMOVE_SLOT;
                c_switchslot = (char)optarg[0];
                if ( ! SLOT_IS_VALID(SLOT_ID2NUM(c_switchslot)) ) {
                    fprintf( stderr, "Error: invalid command args (slot ID invalid)!!!\n" );
                    print_usage( );
                    exit(EXIT_FAILURE);
                }
                break;
            case 'k':
                _cmd_swichpart = CMD_SWITCHSLOT_INVALIDATE_SLOT;
                c_switchslot = (char)optarg[0];
                if ( ! SLOT_IS_VALID(SLOT_ID2NUM(c_switchslot)) ) {
                    fprintf( stderr, "Error: invalid command args (slot ID invalid)!!!\n" );
                    print_usage( );
                    exit(EXIT_FAILURE);
                }
                break;
            case 'b':
                _cmd_swichpart = CMD_SWITCHSLOT_BOOTOK;
                break;   
            case 'o':
                _cmd_swichpart = CMD_SWITCHSLOT_GET_BOOT_ORDER;
                break;
            case 'u':
                _cmd_swichpart = CMD_SWITCHSLOT_GET_GLOBAL_BOOT_COUNT;
                c_switchslot = (char)optarg[0];
                if ( ! SLOT_IS_VALID(SLOT_ID2NUM(c_switchslot)) ) {
                    fprintf( stderr, "Error: invalid command args (slot ID invalid)!!!\n" );
                    print_usage( );
                    exit(EXIT_FAILURE);
                }
                break;
            case 'c':
                _cmd_swichpart = CMD_SWITCHSLOT_GET_CURRENT_SLOT;
                break;
            case 'l':
                _cmd_swichpart = CMD_SWITCHSLOT_GET_PREVIOUS_SLOT;
                break;
            case 'j':
                _cmd_swichpart = CMD_SWITCHSLOT_GET_FAIL_BOOT_COUNT;
                c_switchslot = (char)optarg[0];
                if ( ! SLOT_IS_VALID(SLOT_ID2NUM(c_switchslot)) ) {
                    fprintf( stderr, "Error: invalid command args (slot ID invalid)!!!\n" );
                    print_usage( );
                    exit(EXIT_FAILURE);
                }
                break;  
            case 'q':
                _cmd_swichpart = CMD_SWITCHSLOT_RESET_FAIL_BOOT_COUNT;
                c_switchslot = (char)optarg[0];
                if ( ! SLOT_IS_VALID(SLOT_ID2NUM(c_switchslot)) ) {
                    fprintf( stderr, "Error: invalid command args (slot ID invalid)!!!\n" );
                    print_usage( );
                    exit(EXIT_FAILURE);
                } 
		break;
            case 'm':
                _cmd_swichpart = CMD_SWITCHSLOT_GET_MAX_FAIL_BOOT;
                break;
            default:
                /* ignore unknown options */
                break;
		}
	}

    /* Reset getopt for the next pass. */
	opterr = 1;
	optind = 1;
}

/*  _____________________________________________________________________________
 * |_____________________________________________________________________________|
 */


int main( int argc, char *argv[] ) {
    int ret, i;
    switching_table_t switch_data;
    char *version;

    strcpy( &p_name[0], argv[0] );
    parse_common_args( argc, argv );

    if ( device_name == NULL ) {
        fprintf( stderr, "The device is not specified!!!\n" );
        exit(EXIT_FAILURE);
    }
    dev_current = is_device_valid( device_name );
    if ( dev_current == -1 ) {
        fprintf( stderr, "Device invalid or not supported!!!\n" );
        exit(EXIT_FAILURE);
    }
    
    if ( _cmd == CMD_NONE ) {

        fprintf( stderr, "Error: command type not defined!!!\n" );
        exit(EXIT_FAILURE);

    } else if ( _cmd == CMD_TABLE ) {

        switch ( _cmd_table ) {
            case CMD_TABLE_GET_VERSION:
                ret = get_sysdata_version( &version );
                if ( ret == 0 ) {
                    if ( version != NULL ) {
                        fprintf( stdout, "Sysdata Version: %s\n", version );
                    } else {
                        fprintf( stdout, "Unable to read Sysdata Version\n" );
                        exit(EXIT_FAILURE);
                    }
                } else {
                    fprintf( stderr, "Error (%d) on retrieving data\n", ret );
                    exit(EXIT_FAILURE);
                }
                break;
            case CMD_TABLE_INITIALIZE:
                ret = sysdata_init( );
                if ( ret == 0 ) {
                    fprintf( stdout, "Sysdata memory initialized\n" );
                } else {
                    fprintf( stderr, "Error (%d) on initializing data\n", ret );
                    exit(EXIT_FAILURE);
                }
                break;
            default:
                fprintf( stderr, "Generic command: invalid command!\n" );
                break;
        }

    } else if ( _cmd == CMD_SWITCHSLOT ) {

        parse_switchpart_args( argc, argv );
        switch ( _cmd_swichpart ) {
            case CMD_SWITCHSLOT_INIT:
                fprintf( stdout, "Switchslot: data initialization... " );
                ret = sysdata_switchslot_init( );
                if ( ret == 0 ) {
                    fprintf( stdout, "OK\n" );
                } else {
                    fprintf( stdout, "error (%d)\n", ret );
                }
                break;
            case CMD_SWITCHSLOT_GLOBAL_COUNTER:
                fprintf( stdout, "Switchslot: %s global boot counter... ", 
                                flag_switchslot == 1 ? "enable" : "disable" );
                ret = sysdata_switchslot_set_enable_global_counter( flag_switchslot );
                if ( ret == 0 ) {
                    fprintf( stdout, "OK\n" );
                } else {
                    fprintf( stdout, "error (%d)\n", ret );
                    fprintf( stderr, "Error %d on CMD_SWITCHSLOT_GLOBAL_COUNTER operation\n", ret );
                    exit(EXIT_FAILURE);
                }
                break;
            case CMD_SWITCHSLOT_FAIL_COUNTER:
                fprintf( stdout, "Switchslot: %s fail boot counter... ", 
                                flag_switchslot == 1 ? "enable" : "disable" );
                ret = sysdata_switchslot_set_enable_fail_counter( flag_switchslot );
                if ( ret == 0 ) {
                    fprintf( stdout, "OK\n" );
                } else {
                    fprintf( stdout, "error (%d)\n", ret );
                    fprintf( stderr, "Error %d on CMD_SWITCHSLOT_FAIL_COUNTER operation\n", ret );
                    exit(EXIT_FAILURE);
                }
                break;
            case CMD_SWITCHSLOT_SET_PRIMARY:
                fprintf( stdout, "Switchslot: Set slot %c as primary... ", c_switchslot );
                ret = sysdata_switchslot_set_primary( c_switchslot );
                if ( ret == 0 ) {
                    fprintf( stdout, "OK\n" );
                } else {
                    fprintf( stdout, "error (%d)\n", ret );
                    fprintf( stderr, "Error %d on CMD_SWITCHSLOT_SET_PRIMARY operation\n", ret );
                    exit(EXIT_FAILURE);
                }
                break;
            case CMD_SWITCHSLOT_REMOVE_SLOT:
                fprintf( stdout, "Switchslot: Remove slot %c from boot order list... ", c_switchslot );
                ret = sysdata_switchslot_remote_from_boot_order( c_switchslot );
                if ( ret == 0 ) {
                    fprintf( stdout, "OK\n" );
                } else {
                    fprintf( stdout, "error (%d)\n", ret );
                    fprintf( stderr, "Error %d on CMD_SWITCHSLOT_REMOVE_SLOT operation\n", ret );
                    exit(EXIT_FAILURE);
                }
                break;
            case CMD_SWITCHSLOT_INVALIDATE_SLOT:
                fprintf( stdout, "Switchslot: Invalidate slot %c... ", c_switchslot );
                ret = sysdata_switchslot_invalidate_slot( c_switchslot );
                if ( ret == 0 ) {
                    fprintf( stdout, "OK\n" );
                } else {
                    fprintf( stdout, "error (%d)\n", ret );
                    fprintf( stderr, "Error %d on CMD_SWITCHSLOT_INVALIDATE_SLOT operation\n", ret );
                    exit(EXIT_FAILURE);
                }
                break;
            case CMD_SWITCHSLOT_BOOTOK:
                fprintf( stdout, "Switchslot: validate boot... " );
                ret = sysdata_switchslot_boot_ok( );
                 if ( ret == 0 ) {
                    fprintf( stdout, "OK\n" );
                } else {
                    fprintf( stdout, "error (%d)\n", ret );
                    fprintf( stderr, "Error %d on CMD_SWITCHSLOT_BOOTOK operation\n", ret );
                    exit(EXIT_FAILURE);
                }
                break;
            case CMD_SWITCHSLOT_GET_BOOT_ORDER:
                ret = get_switchslot_data( &switch_data );
                if ( ret == 0 ) {
                    for ( i = 1 ; i <= SYSDATA_MAX_SLOTS ; i++ ) {
                        fprintf( stdout, " %c ", SLOT_NUM2ID(switch_data.boot_slot_order[i-1]) );
                    }
                    fprintf( stdout, "\n" );
                } else {
                    fprintf( stderr, "Error (%d) on retrieving data\n", ret );
                    fprintf( stderr, "Error %d on CMD_SWITCHSLOT_GLOBAL_COUNTER operation\n", ret );
                    exit(EXIT_FAILURE);
                }
                break;
            case CMD_SWITCHSLOT_GET_CURRENT_SLOT:
                ret = sysdata_switchslot_get_current_slot( &i );
                if ( ret == 0 ) {
                    fprintf( stdout, "%c\n", SLOT_NUM2ID(i) );
                } else {
                    fprintf( stderr, "Error (%d) on retrieving data\n", ret );
                    fprintf( stderr, "Error %d on CMD_SWITCHSLOT_GET_CURRENT_SLOT operation\n", ret );
                    exit(EXIT_FAILURE);
                }
                break;
            case CMD_SWITCHSLOT_GET_PREVIOUS_SLOT:
                ret = sysdata_switchslot_get_previous_slot( &i );
                 if ( ret == 0 ) {
                    fprintf( stdout, "%c\n", SLOT_NUM2ID(i) );
                } else {
                    fprintf( stderr, "Error (%d) on retrieving data\n", ret );
                    fprintf( stderr, "Error %d on CMD_SWITCHSLOT_GET_CURRENT_SLOT operation\n", ret );
                    exit(EXIT_FAILURE);
                }
                break;
            case CMD_SWITCHSLOT_GET_GLOBAL_BOOT_COUNT:
                ret = get_switchslot_data( &switch_data );
                if ( ret == 0 ) {
                    fprintf( stdout, "%d\n", switch_data.boot_slot_counter[SLOT_ID2NUM(c_switchslot) - 1] );
                } else {
                    fprintf( stderr, "Error (%d) on retrieving data\n", ret );
                    fprintf( stderr, "Error %d on CMD_SWITCHSLOT_GLOBAL_COUNTER operation\n", ret );
                    exit(EXIT_FAILURE);
                }
                break;
            case CMD_SWITCHSLOT_GET_FAIL_BOOT_COUNT:
                ret = get_switchslot_data( &switch_data );
                if ( ret == 0 ) {
                    fprintf( stdout, "%d\n", switch_data.boot_fail_counter[SLOT_ID2NUM(c_switchslot) - 1] );
                } else {
                    fprintf( stderr, "Error (%d) on retrieving data\n", ret );
                    fprintf( stderr, "Error %d on CMD_SWITCHSLOT_GLOBAL_COUNTER operation\n", ret );
                    exit(EXIT_FAILURE);
                }
                break;
            case CMD_SWITCHSLOT_RESET_FAIL_BOOT_COUNT:
                fprintf( stdout, "Switchslot: reset fail boot counter for slot %c... ", c_switchslot );
                ret = sysdata_switchslot_reset_fail_counter( c_switchslot );
                if ( ret == 0 ) {
                    fprintf( stdout, "OK\n" );
                } else {
                    fprintf( stdout, "error (%d)\n", ret );
                    fprintf( stderr, "Error %d on CMD_SWITCHSLOT_RESET_FAIL_BOOT_COUNT operation\n", ret );
                    exit(EXIT_FAILURE);
                }
                break;
            case CMD_SWITCHSLOT_GET_MAX_FAIL_BOOT:
                ret = get_switchslot_max_fail_boot( );
                fprintf( stdout, "%d\n", ret );
                break;
            case CMD_SWITCHSLOT_STATUS:
                fprintf( stdout, "Switchslot - current state:\n" );
                
                ret = get_switchslot_data( &switch_data );
                 if ( ret == 0 ) {
                    /* since the boot_partition's groups are few
                       we use a pointer in order to avoid aggressive-loop-optimizations
                    */
                    for ( i = 1 ; i <= SYSDATA_MAX_SLOTS ; i++ ) {
                        printf( "Boot on slot %c: %05d\n", SLOT_NUM2ID(i), switch_data.boot_slot_counter[i-1] );
                        printf( "Consecutive fail boot on slot %c: %01d\n", SLOT_NUM2ID(i), switch_data.boot_fail_counter[i-1]);
                    }
                    printf( "---------------\n" );
                    printf( "Boot order: " );
                    for ( i = 1 ; i <= SYSDATA_MAX_SLOTS ; i++ ) {
                        printf( " %c ", SLOT_NUM2ID(switch_data.boot_slot_order[i-1]) );
                    }
                    printf( "\n---------------\n" );
                    printf( "Current boot slot: %c\n", SLOT_NUM2ID(switch_data.current_boot_slot) );
                    printf( "Previous boot slot: %c\n", SLOT_NUM2ID(switch_data.previous_boot_slot) );
                    printf( "Fail Boot counter state: %s\n", 
                                GET_FAIL_BOOT_COUNT_ENABLE(switch_data.enable_flags) ? "enabled" : "disabled" );
                    printf( "Global Boot counter state: %s\n",
                                GET_GLOBAL_BOOT_COUNT_ENABLE(switch_data.enable_flags) ? "enabled" : "disabled" );
                } else {
                    fprintf( stderr, "Error (%d) on retrieving data\n", ret );
                }
                break;
            default:
                fprintf( stderr, "Switchslot: invalid command!\n" );
                break;
        }

    }
    return 0;
}
