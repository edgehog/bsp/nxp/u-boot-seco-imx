# PROCESSOR
CONFIG_ARM=y
CONFIG_ARCH_MX6=y
CONFIG_MX6QDL=y
CONFIG_NR_DRAM_BANKS=1
CONFIG_TARGET_MX6QDL_SECO_A62=y
CONFIG_SYS_EXTRA_OPTIONS="IMX_CONFIG=arch/arm/mach-imx/spl_sd.cfg,MX6QDL,SPL"
# CONFIG_SYS_MALLOC_F is not set
CONFIG_SYS_TEXT_BASE=0x17800000
CONFIG_SPL_TEXT_BASE=0x00908000
CONFIG_ENV_OFFSET=0xC0000
CONFIG_ENV_SIZE=0x2000
CONFIG_ENV_SECT_SIZE=0x10000

# RAM CONFIGURATION
CONFIG_SECOMX6_1GB_4x256=y

# SPL
CONFIG_SPL=y
CONFIG_SPL_LDSCRIPT=y
CONFIG_SPL_LIBCOMMON_SUPPORT=y
CONFIG_SPL_LIBGENERIC_SUPPORT=y
CONFIG_SPL_MMC_SUPPORT=y
CONFIG_SPL_SERIAL_SUPPORT=y
CONFIG_SPL_LIBDISK_SUPPORT=y
CONFIG_SPL_GPIO_SUPPORT=y
CONFIG_SPL_SEPARATE_BSS=y
CONFIG_SPL_FIT_IMAGE_TINY=y
CONFIG_SPL_I2C_SUPPORT=y
#CONFIG_SPL_OS_BOOT=y
CONFIG_SPL_RAW_IMAGE_SUPPORT=y
CONFIG_SPL_FIT_PRINT=y
CONFIG_SPL_LOAD_FIT=y
#CONFIG_SPL_OF_CONTROL=y
#CONFIG_SPL_MULTI_DTB_FIT=y
#CONFIG_SPL_OF_LIST="seco-imx6q-a62 seco-imx6dl-a62 seco-imx6qp-a62"
#CONFIG_SPL_DM_SEQ_ALIAS=y
#CONFIG_SPL_OF_TRANSLATE=y
# CONFIG_SPL_DM_USB is not set
# CONFIG_SPL_DOS_PARTITION is not set
#CONFIG_SPL_DM=y
#CONFIG_SPL_DM_GPIO=y
#CONFIG_SPL_DM_REGULATOR=y
#CONFIG_SPL_DM_REGULATOR_FIXED=y
#CONFIG_SPL_DM_REGULATOR_GPIO=y
#CONFIG_SPL_DM_MMC=y
#CONFIG_SPL_DM_SERIAL=y
#CONFIG_SPL_MULTI_DTB_FIT_NO_COMPRESSION=y
CONFIG_CMD_SPL=y
CONFIG_CMD_SPL_WRITE_SIZE=0x20000
CONFIG_SDP_LOADADDR=0x40400000
CONFIG_SPL_USB_SDP_SUPPORT=y
CONFIG_SPL_USB_GADGET=y
CONFIG_SPL_USB_HOST_SUPPORT=y

# SYSTEM
CONFIG_SUPPORT_RAW_INITRD=y
CONFIG_BOUNCE_BUFFER=y
CONFIG_HUSH_PARSER=y
CONFIG_CMD_BOOTZ=y
CONFIG_CMD_CACHE=y
CONFIG_CMD_GPIO=y
# CONFIG_CMD_FLASH is not set
CONFIG_CI_UDC=y
#CONFIG_SUPPORT_EMMC_BOOT=y
CONFIG_SYS_RELOC_GD_ENV_ADDR=y

# ENVIRONMENT
CONFIG_SYS_CONSOLE_IS_IN_ENV=y
CONFIG_SYS_CONSOLE_OVERWRITE_ROUTINE=y
# CONFIG_CONSOLE_MUX is not set
CONFIG_BOOTDELAY=3
CONFIG_ENV_IS_IN_MMC=y
CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG=y
CONFIG_SECO_ENV_MANAGER=y

# FDT
CONFIG_OF_CONTROL=y
CONFIG_DEFAULT_DEVICE_TREE="seco-imx6q-a62"
CONFIG_OF_LIST="seco-imx6q-a62 seco-imx6dl-a62 seco-imx6qp-a62"
CONFIG_DM_MMC=y
CONFIG_DM_I2C=y
CONFIG_DM_SPI=y
CONFIG_DM_SPI_FLASH=y
CONFIG_DM_USB=y
CONFIG_DM_GPIO=y
CONFIG_DM_REGULATOR=y
CONFIG_DM_REGULATOR_PFUZE100=y
CONFIG_DM_REGULATOR_FIXED=y
CONFIG_DM_REGULATOR_GPIO=y
CONFIG_DM_PMIC=y
CONFIG_DM_ETH=y
CONFIG_DM_SERIAL=y
CONFIG_DM_MDIO=y
CONFIG_DM_PCI=y
CONFIG_FIT=y
CONFIG_MULTI_DTB_FIT=y
CONFIG_PINCTRL=y
CONFIG_PINCTRL_FULL=y
CONFIG_PINCTRL_IMX6=y

# FDT FOR KERNEL
CONFIG_OF_LIBFDT=y
CONFIG_OF_LIBFDT_OVERLAY=y
CONFIG_CMD_FDT=y

# STORAGE
CONFIG_CMD_MMC=y
CONFIG_CMD_PART=y
CONFIG_CMD_SF=y
CONFIG_CMD_EXT2=y
CONFIG_CMD_EXT4=y
CONFIG_CMD_EXT4_WRITE=y
CONFIG_CMD_FAT=y
CONFIG_CMD_FS_GENERIC=y
CONFIG_EFI_PARTITION=y
CONFIG_FSL_USDHC=y
CONFIG_LIBATA=y
CONFIG_IMX_AHCI=y
CONFIG_SATA_CEVA=y
CONFIG_CMD_SATA=y
CONFIG_DWC_AHCI=y
CONFIG_AHCI=y
CONFIG_DWC_AHSATA=y

# PCI
#CONFIG_PCI=y
#CONFIG_CMD_PCI=y

# I2c
CONFIG_SYS_I2C=y
CONFIG_CMD_I2C=y
CONFIG_SYS_I2C_MXC=y
CONFIG_CMD_EEPROM=y
CONFIG_I2C_EEPROM=y
CONFIG_SYS_I2C_EEPROM_BUS=1

# SPI
CONFIG_SPI=y
CONFIG_MXC_SPI=y
CONFIG_SPI_FLASH=y
CONFIG_SF_DEFAULT_MODE=0
CONFIG_SF_DEFAULT_SPEED=20000000
CONFIG_SPI_FLASH_STMICRO=y
CONFIG_SPI_FLASH_SPANSION=y
CONFIG_SPI_FLASH_SST=y
CONFIG_MTD=y


# USB
CONFIG_USB=y
CONFIG_USB_GADGET=y
CONFIG_USB_GADGET_MANUFACTURER="FSL"
CONFIG_USB_GADGET_VENDOR_NUM=0x0525
CONFIG_USB_GADGET_PRODUCT_NUM=0xa4a5
CONFIG_USB_STORAGE=y
CONFIG_USB_HOST_ETHER=y
CONFIG_USB_ETHER_ASIX=y
CONFIG_CMD_USB=y
CONFIG_CMD_USB_SDP=y

CONFIG_CMD_USB_MASS_STORAGE=y
CONFIG_USB_GADGET_DOWNLOAD=y

# ETHERNET
CONFIG_FEC_MXC=y
CONFIG_PHYLIB=y
CONFIG_MII=y
CONFIG_NET_RANDOM_ETHADDR=y
CONFIG_CMD_DHCP=y
CONFIG_DM_ETH_PHY=y
CONFIG_CMD_MII=y
CONFIG_CMD_PING=y
CONFIG_PHY_MICREL=y
CONFIG_PHY_ATHEROS=y
CONFIG_PHY_MICREL_KSZ90X1=y
CONFIG_PHY_MICREL_KSZ8XXX=y

# VIDEO
CONFIG_SECO_VIDEO=y
CONFIG_VIDEO_IPUV3=y
CONFIG_VIDEO=y
CONFIG_DM_VIDEO=y
CONFIG_IMX_VIDEO_SKIP=y
CONFIG_BACKLIGHT=y
CONFIG_BACKLIGHT_GPIO=y
CONFIG_BACKLIGHT_PWM=y
CONFIG_PANEL=y
CONFIG_CMD_BMP=y
CONFIG_VIDEO_SW_CURSOR is not set
CONFIG_VIDEO_BPP8 is not set
CONFIG_VIDEO_BPP32 is not set
CONFIG_SYS_WHITE_ON_BLACK=y
CONFIG_SPLASH_SCREEN=y
CONFIG_SPLASH_SCREEN_ALIGN=y
CONFIG_SPLASH_SOURCE=y


# FASTBOOT
CONFIG_FASTBOOT_FLASH=y
CONFIG_FASTBOOT_FLASH_MMC=y
CONFIG_USB_FUNCTION_FASTBOOT=y
CONFIG_FASTBOOT_BUF_ADDR=0x12C00000
CONFIG_FASTBOOT_BUF_SIZE=0x40000000
CONFIG_FASTBOOT_FLASH_MMC_DEV=0
CONFIG_FASTBOOT_USB_DEV=0
