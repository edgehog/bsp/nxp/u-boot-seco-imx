/*
 * (C) Copyright 2022 Seco
 *
 * Author: Gianfranco Mariotti <gianfranco.mariotti@seco.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

#ifndef _C43_REVISION_H_
#define _C43_REVISION_H_

int strap_get_ram_cfg (void);
int strap_get_revision_cfg (void);
int strap_get_board_cfg(void);
void strap_show(void);

typedef enum {
	RAM_RESERVED = 0x0,
	RAM_2GB      = 0x1,
	RAM_4GB      = 0x2,
	RAM_8GB      = 0x3,
} RAM_STRAPS;

typedef enum {
	REV_B = 0x0,
	REV_C = 0x1,
	REV_D = 0x2,
} BOARD_REV;

typedef struct {
	u8 ram_size;
	u8 hw_rev;
} strap_conf_t;

#endif
