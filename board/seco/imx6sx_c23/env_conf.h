
#include <common.h>
#include <command.h>
#include <env.h>

#include <seco/env_common.h>

DECLARE_GLOBAL_DATA_PTR;
#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)


/* *********************************** IMX6SX *********************************** */

data_boot_dev_t kern_dev_imx6sx_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_KERNEL_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_KERNEL_LOCAL_DEV,   SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_KERNEL_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EXT_SD,  LOAD_ADDR_KERNEL_LOCAL_DEV,   SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_SPI,      SECO_DEV_LABEL_FLASH,  STR(MACRO_ENV_KERNEL_SRC_SPI),      SCFG_BOOT_DEV_ID_SPI,     LOAD_ADDR_KERNEL_LOCAL_DEV,   SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_KERNEL_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_KERNEL_REMOTE_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_KERNEL_SRC_TFTP),     "",                       LOAD_ADDR_KERNEL_LOCAL_DEV,   SCFG_KERNEL_FILENAME },
};
size_t kern_dev_imx6sx_size = sizeof( kern_dev_imx6sx_list ) / sizeof( kern_dev_imx6sx_list[0] );


data_boot_dev_t fdt_dev_imx6sx_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FDT_SRC_USDHCI),    SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_FDT_LOCAL_DEV,   SCFG_DEFAULT_FDT_IMX6SX_FILE },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_FDT_SRC_USDHCI),    SCFG_BOOT_DEV_ID_EXT_SD,  LOAD_ADDR_FDT_LOCAL_DEV,   SCFG_DEFAULT_FDT_IMX6SX_FILE },
	{ SECO_DEV_TYPE_SPI,      SECO_DEV_LABEL_FLASH,  STR(MACRO_ENV_FDT_SRC_SPI),       SCFG_BOOT_DEV_ID_SPI,     LOAD_ADDR_FDT_LOCAL_DEV,   ""                          },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FDT_SRC_USB),       SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_FDT_REMOTE_DEV,  SCFG_DEFAULT_FDT_IMX6SX_FILE },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_FDT_SRC_TFTP),      "",                       LOAD_ADDR_FDT_LOCAL_DEV,   SCFG_DEFAULT_FDT_IMX6SX_FILE },
};
size_t fdt_dev_imx6sx_size = sizeof( fdt_dev_imx6sx_list ) / sizeof( fdt_dev_imx6sx_list[0] );


#ifdef CONFIG_OF_LIBFDT_OVERLAY
data_boot_dev_t fdt_overlay_dev_imx6sx_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),    SCFG_BOOT_DEV_ID_EMMC,    "", "" },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),    SCFG_BOOT_DEV_ID_EXT_SD,  "", "" },
	{ SECO_DEV_TYPE_SPI,      SECO_DEV_LABEL_FLASH,  STR(MACRO_ENV_FDT_OVERLAY_SRC_SPI),       SCFG_BOOT_DEV_ID_SPI,     "", "" },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FDT_OVERLAY_SRC_USB),       SCFG_BOOT_DEV_ID_USB,     "", "" },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_FDT_OVERLAY_SRC_TFTP),      "",                       "", "" },
};
size_t fdt_overlay_dev_imx6sx_size = sizeof( fdt_overlay_dev_imx6sx_list ) / sizeof( fdt_overlay_dev_imx6sx_list[0] );
#endif  /* CONFIG_OF_LIBFDT_OVERLAY  */


data_boot_dev_t ramfs_dev_imx6sx_list [] = {
	{ SECO_DEV_TYPE_NONE,     SECO_DEV_LABEL_NONE,   "0x0",                            "0",                      LOAD_ADDR_RAMFS_LOCAL_DEV,  ""          },
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_RAMFS_SRC_USDHCI),  SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_RAMFS_LOCAL_DEV,  SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_RAMFS_SRC_USDHCI),  SCFG_BOOT_DEV_ID_EXT_SD,  LOAD_ADDR_RAMFS_LOCAL_DEV,  SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_SPI,      SECO_DEV_LABEL_FLASH,  STR(MACRO_ENV_RAMFS_SRC_SPI),     SCFG_BOOT_DEV_ID_SPI,     LOAD_ADDR_RAMFS_LOCAL_DEV,  SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_RAMFS_SRC_USB),     SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_RAMFS_REMOTE_DEV, SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_RAMFS_SRC_TFTP),    "",                       LOAD_ADDR_RAMFS_LOCAL_DEV,  SCFG_RAMFS_FILENAME },
};
size_t ramfs_dev_imx6sx_size = sizeof( ramfs_dev_imx6sx_list ) / sizeof( ramfs_dev_imx6sx_list[0] );


data_boot_dev_t filesystem_dev_imx6sx_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FS_SRC_USDHCI),  SCFG_ROOT_DEV_ID_EMMC,    "", "" },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_FS_SRC_USDHCI),  SCFG_ROOT_DEV_ID_EXT_SD,  "", "" },
	{ SECO_DEV_TYPE_NFS,      SECO_DEV_LABEL_NFS,    STR(MACRO_ENV_FS_SRC_NFS),     "",                       "", "" },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FS_SRC_USB),     "",                       "", "" },
};
size_t filesystem_dev_imx6sx_size = sizeof( filesystem_dev_imx6sx_list ) / sizeof( filesystem_dev_imx6sx_list[0] );

#ifdef CONFIG_TARGET_MX6SX_SECO_SBC_C23
video_mode_t video_mode_list [] = {
	{   
        .label    = "no display",
		.panel_name = "none",
		.dtbo_conf_file = NULL,
    }
};
size_t video_mode_size = sizeof( video_mode_list ) / sizeof( video_mode_list[0] );

#endif

#ifdef CONFIG_OF_LIBFDT_OVERLAY
/* *********************************** FDT OVERLAY *********************************** */

overlay_list_t overlay_peripheral_list [] = {
};
#endif  /* CONFIG_OF_LIBFDT_OVERLAY */
