#ifndef _C20_REVISION_H_
#define _C20_REVISION_H_


void c20_fdt_ram_setup(void *blob, struct bd_info *bdev);
void c20_fdt_vpu_setup(void *blob, struct bd_info *bdev);
void c20_fdt_rev_setup(void *blob, struct bd_info *bdev);
int strap_get_ram_cfg (void);
void strap_show( void );
int strap_get_revision_cfg (void);
int strap_get_board_cfg( void );

typedef enum {
        RAM_1GB = 0,
        RAM_2GB = 0x1,
        RAM_4GB = 0x2,
        RAM_8GB = 0x3,
} RAM_STRAPS;

typedef enum {
        REV_B = 0,
        REV_C = 0x1,
} BOARD_REV;

typedef struct {
        u8 ram_size;
        u8 video;
        u8 hw_rev;
} strap_conf_t;


#define GET_C20_STRAPS  (strap_get_ram_cfg() & 0x3)

#define C20_IS_1GB	(GET_C20_STRAPS == RAM_1GB)
#define C20_IS_2GB	(GET_C20_STRAPS == RAM_2GB)
#define C20_IS_4GB      (GET_C20_STRAPS == RAM_4GB)

#define GET_C20_REV	(strap_get_revision_cfg() & 0x1)

#define C20_IS_REVB	(GET_C20_REV == REV_B)
#define C20_IS_REVC	(GET_C20_REV == REV_C)




#endif
