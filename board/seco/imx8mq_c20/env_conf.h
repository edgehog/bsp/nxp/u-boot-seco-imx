/*
 * (C) Copyright 2015 Seco
 *
 * Author: Davide Cardillo <davide.cardillo@seco.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

#include <common.h>
#include <command.h>
#include <env.h>


#include <seco/env_common.h>
#include <configs/seco_mx8_dtbo.h>


#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

 
/* *********************************** IMX8 *********************************** */

data_boot_dev_t kern_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_KERNEL_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_KERNEL_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_KERNEL_SRC_TFTP),     "",                       LOAD_ADDR_KERNEL_REMOTE_DEV, SCFG_KERNEL_FILENAME },
};

size_t kern_dev_imx8_size = sizeof( kern_dev_imx8_list ) / sizeof( kern_dev_imx8_list[0] );


data_boot_dev_t fdt_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FDT_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX8_FILE },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FDT_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX8_FILE },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_FDT_SRC_TFTP),     "",                       LOAD_ADDR_FDT_REMOTE_DEV, SCFG_DEFAULT_FDT_IMX8_FILE },
};

size_t fdt_dev_imx8_size = sizeof( fdt_dev_imx8_list ) / sizeof( fdt_dev_imx8_list[0] );


#ifdef CONFIG_OF_LIBFDT_OVERLAY
data_boot_dev_t fdt_overlay_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    "", "" },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FDT_OVERLAY_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     "", "" },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_FDT_OVERLAY_SRC_TFTP),     "",                       "", "" },
};

size_t fdt_overlay_dev_imx8_size = sizeof( fdt_overlay_dev_imx8_list ) / sizeof( fdt_overlay_dev_imx8_list[0] );
#endif  /* CONFIG_OF_LIBFDT_OVERLAY */


data_boot_dev_t ramfs_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_NONE,     SECO_DEV_LABEL_NONE,   "0x0",                             "0",                      LOAD_ADDR_RAMFS_LOCAL_DEV,    ""                 },
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_RAMFS_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_RAMFS_LOCAL_DEV,    SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_RAMFS_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_RAMFS_LOCAL_DEV,    SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_RAMFS_SRC_TFTP),     "",                       LOAD_ADDR_RAMFS_REMOTE_DEV,   SCFG_RAMFS_FILENAME },
};

size_t ramfs_dev_imx8_size = sizeof( ramfs_dev_imx8_list ) / sizeof( ramfs_dev_imx8_list[0] );


data_boot_dev_t filesystem_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FS_SRC_USDHCI),   SCFG_ROOT_DEV_ID_EMMC,    "", "" },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FS_SRC_USB),      SCFG_ROOT_DEV_ID_USB,     "", "" },
	{ SECO_DEV_TYPE_NFS,      SECO_DEV_LABEL_NFS,    STR(MACRO_ENV_FS_SRC_NFS),      "",                       "", "" },
};

size_t filesystem_dev_imx8_size = sizeof( filesystem_dev_imx8_list ) / sizeof( filesystem_dev_imx8_list[0] );



/* LVDSx1, eDP */

video_mode_t video_mode_list_cfg [] = {
    {   
		/* NO DISPLAY */
        .label    = SECO_VIDEO_LABEL_NONE,
        .video = { 
                	{ VIDEO_NOT_USED, NO_VIDEO, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = NULL,
        .use_bootargs = 0,
    }, {
		/* LVDS */   
        .label    = SECO_VIDEO_LABEL_LVDSx2,
        .video = { 
        	{ VIDEO_USED, VIDEO_LVDSx2, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = STR(ENV_DTBO_C20_LVDS_DUAL),
        .use_bootargs = 0,
    }, {
		/* HDMI */   
        .label    = SECO_VIDEO_LABEL_HDMI,
        .video = { 
			{ VIDEO_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = STR(ENV_DTBO_C20_HDMI),
        .use_bootargs = 0,

    }, {
		/* eDP */   
        .label    = SECO_VIDEO_LABEL_EDP,
        .video = { 
			{ VIDEO_USED, VIDEO_EDP, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = STR(ENV_DTBO_C20_EDP),
        .use_bootargs = 0,


    },
};


size_t video_mode_size_cfg = sizeof( video_mode_list_cfg ) / sizeof( video_mode_list_cfg[0] );


#ifdef CONFIG_OF_LIBFDT_OVERLAY
/* *********************************** FDT OVERLAY *********************************** */

overlay_list_t overlay_peripheral_list [] = {
	        {
                .title = "pcie support",
                .options = {
                        { "not use", "" },              // default
                        { "pcie", STR(ENV_DTBO_C20_PCIE) },
                },
        },
};

size_t overlay_peripheral_size = sizeof( overlay_peripheral_list ) / sizeof( overlay_peripheral_list[0] );

#endif  /* CONFIG_OF_LIBFDT_OVERLAY */
