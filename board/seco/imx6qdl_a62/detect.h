/*
 * (C) Copyright 2015 Seco
 *
 * Author: Davide Cardillo <davide.cardillo@seco.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */
/*
 * Detect board revision.
 * Rev_A/B
 * Rev_C
 */


#define SECO_CODE_0            IMX_GPIO_NR(5, 14)
#define SECO_CODE_1            IMX_GPIO_NR(5, 15)
#define SECO_CODE_2            IMX_GPIO_NR(5, 16)
#define SECO_CODE_3            IMX_GPIO_NR(5, 17)


#define is_SBC_REVA_B(x)       (((x) & 0x1) == 0x0)
#define is_SBC_REVC(x)         (((x) & 0x1) == 0x1)
