/*
 * (C) Copyright 2015 Seco
 *
 * Author: Davide Cardillo <davide.cardillo@seco.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

#include <common.h>
#include <command.h>
#include <env.h>


#include <seco/env_common.h>
#include <configs/seco_mx6_dtbo.h>


#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)


/* *********************************** IMX6DL *********************************** */

data_boot_dev_t kern_dev_imx6dl_list [] = {
    { SECO_DEV_TYPE_EMMC,     "eMMC onboard",   STR(MACRO_ENV_KERNEL_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
    { SECO_DEV_TYPE_SD_EXT,   "external SD",    STR(MACRO_ENV_KERNEL_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD_EXT,  LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
    { SECO_DEV_TYPE_TFTP,     "TFTP",           STR(MACRO_ENV_KERNEL_SRC_TFTP),     "",                       LOAD_ADDR_KERNEL_REMOTE_DEV, SCFG_KERNEL_FILENAME },
    { SECO_DEV_TYPE_USB,      "USB",            STR(MACRO_ENV_KERNEL_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
};

size_t kern_dev_imx6dl_size = sizeof( kern_dev_imx6dl_list ) / sizeof( kern_dev_imx6dl_list[0] );


data_boot_dev_t fdt_dev_imx6dl_list [] = {
    { SECO_DEV_TYPE_EMMC,     "eMMC onboard",   STR(MACRO_ENV_FDT_SRC_USDHCI),    SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX6DL_FILENAME },
    { SECO_DEV_TYPE_SD_EXT,   "external SD",    STR(MACRO_ENV_FDT_SRC_USDHCI),    SCFG_BOOT_DEV_ID_SD_EXT,  LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX6DL_FILENAME },
    { SECO_DEV_TYPE_TFTP,     "TFTP",           STR(MACRO_ENV_FDT_SRC_TFTP),      "",                       LOAD_ADDR_FDT_REMOTE_DEV, SCFG_DEFAULT_FDT_IMX6DL_FILENAME },
    { SECO_DEV_TYPE_USB,      "USB",            STR(MACRO_ENV_FDT_SRC_USB),       SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX6DL_FILENAME },
};

size_t fdt_dev_imx6dl_size = sizeof( fdt_dev_imx6dl_list ) / sizeof( fdt_dev_imx6dl_list[0] );


#ifdef CONFIG_OF_LIBFDT_OVERLAY
data_boot_dev_t fdt_overlay_dev_imx6dl_list [] = {
    { SECO_DEV_TYPE_EMMC,     "eMMC onboard",   STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),    SCFG_BOOT_DEV_ID_EMMC,    "" },
    { SECO_DEV_TYPE_SD_EXT,   "external SD",    STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),    SCFG_BOOT_DEV_ID_SD_EXT,  "" },
    { SECO_DEV_TYPE_TFTP,     "TFTP",           STR(MACRO_ENV_FDT_OVERLAY_SRC_TFTP),      "",                       "" },
    { SECO_DEV_TYPE_USB,      "USB",            STR(MACRO_ENV_FDT_OVERLAY_SRC_USB),       SCFG_BOOT_DEV_ID_USB,     "" },
};

size_t fdt_overlay_dev_imx6dl_size = sizeof( fdt_overlay_dev_imx6dl_list ) / sizeof( fdt_overlay_dev_imx6dl_list[0] );
#endif  /* CONFIG_OF_LIBFDT_OVERLAY  */


data_boot_dev_t ramfs_dev_imx6dl_list [] = {
    { SECO_DEV_TYPE_NONE,     "Not use",        "0x0",                            "0",                      LOAD_ADDR_RAMFS_LOCAL_DEV,  ""          },
    { SECO_DEV_TYPE_EMMC,     "eMMC onboard",   STR(MACRO_ENV_RAMFS_SRC_USDHCI),  SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_RAMFS_LOCAL_DEV,  SCFG_RAMFS_FILENAME },
    { SECO_DEV_TYPE_SD_EXT,   "external SD",    STR(MACRO_ENV_RAMFS_SRC_USDHCI),  SCFG_BOOT_DEV_ID_SD_EXT,  LOAD_ADDR_RAMFS_LOCAL_DEV,  SCFG_RAMFS_FILENAME },
    { SECO_DEV_TYPE_TFTP,     "TFTP",           STR(MACRO_ENV_RAMFS_SRC_TFTP),    "",                       LOAD_ADDR_RAMFS_REMOTE_DEV, SCFG_RAMFS_FILENAME },
    { SECO_DEV_TYPE_USB,      "USB",            STR(MACRO_ENV_RAMFS_SRC_USB),     SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_RAMFS_LOCAL_DEV,  SCFG_RAMFS_FILENAME },
};

size_t ramfs_dev_imx6dl_size = sizeof( ramfs_dev_imx6dl_list ) / sizeof( ramfs_dev_imx6dl_list[0] );


data_boot_dev_t filesystem_dev_imx6dl_list [] = {
    { SECO_DEV_TYPE_EMMC,     "eMMC onboard",    STR(MACRO_ENV_FS_SRC_USDHCI),  SCFG_ROOT_DEV_ID_EMMC,    "" },
    { SECO_DEV_TYPE_SD_EXT,   "external SD",     STR(MACRO_ENV_FS_SRC_USDHCI),  SCFG_ROOT_DEV_ID_SD_EXT,  "" },
    { SECO_DEV_TYPE_NFS,      "NFS",             STR(MACRO_ENV_FS_SRC_NFS),     "",                       "" },
    { SECO_DEV_TYPE_USB,      "USB",             STR(MACRO_ENV_FS_SRC_USB),     "",                       "" },
};

size_t filesystem_dev_imx6dl_size = sizeof( filesystem_dev_imx6dl_list ) / sizeof( filesystem_dev_imx6dl_list[0] );


static panel_parameters_t lvds_video_spec_list [] = {
    { "CLAA-WVGA   [800x480]",   "CLAA-WVGA",    "RGB666",      NULL,   -1, NULL,   1 }
};

video_mode_t video_mode_list [] = {
    {
/* NO DISPLAY */
        .label    = "no display",
        .video = {
                { VIDEO_NOT_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
                { VIDEO_NOT_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
                { VIDEO_NOT_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = NULL,
        .use_bootargs = 0,
    }, {
/* PARALLEL RGB ONLY */
        .label    = "LCD Parallel RGB [800x480]",
        .video = {
                {
                    .used = VIDEO_USED,
                    .type = VIDEO_RGB,
                    .video_args = {
                        .name         = "LCD-RGB",
                        .buffer       = "mxcfb0",
                         .driver       = "lcd",
                        PANEL_LIST(lvds_video_spec_list)
                    },
                },
                { VIDEO_NOT_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
                { VIDEO_NOT_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
        },
        .panel_name = "Fannal FN0700D083A",
        .dtbo_conf_file = STR(ENV_DTBO_SANTINO_RGB),
        .use_bootargs = 0,
     }
};

size_t video_mode_size = sizeof( video_mode_list ) / sizeof( video_mode_list[0] );



#ifdef CONFIG_OF_LIBFDT_OVERLAY
/* *********************************** FDT OVERLAY *********************************** */



#endif  /* CONFIG_OF_LIBFDT_OVERLAY */

