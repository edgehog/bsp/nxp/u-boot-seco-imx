// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2012-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 *
 * Author: Fabio Estevam <fabio.estevam@freescale.com>
 */


#include <image.h>
#include <init.h>
#include <net.h>
#include <asm/arch/clock.h>
#include <asm/arch/imx-regs.h>
#include <asm/arch/iomux.h>
#include <asm/arch/mx6-pins.h>
#include <asm/global_data.h>
#include <asm/mach-imx/spi.h>
#include <env.h>
#include <linux/errno.h>
#include <linux/delay.h>
#include <asm/gpio.h>
#include <asm/mach-imx/mxc_i2c.h>
#include <asm/mach-imx/iomux-v3.h>
#include <asm/mach-imx/boot_mode.h>
#include <asm/mach-imx/video.h>
#include <mmc.h>
#include <fsl_esdhc_imx.h>
#include <miiphy.h>
#include <asm/arch/mxc_hdmi.h>
#include <asm/arch/crm_regs.h>
#include <asm/io.h>
#include <asm/arch/sys_proto.h>
#include <i2c.h>
#include <input.h>
#include <power/pmic.h>
#include <power/pfuze100_pmic.h>
#include <power/pfuze3000_pmic.h>
#include "../common/pfuze.h"
#include <usb.h>
#include <usb/ehci-ci.h>
#include <asm/arch/mx6-ddr.h>
#include <power/regulator.h>
#if defined(CONFIG_MX6DL) && defined(CONFIG_MXC_EPDC)
#include <lcd.h>
#include <mxc_epdc_fb.h>
#endif
#ifdef CONFIG_FSL_FASTBOOT
#include <fb_fsl.h>
#ifdef CONFIG_ANDROID_RECOVERY
#include <recovery.h>
#endif
#endif /*CONFIG_FSL_FASTBOOT*/

#include "../common/proto_seco.h"

#ifdef CONFIG_SECO_ENV_MANAGER
    #include <seco/env_common.h>
    #include "env_conf.h"
#endif

DECLARE_GLOBAL_DATA_PTR;

#define UART_PAD_CTRL  (PAD_CTL_PUS_100K_UP |			\
    PAD_CTL_SPEED_MED | PAD_CTL_DSE_40ohm |			\
    PAD_CTL_SRE_FAST  | PAD_CTL_HYS)

#define USDHC_PAD_CTRL (PAD_CTL_PUS_47K_UP |			\
    PAD_CTL_SPEED_LOW | PAD_CTL_DSE_80ohm |			\
    PAD_CTL_SRE_FAST  | PAD_CTL_HYS)

#define SPI_PAD_CTRL (PAD_CTL_HYS | PAD_CTL_SPEED_MED | \
              PAD_CTL_DSE_40ohm | PAD_CTL_SRE_FAST)

#define I2C_PAD_CTRL  (PAD_CTL_PUS_100K_UP |			\
    PAD_CTL_SPEED_MED | PAD_CTL_DSE_40ohm | PAD_CTL_HYS |	\
    PAD_CTL_ODE | PAD_CTL_SRE_FAST)

#define EPDC_PAD_CTRL    (PAD_CTL_PKE | PAD_CTL_SPEED_MED |	\
    PAD_CTL_DSE_40ohm | PAD_CTL_HYS)

#define OTG_ID_PAD_CTRL (PAD_CTL_PKE | PAD_CTL_PUE |		\
    PAD_CTL_PUS_47K_UP  | PAD_CTL_SPEED_LOW |		\
    PAD_CTL_DSE_80ohm   | PAD_CTL_SRE_FAST  | PAD_CTL_HYS)

#define PFUZE3000_SW3_REG_to_MVOLTS(x)      ((x) * 500 + 9000)
#define I2C_PMIC	1

#define I2C_PAD MUX_PAD_CTRL(I2C_PAD_CTRL)

int usdhc_devno[4] = { -1, BOARD_BOOT_ID_EXT_SD, -1, BOARD_BOOT_ID_EMMC };

static const iomux_v3_cfg_t uart2_pads[] = {
    IOMUX_PADS(PAD_SD3_DAT4__UART2_RX_DATA | MUX_PAD_CTRL(UART_PAD_CTRL)),
    IOMUX_PADS(PAD_SD3_DAT5__UART2_TX_DATA | MUX_PAD_CTRL(UART_PAD_CTRL)),
    IOMUX_PADS(PAD_SD3_CMD__UART2_CTS_B | MUX_PAD_CTRL(UART_PAD_CTRL)),
    IOMUX_PADS(PAD_SD3_CLK__UART2_RTS_B | MUX_PAD_CTRL(UART_PAD_CTRL)),
};

#ifdef CONFIG_SYS_I2C
static struct i2c_pads_info i2c_pad_info1 = {
    .scl = {
        .i2c_mode = MX6_PAD_CSI0_DAT8__I2C1_SDA | I2C_PAD,
        .gp = IMX_GPIO_NR(5, 26)
    },
    .sda = {
        .i2c_mode = MX6_PAD_CSI0_DAT9__I2C1_SCL | I2C_PAD,
        .gp = IMX_GPIO_NR(5, 27)
    }
};
#endif

static void setup_iomux_uart(void)
{
    SETUP_IOMUX_PADS(uart2_pads);
}

#ifdef CONFIG_FSL_ESDHC_IMX
#if !CONFIG_IS_ENABLED(DM_MMC)

static iomux_v3_cfg_t const usdhc2_pads[] = {
    IOMUX_PADS(PAD_SD2_CLK__SD2_CLK	    | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
    IOMUX_PADS(PAD_SD2_CMD__SD2_CMD	    | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
    IOMUX_PADS(PAD_SD2_DAT0__SD2_DATA0	| MUX_PAD_CTRL(USDHC_PAD_CTRL)),
    IOMUX_PADS(PAD_SD2_DAT1__SD2_DATA1	| MUX_PAD_CTRL(USDHC_PAD_CTRL)),
    IOMUX_PADS(PAD_SD2_DAT2__SD2_DATA2	| MUX_PAD_CTRL(USDHC_PAD_CTRL)),
    IOMUX_PADS(PAD_SD2_DAT3__SD2_DATA3	| MUX_PAD_CTRL(USDHC_PAD_CTRL)),
    IOMUX_PADS(PAD_GPIO_4__SD2_CD_B	    | MUX_PAD_CTRL(NO_PAD_CTRL)), /* CD */
    IOMUX_PADS(PAD_GPIO_2__SD2_WP	    | MUX_PAD_CTRL(NO_PAD_CTRL)), /* WP */
};

static iomux_v3_cfg_t const usdhc4_pads[] = {
    IOMUX_PADS(PAD_SD4_CLK__SD4_CLK    | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
    IOMUX_PADS(PAD_SD4_CMD__SD4_CMD    | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
    IOMUX_PADS(PAD_SD4_DAT0__SD4_DATA0 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
    IOMUX_PADS(PAD_SD4_DAT1__SD4_DATA1 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
    IOMUX_PADS(PAD_SD4_DAT2__SD4_DATA2 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
    IOMUX_PADS(PAD_SD4_DAT3__SD4_DATA3 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
    IOMUX_PADS(PAD_SD4_DAT4__SD4_DATA4 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
    IOMUX_PADS(PAD_SD4_DAT5__SD4_DATA5 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
    IOMUX_PADS(PAD_SD4_DAT6__SD4_DATA6 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
    IOMUX_PADS(PAD_SD4_DAT7__SD4_DATA7 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
};

struct fsl_esdhc_cfg usdhc_cfg[CONFIG_SYS_FSL_USDHC_NUM] = {
    { USDHC2_BASE_ADDR, 0, 4 },
    { USDHC4_BASE_ADDR, 0, 8 },
};

#define USDHC2_CD_GPIO	IMX_GPIO_NR(1, 4)
#define USDHC2_WP_GPIO	IMX_GPIO_NR(1, 2)

enum mxc_clock usdhc_clk[CONFIG_SYS_FSL_USDHC_NUM] = {
    MXC_ESDHC2_CLK,
    MXC_ESDHC4_CLK
};

struct usdhc_l usdhc_list_spl[CONFIG_SYS_FSL_USDHC_NUM] = {
    {usdhc2_pads, ARRAY_SIZE(usdhc2_pads)/2, USDHC2_CD_GPIO, USDHC2_WP_GPIO},
    {usdhc4_pads, ARRAY_SIZE(usdhc4_pads)/2, -1, IMX_GPIO_NR(7, 0)}
};

boot_mem_dev_t boot_mem_dev_list[SECO_NUM_BOOT_DEV] = {
    { 0x4, 0x5, -1,   1, -1, "External SD" },
    { 0x6, 0x7, -1,   3, -1, "eMMC" },
};

int board_mmc_getcd(struct mmc *mmc)
{
    struct fsl_esdhc_cfg *cfg = (struct fsl_esdhc_cfg *)mmc->priv;
    int ret = 0;
    switch (cfg->esdhc_base) {
    case USDHC2_BASE_ADDR:
        ret = !gpio_get_value(USDHC2_CD_GPIO);
        break;
    case USDHC4_BASE_ADDR:
        ret = 1; /* eMMC/uSDHC4 is always present */
        break;
    }
    return ret;
}

int board_mmc_init( struct bd_info *bis ) {
    struct src *psrc = (struct src *)SRC_BASE_ADDR;
    unsigned reg_noshift = readl(&psrc->sbmr1);
    unsigned reg = readl(&psrc->sbmr1) >> 11;
    /*
     * Upon reading BOOT_CFG register the following map is done:
     * Bit 11 and 12 of BOOT_CFG register can determine the current
     * mmc port
     * 0x1                  SD2
     * 0x3                  SD4
     */
    int ret = 0;
    int index = 0;

    switch (reg & 0x3) {
        case 0x1:
            index = 0;
            break;
        case 0x3:
            index = 1;
            break;
        default:
            break;
    }

    print_boot_device( );
    imx_iomux_v3_setup_multiple_pads( usdhc_list_spl[index].pad,
                usdhc_list_spl[index].num );
    usdhc_cfg[index].sdhc_clk = mxc_get_clock( usdhc_clk[index] );
    gd->arch.sdhc_clk = usdhc_cfg[index].sdhc_clk;
    ret = fsl_esdhc_initialize( bis, &usdhc_cfg[index] );

    if ( ret )
        printf( "Warning: failed to initialize mmc dev %d\n", index );

    return ret;
}
#endif
#endif

static void setup_fec(void)
{
    /* do nothing */
}

#ifdef CONFIG_USB_EHCI_MX6
int board_ehci_hcd_init(int port)
{
    switch (port) {
    case 0:
        /*
          * Set daisy chain for otg_pin_id on 6q.
         *  For 6dl, this bit is reserved.
         */
        imx_iomux_set_gpr_register(1, 13, 1, 0);
        break;
    case 1:
        break;
    default:
        printf("MXC USB port %d not yet supported\n", port);
        return -EINVAL;
    }
    return 0;
}
#endif

int board_early_init_f(void)
{
    setup_iomux_uart();

    return 0;
}

int board_init(void)
{
    /* address of boot parameters */
    gd->bd->bi_boot_params = PHYS_SDRAM + 0x100;

#if defined(CONFIG_DM_REGULATOR)
    regulators_enable_boot_on(false);
#endif

#ifdef CONFIG_SYS_I2C
    setup_i2c(1, CONFIG_SYS_I2C_SPEED, 0x7f, &i2c_pad_info1);
#endif

#ifdef CONFIG_FEC_MXC
    setup_fec();
#endif

    return 0;
}

int power_init_board(void)
{
    struct udevice *dev;
    unsigned int reg;
    int ret;

    dev = pfuze3000_common_init();
    if (!dev)
        return -ENODEV;

    ret = pfuze_mode_init(dev, APS_APS);
    if (ret < 0)
        return ret;

    /* set DDR_1_5V to 1.350V */
    reg = pmic_reg_read(dev, PFUZE3000_SW3VOLT);
    reg &= ~0x0f;
    reg |= PFUZE3000_SW3_SETP(13500);
    ret = pmic_reg_write(dev, PFUZE3000_SW3VOLT, reg);
    reg = pmic_reg_read(dev, PFUZE3000_SW3VOLT);
    if (ret)
        return ret;

    printf("DDR3 Voltage: %u mV\n", PFUZE3000_SW3_REG_to_MVOLTS(reg));

    return 0;
}

#ifdef CONFIG_CMD_BMODE
static const struct boot_mode board_boot_modes[] = {
    /* 4 bit bus width */
    {"sd2",	 MAKE_CFGVAL(0x40, 0x28, 0x00, 0x00)},
    {"sd3",	 MAKE_CFGVAL(0x40, 0x30, 0x00, 0x00)},
    /* 8 bit bus width */
    {"emmc", MAKE_CFGVAL(0x60, 0x58, 0x00, 0x00)},
    {NULL,	 0},
};
#endif

int board_late_init(void)
{
#ifdef CONFIG_CMD_BMODE
    add_board_boot_modes(board_boot_modes);
#endif

    env_set("tee", "no");
#ifdef CONFIG_IMX_OPTEE
    env_set("tee", "yes");
#endif

#ifdef CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG
    env_set("board_name", "SANTINO");
    env_set("board_rev", "MX6DL" );
#endif

#ifdef CONFIG_SECO_ENV_MANAGER

if  (is_mx6sdl( ) ) {
    gd->bsp_sources.kern_dev_list            = &kern_dev_imx6dl_list[0];
    gd->bsp_sources.kern_dev_num             = kern_dev_imx6dl_size;
    gd->bsp_sources.fdt_dev_list             = &fdt_dev_imx6dl_list[0];
    gd->bsp_sources.fdt_dev_num              = fdt_dev_imx6dl_size;
#ifdef CONFIG_OF_LIBFDT_OVERLAY
    gd->bsp_sources.fdt_overlay_dev_list     = fdt_overlay_dev_imx6dl_list;
    gd->bsp_sources.fdt_overlay_dev_num      = fdt_overlay_dev_imx6dl_size;
#endif
    gd->bsp_sources.ramfs_dev_list           = &ramfs_dev_imx6dl_list[0];
    gd->bsp_sources.ramfs_dev_num            = ramfs_dev_imx6dl_size;
    gd->bsp_sources.filesystem_dev_list      = &filesystem_dev_imx6dl_list[0];
    gd->bsp_sources.filesystem_dev_num       = filesystem_dev_imx6dl_size;
}
    gd->boot_setup.video_mode_list           = video_mode_list;
    gd->boot_setup.video_mode_num            = video_mode_size;

#endif /* CONFIG_SECO_ENV_MANAGER */

#ifdef CONFIG_ENV_IS_IN_MMC
    board_late_mmc_env_init();
#endif

    return 0;
}

#ifdef CONFIG_FSL_FASTBOOT
#ifdef CONFIG_ANDROID_RECOVERY

#define GPIO_VOL_DN_KEY IMX_GPIO_NR(1, 5)
iomux_v3_cfg_t const recovery_key_pads[] = {
    IOMUX_PADS(PAD_GPIO_5__GPIO1_IO05 | MUX_PAD_CTRL(NO_PAD_CTRL)),
};

int is_recovery_key_pressing(void)
{
    int button_pressed = 0;
    int ret;
    struct gpio_desc desc;

    /* Check Recovery Combo Button press or not. */
    SETUP_IOMUX_PADS(recovery_key_pads);

    ret = dm_gpio_lookup_name("GPIO1_5", &desc);
    if (ret) {
        printf("%s lookup GPIO1_5 failed ret = %d\n", __func__, ret);
        return;
    }

    ret = dm_gpio_request(&desc, "volume_dn_key");
    if (ret) {
        printf("%s request volume_dn_key failed ret = %d\n", __func__, ret);
        return;
    }

    dm_gpio_set_dir_flags(&desc, GPIOD_IS_IN);

    if (dm_gpio_get_value(&desc) == 0) { /* VOL_DN key is low assert */
        button_pressed = 1;
        printf("Recovery key pressed\n");
    }

    return  button_pressed;
}

#endif /*CONFIG_ANDROID_RECOVERY*/

#endif /*CONFIG_FSL_FASTBOOT*/

#ifdef CONFIG_SPL_BUILD
#include <asm/arch/mx6-ddr.h>
#include <spl.h>
#include <linux/libfdt.h>

#include "ddr_config_2x256.h"
#include "ddr_config_2x512.h"

static void spl_dram_init( void ) {

#if defined( CONFIG_SECOMX6_512MB_2x256 )
        ddr_init( mx6dl_32bit_dcd_table, ARRAY_SIZE( mx6dl_32bit_dcd_table ) );
        ddr_init( mx6dl_2x256_dcd_table, ARRAY_SIZE( mx6dl_2x256_dcd_table ) );
#elif defined( CONFIG_SECOMX6_1GB_2x512 )
        ddr_init( mx6dl_64bit_dcd_table, ARRAY_SIZE( mx6dl_64bit_dcd_table ) );
        ddr_init( mx6dl_2x512_dcd_table, ARRAY_SIZE( mx6dl_2x512_dcd_table ) );
#endif
}

void board_init_f(ulong dummy)
{
    /* DDR initialization */
    spl_dram_init();

    /* setup AIPS and disable watchdog */
    arch_cpu_init();

    ccgr_init();
    gpr_init();

    /* iomux and setup of i2c */
    board_early_init_f();

    /* setup GP timer */
    timer_init();

    /* UART clocks enabled and gd valid - init serial console */
    preloader_console_init();

    /* Clear the BSS. */
    memset(__bss_start, 0, __bss_end - __bss_start);

    /* load/boot image from boot device */
    board_init_r(NULL, 0);
}

void reset_cpu (ulong addr){
}
#endif

#ifdef CONFIG_SPL_LOAD_FIT
int board_fit_config_name_match(const char *name)
{
    if (is_mx6dl()) {
        if (!strcmp(name, "seco-imx6dl-santino"))
            return 0;
    }

    return -1;
}
#endif
