#include <common.h>
#include <splash.h>
#include <mmc.h>

static struct splash_location splash_locations[] = {
	{
		.name = "mmc_fs",
		.storage = SPLASH_STORAGE_MMC,
		.flags = SPLASH_STORAGE_FS,
		.devpart = "1:1",
	},
};

int splash_screen_prepare(void)
{
	char devpart[16] = {0};
	int dev_no = (int)mmc_get_env_dev();

	snprintf(devpart, ARRAY_SIZE(devpart), "%d:1", dev_no);
	splash_locations[0].devpart = devpart;

	return splash_source_load(splash_locations,
			  ARRAY_SIZE(splash_locations));
}