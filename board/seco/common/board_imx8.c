/*
 * (C) Copyright 2015 Seco
 *
 * Author: Davide Cardillo <davide.cardillo@seco.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

#include <common.h>
#include <malloc.h>
#include <asm/io.h>
#include <asm/arch/clock.h>
#include <asm/arch/imx-regs.h>
#include <linux/delay.h>

#ifdef CONFIG_ARCH_IMX8
#include <asm/arch-imx8/iomux.h>
#include <asm/arch-imx8/imx8-pins.h>
#endif
#ifdef CONFIG_ARCH_IMX8M
#include <asm/arch/sys_proto.h>
#include <asm/mach-imx/iomux-v3.h>
#include <dt-bindings/pinctrl/pins-imx8mq.h>
#endif
#include <asm/gpio.h>

#include <asm/mach-imx/boot_mode.h>
#include <spl.h>

#include "proto_seco.h"


DECLARE_GLOBAL_DATA_PTR; 


// int dram_init( void ) {
// 	gd->ram_size = imx_ddr_size();
// 	return 0;
// }


/*
 * Do not overwrite the console
 * Use always serial for U-Boot console
 */
int overwrite_console (void) {
	return 1;
}


// void ldo_mode_set(int ldo_bypass) {}

/*  __________________________________________________________________________
 * |                                                                          |
 * |                               BOOT VALIDATE                              |
 * |__________________________________________________________________________|
 */
#ifdef CONFIG_ARCH_IMX8
void boot_validate (int gpio, const iomux_cfg_t gpio_pad) {
#endif
#ifdef CONFIG_ARCH_IMX8M
void boot_validate (int gpio, const iomux_v3_cfg_t gpio_pad) {
#endif

	SETUP_IOMUX_IMX8_PADS (gpio_pad);
        gpio_request(gpio,"boot_validate");
        gpio_direction_output (gpio, 1);
        mdelay(20);

        /* Set Low */
        gpio_set_value (gpio, 0);
        mdelay(20);

        /* Set High */
        gpio_set_value (gpio, 1);
        gpio_free(gpio);

}


/*  __________________________________________________________________________
 * |                                                                          |
 * |                                 BOOT DEVICE                              |
 * |__________________________________________________________________________|
 */
int spl_board_boot_device(enum boot_device boot_dev_spl) {
	switch (boot_dev_spl) {
#if (defined(CONFIG_IMX8MN) || defined(CONFIG_IMX8MP) || defined(CONFIG_IMX8MM)) && \
    !defined(CONFIG_TARGET_SECO_IMX8MM_MYON2)
		case SD1_BOOT:
		case MMC1_BOOT:
		case SD2_BOOT:
		case MMC2_BOOT:
			return BOOT_DEVICE_MMC1;
		case SD3_BOOT:
		case MMC3_BOOT:
			return BOOT_DEVICE_MMC2;
		case QSPI_BOOT:
			return BOOT_DEVICE_NOR;
#else
		case MMC1_BOOT:
			return BOOT_DEVICE_MMC1;
		case SD2_BOOT:
			return BOOT_DEVICE_MMC2;
		case SD3_BOOT:
			return BOOT_DEVICE_MMC2_2;
		case FLEXSPI_BOOT:
			return BOOT_DEVICE_SPI;
#endif
		case USB_BOOT:
			return BOOT_DEVICE_BOARD;
		default:
			return BOOT_DEVICE_NONE;
	}
}


// /*  __________________________________________________________________________
//  * |                                                                          |
//  * |                               BOARD REVISION                             |
//  * |__________________________________________________________________________|
//  */
// int scode_value (int seco_code) {
// 	gpio_direction_input(seco_code);
// 	return gpio_get_value(seco_code);
// }


// #ifdef CONFIG_DM_SECO_MSP430_ECTRL
// struct udevice *get_ectrl_device( ) {
// 	int             ret;
// 	struct udevice  *dev;

// 	ret = uclass_get_device_by_name( UCLASS_I2C_GENERIC, "ectrl@40", &dev );
// 	if ( ret == -ENODEV ) {
// 	 	printf( "Ectrl device not found\n" );
// 		return NULL;
// 	}

// 	return dev;
// }


// int get_ectrl_board_revision( struct udevice *dev, u8 *id, char *major_v, u8 *minor_v ) {
// 	const struct dm_ectrl_ops  *ops;

// 	if ( !dev )
// 		return -1;

// 	ops = dev_get_driver_ops( dev );
// 	if ( !ops ) 
// 		return -1;

//  	return ops->get_board_id( dev, id, major_v, minor_v );
// }
// #endif


// /*  __________________________________________________________________________
//  * |                                                                          |
//  * |                                 DISPLAY INFO                             |
//  * |__________________________________________________________________________|
//  */
// #if CONFIG_VERBOSE_BOARD_NAME
// int checkboard (void) {

// 	custom_check_board( );
// 	printf("Board: %s\n", board_name);

// 	return 0;
// }
// #endif

int print_bootinfo(void) {
	int i;
	char *label = NULL;
	enum boot_device bt_dev = get_boot_device();

	
	for ( i = 0 ; i < ARRAY_SIZE(boot_mem_dev_list) ; i++ ) {
		if ( boot_mem_dev_list[i].boot_dev_id == bt_dev ) {
			label = malloc( sizeof( char ) * 128 );
			sprintf( label,  " - %s", boot_mem_dev_list[i].label );
			break;
		}
	}
	if ( label == NULL ) {
		label = malloc( sizeof ( char ) );
		sprintf( label, "\0" );
	}

	puts("Boot:  ");
	switch (bt_dev) {
	case SD1_BOOT:
		printf("SD0%s\n", label);
		break;
	case SD2_BOOT:
		printf("SD1%s\n", label);
		break;
	case SD3_BOOT:
		printf("SD2%s\n", label);
		break;
	case MMC1_BOOT:
		printf("MMC0%s\n", label);
		break;
	case MMC2_BOOT:
		printf("MMC1%s\n", label);
		break;
	case MMC3_BOOT:
		printf("MMC2%s\n", label);
		break;
	case FLEXSPI_BOOT:
		printf("FLEXSPI%s\n", label);
		break;
	case SATA_BOOT:
		printf("SATA%s\n", label);
		break;
	case NAND_BOOT:
		printf("NAND%s\n", label);
		break;
	case USB_BOOT:
		printf("USB%s\n", label);
		break;
	default:
		printf("Unknown device %u\n", bt_dev);
		break;
	}

	return 0;
}




// /*  __________________________________________________________________________
//  * |                                                                          |
//  * |                                 FILE INFO                                |
//  * |__________________________________________________________________________|
//  */
// int fdt_autodetect( void ) {
// 	char *autodetect_str = env_get ("fdt_autodetect");

// 	if ( (autodetect_str != NULL) && (strcmp(autodetect_str, "yes") == 0) ) {
// 		return 1;
// 	}

// 	return 0;
// }


// void fdt_set( void ) {
// 	if ( fdt_autodetect( ) ) {
// 		printf( "FDT autodetect: " );
// 		if ( is_mx6dl() || is_mx6solo() ) {
// #ifdef SCFG_DEFAULT_FDT_IMX6DL_FILE		
// 			env_set( "fdt_file", SCFG_DEFAULT_FDT_IMX6DL_FILE );
// 			printf ("%s\n", SCFG_DEFAULT_FDT_IMX6DL_FILE );
// #else
// 			printf ("ftd: CPU type not supported\n" );
// #endif
// 		} else if ( is_mx6dq() ) {
// #ifdef SCFG_DEFAULT_FDT_IMX6Q_FILE		
// 			env_set( "fdt_file", SCFG_DEFAULT_FDT_IMX6Q_FILE );
// 			printf ("%s\n", SCFG_DEFAULT_FDT_IMX6Q_FILE );
// #else
// 			printf ("ftd: CPU type not supported\n" );
// #endif
// 		} else if ( is_mx6sx() ) {
// #ifdef SCFG_DEFAULT_FDT_IMX6SX_FILE		
// 			env_set( "fdt_file", SCFG_DEFAULT_FDT_IMX6SX_FILE );
// 			printf ("%s\n", SCFG_DEFAULT_FDT_IMX6SX_FILE );
// #else
// 			printf ("ftd: CPU type not supported\n" );
// #endif
// 		} else if ( is_mx6dqp() ) {
// #ifdef SCFG_DEFAULT_FDT_IMX6QP_FILE		
// 			env_set( "fdt_file", SCFG_DEFAULT_FDT_IMX6QP_FILE );
// 			printf ("%s\n", SCFG_DEFAULT_FDT_IMX6QP_FILE );
// #else
// 			printf ("ftd: CPU type not supported\n" );
// #endif
// 		} else {
// 			printf ("UNKNOW\n" );
// 		}
// 	}
// }


// int mem_autodetect( void ) {
// 	char *autodetect_str = env_get ("mem_autodetect");

// 	if ( (autodetect_str != NULL) && (strcmp(autodetect_str, "yes") == 0) ) {
// 		return 1;
// 	}

// 	return 0;
// }


// void memory_set( void ) {
// 	char memory_buff[50];
// 	ulong size = gd->ram_size / (1 << 20); // get size in MB

// 	if ( mem_autodetect( ) ) {

// 		printf( "kernel MEM  autodetect: " );

// 		if ( size < DEFAULT_CMA_VALUE )
// 			sprintf( memory_buff, "mem=%luM cma=128M", size );
// 		else
// 			sprintf( memory_buff, "mem=%luM", size );

// 		printf( "%s\n", memory_buff );
// 		env_set( "memory", memory_buff );

// 	}
// }
