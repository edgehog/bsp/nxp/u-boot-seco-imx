/*
 * File Name  : seco_eeprom_manager.c
 * Description: Seco eeprom management library
 *
 * Copyright (C) 2024 Seco SpA
 * Author: shirin raeisi <shirin.raeisi@seco.com>
 */


#include <common.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <mmc.h>
#include <i2c.h>
#include <dm/uclass-internal.h>
#include <dm/uclass.h>
#include "seco_eeprom_manager.h"

//#define SECO_EEPROM_DEBUG

u8 g_raw_data[EEPROM_SIZE];
struct eeprom_data g_eeprom_data;
bool g_eeprom_initialized = false;

int print_ssect(struct eeprom_ssect *ssect)
{
	u8 i = 0;
	u8 uid_data[SSECT_DATA_MAX_LEN + 1];

	memset(uid_data, 0x00 , sizeof(uid_data));
	memcpy(uid_data, ssect->data, ssect->len > SSECT_DATA_MAX_LEN? SSECT_DATA_MAX_LEN : ssect->len);

	switch (ssect->uid)
	{
		case UID_BOARD_SERIAL_NUMBER:
			printf("BSN        : %s\n", uid_data);
		break;

		case UID_BOARD_PART_NUMBER:
			printf("BPN:       : %.4s-%.4s-%.4s-%.2s\n", 
								(char*)(uid_data),
								(char*)(uid_data+4),
								(char*)(uid_data+8),
								(char*)(uid_data+12)
					);
		break;

		case UID_SYSTEM_SERIAL_NUMBER:
			printf("SSN        : %s\n", uid_data);
		break;

		case UID_SYSTEM_PART_NUMBER:
			printf("SPN        : %s\n", uid_data);
		break;

		case UID_MAC_ADDRESS:
			printf("MAC        : %pM\n", uid_data);
		break;

		case UID_PANEL_ID:
			printf("PANEL ID   : 0x%02X\n", uid_data[0]);
		break;

		case UID_PANEL_ORIENTATION:
			printf("ORIENTATION: %c\n", uid_data[0]);
		break;

		case UID_TOUCH_ID:
			printf("TOUCH ID   : 0x%02X\n", uid_data[0]);
		break;

		/* This should never happens */
		case UID_NONE:
			printf("UID None\n");
			break;

		default:
			printf("Unknown UID: 0x%02X(%02i) --- ", ssect->uid, ssect->len);
			for (i = 0; i < ssect->len; i++)
				printf("%02X ", uid_data[i]);
			printf("\n");
			break;
	}

	return 0;
}

int populate_uids(void)
{
	u8 pos = EEPROM_OFFSET_DATA;
	struct eeprom_ssect subsection;

	while (pos < EEPROM_SIZE && g_raw_data[pos] != UID_NONE) {
		if(g_raw_data[pos+1] > SSECT_DATA_MAX_LEN){
			printf("Seco Eeprom Manager - UID 0x%02X \
				subsection data lenght (%d) exceeds maximum (%d)\n",
				g_raw_data[pos], g_raw_data[pos+1], SSECT_DATA_MAX_LEN);
			return -1;
		}
		memset(&subsection, 0x00 , sizeof(subsection));
		memcpy(&subsection, &(g_raw_data[pos]), g_raw_data[pos+1]+SSECT_HEADER_LEN);
		pos += subsection.len + SSECT_HEADER_LEN;

		switch (subsection.uid) {
			case UID_BOARD_PART_NUMBER:
			case UID_SYSTEM_SERIAL_NUMBER:
			case UID_SYSTEM_PART_NUMBER:
				break;

			case UID_BOARD_SERIAL_NUMBER:
				memcpy(g_eeprom_data.bsn, &subsection.data, subsection.len);
				break;

			case UID_MAC_ADDRESS:
				memcpy(g_eeprom_data.mac_address, &subsection.data, subsection.len);
				break;

			case UID_PANEL_ID:
				g_eeprom_data.panel_id = subsection.data[0];
				break;

			case UID_PANEL_ORIENTATION:
				g_eeprom_data.orientation = subsection.data[0];
				break;

			case UID_TOUCH_ID:
				g_eeprom_data.touch_id = subsection.data[0];
				break;

			default:
#ifdef CONFIG_SECO_EEPROM_MANAGER_CUSTOM_UIDS
				if(is_custom_uid(subsection.uid))
					populate_custom_uid(subsection);
#endif
				/* Unknown, but valid, UID: nothing to do */
				break;
		}
	};
	
	return 0;
}

void dump_raw_data(void)
{
	int i = 0;
	for (i = 0; i < EEPROM_SIZE; i++)
	{
		if (i % 16 == 0)
			printf("\n");
			
		printf("0x%02X ", g_raw_data[i]);
	}

	printf("\n");
}

int read_raw_data(u8 i2c_bus_number, u8 i2c_addr)
{
	struct udevice *i2c_eeprom;
	struct udevice *i2c_bus;
	int ret = 0;

	uclass_get_device_by_seq(UCLASS_I2C, i2c_bus_number, &i2c_bus);
	ret = dm_i2c_probe(i2c_bus, i2c_addr, 0, &i2c_eeprom);
	if (ret)
	{
		printf("Seco Eeprom Manager - failed to connect - bus:0x%02x, chip address:0x%02x\n", i2c_bus_number, i2c_addr);
		return ret;
	}

	/*reading all 256 bytes including address 0xff*/
	ret = dm_i2c_read(i2c_eeprom, 0, g_raw_data, EEPROM_SIZE);
	if (ret)
	{
		printf("Seco Eeprom Manager - raw read failed: %i\n", ret);
		return ret;
	}

#ifdef SECO_EEPROM_DEBUG
	dump_raw_data();
#endif	

	return ret;
}

int check_crc(void)
{
	u16 i;
	u32 current_crc = 0;

	/* using union for endianness ordering because in the eeprom crc is saved as little endian */
	typedef union
	{
		u8 crc_byte[4];
		u32 crc;

	} calc_crc;
	calc_crc saved;

	// calculate current crc
	for (i = EEPROM_OFFSET_DATA; i <= 0xff; i++)
	{
		current_crc = g_raw_data[i] + current_crc;
	}

	current_crc += g_eeprom_data.version[0] + g_eeprom_data.version[1];

	//copy and ordering saved crc as big endian
	for (i = 0; i < 4; i++)
		saved.crc_byte[i] = g_raw_data[5-i];

	if (current_crc != saved.crc)
	{
		printf("Seco Eeprom Manager - bad CRC (calculated %04X - read %04X) \n", current_crc, saved.crc);
		return -1;
	}

	g_eeprom_data.crc = current_crc;

	return 0;
}

int check_header(void)
{
	if (g_raw_data[0] != VERSION_MAJOR && g_raw_data[1] != VERSION_MINOR)
	{
		printf("Seco Eeprom Manager - version not compatible or eeprom not formatted\n");
		printf("	Version = %02i.%02i\n", g_raw_data[0], g_raw_data[1]);
		return -1;
	}

	g_eeprom_data.version[0] = g_raw_data[0];
	g_eeprom_data.version[1] = g_raw_data[1];

	return 0;
}

// Exported functions
// ---------------------
int seco_eeprom_init(u8 i2c_bus_number, u8 i2c_addr)
{
	memset(g_raw_data, 0, sizeof(g_raw_data));
	memset(&g_eeprom_data, 0, sizeof(g_eeprom_data));

	if(read_raw_data(i2c_bus_number, i2c_addr))
		return -1;

	if(check_header())
		return -1;

	if(check_crc())
		return -1;

	if(populate_uids())
		return -1;
		
	g_eeprom_initialized = true;

	return 0;
}

int seco_eeprom_get_macaddr(u8 *mac_addr)
{
	if (g_eeprom_initialized == false)
		return -1;

	memcpy(mac_addr, g_eeprom_data.mac_address, sizeof(g_eeprom_data.mac_address));

	return 0;
}

int seco_eeprom_get_panel_id(u8 *panel_id)
{
	if (g_eeprom_initialized == false)
		return -1;

	*panel_id = g_eeprom_data.panel_id;

	return 0;
}

int seco_eeprom_get_orientation(u8 *orientation)
{
	if (g_eeprom_initialized == false)
		return -1;

	*orientation = g_eeprom_data.orientation;

	return 0;
}

int seco_eeprom_get_touch_id(u8 *touch_id)
{
	if (g_eeprom_initialized == false)
		return -1;

	*touch_id = g_eeprom_data.touch_id;

	return 0;
}

int seco_eeprom_get_bsn(char* bsn)
{
	if (g_eeprom_initialized == false)
		return -1;

	memcpy(bsn, g_eeprom_data.bsn, ARRAY_SIZE(g_eeprom_data.bsn));

	printf("Copying BSN: %s %s\n", bsn, g_eeprom_data.bsn);

	return 0;
}

int seco_eeprom_print_all(void)
{
	u8 pos = EEPROM_OFFSET_DATA;
	struct eeprom_ssect subsection;

	if (g_eeprom_initialized == false)
		return -1;

	printf("Seco Eeprom Manager - Print all UIDs\n");
	while (pos < EEPROM_SIZE && g_raw_data[pos] != UID_NONE) {
		if(g_raw_data[pos+1] > SSECT_DATA_MAX_LEN){
			printf("Seco Eeprom Manager - UID 0x%02X \
				subsection data lenght (%d) exceeds maximum (%d)\n",
				g_raw_data[pos], g_raw_data[pos+1], SSECT_DATA_MAX_LEN);
			break;
		}
		memset(&subsection, 0x00 , sizeof(subsection));
		memcpy(&subsection, &(g_raw_data[pos]), g_raw_data[pos+1]+SSECT_HEADER_LEN);
		pos += subsection.len + SSECT_HEADER_LEN;

		print_ssect(&subsection);
	};

	return 0;
}
