/*
 * (C) Copyright 2015 Seco
 *
 * Author: Davide Cardillo <davide.cardillo@seco.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */
#include <common.h>
#include <command.h>
#include <asm/arch/sys_proto.h>
#include <linux/errno.h>
#include <asm/io.h>
#include <stdbool.h>
#include <mmc.h>
#include <env.h>
#include <seco/env_common.h>


static int check_mmc_autodetect(void)
{
	char *autodetect_str = env_get("mmcautodetect");

	if ((autodetect_str != NULL) &&
		(strcmp(autodetect_str, "yes") == 0)) {
		return 1;
	}

	return 0;
}


__weak int mmc_map_to_kernel_blk(int dev_no)
{
	return dev_no;
}

void board_late_mmc_env_init(void)
{
	char cmd[32];
	char mmcblk[32];
	int dev_no = (int)mmc_get_env_dev();
	char *str = simple_itoa( dev_no );

	if (!check_mmc_autodetect())
		return;

	env_set_ulong("mmcdev", dev_no);

	SAVE_KERNEL_DEVICE_ID( str );
	SAVE_FDT_DEVICE_ID( str );
	SAVE_RAMFS_DEVICE_ID( str );
	SAVE_FILESYSTEM_DEVICE_ID( str );
	SAVE_BOOTSCRIPT_DEVICE_ID( str );

	/* Set mmcblk env */
	sprintf(mmcblk, "/dev/mmcblk%dp2 rootwait rw",
		mmc_map_to_kernel_blk(dev_no));
	env_set("mmcroot", mmcblk);

	sprintf(cmd, "mmc dev %d", dev_no);
	run_command(cmd, 0);
}
