/*
 * (C) Copyright 2015 Seco
 *
 * Author: Davide Cardillo <davide.cardillo@seco.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

#include <asm/arch/clock.h>
#include <asm/arch/imx-regs.h>
#include <asm/arch/iomux.h>
#include <asm/arch/mx6-pins.h>
#include <linux/errno.h>
#include <asm/gpio.h>
#include <asm/mach-imx/iomux-v3.h>
#include <asm/mach-imx/boot_mode.h>
#include <mmc.h>
#include <miiphy.h>
#include <netdev.h>
#include <asm/arch/crm_regs.h>
#include <asm/io.h>
#include <asm/arch/sys_proto.h>
#include <power/regulator.h>
#include <micrel.h>


#include "imx6qdl_928.h"
#include "../common/proto_seco.h"

#ifdef CONFIG_SECO_ENV_MANAGER
	#include <seco/env_common.h>
	#include "env_conf.h"
#endif 

DECLARE_GLOBAL_DATA_PTR;


/*  __________________________________________________________________________
 * |                                                                          |
 * |                                   UART                                   |
 * |__________________________________________________________________________|
 */
static void setup_iomux_uart( void ) {
	SETUP_IOMUX_PADS( uart_debug_pads );
}
/*  __________________________________________________________________________
 * |__________________________________________________________________________|
 */


/*  __________________________________________________________________________
 * |                                                                          |
 * |                             BOOT VALIDATE                                |
 * |__________________________________________________________________________|
 */
#ifdef CONFIG_SPL_BUILD
#define BOOT_VALIDATE_GPIO                       IMX_GPIO_NR(2, 13)
static void validate( void ) {
	boot_validate( BOOT_VALIDATE_GPIO, gpio_boot_validate_pad, NUM_OF_PADS(gpio_boot_validate_pad) );
}
#endif
/*  __________________________________________________________________________
 * |__________________________________________________________________________|
 */


/*  __________________________________________________________________________
 * |                                                                          |
 * |                                   USDHC                                  |
 * |__________________________________________________________________________|
 */
#ifdef CONFIG_FSL_USDHC

/* map the usdhc controller id to the devno given to the board device */
int usdhc_devno[4] = { 1, -1, 0, 2};

boot_mem_dev_t boot_mem_dev_list[SECO_NUM_BOOT_DEV] = {
	{ 0x4, 0x5, -1,   0, -1, "External SD" },
	{ 0x4, 0x5, -1,   3, -1, "OnBoard uSD" },
	{ 0x6, 0x7, -1,  -1, -1, "eMMC" },
};

#ifdef CONFIG_SPL_BUILD

#define USDHC1_CD_GPIO	                         IMX_GPIO_NR(6, 15)
#define USDHC4_CD_GPIO	                         IMX_GPIO_NR(2, 6)

/* USDHC map:
 * 	USDHC4  -->  eMMC          -->  FSL_SDHC: 0
 * 	USDHC3  -->  internaluSD   -->  FSL_SDHC: 1
 * 	USDHC1  -->  external uSD  -->  FSL_SDHC: 2
 */

struct fsl_esdhc_cfg usdhc_cfg[CONFIG_SYS_FSL_USDHC_NUM] = {
	{ USDHC3_BASE_ADDR, 0, 8 },
	{ USDHC1_BASE_ADDR, 0, 4 },
	{ USDHC4_BASE_ADDR, 0, 4 },
};


struct usdhc_l usdhc_list_spl[CONFIG_SYS_FSL_USDHC_NUM] = {
	{usdhc3_pads, ARRAY_SIZE(usdhc3_pads)/2, -1},
	{usdhc1_pads, ARRAY_SIZE(usdhc1_pads)/2, USDHC1_CD_GPIO},
	{usdhc4_pads, ARRAY_SIZE(usdhc4_pads)/2, USDHC4_CD_GPIO},
};

enum mxc_clock usdhc_clk[CONFIG_SYS_FSL_USDHC_NUM] = {
	MXC_ESDHC3_CLK,
	MXC_ESDHC_CLK,
	MXC_ESDHC4_CLK,
};




int board_mmc_getcd (struct mmc *mmc) {
	struct fsl_esdhc_cfg *cfg = (struct fsl_esdhc_cfg *)mmc->priv;
	int ret = 0;
	switch (cfg->esdhc_base) {
		case USDHC1_BASE_ADDR:
			ret = !gpio_get_value( USDHC1_CD_GPIO );
			break;
		case USDHC4_BASE_ADDR:
			ret = !gpio_get_value( USDHC4_CD_GPIO );
			break;
		case USDHC3_BASE_ADDR:
			ret = 1; /* eMMC/uSDHC4 is always present */
			break;
	}
	return ret;
}


#if defined(CONFIG_MX6QDL)

int board_mmc_init( struct bd_info *bis ) {
	struct src *psrc = (struct src *)SRC_BASE_ADDR;
	unsigned reg = readl(&psrc->sbmr1) >> 11;
	/*
	 * Upon reading BOOT_CFG register the following map is done:
	 * Bit 11 and 12 of BOOT_CFG register can determine the current
	 * mmc port
	 * 0x2                  SD3
	 * 0x3                  SD4
	 */
	int ret = 0;
	int index = 0;
	
	switch (reg & 0x3) {
		case 0:
			index = 1;
			break;
		case 2:
			index = 0;
			break;
		case 3:
			index = 2;
			break;
	}


	print_boot_device( );
	
	imx_iomux_v3_setup_multiple_pads( usdhc_list_spl[index].pad, 
				usdhc_list_spl[index].num );	
	usdhc_cfg[index].sdhc_clk = mxc_get_clock( usdhc_clk[index] );
	gd->arch.sdhc_clk = usdhc_cfg[index].sdhc_clk;
	ret = fsl_esdhc_initialize( bis, &usdhc_cfg[index] );

	if ( ret ) 
		printf( "Warning: failed to initialize mmc dev %d\n", index );

	return ret;
}

#endif
#endif	/*  CONFIG_SPL_BUILD  */
#endif  /*  CONFIG_FSL_USDHC  */
/*  __________________________________________________________________________
 * |__________________________________________________________________________|
 */


/*  __________________________________________________________________________
 * |                                                                          |
 * |                                   ETHERNET                               |
 * |__________________________________________________________________________|
 */
int board_phy_config (struct phy_device *phydev) {
	// phy_write (phydev, MDIO_DEVAD_NONE, MII_BMCR, 0x2100);
	// phydev->autoneg = 0;

	/* Skew setting */
	ksz9031_phy_extended_write(phydev, 0x02,
			MII_KSZ9031_EXT_RGMII_CTRL_SIG_SKEW,
			MII_KSZ9031_MOD_DATA_NO_POST_INC, 0x0000);
	/* rx data pad skew - devaddr = 0x02, register = 0x05 */
	ksz9031_phy_extended_write(phydev, 0x02,
			MII_KSZ9031_EXT_RGMII_RX_DATA_SKEW,
			MII_KSZ9031_MOD_DATA_NO_POST_INC, 0x0000);
	/* tx data pad skew - devaddr = 0x02, register = 0x05 */
	ksz9031_phy_extended_write(phydev, 0x02,
			MII_KSZ9031_EXT_RGMII_TX_DATA_SKEW,
			MII_KSZ9031_MOD_DATA_NO_POST_INC, 0x0000);
	/* gtx and rx clock pad skew - devaddr = 0x02, register = 0x08 */
	ksz9031_phy_extended_write(phydev, 0x02,
			MII_KSZ9031_EXT_RGMII_CLOCK_SKEW,
			MII_KSZ9031_MOD_DATA_NO_POST_INC, 0x03FF);

	if (phydev->drv->config)
		phydev->drv->config(phydev);

	return 0;
}
/*  __________________________________________________________________________
 * |__________________________________________________________________________|
 */


/*  __________________________________________________________________________
 * |                                                                          |
 * |                              DISPLAY SETTINGS                            |
 * |__________________________________________________________________________|
 */
#ifdef CONFIG_VIDEO_IPUV3

char *display_name;


static iomux_v3_cfg_t const lvds_pads[] = {
	IOMUX_PADS(PAD_GPIO_4__GPIO1_IO04 | MUX_PAD_CTRL(GPIO_LVDS)),
	IOMUX_PADS(PAD_GPIO_2__GPIO1_IO02 | MUX_PAD_CTRL(GPIO_LVDS)),
};


#define LVDS_BACKLIGHT_GPIO                      IMX_GPIO_NR(4, 29) 
static iomux_v3_cfg_t const lvds_backlight_pads[] = {
	IOMUX_PADS(PAD_DISP0_DAT8__GPIO4_IO29 | MUX_PAD_CTRL(NO_PAD_CTRL)),
};



void setup_iomux_backlight( int en ) {
	SETUP_IOMUX_PADS( lvds_backlight_pads );
	gpio_request( LVDS_BACKLIGHT_GPIO, "lvds backlight" );
	gpio_direction_output ( LVDS_BACKLIGHT_GPIO, !!en );
}


void enable_lvds( struct display_info_t const *dev ) {
	struct iomuxc *iomux = (struct iomuxc *) IOMUXC_BASE_ADDR;
	u32 reg = readl( &iomux->gpr[2] );
	reg |= IOMUXC_GPR2_DATA_WIDTH_CH0_24BIT;
	writel(reg, &iomux->gpr[2]);

	setup_lvds( dev );

	SETUP_IOMUX_PADS(lvds_pads);
	gpio_request( IMX_GPIO_NR(1, 2), "lvds panel on" );
	gpio_request( IMX_GPIO_NR(1, 4), "lvds backlight on" );
	gpio_direction_output( IMX_GPIO_NR(1, 2), 1 );
	gpio_direction_output( IMX_GPIO_NR(1, 4), 1 );

	setup_iomux_backlight( 1 );
}


void disable_lvds( struct display_info_t const *dev ) {
	struct iomuxc *iomux = (struct iomuxc *)IOMUXC_BASE_ADDR;

	int reg = readl( &iomux->gpr[2] );

	reg &= ~(IOMUXC_GPR2_LVDS_CH0_MODE_MASK |
		 IOMUXC_GPR2_LVDS_CH1_MODE_MASK);

	writel(reg, &iomux->gpr[2]);

	SETUP_IOMUX_PADS(lvds_pads);
	gpio_request( IMX_GPIO_NR(1, 2), "lvds panel on" );
	gpio_request( IMX_GPIO_NR(1, 4), "lvds backlight on" );

	setup_iomux_backlight( 0 );
	gpio_direction_output( IMX_GPIO_NR(1, 2), 0 );
	gpio_direction_output( IMX_GPIO_NR(1, 4), 0 );

	gpio_free( IMX_GPIO_NR(1, 2) );
	gpio_free( IMX_GPIO_NR(1, 4) );
}


#endif
/*  __________________________________________________________________________
 * |__________________________________________________________________________|
 */


int board_early_init_f( void ) {
	setup_iomux_uart();

#ifndef CONFIG_SPL_BUILD
#if defined(CONFIG_VIDEO_IPUV3)
	setup_display();
#endif
#endif

	return 0;
}

bool is_revE = false;

int board_init( void ) {
	u8 *boardrev = NULL;
#ifndef CONFIG_SPL_BUILD
	struct udevice *dev;
	u8             id;
	char           major_v;
	u8             minor_v;
#endif

	/* address of boot parameters */
	gd->bd->bi_boot_params = PHYS_SDRAM + 0x100;

#if defined(CONFIG_DM_REGULATOR)
	regulators_enable_boot_on( false );
#endif

#ifndef CONFIG_SPL_BUILD
	dev = get_ectrl_device( );
	if ( !get_ectrl_board_revision( dev, &id, &major_v, &minor_v ) )
		printf( "ID: 9%X, rev %c%X\n", id, major_v, minor_v );
	else
		printf( "Unable to read ID from ectrl device\n" );

	if(major_v =='e'){
		is_revE = true;
		/* Set right HW configuration as soon as possible */
		SETUP_IOMUX_PADS(gpio_pads);
		gpio_request( IMX_GPIO_NR(2, 4), "revE PLT_RST#" );
		gpio_direction_output(IMX_GPIO_NR(2, 4), 1);
	}
#endif
	print_boot_device( );

	return 0;
}

#ifdef CONFIG_CMD_BMODE
static const struct boot_mode board_boot_modes[] = {
	/* 4 bit bus width */
	{"sd1",	 MAKE_CFGVAL(0x40, 0x28, 0x00, 0x00)},
	{"sd3",	 MAKE_CFGVAL(0x40, 0x30, 0x00, 0x00)},
	/* 8 bit bus width */
	{"emmc", MAKE_CFGVAL(0x60, 0x58, 0x00, 0x00)},
	{NULL,	 0},
};
#endif


void fdt_set_revE( void ) {
	if(fdt_autodetect( ) ) {
		printf( "FDT autodetect: " );
		if ( is_mx6dl() || is_mx6solo() ) {
#ifdef FDT_928revE_IMX6DL_FILENAME
			env_set( "fdt_file", FDT_928revE_IMX6DL_FILENAME );
			printf ("%s\n", FDT_928revE_IMX6DL_FILENAME );
#else
			printf ("ftd: CPU type not supported\n" );
#endif
		} else if ( is_mx6dq() ) {
#ifdef FDT_928revE_IMX6Q_FILENAME
			env_set( "fdt_file", FDT_928revE_IMX6Q_FILENAME );
			printf ("%s\n", FDT_928revE_IMX6Q_FILENAME );
#else
			printf ("ftd: CPU type not supported\n" );
#endif
		} else if ( is_mx6sx() ) {
#ifdef FDT_928revE_IMX6SX_FILENAME
			env_set( "fdt_file", FDT_928revE_IMX6SX_FILENAME );
			printf ("%s\n", FDT_928revE_IMX6SX_FILENAME );
#else
			printf ("ftd: CPU type not supported\n" );
#endif
		} else if ( is_mx6dqp() ) {
#ifdef FDT_928revE_IMX6QP_FILENAME
			env_set( "fdt_file", FDT_928revE_IMX6QP_FILENAME );
			printf ("%s\n", FDT_928revE_IMX6QP_FILENAME );
#else
			printf ("ftd: CPU type not supported\n" );
#endif
		}
	}else {
		printf ("UNKNOW\n" );
	}
}

int board_late_init( void ) {
#ifdef CONFIG_CMD_BMODE
	add_board_boot_modes( board_boot_modes );
#endif


#ifdef CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG

	if ( is_mx6dqp( ) )
		env_set( "board_rev", "MX6QP" );
	else if ( is_mx6dq( ) )
		env_set( "board_rev", "MX6Q" );
	else if  (is_mx6sdl( ) )
		env_set( "board_rev", "MX6DL" );
#endif

#ifdef CONFIG_SECO_ENV_MANAGER

if ( is_mx6dqp( ) || is_mx6dq( ) ) {
	gd->bsp_sources.kern_dev_list            = &kern_dev_imx6q_list[0];
	gd->bsp_sources.kern_dev_num             = kern_dev_imx6q_size;
	if(is_revE)
		gd->bsp_sources.fdt_dev_list             = &fdt_dev_imx6q_list_revE[0];
	else
		gd->bsp_sources.fdt_dev_list             = &fdt_dev_imx6q_list[0];
	gd->bsp_sources.fdt_dev_num              = fdt_dev_imx6q_size;
#ifdef CONFIG_OF_LIBFDT_OVERLAY
	gd->bsp_sources.fdt_overlay_dev_list     = fdt_overlay_dev_imx6q_list;
	gd->bsp_sources.fdt_overlay_dev_num      = fdt_overlay_dev_imx6q_size;
#endif
	gd->bsp_sources.ramfs_dev_list           = &ramfs_dev_imx6q_list[0];
	gd->bsp_sources.ramfs_dev_num            = ramfs_dev_imx6q_size;
	gd->bsp_sources.filesystem_dev_list      = &filesystem_dev_imx6q_list[0];
	gd->bsp_sources.filesystem_dev_num       = filesystem_dev_imx6q_size;
}else if  (is_mx6sdl( ) ) {
	gd->bsp_sources.kern_dev_list            = &kern_dev_imx6dl_list[0];
	gd->bsp_sources.kern_dev_num             = kern_dev_imx6dl_size;
	if(is_revE)
		gd->bsp_sources.fdt_dev_list         = &fdt_dev_imx6dl_list_revE[0];
	else
		gd->bsp_sources.fdt_dev_list         = &fdt_dev_imx6dl_list[0];
	gd->bsp_sources.fdt_dev_num              = fdt_dev_imx6dl_size;
#ifdef CONFIG_OF_LIBFDT_OVERLAY
	gd->bsp_sources.fdt_overlay_dev_list     = fdt_overlay_dev_imx6dl_list;
	gd->bsp_sources.fdt_overlay_dev_num      = fdt_overlay_dev_imx6dl_size;
#endif
	gd->bsp_sources.ramfs_dev_list           = &ramfs_dev_imx6dl_list[0];
	gd->bsp_sources.ramfs_dev_num            = ramfs_dev_imx6dl_size;
	gd->bsp_sources.filesystem_dev_list      = &filesystem_dev_imx6dl_list[0];
	gd->bsp_sources.filesystem_dev_num       = filesystem_dev_imx6dl_size;
}	

	gd->boot_setup.video_mode_list           = video_mode_list;
	gd->boot_setup.video_mode_num            = video_mode_size;
#ifdef CONFIG_OF_LIBFDT_OVERLAY
	if(is_revE){
		gd->boot_setup.overlay_peripheral_list   = overlay_peripheral_list_revE;
		gd->boot_setup.overlay_peripheral_num    = ARRAY_SIZE(overlay_peripheral_list_revE);
	}else{
		gd->boot_setup.overlay_peripheral_list   = overlay_peripheral_list;
		gd->boot_setup.overlay_peripheral_num    = ARRAY_SIZE(overlay_peripheral_list);
	}
#endif

#endif /* CONFIG_SECO_ENV_MANAGER */

#ifdef CONFIG_ENV_IS_IN_MMC
	board_late_mmc_env_init( );
#endif

	if(is_revE)
		fdt_set_revE( );
	else
		fdt_set( );

	memory_set( );

	return 0;
}


#ifdef CONFIG_SPL_BUILD

#include <spl.h>
#include <linux/libfdt.h>
#include <init.h>

#include "ddr_config_4x256.h"
#include "ddr_config_4x512.h"
#include "ddr_config_8x512.h"

#ifdef CONFIG_SPL_OS_BOOT
int spl_start_uboot( void ) {
	return 0;
}
#endif


static void spl_dram_init( void ) {

#if defined( CONFIG_SECOMX6_1GB_4x256 )
	if ( is_mx6dq( ) ) {
		ddr_init( mx6qd_64bit_dcd_table, ARRAY_SIZE( mx6qd_64bit_dcd_table ) );
		ddr_init( mx6qd_4x256_dcd_table, ARRAY_SIZE( mx6qd_4x256_dcd_table ) );
	} else if ( is_mx6dl( ) ) {
		ddr_init( mx6dl_64bit_dcd_table, ARRAY_SIZE( mx6dl_64bit_dcd_table ) );
		ddr_init( mx6dl_4x256_dcd_table, ARRAY_SIZE( mx6dl_4x256_dcd_table ) );
	} else if ( is_mx6solo( ) ) {
	}
#endif

#if defined( CONFIG_SECOMX6_2GB_4x512 )
	if ( is_mx6dq( ) ) {
		ddr_init( mx6qd_64bit_dcd_table, ARRAY_SIZE( mx6qd_64bit_dcd_table ) );
		ddr_init( mx6qd_4x512_dcd_table, ARRAY_SIZE( mx6qd_4x512_dcd_table ) );
	} else if ( is_mx6dl( ) ) {
		ddr_init( mx6dl_64bit_dcd_table, ARRAY_SIZE( mx6dl_64bit_dcd_table ) );
		ddr_init( mx6dl_4x512_dcd_table, ARRAY_SIZE( mx6dl_4x512_dcd_table ) );
	} else if ( is_mx6solo( ) ) {
	} else if ( is_mx6dqp( ) ) {
	}
#endif

#if defined( CONFIG_SECOMX6_4GB_8x512 )
	if ( is_mx6dq( ) ) {
		ddr_init( mx6qd_64bit_dcd_table, ARRAY_SIZE( mx6qd_64bit_dcd_table ) );
		ddr_init( mx6qd_8x512_dcd_table, ARRAY_SIZE( mx6qd_8x512_dcd_table ) );
	} else if ( is_mx6dl( ) ) {
		ddr_init( mx6dl_64bit_dcd_table, ARRAY_SIZE( mx6dl_64bit_dcd_table ) );
		ddr_init( mx6dl_8x512_dcd_table, ARRAY_SIZE( mx6dl_8x512_dcd_table ) );
	} else if ( is_mx6solo( ) ) {
	}
#endif

}


void board_init_f( ulong dummy ) {
	/* DDR initialization */
	spl_dram_init();

	/* setup AIPS and disable watchdog */
	arch_cpu_init();

	ccgr_init();
	gpr_init();

	/* iomux and setup of i2c */
	board_early_init_f();

	/* setup GP timer */
	timer_init();

	/*  boot validate  */
	validate ();

	/* UART clocks enabled and gd valid - init serial console */
	preloader_console_init();

	/* Clear the BSS. */
	memset(__bss_start, 0, __bss_end - __bss_start);

	/* load/boot image from boot device */
	board_init_r(NULL, 0);
}

void reset_cpu (ulong addr){
}

#endif

#ifdef CONFIG_SPL_LOAD_FIT
int board_fit_config_name_match(const char *name)
{
	if (is_mx6dq()) {
		if (!strcmp(name, "seco-imx6q-928"))
			return 0;
	} else if ( is_mx6dl( ) || is_mx6solo( ) ) {
		if (!strcmp(name, "seco-imx6dl-928"))
			return 0;
	} else if ( is_mx6dqp( ) ) {
		if (!strcmp(name, "seco-imx6qp-928"))
			return 0;
	}

	return -1;
}
#endif
