/*
 * (C) Copyright 2015 Seco
 *
 * Author: Davide Cardillo <davide.cardillo@seco.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

#include <common.h>
#include <command.h>
#include <env.h>


#include <seco/env_common.h>
#include <configs/seco_mx6_dtbo.h>


#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

 
/* *********************************** IMX6Q *********************************** */

data_boot_dev_t kern_dev_imx6q_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_KERNEL_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD,     STR(MACRO_ENV_KERNEL_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD,      LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_SD_EXT,   SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_KERNEL_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD_EXT,  LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_KERNEL_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_KERNEL_SRC_TFTP),     "",                       LOAD_ADDR_KERNEL_REMOTE_DEV, SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_SATA,     SECO_DEV_LABEL_SATA,   STR(MACRO_ENV_KERNEL_SRC_SATA),     SCFG_BOOT_DEV_ID_SATA,    LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
};

size_t kern_dev_imx6q_size = sizeof( kern_dev_imx6q_list ) / sizeof( kern_dev_imx6q_list[0] );


data_boot_dev_t fdt_dev_imx6q_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FDT_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX6Q_FILENAME },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD,     STR(MACRO_ENV_FDT_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD,      LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX6Q_FILENAME },
	{ SECO_DEV_TYPE_SD_EXT,   SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_FDT_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD_EXT,  LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX6Q_FILENAME },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FDT_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX6Q_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_FDT_SRC_TFTP),     "",                       LOAD_ADDR_FDT_REMOTE_DEV, SCFG_DEFAULT_FDT_IMX6Q_FILENAME },
	{ SECO_DEV_TYPE_SATA,     SECO_DEV_LABEL_SATA,   STR(MACRO_ENV_FDT_SRC_SATA),     SCFG_BOOT_DEV_ID_SATA,    LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX6Q_FILENAME },
};

data_boot_dev_t fdt_dev_imx6q_list_revE [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FDT_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_FDT_LOCAL_DEV,  FDT_928revE_IMX6Q_FILENAME },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD,     STR(MACRO_ENV_FDT_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD,      LOAD_ADDR_FDT_LOCAL_DEV,  FDT_928revE_IMX6Q_FILENAME },
	{ SECO_DEV_TYPE_SD_EXT,   SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_FDT_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD_EXT,  LOAD_ADDR_FDT_LOCAL_DEV,  FDT_928revE_IMX6Q_FILENAME },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FDT_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_FDT_LOCAL_DEV,  FDT_928revE_IMX6Q_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_FDT_SRC_TFTP),     "",                       LOAD_ADDR_FDT_REMOTE_DEV, FDT_928revE_IMX6Q_FILENAME },
	{ SECO_DEV_TYPE_SATA,     SECO_DEV_LABEL_SATA,   STR(MACRO_ENV_FDT_SRC_SATA),     SCFG_BOOT_DEV_ID_SATA,    LOAD_ADDR_FDT_LOCAL_DEV,  FDT_928revE_IMX6Q_FILENAME },
};

size_t fdt_dev_imx6q_size = sizeof( fdt_dev_imx6q_list ) / sizeof( fdt_dev_imx6q_list[0] );


#ifdef CONFIG_OF_LIBFDT_OVERLAY
data_boot_dev_t fdt_overlay_dev_imx6q_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    "", "" },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD,     STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD,      "", "" },
	{ SECO_DEV_TYPE_SD_EXT,   SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD_EXT,  "", "" },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FDT_OVERLAY_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     "", "" },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_FDT_OVERLAY_SRC_TFTP),     "",                       "", "" },
	{ SECO_DEV_TYPE_SATA,     SECO_DEV_LABEL_SATA,   STR(MACRO_ENV_FDT_OVERLAY_SRC_SATA),     SCFG_BOOT_DEV_ID_SATA,    "", "" },
};

size_t fdt_overlay_dev_imx6q_size = sizeof( fdt_overlay_dev_imx6q_list ) / sizeof( fdt_overlay_dev_imx6q_list[0] );
#endif  /* CONFIG_OF_LIBFDT_OVERLAY */


data_boot_dev_t ramfs_dev_imx6q_list [] = {
	{ SECO_DEV_TYPE_NONE,     SECO_DEV_LABEL_NONE,   "0x0",                             "0",                      LOAD_ADDR_RAMFS_LOCAL_DEV,    ""                 },
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_RAMFS_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_RAMFS_LOCAL_DEV,    SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD,     STR(MACRO_ENV_RAMFS_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD,      LOAD_ADDR_RAMFS_LOCAL_DEV,    SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_SD_EXT,   SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_RAMFS_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD_EXT,  LOAD_ADDR_RAMFS_LOCAL_DEV,    SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_RAMFS_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_RAMFS_LOCAL_DEV,    SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_RAMFS_SRC_TFTP),     "",                       LOAD_ADDR_RAMFS_REMOTE_DEV,   SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_SATA,     SECO_DEV_LABEL_SATA,   STR(MACRO_ENV_RAMFS_SRC_SATA),     SCFG_BOOT_DEV_ID_SATA,    LOAD_ADDR_RAMFS_LOCAL_DEV,    SCFG_RAMFS_FILENAME },
};

size_t ramfs_dev_imx6q_size = sizeof( ramfs_dev_imx6q_list ) / sizeof( ramfs_dev_imx6q_list[0] );


data_boot_dev_t filesystem_dev_imx6q_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FS_SRC_USDHCI),   SCFG_ROOT_DEV_ID_EMMC,    "", "" },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD,     STR(MACRO_ENV_FS_SRC_USDHCI),   SCFG_ROOT_DEV_ID_SD,      "", "" },
	{ SECO_DEV_TYPE_SD_EXT,   SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_FS_SRC_USDHCI),   SCFG_ROOT_DEV_ID_SD_EXT,  "", "" },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FS_SRC_USB),      SCFG_ROOT_DEV_ID_USB,     "", "" },
	{ SECO_DEV_TYPE_NFS,      SECO_DEV_LABEL_NFS,    STR(MACRO_ENV_FS_SRC_NFS),      "",                       "", "" },
	{ SECO_DEV_TYPE_SATA,     SECO_DEV_LABEL_SATA,   STR(MACRO_ENV_FS_SRC_SATA),     SCFG_ROOT_DEV_ID_SATA,    "", "" },
};

size_t filesystem_dev_imx6q_size = sizeof( filesystem_dev_imx6q_list ) / sizeof( filesystem_dev_imx6q_list[0] );



/* *********************************** IMX6DL *********************************** */

data_boot_dev_t kern_dev_imx6dl_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_KERNEL_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD,     STR(MACRO_ENV_KERNEL_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD,      LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_SD_EXT,   SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_KERNEL_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD_EXT,  LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_KERNEL_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_KERNEL_SRC_TFTP),     "",                       LOAD_ADDR_KERNEL_REMOTE_DEV, SCFG_KERNEL_FILENAME },
};

size_t kern_dev_imx6dl_size = sizeof( kern_dev_imx6dl_list ) / sizeof( kern_dev_imx6dl_list[0] );


data_boot_dev_t fdt_dev_imx6dl_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FDT_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX6DL_FILENAME },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD,     STR(MACRO_ENV_FDT_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD,      LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX6DL_FILENAME },
	{ SECO_DEV_TYPE_SD_EXT,   SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_FDT_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD_EXT,  LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX6DL_FILENAME },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FDT_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX6DL_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_FDT_SRC_TFTP),     "",                       LOAD_ADDR_FDT_REMOTE_DEV, SCFG_DEFAULT_FDT_IMX6DL_FILENAME },
};

data_boot_dev_t fdt_dev_imx6dl_list_revE [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FDT_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_FDT_LOCAL_DEV,  FDT_928revE_IMX6DL_FILENAME },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD,     STR(MACRO_ENV_FDT_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD,      LOAD_ADDR_FDT_LOCAL_DEV,  FDT_928revE_IMX6DL_FILENAME },
	{ SECO_DEV_TYPE_SD_EXT,   SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_FDT_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD_EXT,  LOAD_ADDR_FDT_LOCAL_DEV,  FDT_928revE_IMX6DL_FILENAME },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FDT_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_FDT_LOCAL_DEV,  FDT_928revE_IMX6DL_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_FDT_SRC_TFTP),     "",                       LOAD_ADDR_FDT_REMOTE_DEV, FDT_928revE_IMX6DL_FILENAME },
};

size_t fdt_dev_imx6dl_size = sizeof( fdt_dev_imx6dl_list ) / sizeof( fdt_dev_imx6dl_list[0] );


#ifdef CONFIG_OF_LIBFDT_OVERLAY
data_boot_dev_t fdt_overlay_dev_imx6dl_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    "", "" },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD,     STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD,      "", "" },
	{ SECO_DEV_TYPE_SD_EXT,   SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD_EXT,  "", "" },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FDT_OVERLAY_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     "", "" },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_FDT_OVERLAY_SRC_TFTP),     "",                       "", "" },
};

size_t fdt_overlay_dev_imx6dl_size = sizeof( fdt_overlay_dev_imx6dl_list ) / sizeof( fdt_overlay_dev_imx6dl_list[0] );
#endif  /* CONFIG_OF_LIBFDT_OVERLAY  */


data_boot_dev_t ramfs_dev_imx6dl_list [] = {
	{ SECO_DEV_TYPE_NONE,     SECO_DEV_LABEL_NONE,   "0x0",                             "0",                      LOAD_ADDR_RAMFS_LOCAL_DEV,    ""                 },
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_RAMFS_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_RAMFS_LOCAL_DEV,    SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD,     STR(MACRO_ENV_RAMFS_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD,      LOAD_ADDR_RAMFS_LOCAL_DEV,    SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_SD_EXT,   SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_RAMFS_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD_EXT,  LOAD_ADDR_RAMFS_LOCAL_DEV,    SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_RAMFS_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_RAMFS_LOCAL_DEV,    SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_RAMFS_SRC_TFTP),     "",                       LOAD_ADDR_RAMFS_REMOTE_DEV,   SCFG_RAMFS_FILENAME },
};

size_t ramfs_dev_imx6dl_size = sizeof( ramfs_dev_imx6dl_list ) / sizeof( ramfs_dev_imx6dl_list[0] );


data_boot_dev_t filesystem_dev_imx6dl_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FS_SRC_USDHCI),   SCFG_ROOT_DEV_ID_EMMC,    "", "" },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD,     STR(MACRO_ENV_FS_SRC_USDHCI),   SCFG_ROOT_DEV_ID_SD,      "", "" },
	{ SECO_DEV_TYPE_SD_EXT,   SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_FS_SRC_USDHCI),   SCFG_ROOT_DEV_ID_SD_EXT,  "", "" },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FS_SRC_USB),      SCFG_ROOT_DEV_ID_USB,     "", "" },
	{ SECO_DEV_TYPE_NFS,      SECO_DEV_LABEL_NFS,    STR(MACRO_ENV_FS_SRC_NFS),      "",                       "", "" },
};

size_t filesystem_dev_imx6dl_size = sizeof( filesystem_dev_imx6dl_list ) / sizeof( filesystem_dev_imx6dl_list[0] );








static panel_parameters_t lvds_video_spec_list [] = {
	{ "WVGA	   [800x480]",   "LDB-WVGA",    "RGB666",      "datamap=spwg",   -1, NULL,   1 },
	{ "SVGA	   [800x600]",   "LDB-SVGA",    "RGB666",      "datamap=spwg",   -1, NULL,   1 },
	{ "XGA	   [1024x768]",  "LDB-XGA",     "RGB666",      "datamap=spwg",   -1, NULL,   1 },
	{ "WXGA	   [1368x768]",  "LDB-WXGA",    "RGB24",       "datamap=jeida",  -1, NULL,   1 },
	{ "WXGAP60 [1280x800]",  "LDB-1280P60", "RGB24,bpp=32","datamap=spwg",   -1, NULL,   1 },
	{ "SXGA	   [1280x1024]", "LDB-SXGA",    "RGB24",       "datamap=jeida",  -1, NULL,   1 },
};


static panel_parameters_t lvds_clone_video_spec_list [] = {
	{ "WVGA	   [800x480]",   "LDB-WVGA",    "RGB666",      "datamap=spwg",   -1, "ldb=dual",   1 },
	{ "SVGA	   [800x600]",   "LDB-SVGA",    "RGB666",      "datamap=spwg",   -1, "ldb=dual",   1 },
	{ "XGA	   [1024x768]",  "LDB-XGA",     "RGB666",      "datamap=spwg",   -1, "ldb=dual",   1 },
	{ "WXGA	   [1368x768]",  "LDB-WXGA",    "RGB24",       "datamap=jeida",  -1, "ldb=dual",   1 },
	{ "WXGAP60 [1280x800]",  "LDB-1280P60", "RGB24,bpp=32","datamap=spwg",   -1, "ldb=dual",   1 },
	{ "SXGA	   [1280x1024]", "LDB-SXGA",    "RGB24",       "datamap=jeida",  -1, "ldb=dual",   1 },
};


static panel_parameters_t lvds_dual_video_spec_list [] = {
	{ "HD1080  [1920x1080]", "LDB-1080P60", "RGB24",       "datamap=spwg",   -1, "ldb=spl0", 2 },
};


static panel_parameters_t hdmi_video_spec_list [] = {
	{ "FULL HD [1920x1080]", "1920x1080M@60", "RGB24,bpp=32",       NULL,   -1, NULL, 0 },
};



/* LVDS0, LVDS1, HDMI */

video_mode_t video_mode_list [] = {
    {   
/* NO DISPLAY */
        .label    = "no display",
        .video = { 
                { VIDEO_NOT_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
				{ VIDEO_NOT_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
                { VIDEO_NOT_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = NULL,
        .use_bootargs = 0,
    }, {
/* LVDS0 ONLY */   
        .label    = "LVDS",
        .video = { 
                { 
					.used = VIDEO_USED, 
					.type = VIDEO_LVDS, 
					.video_args = {
						.name         = "LVDS ch0",
						.buffer       = "mxcfb0",
					 	.driver       = "ldb",
						PANEL_LIST(lvds_video_spec_list)
					},
				},
				{ VIDEO_NOT_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
                { VIDEO_NOT_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = STR(ENV_DTBO_928_LVDS),
        .use_bootargs = 0,
     }, {   
/* HDMI ONLY */   
        .label    = "HDMI",
        .video = { 
                { VIDEO_NOT_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
				{ VIDEO_NOT_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
				{ 
					.used = VIDEO_USED, 
					.type = VIDEO_HDMI, 
					.video_args = {
						.name         = "HDMI",
						.buffer       = "mxcfb0",
					 	.driver       = "hdmi",
						PANEL_LIST(hdmi_video_spec_list)
					},
				},
        },
        .panel_name = "none",
        .dtbo_conf_file = STR(ENV_DTBO_928_HDMI),
        .use_bootargs = 0,
    }, {  
		.label    = "LVDS ch0/ch1 clone",
        .video = { 
                { 
					.used = VIDEO_USED, 
					.type = VIDEO_LVDS, 
					.video_args = {
						.name         = "LVDS ch0/ch1 clone",
						.buffer       = "mxcfb0",
					 	.driver       = "ldb",
						PANEL_LIST(lvds_clone_video_spec_list)
					},
				},
				{ VIDEO_NOT_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
                { VIDEO_NOT_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = STR(ENV_DTBO_928_LDB_CLONE),
        .use_bootargs = 0,
    }, {
/* LVDS0 + LVDS1 DUAL CHANNEL */   
        .label    = "LVDS dual channel",
        .video = { 
                { 
					.used = VIDEO_USED, 
					.type = VIDEO_LVDS, 
					.video_args = {
						.name         = "LVDS ch0/ch1 dual",
						.buffer       = "mxcfb0",
					 	.driver       = "ldb",
						PANEL_LIST(lvds_dual_video_spec_list)
					},
				},
				{ VIDEO_NOT_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
                { VIDEO_NOT_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = STR(ENV_DTBO_928_LDB_DUAL),
        .use_bootargs = 0,
    }, {
/* LVDS0 + HDMI */   
        .label    = "LVDS + HDMI",
        .video = { 
                { 
					.used = VIDEO_USED, 
					.type = VIDEO_LVDS, 
					.video_args = {
						.name         = "LVDS ch0",
						.buffer       = "mxcfb0",
					 	.driver       = "ldb",
						PANEL_LIST(lvds_video_spec_list)
					},
				},
				{ 
					.used = VIDEO_USED, 
					.type = VIDEO_HDMI, 
					.video_args = {
						.name         = "HDMI",
						.buffer       = "mxcfb1",
					 	.driver       = "hdmi",
						PANEL_LIST(hdmi_video_spec_list)
					},
				},
                { VIDEO_NOT_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = STR(ENV_DTBO_928_LVDS_HDMI),
        .use_bootargs = 0,
	}, {
/* HDMI + LVDS0 */   
        .label    = "HDMI + LVDS",
        .video = { 
                { 
					.used = VIDEO_USED, 
					.type = VIDEO_LVDS, 
					.video_args = {
						.name         = "LVDS ch0",
						.buffer       = "mxcfb1",
					 	.driver       = "ldb",
						PANEL_LIST(lvds_video_spec_list)
					},
				},
				{ 
					.used = VIDEO_USED, 
					.type = VIDEO_HDMI, 
					.video_args = {
						.name         = "HDMI",
						.buffer       = "mxcfb0",
					 	.driver       = "hdmi",
						PANEL_LIST(hdmi_video_spec_list)
					},
				},
                { VIDEO_NOT_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = STR(ENV_DTBO_928_HDMI_LVDS),
        .use_bootargs = 0,
     }, 
};




size_t video_mode_size = sizeof( video_mode_list ) / sizeof( video_mode_list[0] );



#ifdef CONFIG_OF_LIBFDT_OVERLAY
/* *********************************** FDT OVERLAY *********************************** */

overlay_list_t overlay_peripheral_list [] = {
 	{
		.title = "serial device",
		.options = { 
			{ "flexcan", "" },              // default
			{ "uart4", STR(ENV_DTBO_928_UART4) },
		},
	},
	{
		.title = "audio type",
		.options = { 
			{ "SGTL5000 (I2S)", "" },               // default
			{ "AC97", STR(ENV_DTBO_928_AC97) },
		},
	},
	{
		.title = "CPLD firmware",
		.options = { 
			{ "do not use it", "" },                // default
			{ "use as LPC bridge with SuperIO Maxlinear XR28V382 (UARTx2)", STR(ENV_DTBO_928_CPLD_MAXLINEAR) },
			{ "use as LPC bridge with SuperIO Nuvoton W83627DHG (UARTx2 + GPIOx8)", STR(ENV_DTBO_928_CPLD_NUVOTON) },
			{ "use as GPIO Expander (IOx8) + PWM", STR(ENV_DTBO_928_CPLD_GPIO_PWM) },
		},
	},
};

size_t overlay_peripheral_size = sizeof( overlay_peripheral_list ) / sizeof( overlay_peripheral_list[0] );

overlay_list_t overlay_peripheral_list_revE [] = {
	{
		.title = "serial device",
		.options = {
			{ "flexcan", "" },              // default
			{ "uart4", STR(ENV_DTBO_928_UART4) },
		},
	},
	{
		.title = "audio type",
		.options = {
			{ "SGTL5000 (I2S)", "" },               // default
			{ "AC97", STR(ENV_DTBO_928_AC97) },
		},
	}
};

#endif  /* CONFIG_OF_LIBFDT_OVERLAY */

