/*
 * Copyright 2018-2019 NXP
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <command.h>
#include <cpu_func.h>
#include <hang.h>
#include <image.h>
#include <init.h>
#include <log.h>
#include <spl.h>
#include <asm/global_data.h>
#include <asm/io.h>
#include <errno.h>
#include <asm/io.h>
#include <asm/mach-imx/iomux-v3.h>
#include <asm/arch/imx8mp_pins.h>
#include <asm/arch/sys_proto.h>
#include <asm/mach-imx/boot_mode.h>
#include <power/pmic.h>

#include <power/pca9450.h>
#include <asm/arch/clock.h>
#include <dm/uclass.h>
#include <dm/device.h>
#include <dm/uclass-internal.h>
#include <dm/device-internal.h>
#include <asm/mach-imx/gpio.h>
#include <asm/mach-imx/mxc_i2c.h>
#include <fsl_esdhc_imx.h>
#include <mmc.h>
#include <asm/arch/ddr.h>

#include "strap_cfg.h"

DECLARE_GLOBAL_DATA_PTR;

#define USDHC_GPIO_PAD_CTRL (PAD_CTL_HYS | PAD_CTL_DSE1)
#define USDHC2_WP_GPIO      IMX_GPIO_NR(2, 20)

static iomux_v3_cfg_t const usdhc2_wp_pads[] = {
	MX8MP_PAD_SD2_WP__GPIO2_IO20 | MUX_PAD_CTRL(USDHC_GPIO_PAD_CTRL),
};

void spl_dram_init(void)
{
	if(D18_IS_1GB) {
		printf("D18 is 1GB\n");
		/* DDR Parameter */
		dram_timing.ddrc_cfg[2].val  = 0xa1080020;
		dram_timing.ddrc_cfg[5].val  = 0x7a00b4;
		dram_timing.ddrc_cfg[6].val  = 0x61027f10;
		dram_timing.ddrc_cfg[23].val = 0xbc;
		dram_timing.ddrc_cfg[39].val = 0x1f;
		dram_timing.ddrc_cfg[44].val = 0xf070707;
		dram_timing.ddrc_cfg[45].val = 0xf0f;
		dram_timing.ddrc_cfg[61].val = 0xc0012;
		dram_timing.ddrc_cfg[76].val = 0x13;
		dram_timing.ddrc_cfg[86].val = 0x30005;
		dram_timing.ddrc_cfg[101].val = 0x5;
		/* Training */
		dram_timing.fsp_msg[0].fsp_cfg[9].val = 0x110;
		dram_timing.fsp_msg[1].fsp_cfg[10].val = 0x110;
		dram_timing.fsp_msg[2].fsp_cfg[10].val = 0x110;
		dram_timing.fsp_msg[3].fsp_cfg[10].val = 0x110;
		dram_timing.fsp_msg[0].fsp_cfg[21].val = 0x1;
		dram_timing.fsp_msg[1].fsp_cfg[22].val = 0x1;
		dram_timing.fsp_msg[2].fsp_cfg[22].val = 0x1;
		dram_timing.fsp_msg[3].fsp_cfg[22].val = 0x1;
	}
	if(D18_IS_2GB) {
		printf("D18 is 2GB\n");
		/* DDR Parameter */
		dram_timing.ddrc_cfg[2].val  = 0xa1080020;
		dram_timing.ddrc_cfg[5].val  = 0x7a0118;
		dram_timing.ddrc_cfg[6].val  = 0x61027f10;
		dram_timing.ddrc_cfg[23].val = 0x120;
		dram_timing.ddrc_cfg[39].val = 0x1f;
		dram_timing.ddrc_cfg[44].val = 0x7070707;
		dram_timing.ddrc_cfg[45].val = 0xf0f;
		dram_timing.ddrc_cfg[61].val = 0xc001c;
		dram_timing.ddrc_cfg[76].val = 0x1d;
		dram_timing.ddrc_cfg[86].val = 0x30007;
		dram_timing.ddrc_cfg[101].val = 0x8;
		/* Training */
		dram_timing.fsp_msg[0].fsp_cfg[9].val = 0x110;
		dram_timing.fsp_msg[1].fsp_cfg[10].val = 0x110;
		dram_timing.fsp_msg[2].fsp_cfg[10].val = 0x110;
		dram_timing.fsp_msg[3].fsp_cfg[10].val = 0x110;
		dram_timing.fsp_msg[0].fsp_cfg[21].val = 0x1;
		dram_timing.fsp_msg[1].fsp_cfg[22].val = 0x1;
		dram_timing.fsp_msg[2].fsp_cfg[22].val = 0x1;
		dram_timing.fsp_msg[3].fsp_cfg[22].val = 0x1;
	}
	if(D18_IS_4GB) {
		printf("D18 is 4GB\n");
		/* DDR Parameter */
		dram_timing.ddrc_cfg[5].val  = 0x7a0118;
		dram_timing.ddrc_cfg[6].val  = 0x61027f10;
		dram_timing.ddrc_cfg[23].val = 0x120;
		dram_timing.ddrc_cfg[39].val = 0x17;
		dram_timing.ddrc_cfg[44].val = 0x7070707;
		dram_timing.ddrc_cfg[45].val = 0xf0f;
		dram_timing.ddrc_cfg[61].val = 0xc001c;
		dram_timing.ddrc_cfg[76].val = 0x1d;
		dram_timing.ddrc_cfg[86].val = 0x30007;
		dram_timing.ddrc_cfg[101].val = 0x8;
	}
	if(D18_IS_6GB) {
		printf("D18 is 6GB\n");
	}
	if(D18_IS_8GB) {
		printf("D18 is 8GB\n");
		/* DDR Parameter */
		dram_timing.ddrc_cfg[6].val = 0x7027f90;
		dram_timing.ddrc_cfg[7].val = 0x790;
		dram_timing.ddrc_cfg[20].val = 0x502;
		dram_timing.ddrc_cfg[37].val = 0x799;
		dram_timing.ddrc_cfg[38].val = 0x9121b1c;
		dram_timing.ddrc_cfg[39].val = 0x18;
		dram_timing.ddrc_cfg[44].val = 0x7070707;
		dram_timing.ddrc_cfg[45].val = 0xf07;
		dram_timing.ddrc_cfg[73].val = 0x302;
		dram_timing.ddrc_cfg[82].val = 0x599;
		dram_timing.ddrc_cfg[98].val = 0x302;
		dram_timing.ddrc_cfg[107].val = 0x599;
		/* PHY init engine */
		dram_timing.ddrphy_pie[480].val = 0x465;
		dram_timing.ddrphy_pie[484].val = 0x70;
		dram_timing.ddrphy_pie[488].val = 0x1c;
	}

	ddr_init(&dram_timing);
}

#if CONFIG_IS_ENABLED(DM_PMIC_PCA9450)
int power_init_board(void)
{
	struct udevice *dev;
	int ret;

	ret = pmic_get("pca9450@25", &dev);
	if (ret == -ENODEV) {
		puts("No pca9450@25\n");
		return 0;
	}
	if (ret != 0)
		return ret;

	/* BUCKxOUT_DVS0/1 control BUCK123 output */
	pmic_reg_write(dev, PCA9450_BUCK123_DVS, 0x29);

#ifdef CONFIG_IMX8M_LPDDR4
	/*
	 * increase VDD_SOC to typical value 0.95V before first
	 * DRAM access, set DVS1 to 0.85v for suspend.
	 * Enable DVS control through PMIC_STBY_REQ and
	 * set B1_ENMODE=1 (ON by PMIC_ON_REQ=H)
	 */
#ifdef CONFIG_IMX8M_VDD_SOC_850MV
	/* set DVS0 to 0.85v for special case*/
	pmic_reg_write(dev, PCA9450_BUCK1OUT_DVS0, 0x14);
#else
	pmic_reg_write(dev, PCA9450_BUCK1OUT_DVS0, 0x1C);
#endif
	pmic_reg_write(dev, PCA9450_BUCK1OUT_DVS1, 0x14);
	pmic_reg_write(dev, PCA9450_BUCK1CTRL, 0x59);

	/* Kernel uses OD/OD freq for SOC */
	/* To avoid timing risk from SOC to ARM,increase VDD_ARM to OD voltage 0.95v */
	pmic_reg_write(dev, PCA9450_BUCK2OUT_DVS0, 0x1C);
#elif defined(CONFIG_IMX8M_DDR4)
	/* DDR4 runs at 3200MTS, uses default ND 0.85v for VDD_SOC and VDD_ARM */
	pmic_reg_write(dev, PCA9450_BUCK1CTRL, 0x59);

	/* Set NVCC_DRAM to 1.2v for DDR4 */
	pmic_reg_write(dev, PCA9450_BUCK6OUT, 0x18);
#endif

	/* set WDOG_B_CFG to cold reset */
	pmic_reg_write(dev, PCA9450_RESET_CTRL, 0xA1);

	return 0;
}
#endif

void spl_board_init(void)
{
	struct udevice *dev;
	uclass_find_first_device(UCLASS_MISC, &dev);

	for (; dev; uclass_find_next_device(&dev)) {
		if (device_probe(dev))
			continue;
	}

	/* Set GIC clock to 500Mhz for OD VDD_SOC. Kernel driver does not allow to change it.
	 * Should set the clock after PMIC setting done.
	 * Default is 400Mhz (system_pll1_800m with div = 2) set by ROM for ND VDD_SOC
	 */
#if defined(CONFIG_IMX8M_LPDDR4) && !defined(CONFIG_IMX8M_VDD_SOC_850MV)
	clock_enable(CCGR_GIC, 0);
	clock_set_target_val(GIC_CLK_ROOT, CLK_ROOT_ON | CLK_ROOT_SOURCE_SEL(5));
	clock_enable(CCGR_GIC, 1);
#endif

	puts("Normal Boot\n");
}

#ifdef CONFIG_SPL_LOAD_FIT
int board_fit_config_name_match(const char *name)
{
	/* Just empty function now - can't decide what to choose */
	debug("%s: %s\n", __func__, name);

	return 0;
}
#endif

void board_init_f(ulong dummy)
{
	struct udevice *dev;
	int ret;

	/* Clear the BSS. */
	memset(__bss_start, 0, __bss_end - __bss_start);

	arch_cpu_init();

	board_early_init_f();

	timer_init();

	preloader_console_init();

	ret = spl_early_init();
	if (ret) {
		debug("spl_early_init() failed: %d\n", ret);
		hang();
	}

	ret = uclass_get_device_by_name(UCLASS_CLK,
					"clock-controller@30380000",
					&dev);
	if (ret < 0) {
		printf("Failed to find clock node. Check device tree\n");
		hang();
	}

	enable_tzc380();

	power_init_board();

	/* init sd2_wp early as possibile to correct wrong level */
	imx_iomux_v3_setup_multiple_pads(usdhc2_wp_pads, ARRAY_SIZE(usdhc2_wp_pads));
	gpio_request(USDHC2_WP_GPIO, "usdhc2 wp");
	gpio_direction_input(USDHC2_WP_GPIO);

	/* DDR initialization */
	spl_dram_init();

	board_init_r(NULL, 0);
}
