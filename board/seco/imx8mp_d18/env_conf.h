#include <common.h>
#include <command.h>
#include <env.h>


#include <seco/env_common.h>
#include <configs/seco_mx8_dtbo.h>


#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

 
/* *********************************** IMX8 *********************************** */

data_boot_dev_t kern_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_KERNEL_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_SD_EXT,   SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_KERNEL_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD_EXT,  LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_KERNEL_SRC_TFTP),     "",                       LOAD_ADDR_KERNEL_REMOTE_DEV, SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_KERNEL_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
};

size_t kern_dev_imx8_size = sizeof( kern_dev_imx8_list ) / sizeof( kern_dev_imx8_list[0] );


data_boot_dev_t fdt_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FDT_SRC_USDHCI),    SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX8_FILE },
	{ SECO_DEV_TYPE_SD_EXT,   SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_FDT_SRC_USDHCI),    SCFG_BOOT_DEV_ID_SD_EXT,  LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX8_FILE },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_FDT_SRC_TFTP),      "",                       LOAD_ADDR_FDT_REMOTE_DEV, SCFG_DEFAULT_FDT_IMX8_FILE },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FDT_SRC_USB),       SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX8_FILE },
};

size_t fdt_dev_imx8_size = sizeof( fdt_dev_imx8_list ) / sizeof( fdt_dev_imx8_list[0] );


#ifdef CONFIG_OF_LIBFDT_OVERLAY
data_boot_dev_t fdt_overlay_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),    SCFG_BOOT_DEV_ID_EMMC,    "" , "" },
	{ SECO_DEV_TYPE_SD_EXT,   SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),    SCFG_BOOT_DEV_ID_SD_EXT,  "" , "" },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_FDT_OVERLAY_SRC_TFTP),      "",                       "" , ""},
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FDT_OVERLAY_SRC_USB),       SCFG_BOOT_DEV_ID_USB,     "" , ""},
};

size_t fdt_overlay_dev_imx8_size = sizeof( fdt_overlay_dev_imx8_list ) / sizeof( fdt_overlay_dev_imx8_list[0] );
#endif  /* CONFIG_OF_LIBFDT_OVERLAY */


data_boot_dev_t ramfs_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_NONE,     SECO_DEV_LABEL_NONE,   "0x0",                            "0",                      LOAD_ADDR_RAMFS_LOCAL_DEV,  ""          },
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_RAMFS_SRC_USDHCI),  SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_RAMFS_LOCAL_DEV,  SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_SD_EXT,   SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_RAMFS_SRC_USDHCI),  SCFG_BOOT_DEV_ID_SD_EXT,  LOAD_ADDR_RAMFS_LOCAL_DEV,  SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_RAMFS_SRC_TFTP),    "",                       LOAD_ADDR_RAMFS_REMOTE_DEV, SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_RAMFS_SRC_USB),     SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_RAMFS_LOCAL_DEV,  SCFG_RAMFS_FILENAME },
};

size_t ramfs_dev_imx8_size = sizeof( ramfs_dev_imx8_list ) / sizeof( ramfs_dev_imx8_list[0] );


data_boot_dev_t filesystem_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FS_SRC_USDHCI),  SCFG_ROOT_DEV_ID_EMMC,    "",  "" },
	{ SECO_DEV_TYPE_SD_EXT,   SECO_DEV_LABEL_SD_EXT, STR(MACRO_ENV_FS_SRC_USDHCI),  SCFG_ROOT_DEV_ID_SD_EXT,  "",  "" },
	{ SECO_DEV_TYPE_NFS,      SECO_DEV_LABEL_NFS,    STR(MACRO_ENV_FS_SRC_NFS),     "",                       "",  "" },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FS_SRC_USB),     "",                       "",  "" },
};

size_t filesystem_dev_imx8_size = sizeof( filesystem_dev_imx8_list ) / sizeof( filesystem_dev_imx8_list[0] );


/* HDMI, LVDS ChA + ChB */
video_mode_t video_mode_list_CFG_D1 [] = {
	{
		/* NO DISPLAY */
		.label = SECO_VIDEO_LABEL_NONE,
		.video = {
			{ VIDEO_NOT_USED, NO_VIDEO, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = NULL,
		.use_bootargs = 0,
	}, {
		/* HDMI */
		.label = SECO_VIDEO_LABEL_HDMI,
		.video = {
			{ VIDEO_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = STR(ENV_DTBO_D18_HDMI),
		.use_bootargs = 0,
	}, {
		/* LVDS 1920x1080 */
		.label = SECO_VIDEO_LABEL_LVDS_FHD,
		.video = {
			{ VIDEO_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = STR(ENV_DTBO_D18_LVDS_FHD),
		.use_bootargs = 0,
	},{
		/* LVDS ChA 800x480 */
		.label = "LVDS ChA 800x480",
		.video = {
			{ VIDEO_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = "seco-imx8mp-d18-lvds-wvga-ne070nb04f.dtbo",
		.use_bootargs = 0,
	},{
		/* LVDS ChB 800x480 */
		.label = "LVDS ChB 800x480",
		.video = {
			{ VIDEO_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = "seco-imx8mp-d18-lvds-wvga-umsh8596md.dtbo",
		.use_bootargs = 0,
	},{
		/* HDMI + LVDS 1920x1080 */
		.label = "HDMI + LVDS 1920x1080",
		.video = {
			{ VIDEO_USED, VIDEO_HDMI_LVDS, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = "seco-imx8mp-d18-hdmi.dtbo seco-imx8mp-d18-lvds-fhd.dtbo",
		.use_bootargs = 0,
	},{
		/* LVDS ChA 7-inch (1024x600) */
		.label = "LVDS 7-inch (1024x600 / QX-070WSVGAMML01D)",
		.video = {
			{ VIDEO_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = "seco-imx8mp-d18-lvds-wsvga-qx_070wsvgamml01d.dtbo",
		.use_bootargs = 0,
	},{
		/* LVDS ChA 7-inch (1024x600) */
		.label = "LVDS 7-inch (1024x600 / FN0700D101A)",
		.video = {
			{ VIDEO_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = "seco-imx8mp-d18-lvds-wsvga-fn0700d101a.dtbo",
		.use_bootargs = 0,
	},{
		/* LVDS ChA 10.1-inch (1280x800) */
		.label = "LVDS 10.1-inch (1280x800)",
		.video = {
			{ VIDEO_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = "seco-imx8mp-d18-lvds-wxga-g101ean02.dtbo",
		.use_bootargs = 0,
	},{
		/* LVDS 15.6-inch (1920x1080) */
		.label = "LVDS 15.6-inch (1920x1080)",
		.video = {
			{ VIDEO_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = "seco-imx8mp-d18-lvds-fhd-g156han02.dtbo",
		.use_bootargs = 0,
	},
};

size_t video_mode_size_CFG_D1 = sizeof( video_mode_list_CFG_D1 ) / sizeof( video_mode_list_CFG_D1[0] );


/* HDMI, eDP, LVDS ChB */
video_mode_t video_mode_list_CFG_D2 [] = {
	{
		/* NO DISPLAY */
		.label = SECO_VIDEO_LABEL_NONE,
		.video = {
			{ VIDEO_NOT_USED, NO_VIDEO, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = NULL,
		.use_bootargs = 0,
	}, {
		/* HDMI */
		.label = SECO_VIDEO_LABEL_HDMI,
		.video = {
			{ VIDEO_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = STR(ENV_DTBO_D18_HDMI),
		.use_bootargs = 0,
	}, {
		/* DSI-to-eDP */
		.label = SECO_VIDEO_LABEL_EDP,
		.video = {
			{ VIDEO_USED, VIDEO_EDP, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = STR(ENV_DTBO_D18_EDP),
		.use_bootargs = 0,
	},{
		/* LVDS ChB 800x480 */
		.label = "LVDS ChB 800x480",
		.video = {
			{ VIDEO_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = "seco-imx8mp-d18-lvds-wvga-umsh8596md.dtbo",
		.use_bootargs = 0,
	},{
		/* eDP + LVDS ChB 800x480 */
		.label = "eDP + LVDS ChB 800x480",
		.video = {
			{ VIDEO_USED, VIDEO_LVDS_EDP, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = "seco-imx8mp-d18-edp.dtbo seco-imx8mp-d18-lvds-wvga-umsh8596md.dtbo",
		.use_bootargs = 0,
	},{
		/* HDMI + eDP */
		.label = SECO_VIDEO_LABEL_HDMI_EDP,
		.video = {
			{ VIDEO_USED, VIDEO_HDMI_EDP, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = "seco-imx8mp-d18-hdmi.dtbo seco-imx8mp-d18-edp.dtbo",
		.use_bootargs = 0,
	},
};

size_t video_mode_size_CFG_D2 = sizeof( video_mode_list_CFG_D2 ) / sizeof( video_mode_list_CFG_D2[0] );

/* Touch controllers */
touch_mode_t touch_mode_list [] = {
	{
		/* NO TOUCH */
		.label = SECO_TOUCH_LABEL_NONE,
		.touch_name = "none",
		.dtbo_conf_file = NULL,
	}, {
		/* EETI */
		.label = SECO_TOUCH_LABEL_I2C_EETI,
		.touch_name = "none",
		.dtbo_conf_file = STR(ENV_DTBO_D18_TOUCH_EETI),
	}, {
		/* ILITEK */
		.label = SECO_TOUCH_LABEL_I2C_ILITEK,
		.touch_name = "none",
		.dtbo_conf_file = STR(ENV_DTBO_D18_TOUCH_ILITEK),
	},{
		/* MXT */
		.label = SECO_TOUCH_LABEL_I2C_MXT,
		.touch_name = "none",
		.dtbo_conf_file = STR(ENV_DTBO_D18_TOUCH_MXT),
	},
};

size_t touch_mode_size = sizeof( touch_mode_list ) / sizeof( touch_mode_list[0] );

#ifdef CONFIG_OF_LIBFDT_OVERLAY
/* *********************************** FDT OVERLAY *********************************** */

/* REV_A */
overlay_list_t overlay_peripheral_list_REVA [] = {
	{
		.title = "camera CSI0",
		.options = {
			{ "not used", "" }, // default
			{ "OV5640", STR(ENV_DTBO_D18_OV5640_CSI0_REVA) },
		},
	}, {
		.title = "camera CSI1",
		.options = {
			{ "not used", "" }, // default
			{ "OV5640", STR(ENV_DTBO_D18_OV5640_CSI1_REVA) },
		},
	}, {
		.title = "second audio",
		.options = {
			{ "not used", "" }, // default
			{ "audio bluetooth", STR(ENV_DTBO_D18_BTSCO) },
		},
	},
};

size_t overlay_peripheral_size_REVA = sizeof( overlay_peripheral_list_REVA ) / sizeof( overlay_peripheral_list_REVA[0] );


/* After REV_A */
overlay_list_t overlay_peripheral_list [] = {
	{
		.title = "camera CSI0",
		.options = {
			{ "not used", "" }, // default
			{ "OV5640", STR(ENV_DTBO_D18_OV5640_CSI0) },
		},
	}, {
		.title = "camera CSI1",
		.options = {
			{ "not used", "" }, // default
			{ "OV5640", STR(ENV_DTBO_D18_OV5640_CSI1) },
		},
	}, {
		.title = "second audio",
		.options = {
			{ "not used", "" }, // default
			{ "audio bluetooth", STR(ENV_DTBO_D18_BTSCO) },
		},
	},
};

size_t overlay_peripheral_size = sizeof( overlay_peripheral_list ) / sizeof( overlay_peripheral_list[0] );

#endif  /* CONFIG_OF_LIBFDT_OVERLAY */
