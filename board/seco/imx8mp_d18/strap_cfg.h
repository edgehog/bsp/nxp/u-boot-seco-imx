#ifndef _D18_REVISION_H_
#define _D18_REVISION_H_

#define UART_PAD_CTRL   ((PAD_CTL_DSE6 | PAD_CTL_FSEL1))
#define WDOG_PAD_CTRL   ((PAD_CTL_DSE6 | PAD_CTL_ODE | PAD_CTL_PUE | PAD_CTL_PE))
#define PULLUP_PAD_CTRL ((PAD_CTL_PUE | PAD_CTL_PE | PAD_CTL_DSE1))

int strap_get_ram_cfg (void);
int strap_get_revision_cfg (void);

int exp_strap_get_gpio_edp_cfg (void);
int exp_strap_get_gpio_eth1_cfg (void);

void exp_strap_fdt_eth1_remove (void *blob, struct bd_info *bd);
void strap_fdt_ram_setup (void *blob, struct bd_info *bd);

void strap_show (void);
void exp_strap_show (void);

typedef enum {
	REV_A0 = 0x0,
	REV_A1 = 0x1,
	REV_B  = 0x2,
} BOARD_REV;

typedef enum {
	RAM_1GB   = 0x0,
	RAM_2GB   = 0x1,
	RAM_4GB   = 0x2,
	RAM_6GB   = 0x3,
	RAM_8GB   = 0x4,
} RAM_STRAPS;

typedef enum {
	YES_EDP = 0x0,
	NO_EDP  = 0x1,
} EDP_STRAPS;

typedef enum {
	YES_ETH1 = 0x0,
	NO_ETH1  = 0x1,
} ETH_STRAPS;

typedef struct {
	u8 ram_size;
	u8 hw_rev;
} strap_conf_t;

typedef struct {
	u8 edp;
	u8 eth1;
} exp_strap_conf_t;

#define GET_D18_RAM_STRAPS  (strap_get_ram_cfg())
#define D18_IS_1GB          (GET_D18_RAM_STRAPS == RAM_1GB)
#define D18_IS_2GB          (GET_D18_RAM_STRAPS == RAM_2GB)
#define D18_IS_4GB          (GET_D18_RAM_STRAPS == RAM_4GB)
#define D18_IS_6GB          (GET_D18_RAM_STRAPS == RAM_6GB)
#define D18_IS_8GB          (GET_D18_RAM_STRAPS == RAM_8GB)

#define GET_D18_REV_STRAPS  (strap_get_revision_cfg())
#define D18_IS_REVA0        (GET_D18_REV_STRAPS == REV_A0)
#define D18_IS_REVA1        (GET_D18_REV_STRAPS == REV_A1)
#define D18_IS_REVB         (GET_D18_REV_STRAPS == REV_B)

#define GET_D18_EDP_STRAPS  (exp_strap_get_gpio_edp_cfg())
#define D18_HAS_EDP         (GET_D18_EDP_STRAPS == YES_EDP)

#define GET_D18_ETH1_STRAPS (exp_strap_get_gpio_eth1_cfg())
#define D18_HAS_ETH1        (GET_D18_ETH1_STRAPS == YES_ETH1)

#endif
