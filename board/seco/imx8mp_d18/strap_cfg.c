/*
 *
 * Reading D18 Board type
 *
 * gianfranco.mariotti@seco.com
 *
 */

#include <common.h>
#include <malloc.h>
#include <errno.h>
#include <asm/io.h>
#include <miiphy.h>
#include <netdev.h>
#include <asm/mach-imx/iomux-v3.h>
#include <asm-generic/gpio.h>
#include <fsl_esdhc.h>
#include <mmc.h>
#include <asm/arch/imx8mp_pins.h>
#include <asm/arch/sys_proto.h>
#include <asm/mach-imx/gpio.h>
#include "strap_cfg.h"

/* ____________________________________________________________________________
  |                                                                            |
  |                                  D18 REVISION                              |
  |____________________________________________________________________________|
*/
#define D18_IOMUX_REG(x)        ((x))
#define D18_PADCTRL_REG(x)      ((x))
#define D18_GDIR_REG_IN(x,n)    writel((readl(x + 0x4)) & ~(1<<n), x + 0x4 )
#define D18_PSR_REG(x,n)       (readl(x + 0x8) & (1<<n))

#define GPIO1_PAD_BASE          0x30200000		
#define GPIO2_PAD_BASE          0x30210000		
#define GPIO3_PAD_BASE          0x30220000		
#define GPIO4_PAD_BASE          0x30230000		

/* RAM CONFIG */
#define GPIO_CFG_0              11
#define GPIO_CFG_1              10
#define GPIO_CFG_2              9

/* HW REVISION */
#define HW_CFG_0              	8
#define HW_CFG_1              	5
#define HW_CFG_2              	18

struct sizes {
	u32 s0;
	u32 s1;
};

iomux_v3_cfg_t const board_conf_pads[] = {
	/* RAM CONFIG */
	MX8MP_PAD_SD1_STROBE__GPIO2_IO11  | MUX_PAD_CTRL(PULLUP_PAD_CTRL),
	MX8MP_PAD_SD1_RESET_B__GPIO2_IO10 | MUX_PAD_CTRL(PULLUP_PAD_CTRL),
	MX8MP_PAD_SD1_DATA7__GPIO2_IO09   | MUX_PAD_CTRL(PULLUP_PAD_CTRL),
	/* HW REVISION */
	MX8MP_PAD_SD1_DATA6__GPIO2_IO08   | MUX_PAD_CTRL(PULLUP_PAD_CTRL),
	MX8MP_PAD_GPIO1_IO05__GPIO1_IO05  | MUX_PAD_CTRL(PULLUP_PAD_CTRL),
	MX8MP_PAD_SAI1_TXD6__GPIO4_IO18   | MUX_PAD_CTRL(PULLUP_PAD_CTRL),
};

static int strap_get_seco_cfg( void ) {
	/* 
	 * SECO_CODE is composed in this bit order:
	 * [HW_CFG_2, HW_CFG_1, HW_CFG_0, GPIO_CFG_2, GPIO_CFG_1, GPIO_CFG_0]
	 */
	int value = 0;

	imx_iomux_v3_setup_multiple_pads(board_conf_pads, ARRAY_SIZE(board_conf_pads));

	/* Mux as Input */
	D18_GDIR_REG_IN(GPIO2_PAD_BASE,GPIO_CFG_0);
	D18_GDIR_REG_IN(GPIO2_PAD_BASE,GPIO_CFG_1);
	D18_GDIR_REG_IN(GPIO2_PAD_BASE,GPIO_CFG_2);
	D18_GDIR_REG_IN(GPIO2_PAD_BASE,HW_CFG_0);
	D18_GDIR_REG_IN(GPIO1_PAD_BASE,HW_CFG_1);
	D18_GDIR_REG_IN(GPIO4_PAD_BASE,HW_CFG_2);

	/* Read Conf value */
	value = (D18_PSR_REG(GPIO2_PAD_BASE,GPIO_CFG_0) >> GPIO_CFG_0) << 0 |
	        (D18_PSR_REG(GPIO2_PAD_BASE,GPIO_CFG_1) >> GPIO_CFG_1) << 1 |
	        (D18_PSR_REG(GPIO2_PAD_BASE,GPIO_CFG_2) >> GPIO_CFG_2) << 2 |
	        (D18_PSR_REG(GPIO2_PAD_BASE,HW_CFG_0) >> HW_CFG_0)     << 3 |
	        (D18_PSR_REG(GPIO1_PAD_BASE,HW_CFG_1) >> HW_CFG_1)     << 4 |
	        (D18_PSR_REG(GPIO4_PAD_BASE,HW_CFG_2) >> HW_CFG_2)     << 5 ;

	return value & 0b111111;
}

int strap_get_ram_cfg (void) {
	return strap_get_seco_cfg() & 0b111;
};

int strap_get_revision_cfg (void) {
	return (strap_get_seco_cfg() >> 3) & 0b111;
};

void strap_show( void ) {
	strap_conf_t d18_strap_conf;
	d18_strap_conf.ram_size = GET_D18_RAM_STRAPS;
	d18_strap_conf.hw_rev = GET_D18_REV_STRAPS;

	printf( "Straps:\n" );

	printf( " - RAM code: %d (", d18_strap_conf.ram_size );
	switch ( d18_strap_conf.ram_size ) {
		case RAM_1GB:
			printf( "1GB" );
			break;
		case RAM_2GB:
			printf( "2GB" );
			break;
		case RAM_4GB:
			printf( "4GB" );
			break;
		case RAM_6GB:
			printf( "6GB" );
			break;
		case RAM_8GB:
			printf( "8GB" );
			break;
		default:
			printf( "Unknown" );
			break;
	}
	printf ( ")\n" );

	printf( " - Revision code: %d (", d18_strap_conf.hw_rev );
	switch ( d18_strap_conf.hw_rev ) {
		case REV_A0:
			printf( "REVA0" );
			break;
		case REV_A1:
			printf( "REVA1" );
			break;
		case REV_B:
			printf( "REVB" );
			break;
		default:
			printf( "Unknown" );
			break;
	}
	printf ( ")\n" );
}

void exp_strap_show( void ) {
	exp_strap_conf_t d18_exp_strap_conf;
	d18_exp_strap_conf.edp = GET_D18_EDP_STRAPS;
	d18_exp_strap_conf.eth1 = GET_D18_ETH1_STRAPS;

	printf( "Expander straps:\n" );

	printf( " - eDP code: %d (", d18_exp_strap_conf.edp );
	switch ( d18_exp_strap_conf.edp ) {
		case YES_EDP:
			printf( "eDP + LVDS CHB" );
			break;
		case NO_EDP:
			printf( "NO eDP: LVDS CHA + CHB" );
			break;
		default:
			printf( "Unknown" );
			break;
	}
	printf ( ")\n" );

	printf( " - ETH1 code: %d (", d18_exp_strap_conf.eth1 );
	switch ( d18_exp_strap_conf.eth1 ) {
		case YES_ETH1:
			printf( "ETH1" );
			break;
		case NO_ETH1:
			printf( "NO ETH1" );
			break;
		default:
			printf( "Unknown" );
			break;
	}
	printf ( ")\n" );
}

int exp_strap_get_gpio_edp_cfg ( void ) {
	struct gpio_desc desc_video;
	int ret, value_sum;

	ret = dm_gpio_lookup_name("gpio@20_14", &desc_video);
	if (ret)
		return -1;

	ret = dm_gpio_request(&desc_video, "SECO_CODE_7");
	if (ret)
		return -1;

	dm_gpio_set_dir_flags(&desc_video, GPIOD_IS_IN);

	value_sum = dm_gpio_get_value(&desc_video);

	ret = dm_gpio_free((desc_video.dev), &desc_video);
	if (ret)
		return -1;

	return value_sum & 0b1;
}

int exp_strap_get_gpio_eth1_cfg ( void ) {
	struct gpio_desc desc_enet1;
	int ret, value_sum;

	ret = dm_gpio_lookup_name("gpio@20_15", &desc_enet1);
	if (ret)
		return -1;

	ret = dm_gpio_request(&desc_enet1, "SECO_CODE_8");
	if (ret)
		return -1;

	dm_gpio_set_dir_flags(&desc_enet1, GPIOD_IS_IN);

	value_sum = dm_gpio_get_value(&desc_enet1);

	ret = dm_gpio_free((desc_enet1.dev), &desc_enet1);
	if (ret)
		return -1;

	/* Debug straps */
	//printf("PCA953x cfg eth1 = 0x%x\n",value_sum);

	return value_sum & 0b1;
}

void exp_strap_fdt_eth1_remove(void *blob, struct bd_info *bd) {
	int offset, ret;
	char status[10];

	printf("Overlay dts: eth1 not present, disabling\n");
	offset = fdt_path_offset(blob, "/soc@0/bus@30800000/ethernet@30be0000/");
	if (offset < 0) {
		printf("ERROR: find node /: %s.\n", fdt_strerror(offset));
		return;
	}

	sprintf(status, "%s","disabled" );
	ret = fdt_setprop(blob, offset, "status", status, sizeof(status));
	if (ret < 0)
		printf("ERROR: could not update revision property %s.\n", fdt_strerror(ret));
}

void strap_fdt_ram_setup(void *blob, struct bd_info *bd) {
	int offset, ret;
	struct sizes ssize;

	offset = fdt_path_offset(blob, "/reserved-memory/linux,cma/");
	if (offset < 0) {
		printf("ERROR: find node /: %s.\n", fdt_strerror(offset));
		return;
	}

	if (D18_IS_1GB) {
		ssize.s0 = cpu_to_fdt32(0x0);
		ssize.s1 = cpu_to_fdt32(0x14000000);
		printf("Overlay dts: /reserved-memory/linux,cma/ size = ");
		printf("<0x0 0x14000000>\n");
	} else if (D18_IS_2GB) {
		ssize.s0 = cpu_to_fdt32(0x0);
		ssize.s1 = cpu_to_fdt32(0x20000000);
		printf("Overlay dts: /reserved-memory/linux,cma/ size = ");
		printf("<0x0 0x20000000>\n");
	} else {
		return;
	}

	ret = fdt_setprop(blob, offset, "size", &ssize, sizeof(ssize));
	if (ret < 0)
		printf("ERROR: could not update revision property %s.\n", fdt_strerror(ret));
}
