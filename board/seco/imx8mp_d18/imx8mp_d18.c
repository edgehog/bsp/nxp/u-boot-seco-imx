// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright 2019 NXP
 */

#include <common.h>
#include <env.h>
#include <errno.h>
#include <init.h>
#include <miiphy.h>
#include <netdev.h>
#include <linux/delay.h>
#include <asm/global_data.h>
#include <asm/io.h>
#include <asm/mach-imx/iomux-v3.h>
#include <asm-generic/gpio.h>
#include <asm/arch/imx8mp_pins.h>
#include <asm/arch/clock.h>
#include <asm/arch/sys_proto.h>
#include <asm/mach-imx/gpio.h>
#include <asm/mach-imx/mxc_i2c.h>
#include <spl.h>
#include <asm/mach-imx/dma.h>
#include <power/pmic.h>
#include "../common/tcpc.h"
#include <usb.h>
#include <dwc3-uboot.h>
#include <imx_sip.h>
#include <linux/arm-smccc.h>
#include <mmc.h>

#include "../common/proto_seco.h"
#include "seco/seco_env_gd.h"

#include "strap_cfg.h"

#ifdef CONFIG_SECO_ENV_MANAGER
        #include <seco/env_common.h>
        #include "env_conf.h"
#endif

DECLARE_GLOBAL_DATA_PTR;

#define LCD0_BKLT_EN        IMX_GPIO_NR(1, 6)
#define LCD0_VDD_EN         IMX_GPIO_NR(4, 21)
#define VIDEO_eDP_BRG_EN    IMX_GPIO_NR(1, 13)
#define GPIO_EXP_RST        IMX_GPIO_NR(1, 7)

static iomux_v3_cfg_t const uart_pads[] = {
	MX8MP_PAD_UART2_RXD__UART2_DCE_RX | MUX_PAD_CTRL(UART_PAD_CTRL),
	MX8MP_PAD_UART2_TXD__UART2_DCE_TX | MUX_PAD_CTRL(UART_PAD_CTRL),
};

static iomux_v3_cfg_t const wdog_pads[] = {
	MX8MP_PAD_GPIO1_IO02__WDOG1_WDOG_B  | MUX_PAD_CTRL(WDOG_PAD_CTRL),
};

static iomux_v3_cfg_t const video_pads[] = {
	MX8MP_PAD_GPIO1_IO06__GPIO1_IO06    | MUX_PAD_CTRL(PULLUP_PAD_CTRL),
	MX8MP_PAD_SAI2_RXFS__GPIO4_IO21     | MUX_PAD_CTRL(PULLUP_PAD_CTRL),
	MX8MP_PAD_GPIO1_IO13__GPIO1_IO13    | MUX_PAD_CTRL(PULLUP_PAD_CTRL),
};

static iomux_v3_cfg_t const gpio_exp_pads[] = {
	MX8MP_PAD_GPIO1_IO07__GPIO1_IO07    | MUX_PAD_CTRL(PULLUP_PAD_CTRL),
};

boot_mem_dev_t boot_mem_dev_list[SECO_NUM_BOOT_DEV] = {
        { SD1_BOOT, SECO_DEV_LABEL_EMMC },
        { SD3_BOOT, SECO_DEV_LABEL_SD_EXT },
};

int usdhc_devno[3] = { -1, BOARD_BOOT_ID_SD_EXT, BOARD_BOOT_ID_EMMC};

#ifdef CONFIG_NAND_MXS
static void setup_gpmi_nand(void)
{
	init_nand_clk();
}
#endif

static void video_init(void)
{
	imx_iomux_v3_setup_multiple_pads(video_pads, ARRAY_SIZE(video_pads));
	gpio_request(LCD0_VDD_EN, "LCD0_VDD_EN");
	gpio_request(LCD0_BKLT_EN, "LCD0_BKLT_EN");
	gpio_request(VIDEO_eDP_BRG_EN, "eDP_BRG_EN");
}

static void gpio_exp_init(void)
{
	imx_iomux_v3_setup_multiple_pads(gpio_exp_pads, ARRAY_SIZE(gpio_exp_pads));
	gpio_request(GPIO_EXP_RST, "IO_EXP_RST#");
	gpio_direction_output(GPIO_EXP_RST, 1);
	udelay(10);
}

int board_early_init_f(void)
{
	struct wdog_regs *wdog = (struct wdog_regs *)WDOG1_BASE_ADDR;

	imx_iomux_v3_setup_multiple_pads(wdog_pads, ARRAY_SIZE(wdog_pads));

	set_wdog_reset(wdog);

	imx_iomux_v3_setup_multiple_pads(uart_pads, ARRAY_SIZE(uart_pads));

	init_uart_clk(1);

	return 0;
}

int dram_init(void)
{
	unsigned long long sdram_size;
	if(D18_IS_1GB)
		sdram_size = PHYS_DRAM_IS_1GB ;
	if(D18_IS_2GB)
		sdram_size = PHYS_DRAM_IS_2GB ;
	if(D18_IS_4GB || D18_IS_6GB || D18_IS_8GB)
		sdram_size = PHYS_DRAM_IS_3GB ;

	/* rom_pointer[1] contains the size that TEE occupies */
	if (rom_pointer[1])
		gd->ram_size = sdram_size - rom_pointer[1];
	else
		gd->ram_size = sdram_size;

#if CONFIG_NR_DRAM_BANKS > 1
	if(D18_IS_4GB)
		gd->ram_size += PHYS_DRAM_IS_1GB;
	if(D18_IS_6GB)
		gd->ram_size += PHYS_DRAM_IS_3GB;
	if(D18_IS_8GB)
		gd->ram_size += PHYS_DRAM_IS_5GB;
#endif

	return 0;
}

int dram_init_banksize(void)
{
	int bank = 0;
	phys_size_t sdram_size;

	if(D18_IS_1GB)
		sdram_size = PHYS_DRAM_IS_1GB ;
	if(D18_IS_2GB)
		sdram_size = PHYS_DRAM_IS_2GB ;
	if(D18_IS_4GB || D18_IS_6GB || D18_IS_8GB)
		sdram_size = PHYS_DRAM_IS_3GB ;

	gd->bd->bi_dram[bank].start = PHYS_SDRAM;
	if (rom_pointer[1]) {
		phys_addr_t optee_start = (phys_addr_t)rom_pointer[0];
		phys_size_t optee_size = (size_t)rom_pointer[1];

		gd->bd->bi_dram[bank].size = optee_start -gd->bd->bi_dram[bank].start;
		if ((optee_start + optee_size) < (PHYS_SDRAM + sdram_size)) {
			if ( ++bank >= CONFIG_NR_DRAM_BANKS) {
				puts("CONFIG_NR_DRAM_BANKS is not enough\n");
				return -1;
			}

			gd->bd->bi_dram[bank].start = optee_start + optee_size;
			gd->bd->bi_dram[bank].size = PHYS_SDRAM +
			sdram_size - gd->bd->bi_dram[bank].start;
		}
	} else {
		gd->bd->bi_dram[bank].size = sdram_size;
	}

#if CONFIG_NR_DRAM_BANKS > 1
	if(D18_IS_4GB) {
		gd->bd->bi_dram[bank+1].start = PHYS_SDRAM_2;
		gd->bd->bi_dram[bank+1].size = PHYS_DRAM_IS_1GB;
	}
	if(D18_IS_6GB) {
		gd->bd->bi_dram[bank+1].start = PHYS_SDRAM_2;
		gd->bd->bi_dram[bank+1].size = PHYS_DRAM_IS_3GB;
	}
	if(D18_IS_8GB) {
		gd->bd->bi_dram[bank+1].start = PHYS_SDRAM_2;
		gd->bd->bi_dram[bank+1].size = PHYS_DRAM_IS_5GB;
	}
#endif

	return 0;
}

#ifdef CONFIG_OF_BOARD_SETUP
int ft_board_setup(void *blob, struct bd_info *bd)
{
#ifdef CONFIG_IMX8M_DRAM_INLINE_ECC
	int rc;
	phys_addr_t ecc0_start = 0xb0000000;
	phys_addr_t ecc1_start = 0x130000000;
	phys_addr_t ecc2_start = 0x1b0000000;
	size_t ecc_size = 0x10000000;

	rc = add_res_mem_dt_node(blob, "ecc", ecc0_start, ecc_size);
	if (rc < 0) {
		printf("Could not create ecc0 reserved-memory node.\n");
		return rc;
	}

	rc = add_res_mem_dt_node(blob, "ecc", ecc1_start, ecc_size);
	if (rc < 0) {
		printf("Could not create ecc1 reserved-memory node.\n");
		return rc;
	}

	rc = add_res_mem_dt_node(blob, "ecc", ecc2_start, ecc_size);
	if (rc < 0) {
		printf("Could not create ecc2 reserved-memory node.\n");
		return rc;
	}
#endif
	if(!D18_HAS_ETH1)
		exp_strap_fdt_eth1_remove(blob, bd);

	strap_fdt_ram_setup(blob, bd);

	return 0;
}
#endif

#ifdef CONFIG_USB_DWC3

#define USB_PHY_CTRL0			0xF0040
#define USB_PHY_CTRL0_REF_SSP_EN	BIT(2)

#define USB_PHY_CTRL1			0xF0044
#define USB_PHY_CTRL1_RESET		BIT(0)
#define USB_PHY_CTRL1_COMMONONN		BIT(1)
#define USB_PHY_CTRL1_ATERESET		BIT(3)
#define USB_PHY_CTRL1_VDATSRCENB0	BIT(19)
#define USB_PHY_CTRL1_VDATDETENB0	BIT(20)

#define USB_PHY_CTRL2			0xF0048
#define USB_PHY_CTRL2_TXENABLEN0	BIT(8)

#define USB_PHY_CTRL6			0xF0058

#define HSIO_GPR_BASE                               (0x32F10000U)
#define HSIO_GPR_REG_0                              (HSIO_GPR_BASE)
#define HSIO_GPR_REG_0_USB_CLOCK_MODULE_EN_SHIFT    (1)
#define HSIO_GPR_REG_0_USB_CLOCK_MODULE_EN          (0x1U << HSIO_GPR_REG_0_USB_CLOCK_MODULE_EN_SHIFT)


static struct dwc3_device dwc3_device_data = {
#ifdef CONFIG_SPL_BUILD
	.maximum_speed = USB_SPEED_HIGH,
#else
	.maximum_speed = USB_SPEED_SUPER,
#endif
	.base = USB1_BASE_ADDR,
	.dr_mode = USB_DR_MODE_PERIPHERAL,
	.index = 0,
	.power_down_scale = 2,
};

int usb_gadget_handle_interrupts(int index)
{
	dwc3_uboot_handle_interrupt(index);
	return 0;
}

static void dwc3_nxp_usb_phy_init(struct dwc3_device *dwc3)
{
	u32 RegData;

	/* enable usb clock via hsio gpr */
	RegData = readl(HSIO_GPR_REG_0);
	RegData |= HSIO_GPR_REG_0_USB_CLOCK_MODULE_EN;
	writel(RegData, HSIO_GPR_REG_0);

	/* USB3.0 PHY signal fsel for 100M ref */
	RegData = readl(dwc3->base + USB_PHY_CTRL0);
	RegData = (RegData & 0xfffff81f) | (0x2a<<5);
	writel(RegData, dwc3->base + USB_PHY_CTRL0);

	RegData = readl(dwc3->base + USB_PHY_CTRL6);
	RegData &=~0x1;
	writel(RegData, dwc3->base + USB_PHY_CTRL6);

	RegData = readl(dwc3->base + USB_PHY_CTRL1);
	RegData &= ~(USB_PHY_CTRL1_VDATSRCENB0 | USB_PHY_CTRL1_VDATDETENB0 |
			USB_PHY_CTRL1_COMMONONN);
	RegData |= USB_PHY_CTRL1_RESET | USB_PHY_CTRL1_ATERESET;
	writel(RegData, dwc3->base + USB_PHY_CTRL1);

	RegData = readl(dwc3->base + USB_PHY_CTRL0);
	RegData |= USB_PHY_CTRL0_REF_SSP_EN;
	writel(RegData, dwc3->base + USB_PHY_CTRL0);

	RegData = readl(dwc3->base + USB_PHY_CTRL2);
	RegData |= USB_PHY_CTRL2_TXENABLEN0;
	writel(RegData, dwc3->base + USB_PHY_CTRL2);

	RegData = readl(dwc3->base + USB_PHY_CTRL1);
	RegData &= ~(USB_PHY_CTRL1_RESET | USB_PHY_CTRL1_ATERESET);
	writel(RegData, dwc3->base + USB_PHY_CTRL1);
}
#endif

#if defined(CONFIG_USB_DWC3) || defined(CONFIG_USB_XHCI_IMX8M)
int board_usb_init(int index, enum usb_init_type init)
{
	imx8m_usb_power(index, true);

	if (index == 0 && init == USB_INIT_DEVICE) {
		dwc3_nxp_usb_phy_init(&dwc3_device_data);
		return dwc3_uboot_init(&dwc3_device_data);
	}

	return 0;
}

int board_usb_cleanup(int index, enum usb_init_type init)
{
	if (index == 0 && init == USB_INIT_DEVICE) {
		dwc3_uboot_exit(index);
	}

	imx8m_usb_power(index, false);

	return 0;
}
#endif

static void setup_fec(void)
{
	struct iomuxc_gpr_base_regs *gpr =
		(struct iomuxc_gpr_base_regs *)IOMUXC_GPR_BASE_ADDR;

	/* Enable RGMII TX clk output */
	setbits_le32(&gpr->gpr[1], BIT(22));
}

static int setup_eqos(void)
{
	struct iomuxc_gpr_base_regs *gpr =
		(struct iomuxc_gpr_base_regs *)IOMUXC_GPR_BASE_ADDR;

	/* set INTF as RGMII, enable RGMII TXC clock */
	clrsetbits_le32(&gpr->gpr[1],
			IOMUXC_GPR_GPR1_GPR_ENET_QOS_INTF_SEL_MASK, BIT(16));
	setbits_le32(&gpr->gpr[1], BIT(19) | BIT(21));

	return set_clk_eqos(ENET_125MHZ);
}

#if CONFIG_IS_ENABLED(NET)
int board_phy_config(struct phy_device *phydev)
{
	if (phydev->drv->config)
		phydev->drv->config(phydev);
	return 0;
}
#endif

int checkboard( void ) {
	print_bootinfo();
	strap_show();

	return 0;
}

#define DISPMIX				13
#define MIPI				15

int board_init(void)
{
	struct arm_smccc_res res;

	if (CONFIG_IS_ENABLED(FEC_MXC)) {
		setup_fec();
	}

	if (CONFIG_IS_ENABLED(DWC_ETH_QOS)) {
		setup_eqos();
	}

#ifdef CONFIG_NAND_MXS
	setup_gpmi_nand();
#endif

#if defined(CONFIG_USB_DWC3) || defined(CONFIG_USB_XHCI_IMX8M)
	init_usb_clk();
#endif

	gpio_exp_init();
	exp_strap_show();

	/*
	 * setup U-Boot video:
	 * since at this stage we cannot read the environment yet, set the GPIO
	 * required for display init and toggle them later if video is off.
	 */
	video_init();
	gpio_direction_output(LCD0_VDD_EN, 1);
	gpio_direction_output(VIDEO_eDP_BRG_EN, 1);

	/* enable the dispmix & mipi phy power domain */
	arm_smccc_smc(IMX_SIP_GPC, IMX_SIP_GPC_PM_DOMAIN,
		      DISPMIX, true, 0, 0, 0, 0, &res);
	arm_smccc_smc(IMX_SIP_GPC, IMX_SIP_GPC_PM_DOMAIN,
		      MIPI, true, 0, 0, 0, 0, &res);

	return 0;
}

static int env_setup_video_link(int video_edp)
{
	char *list, *search;
	int video_on;
	ulong video_link;
	bool found = false, setup = false;

	/* setup video link */
	video_link = env_get_ulong("video_link", 10, (ulong)-1);
	if (video_edp) {
		if (video_link != 0) {
			env_set_ulong("video_link", 0);
			setup = true;
		}
	} else {
		if (video_link != 1) {
			env_set_ulong("video_link", 1);
			setup = true;
		}
	}

	/* video link on if specific fdt video overlay are selected */
	list = env_get("fdt_overlay_video_list");
	if (list) {
		if (video_edp) {
			search = strstr(list, "seco-imx8mp-d18-edp.dtbo");
			if (search)
				found = true;
		} else {
			search = strstr(list, "seco-imx8mp-d18-lvds-fhd.dtbo");
			if (search)
				found = true;
		}
	}

	video_on = (env_get_yesno("video_off") == 1) ? 0 : 1;
	if (video_on) {
		if (!found) {
			env_set_ulong("video_off", 1);
			setup = true;
		}
	} else {
		if (found) {
			env_set("video_off", "");
			setup = true;
		}
	}

	if (setup) {
		env_save();
		printf("Generating reset request to finish video setup...\n");
		do_reset(NULL, 0, 0, NULL);
	}

	return video_on;
}

int board_late_init(void)
{
	struct udevice *dev;
	int video_edp, video_on;
	int ret;

#ifdef CONFIG_ENV_IS_IN_MMC
	board_late_mmc_env_init();
#endif
#ifdef CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG
	env_set("board_name", "D18");
	env_set("board_rev", "iMX8MP");
#endif

	/*
	 * setup U-Boot video:
	 * - if video link environment does not match board straps and
	 *   fdt video overlay selection, configure it and reset board.
	 * - set GPIO as required by video state.
	 */
	video_edp = D18_HAS_EDP;
	video_on = env_setup_video_link(video_edp);
	if (video_on) {
		if (!video_edp) {
			/* video on, not eDP: disable eDP_BRG */
			gpio_direction_output(VIDEO_eDP_BRG_EN, 0);
		}
		/* video on: enable BKL */
		gpio_direction_output(LCD0_BKLT_EN, 1);
	} else {
		/* video off: disable all */
		gpio_direction_output(LCD0_VDD_EN, 0);
		gpio_direction_output(LCD0_BKLT_EN, 0);
		gpio_direction_output(VIDEO_eDP_BRG_EN, 0);
	}

	/* setup RTC PMIC voltage */
	ret = i2c_get_chip_for_busnum(0, 0x25, 1, &dev);
	if (!ret) {
		ret = dm_i2c_reg_write(dev, 0x1c, 0x32);
		if(ret)
			printf("RTC PMIC: i2c write failed, err %d\n", ret);
	} else
		printf("RTC PMIC: Cannot find udev for i2c bus 0\n");

/* seco_config variables */
#ifdef CONFIG_SECO_ENV_MANAGER
	gd->bsp_sources.kern_dev_list            = &kern_dev_imx8_list[0];
	gd->bsp_sources.kern_dev_num             = kern_dev_imx8_size;
	gd->bsp_sources.fdt_dev_list             = &fdt_dev_imx8_list[0];
	gd->bsp_sources.fdt_dev_num              = fdt_dev_imx8_size;
#ifdef CONFIG_OF_LIBFDT_OVERLAY
	gd->bsp_sources.fdt_overlay_dev_list     = fdt_overlay_dev_imx8_list;
	gd->bsp_sources.fdt_overlay_dev_num      = fdt_overlay_dev_imx8_size;
#endif
	gd->bsp_sources.ramfs_dev_list           = &ramfs_dev_imx8_list[0];
	gd->bsp_sources.ramfs_dev_num            = ramfs_dev_imx8_size;
	gd->bsp_sources.filesystem_dev_list      = &filesystem_dev_imx8_list[0];
	gd->bsp_sources.filesystem_dev_num       = filesystem_dev_imx8_size;
	if(!video_edp) {
		gd->boot_setup.video_mode_list           = video_mode_list_CFG_D1;
		gd->boot_setup.video_mode_num            = video_mode_size_CFG_D1;
	} else {
		gd->boot_setup.video_mode_list           = video_mode_list_CFG_D2;
		gd->boot_setup.video_mode_num            = video_mode_size_CFG_D2;
	}
	gd->boot_setup.touch_mode_list           = touch_mode_list;
	gd->boot_setup.touch_mode_num            = touch_mode_size;
#ifdef CONFIG_OF_LIBFDT_OVERLAY
	if(D18_IS_REVA0 || D18_IS_REVA1) {
		gd->boot_setup.overlay_peripheral_list   = overlay_peripheral_list_REVA;
		gd->boot_setup.overlay_peripheral_num    = overlay_peripheral_size_REVA;
	} else {
		gd->boot_setup.overlay_peripheral_list   = overlay_peripheral_list;
		gd->boot_setup.overlay_peripheral_num    = overlay_peripheral_size;
	}
#endif
#endif

	return 0;
}

#ifdef CONFIG_ANDROID_SUPPORT
bool is_power_key_pressed(void) {
	return (bool)(!!(readl(SNVS_HPSR) & (0x1 << 6)));
}
#endif

#ifdef CONFIG_SPL_MMC_SUPPORT
unsigned long spl_mmc_get_uboot_raw_sector(struct mmc *mmc)
{
	return CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR;
}
#endif

phys_size_t get_effective_memsize(void)
{
	unsigned long long sdram_size;
	/* return the first bank as effective memory */
	if(D18_IS_1GB)
		sdram_size = PHYS_DRAM_IS_1GB;
	if(D18_IS_2GB)
		sdram_size = PHYS_DRAM_IS_2GB;
	if(D18_IS_4GB || D18_IS_6GB || D18_IS_8GB)
		sdram_size = PHYS_DRAM_IS_3GB;

	if (rom_pointer[1])
		return (sdram_size - rom_pointer[1]);
	else
		return sdram_size;
}

#ifdef CONFIG_FSL_FASTBOOT
#ifdef CONFIG_ANDROID_RECOVERY
int is_recovery_key_pressing(void)
{
	return 0; /* TODO */
}
#endif /* CONFIG_ANDROID_RECOVERY */
#endif /* CONFIG_FSL_FASTBOOT */
