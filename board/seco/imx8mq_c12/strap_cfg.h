#ifndef _C12_REVISION_H_
#define _C12_REVISION_H_

int strap_get_ram_cfg (void);
int strap_get_video_cfg (void);
int strap_get_wifi_cfg (void);
int strap_get_revision_cfg (void);
int strap_get_pcie_cfg (void);
int strap_get_cpu_cfg (void);
int strap_get_spi_cfg (void);

void c12_fdt_ram_setup (void *blob, struct bd_info *bdev);
void c12_fdt_pcie_remove (void *blob, struct bd_info *bdev);

void strap_show ( void );

typedef enum {
	RAM_1GB = 0x0,
	RAM_2GB = 0x1,
	RAM_4GB = 0x2,
} RAM_STRAPS;

typedef enum {
	YES_HDMI = 0x0,
	NO_HDMI  = 0x1,
} HDMI_STRAPS;

typedef enum {
	YES_SD   = 0x0,
	YES_WIFI = 0x1,
} WIFI_STRAPS;

typedef enum {
	REV_C0 = 0x0,
	REV_C1 = 0x1,
	REV_D0 = 0x2,
	REV_D1 = 0x3,
	REV_C2 = 0x7,
} BOARD_REV;

typedef enum {
	YES_PCIE = 0x0,
	NO_PCIE  = 0x1,
} PCIE_STRAPS;

typedef enum {
	YES_CPUQL = 0x0,
	YES_CPUQ  = 0x1,
} CPU_STRAPS;

typedef enum {
	YES_CANRTC = 0x0,
	YES_SPI    = 0x1,
} SPI_STRAPS;

typedef struct {
	u8 ram_size;
	u8 video;
	u8 wifi;
	u8 hw_rev;
	u8 pcie;
	u8 cpu;
	u8 spi;
} strap_conf_t;

#define GET_C12_RAM_STRAPS   (strap_get_ram_cfg())
#define C12_IS_1GB           (GET_C12_RAM_STRAPS == RAM_1GB)
#define C12_IS_2GB           (GET_C12_RAM_STRAPS == RAM_2GB)
#define C12_IS_4GB           (GET_C12_RAM_STRAPS == RAM_4GB)

#define GET_C12_HDMI_STRAPS  (strap_get_video_cfg())
#define C12_HAS_HDMI         (GET_C12_HDMI_STRAPS == YES_HDMI)

#define GET_C12_WIFI_STRAPS  (strap_get_wifi_cfg())
#define C12_HAS_SD           (GET_C12_WIFI_STRAPS == YES_SD)
#define C12_HAS_WIFI         (GET_C12_WIFI_STRAPS == YES_WIFI)

#define GET_C12_REV_STRAPS   (strap_get_revision_cfg())
#define C12_IS_REVC0         (GET_C12_REV_STRAPS == REV_C0)
#define C12_IS_REVC1         (GET_C12_REV_STRAPS == REV_C1)
#define C12_IS_REVC2         (GET_C12_REV_STRAPS == REV_C2)
#define C12_IS_REVD0         (GET_C12_REV_STRAPS == REV_D0)
#define C12_IS_REVD1         (GET_C12_REV_STRAPS == REV_D1)

#define GET_C12_PCIE_STRAPS  (strap_get_pcie_cfg())
#define C12_HAS_PCIE         (GET_C12_PCIE_STRAPS == YES_PCIE)

#define GET_C12_CPU_STRAPS   (strap_get_cpu_cfg())
#define C12_HAS_CPUQL        (GET_C12_CPU_STRAPS == YES_CPUQL)
#define C12_HAS_CPUQ         (GET_C12_CPU_STRAPS == YES_CPUQ)

#define GET_C12_SPI_STRAPS   (strap_get_spi_cfg())
#define C12_HAS_CANRTC       (GET_C12_SPI_STRAPS == YES_CANRTC)
#define C12_HAS_SPI          (GET_C12_SPI_STRAPS == YES_SPI)

#endif
