/*
 * 
 *
 * Reading C12 Board type
 *
 * gianfranco.mariotti@seco.com
 *
 *
 *
 */

#include <common.h>
#include <malloc.h>
#include <errno.h>
#include <asm/io.h>
#include <miiphy.h>
#include <netdev.h>
#include <asm/mach-imx/iomux-v3.h>
#include <asm-generic/gpio.h>
#include <fsl_esdhc.h>
#include <mmc.h>
#include <asm/arch/imx8mq_pins.h>
#include <asm/arch/sys_proto.h>
#include <asm/mach-imx/gpio.h>
#include "strap_cfg.h"

#define PULLUP_PAD_CTRL  (PAD_CTL_PUE)

/* ____________________________________________________________________________
  |                                                                            |
  |                                  C12 REVISION                              |
  |____________________________________________________________________________|
*/
#define C12_IOMUX_REG(x)        ((x))
#define C12_PADCTRL_REG(x)      ((x))
#define C12_GDIR_REG_IN(x,n)    writel((readl(x + 0x4)) & ~(1<<n), x + 0x4 )
#define C12_PSR_REG(x,n)        (readl(x + 0x8) & (1<<n))

#define GPIO1_PAD_BASE          0x30200000
#define GPIO3_PAD_BASE          0x30220000
#define GPIO4_PAD_BASE          0x30230000

/* CFG STRAPS: [GPIO_CFG_9, ..., GPIO_CFG_0] */
#define GPIO_CFG_0              16
#define GPIO_CFG_1              18
#define GPIO_CFG_2              17
#define GPIO_CFG_3              13
#define GPIO_CFG_4              15
#define GPIO_CFG_5              22
#define GPIO_CFG_6              21
#define GPIO_CFG_7              0
#define GPIO_CFG_8              5
#define GPIO_CFG_9              29

struct sizes {
	u32 s0;
	u32 s1;
};

DECLARE_GLOBAL_DATA_PTR;

iomux_v3_cfg_t const board_conf_pads[] = {
	/* RAM SIZE */
	IMX8MQ_PAD_NAND_READY_B__GPIO3_IO16 | MUX_PAD_CTRL(PULLUP_PAD_CTRL), /* cfg_0 */
	IMX8MQ_PAD_NAND_WP_B__GPIO3_IO18    | MUX_PAD_CTRL(PULLUP_PAD_CTRL), /* cfg_1 */
	/* HDMI/DP */
	IMX8MQ_PAD_NAND_WE_B__GPIO3_IO17    | MUX_PAD_CTRL(PULLUP_PAD_CTRL), /* cfg_2 */
	/* uSD | WIFI: Wilink */
	IMX8MQ_PAD_NAND_DATA07__GPIO3_IO13  | MUX_PAD_CTRL(PULLUP_PAD_CTRL), /* cfg_3 */
	/* HW REV */
	IMX8MQ_PAD_GPIO1_IO15__GPIO1_IO15   | MUX_PAD_CTRL(PULLUP_PAD_CTRL), /* cfg_4 */
	IMX8MQ_PAD_SAI2_RXC__GPIO4_IO22     | MUX_PAD_CTRL(PULLUP_PAD_CTRL), /* cfg_5 */
	IMX8MQ_PAD_SAI2_RXFS__GPIO4_IO21    | MUX_PAD_CTRL(PULLUP_PAD_CTRL), /* cfg_6 */
	/* PCIe */
	IMX8MQ_PAD_GPIO1_IO00__GPIO1_IO0    | MUX_PAD_CTRL(PULLUP_PAD_CTRL), /* cfg_7 */
	/* CPU TYPE: Quad | Quad Lite */
	IMX8MQ_PAD_NAND_CLE__GPIO3_IO5      | MUX_PAD_CTRL(PULLUP_PAD_CTRL), /* cfg_8 */
	/* SPI | CAN/RTC */
	IMX8MQ_PAD_SAI3_RXC__GPIO4_IO29     | MUX_PAD_CTRL(PULLUP_PAD_CTRL), /* cfg_9 */
};

static int strap_get_seco_cfg ( void ) {
	int value = 0;

	imx_iomux_v3_setup_multiple_pads(board_conf_pads, ARRAY_SIZE(board_conf_pads));

	/* Mux as Input */
	C12_GDIR_REG_IN(GPIO3_PAD_BASE,GPIO_CFG_0);
	C12_GDIR_REG_IN(GPIO3_PAD_BASE,GPIO_CFG_1);
	C12_GDIR_REG_IN(GPIO3_PAD_BASE,GPIO_CFG_2);
	C12_GDIR_REG_IN(GPIO3_PAD_BASE,GPIO_CFG_3);
	C12_GDIR_REG_IN(GPIO1_PAD_BASE,GPIO_CFG_4);
	C12_GDIR_REG_IN(GPIO4_PAD_BASE,GPIO_CFG_5);
	C12_GDIR_REG_IN(GPIO4_PAD_BASE,GPIO_CFG_6);
	C12_GDIR_REG_IN(GPIO1_PAD_BASE,GPIO_CFG_7);
	C12_GDIR_REG_IN(GPIO3_PAD_BASE,GPIO_CFG_8);
	C12_GDIR_REG_IN(GPIO4_PAD_BASE,GPIO_CFG_9);

	/* Read Conf value */
	value = (C12_PSR_REG(GPIO3_PAD_BASE,GPIO_CFG_0) >> GPIO_CFG_0) << 0 |
	        (C12_PSR_REG(GPIO3_PAD_BASE,GPIO_CFG_1) >> GPIO_CFG_1) << 1 |
	        (C12_PSR_REG(GPIO3_PAD_BASE,GPIO_CFG_2) >> GPIO_CFG_2) << 2 |
	        (C12_PSR_REG(GPIO3_PAD_BASE,GPIO_CFG_3) >> GPIO_CFG_3) << 3 |
	        (C12_PSR_REG(GPIO1_PAD_BASE,GPIO_CFG_4) >> GPIO_CFG_4) << 4 |
	        (C12_PSR_REG(GPIO4_PAD_BASE,GPIO_CFG_5) >> GPIO_CFG_5) << 5 |
	        (C12_PSR_REG(GPIO4_PAD_BASE,GPIO_CFG_6) >> GPIO_CFG_6) << 6 |
	        (C12_PSR_REG(GPIO1_PAD_BASE,GPIO_CFG_7) >> GPIO_CFG_7) << 7 |
	        (C12_PSR_REG(GPIO3_PAD_BASE,GPIO_CFG_8) >> GPIO_CFG_8) << 8 |
	        (C12_PSR_REG(GPIO4_PAD_BASE,GPIO_CFG_9) >> GPIO_CFG_9) << 9;

	return value & 0x3FF;
}

int strap_get_ram_cfg (void) {
	return strap_get_seco_cfg() & 0b11;
};

int strap_get_video_cfg (void) {
	return (strap_get_seco_cfg() >> 2) & 0b1;
};

int strap_get_wifi_cfg (void) {
	return (strap_get_seco_cfg() >> 3) & 0b1;
};

int strap_get_revision_cfg (void) {
	return (strap_get_seco_cfg() >> 4) & 0b111;
};

int strap_get_pcie_cfg (void) {
	return (strap_get_seco_cfg() >> 7) & 0b1;
};

int strap_get_cpu_cfg (void) {
	return (strap_get_seco_cfg() >> 8) & 0b1;
};

int strap_get_spi_cfg (void) {
	return (strap_get_seco_cfg() >> 9) & 0b1;
};

void strap_show( void ) {
	strap_conf_t c12_strap_conf;
	c12_strap_conf.ram_size = GET_C12_RAM_STRAPS;
	c12_strap_conf.video = GET_C12_HDMI_STRAPS;
	c12_strap_conf.wifi = GET_C12_WIFI_STRAPS;
	c12_strap_conf.hw_rev = GET_C12_REV_STRAPS;
	c12_strap_conf.pcie = GET_C12_PCIE_STRAPS;
	c12_strap_conf.cpu = GET_C12_CPU_STRAPS;
	c12_strap_conf.spi = GET_C12_SPI_STRAPS;

	printf( "Straps:\n" );

	printf( " - RAM code: %d (", c12_strap_conf.ram_size );
	switch ( c12_strap_conf.ram_size ) {
		case RAM_1GB:
			printf( "1GB" );
			break;
		case RAM_2GB:
			printf( "2GB" );
			break;
		case RAM_4GB:
			printf( "4GB" );
			break;
		default:
			printf( "Unknown" );
			break;
	}
	printf ( ")\n" );

	printf( " - Revision code: %d (", c12_strap_conf.hw_rev );
	switch ( c12_strap_conf.hw_rev ) {
		case REV_C0:
			printf( "REVC0" );
			break;
		case REV_C1:
			printf( "REVC1" );
			break;
		case REV_C2:
			printf( "REVC2" );
			break;
		case REV_D0:
			printf( "REVD0" );
			break;
		case REV_D1:
			printf( "REVD1" );
			break;
		default:
			printf( "Unknown" );
			break;
	}
	printf ( ")\n" );

	printf( " - HDMI/DP code: %d (", c12_strap_conf.video );
	switch ( c12_strap_conf.video ) {
		case YES_HDMI:
			printf( "HDMI/DP" );
			break;
		case NO_HDMI:
			printf( "NO HDMI/DP" );
			break;
		default:
			printf( "Unknown" );
			break;
	}
	printf ( ")\n" );

	printf( " - uSD | WIFI code: %d (", c12_strap_conf.wifi );
	switch ( c12_strap_conf.wifi ) {
		case YES_SD:
			printf( "uSD" );
			break;
		case YES_WIFI:
			printf( "WIFI" );
			break;
		default:
			printf( "Unknown" );
			break;
	}
	printf ( ")\n" );

	printf( " - PCIe code: %d (", c12_strap_conf.pcie );
	switch ( c12_strap_conf.pcie ) {
		case YES_PCIE:
			printf( "PCIe" );
			break;
		case NO_PCIE:
			printf( "NO PCIe" );
			break;
		default:
			printf( "Unknown" );
			break;
	}
	printf ( ")\n" );

	printf( " - CPU code: %d (", c12_strap_conf.cpu );
	switch ( c12_strap_conf.cpu ) {
		case YES_CPUQL:
			printf( "QUADLITE" );
			break;
		case YES_CPUQ:
			printf( "QUAD" );
			break;
		default:
			printf( "Unknown" );
			break;
	}
	printf ( ")\n" );

	printf( " - CAN/RTC | SPI code: %d (", c12_strap_conf.spi );
	switch ( c12_strap_conf.spi ) {
		case YES_CANRTC:
			printf( "CAN/RTC" );
			break;
		case YES_SPI:
			printf( "SPI on carrier" );
			break;
		default:
			printf( "Unknown" );
			break;
	}
	printf ( ")\n" );
}

void c12_fdt_ram_setup(void *blob, struct bd_info *bdev) {
	int offset, ret;
	struct sizes ssize;
	
	printf("Overlay dts /reserved-memory/linux,cma/: size = ");
	offset = fdt_path_offset(blob, "/reserved-memory/linux,cma/");
	if (offset < 0) {
		printf("ERROR: find node /: %s.\n", fdt_strerror(offset));
		return;
	}
	if(C12_IS_1GB) {
		ssize.s0 = cpu_to_fdt32(0x0);
		ssize.s1 = cpu_to_fdt32(0x14000000);
		printf("<0x0 0x14000000>\n");
	}
	if(C12_IS_4GB || C12_IS_2GB) {
		ssize.s0 = cpu_to_fdt32(0x0);
		ssize.s1 = cpu_to_fdt32(0x20000000);
		printf("<0x0 0x20000000>\n");
	}

	ret = fdt_setprop(blob, offset, "size", &ssize, sizeof(ssize));
	if (ret < 0)
		printf("ERROR: could not update revision property %s.\n",
			fdt_strerror(ret));
	return;
}

void c12_fdt_pcie_remove(void *blob, struct bd_info *bdev) {
	int offset, ret;
	char status[10];

	printf("Overlay dts: disabling pcie\n");
	offset = fdt_path_offset(blob, "/hsio/pcie@33800000");
	if (offset < 0) {
		printf("ERROR: find node /hsio/pcie@33800000\n");
		return;
	}

	sprintf(status, "%s","disabled" );
	ret = fdt_setprop(blob, offset, "status", status, sizeof(status));
	if (ret < 0)
		printf("ERROR: could not update revision property %s.\n",
			fdt_strerror(ret));
	
	offset = fdt_path_offset(blob, "/hsio/pcie@33c00000");
	if (offset < 0) {
		printf("ERROR: find node /hsio/pcie@33c00000\n");
		return;
	}

	sprintf(status, "%s","disabled" );
	ret = fdt_setprop(blob, offset, "status", status, sizeof(status));
	if (ret < 0)
		printf("ERROR: could not update revision property %s.\n",
			fdt_strerror(ret));

	return;
}
