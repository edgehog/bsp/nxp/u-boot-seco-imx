/*
 * (C) Copyright 2015 Seco
 *
 * Author: Davide Cardillo <davide.cardillo@seco.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

#include <common.h>
#include <command.h>
#include <env.h>


#include <seco/env_common.h>
#include <configs/seco_mx8_dtbo.h>


#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

 
/* *********************************** IMX8 *********************************** */

data_boot_dev_t kern_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_KERNEL_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD,     STR(MACRO_ENV_KERNEL_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD,      LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_KERNEL_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_KERNEL_SRC_TFTP),     "",                       LOAD_ADDR_KERNEL_REMOTE_DEV, SCFG_KERNEL_FILENAME },
};

size_t kern_dev_imx8_size = sizeof( kern_dev_imx8_list ) / sizeof( kern_dev_imx8_list[0] );


data_boot_dev_t fdt_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FDT_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX8_FILE },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD,     STR(MACRO_ENV_FDT_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD,      LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX8_FILE },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FDT_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX8_FILE },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_FDT_SRC_TFTP),     "",                       LOAD_ADDR_FDT_REMOTE_DEV, SCFG_DEFAULT_FDT_IMX8_FILE },
};

size_t fdt_dev_imx8_size = sizeof( fdt_dev_imx8_list ) / sizeof( fdt_dev_imx8_list[0] );


#ifdef CONFIG_OF_LIBFDT_OVERLAY
data_boot_dev_t fdt_overlay_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    "", "" },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD,     STR(MACRO_ENV_FDT_SRC_USDHCI),           SCFG_BOOT_DEV_ID_SD,   "", "" },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FDT_OVERLAY_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     "", "" },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_FDT_OVERLAY_SRC_TFTP),     "",                       "", "" },
};

size_t fdt_overlay_dev_imx8_size = sizeof( fdt_overlay_dev_imx8_list ) / sizeof( fdt_overlay_dev_imx8_list[0] );
#endif /* CONFIG_OF_LIBFDT_OVERLAY */


data_boot_dev_t ramfs_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_NONE,     SECO_DEV_LABEL_NONE,   "0x0",                             "0",                      LOAD_ADDR_RAMFS_LOCAL_DEV,    ""                 },
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_RAMFS_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_RAMFS_LOCAL_DEV,    SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD,     STR(MACRO_ENV_RAMFS_SRC_USDHCI),   SCFG_BOOT_DEV_ID_SD,      LOAD_ADDR_RAMFS_LOCAL_DEV,    SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_RAMFS_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_RAMFS_LOCAL_DEV,    SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     SECO_DEV_LABEL_TFTP,   STR(MACRO_ENV_RAMFS_SRC_TFTP),     "",                       LOAD_ADDR_RAMFS_REMOTE_DEV,   SCFG_RAMFS_FILENAME },
};

size_t ramfs_dev_imx8_size = sizeof( ramfs_dev_imx8_list ) / sizeof( ramfs_dev_imx8_list[0] );


data_boot_dev_t filesystem_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_EMMC,     SECO_DEV_LABEL_EMMC,   STR(MACRO_ENV_FS_SRC_USDHCI),   SCFG_ROOT_DEV_ID_EMMC,    "", "" },
	{ SECO_DEV_TYPE_SD,       SECO_DEV_LABEL_SD,     STR(MACRO_ENV_FS_SRC_USDHCI),   SCFG_ROOT_DEV_ID_SD,      "", "" },
	{ SECO_DEV_TYPE_USB,      SECO_DEV_LABEL_USB,    STR(MACRO_ENV_FS_SRC_USB),      SCFG_ROOT_DEV_ID_USB,     "", "" },
	{ SECO_DEV_TYPE_NFS,      SECO_DEV_LABEL_NFS,    STR(MACRO_ENV_FS_SRC_NFS),      "",                       "", "" },
};

size_t filesystem_dev_imx8_size = sizeof( filesystem_dev_imx8_list ) / sizeof( filesystem_dev_imx8_list[0] );


/* LVDS, HDMI */
video_mode_t video_mode_list_CFG_0 [] = {
	{
		/* NO DISPLAY */
		.label = SECO_VIDEO_LABEL_NONE,
		.video = {
			{ VIDEO_NOT_USED, NO_VIDEO, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = NULL,
		.use_bootargs = 0,
	}, {
		/* HDMI */
		.label = SECO_VIDEO_LABEL_HDMI,
		.video = {
			{ VIDEO_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = STR(ENV_DTBO_C12_HDMI),
		.use_bootargs = 0,
	}, {
		/* LVDS-DCSS */
		.label = SECO_VIDEO_LABEL_LVDS_DCSS,
		.video = {
			{ VIDEO_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = STR(ENV_DTBO_C12_LVDS_SINGLE_DCSS),
		.use_bootargs = 0,
	}, {
		/* LVDS-LCDIF */
		.label = SECO_VIDEO_LABEL_LVDS_LCDIF,
		.video = {
			{ VIDEO_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = STR(ENV_DTBO_C12_LVDS_SINGLE_LCDIF),
		.use_bootargs = 0,
	},
};

size_t video_mode_size_CFG_0 = sizeof( video_mode_list_CFG_0 ) / sizeof( video_mode_list_CFG_0[0] );


/* LVDS */
video_mode_t video_mode_list_CFG_1 [] = {
	{
		/* NO DISPLAY */
		.label = SECO_VIDEO_LABEL_NONE,
		.video = {
			{ VIDEO_NOT_USED, NO_VIDEO, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = NULL,
		.use_bootargs = 0,
	}, {
		/* LVDS-DCSS */
		.label = SECO_VIDEO_LABEL_LVDS_DCSS,
		.video = {
			{ VIDEO_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = STR(ENV_DTBO_C12_LVDS_SINGLE_DCSS),
		.use_bootargs = 0,
	}, {
		/* LVDS-LCDIF */
		.label = SECO_VIDEO_LABEL_LVDS_LCDIF,
		.video = {
			{ VIDEO_USED, VIDEO_LVDS, NO_VIDEO_ARGS },
		},
		.panel_name = "none",
		.dtbo_conf_file = STR(ENV_DTBO_C12_LVDS_SINGLE_LCDIF),
		.use_bootargs = 0,
	},
};

size_t video_mode_size_CFG_1 = sizeof( video_mode_list_CFG_1 ) / sizeof( video_mode_list_CFG_1[0] );


#ifdef CONFIG_OF_LIBFDT_OVERLAY
/* *********************************** FDT OVERLAY *********************************** */

overlay_list_t overlay_peripheral_list_CFG_0 [] = {
	{
		.title = "can/rtc support",
		.options = {
			{ "not used", "" }, // default
			{ "can/rtc", STR(ENV_DTBO_C12_CAN_RTC) },
		},
	}, {
		.title = "wilink support",
		.options = {
			{ "not used", "" }, // default
			{ "wilink", STR(ENV_DTBO_C12_WILINK) },
		},
	},
};

size_t overlay_peripheral_size_CFG_0 = sizeof( overlay_peripheral_list_CFG_0 ) / sizeof( overlay_peripheral_list_CFG_0[0] );


overlay_list_t overlay_peripheral_list_CFG_1 [] = {
	{
		.title = "wilink support",
		.options = {
			{ "not used", "" }, // default
			{ "wilink", STR(ENV_DTBO_C12_WILINK) },
		},
	},
};

size_t overlay_peripheral_size_CFG_1 = sizeof( overlay_peripheral_list_CFG_1 ) / sizeof( overlay_peripheral_list_CFG_1[0] );


overlay_list_t overlay_peripheral_list_CFG_2 [] = {
	{
		.title = "can/rtc support",
		.options = {
			{ "not used", "" }, // default
			{ "can/rtc", STR(ENV_DTBO_C12_CAN_RTC) },
		},
	},
};

size_t overlay_peripheral_size_CFG_2 = sizeof( overlay_peripheral_list_CFG_2 ) / sizeof( overlay_peripheral_list_CFG_2[0] );


overlay_list_t overlay_peripheral_list_CFG_3 [] = {
};

size_t overlay_peripheral_size_CFG_3 = sizeof( overlay_peripheral_list_CFG_3 ) / sizeof( overlay_peripheral_list_CFG_3[0] );


#endif  /* CONFIG_OF_LIBFDT_OVERLAY */
