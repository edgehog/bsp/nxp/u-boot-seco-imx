// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright 2018 NXP
 */

#include <common.h>
#include <cpu_func.h>
#include <env.h>
#include <errno.h>
#include <init.h>
#include <asm/global_data.h>
#include <linux/delay.h>
#include <linux/libfdt.h>
#include <fsl_esdhc_imx.h>
#include <fdt_support.h>
#include <asm/io.h>
#include <asm/gpio.h>
#include <asm/arch/clock.h>
#include <asm/arch/sci/sci.h>
#include <asm/arch/imx8-pins.h>
#include <asm/arch/snvs_security_sc.h>
#include <asm/arch/iomux.h>
#include <asm/arch/sys_proto.h>
#include <usb.h>
#include "../common/tcpc.h"
#include <asm/mach-imx/video.h>
#include <imxdpuv1.h>
#include "../common/proto_seco.h"
#include "seco/seco_env_gd.h"
#include "strap_cfg.h"
#include "env_conf.h"
#include "../common/seco_eeprom_manager.h"

DECLARE_GLOBAL_DATA_PTR;

#define ENET_INPUT_PAD_CTRL	((SC_PAD_CONFIG_OD_IN << PADRING_CONFIG_SHIFT) | (SC_PAD_ISO_OFF << PADRING_LPCONFIG_SHIFT) \
                        | (SC_PAD_28FDSOI_DSE_18V_10MA << PADRING_DSE_SHIFT) | (SC_PAD_28FDSOI_PS_PU << PADRING_PULL_SHIFT))

#define ENET_NORMAL_PAD_CTRL	((SC_PAD_CONFIG_NORMAL << PADRING_CONFIG_SHIFT) | (SC_PAD_ISO_OFF << PADRING_LPCONFIG_SHIFT) \
                        | (SC_PAD_28FDSOI_DSE_18V_10MA << PADRING_DSE_SHIFT) | (SC_PAD_28FDSOI_PS_PU << PADRING_PULL_SHIFT))

#define GPIO_PAD_CTRL	((SC_PAD_CONFIG_NORMAL << PADRING_CONFIG_SHIFT) | \
             (SC_PAD_ISO_OFF << PADRING_LPCONFIG_SHIFT) | \
             (SC_PAD_28FDSOI_DSE_DV_HIGH << PADRING_DSE_SHIFT) | \
             (SC_PAD_28FDSOI_PS_PU << PADRING_PULL_SHIFT))

#define UART_PAD_CTRL	((SC_PAD_CONFIG_OUT_IN << PADRING_CONFIG_SHIFT) | \
             (SC_PAD_ISO_OFF << PADRING_LPCONFIG_SHIFT) | \
             (SC_PAD_28FDSOI_DSE_DV_HIGH << PADRING_DSE_SHIFT) | \
             (SC_PAD_28FDSOI_PS_PU << PADRING_PULL_SHIFT))

boot_mem_dev_t boot_mem_dev_list[SECO_NUM_BOOT_DEV] = {
        { MMC1_BOOT, SECO_DEV_LABEL_EMMC },
        { USB_BOOT,  SECO_DEV_LABEL_USB  }
};

int usdhc_devno[2] = { BOARD_BOOT_ID_EMMC, -1};

static iomux_cfg_t uart2_pads[] = {
    SC_P_UART2_RX | MUX_PAD_CTRL(UART_PAD_CTRL),
    SC_P_UART2_TX | MUX_PAD_CTRL(UART_PAD_CTRL),
};

static void setup_iomux_uart(void)
{
    imx8_iomux_setup_multiple_pads(uart2_pads, ARRAY_SIZE(uart2_pads));
}

int board_early_init_f(void)
{
    sc_pm_clock_rate_t rate = SC_80MHZ;
    int ret;

    /* Set UART2 clock root to 80 MHz */
    ret = sc_pm_setup_uart(SC_R_UART_2, rate);
    if (ret)
        return ret;

    setup_iomux_uart();

    return 0;
}

#if CONFIG_IS_ENABLED(DM_GPIO)
static void board_gpio_init(void)
{
}
#else
static inline void board_gpio_init(void) {}
#endif

#if IS_ENABLED(CONFIG_FEC_MXC)
#include <miiphy.h>

#ifndef CONFIG_DM_ETH
static iomux_cfg_t pad_enet1[] = {
    SC_P_SPDIF0_TX | MUX_MODE_ALT(3) | MUX_PAD_CTRL(ENET_INPUT_PAD_CTRL),
    SC_P_SPDIF0_RX | MUX_MODE_ALT(3) | MUX_PAD_CTRL(ENET_INPUT_PAD_CTRL),
    SC_P_ESAI0_TX3_RX2 | MUX_MODE_ALT(3) | MUX_PAD_CTRL(ENET_INPUT_PAD_CTRL),
    SC_P_ESAI0_TX2_RX3 | MUX_MODE_ALT(3) | MUX_PAD_CTRL(ENET_INPUT_PAD_CTRL),
    SC_P_ESAI0_TX1 | MUX_MODE_ALT(3) | MUX_PAD_CTRL(ENET_INPUT_PAD_CTRL),
    SC_P_ESAI0_TX0 | MUX_MODE_ALT(3) | MUX_PAD_CTRL(ENET_INPUT_PAD_CTRL),
    SC_P_ESAI0_SCKR | MUX_MODE_ALT(3) | MUX_PAD_CTRL(ENET_NORMAL_PAD_CTRL),
    SC_P_ESAI0_TX4_RX1 | MUX_MODE_ALT(3) | MUX_PAD_CTRL(ENET_NORMAL_PAD_CTRL),
    SC_P_ESAI0_TX5_RX0 | MUX_MODE_ALT(3) | MUX_PAD_CTRL(ENET_NORMAL_PAD_CTRL),
    SC_P_ESAI0_FST  | MUX_MODE_ALT(3) | MUX_PAD_CTRL(ENET_NORMAL_PAD_CTRL),
    SC_P_ESAI0_SCKT | MUX_MODE_ALT(3) | MUX_PAD_CTRL(ENET_NORMAL_PAD_CTRL),
    SC_P_ESAI0_FSR  | MUX_MODE_ALT(3) | MUX_PAD_CTRL(ENET_NORMAL_PAD_CTRL),

    /* Shared MDIO */
    SC_P_ENET0_MDC | MUX_PAD_CTRL(ENET_NORMAL_PAD_CTRL),
    SC_P_ENET0_MDIO | MUX_PAD_CTRL(ENET_NORMAL_PAD_CTRL),
};

static iomux_cfg_t pad_enet0[] = {
    SC_P_ENET0_RGMII_RX_CTL | MUX_PAD_CTRL(ENET_INPUT_PAD_CTRL),
    SC_P_ENET0_RGMII_RXD0 | MUX_PAD_CTRL(ENET_INPUT_PAD_CTRL),
    SC_P_ENET0_RGMII_RXD1 | MUX_PAD_CTRL(ENET_INPUT_PAD_CTRL),
    SC_P_ENET0_RGMII_RXD2 | MUX_PAD_CTRL(ENET_INPUT_PAD_CTRL),
    SC_P_ENET0_RGMII_RXD3 | MUX_PAD_CTRL(ENET_INPUT_PAD_CTRL),
    SC_P_ENET0_RGMII_RXC | MUX_PAD_CTRL(ENET_INPUT_PAD_CTRL),
    SC_P_ENET0_RGMII_TX_CTL | MUX_PAD_CTRL(ENET_NORMAL_PAD_CTRL),
    SC_P_ENET0_RGMII_TXD0 | MUX_PAD_CTRL(ENET_NORMAL_PAD_CTRL),
    SC_P_ENET0_RGMII_TXD1 | MUX_PAD_CTRL(ENET_NORMAL_PAD_CTRL),
    SC_P_ENET0_RGMII_TXD2 | MUX_PAD_CTRL(ENET_NORMAL_PAD_CTRL),
    SC_P_ENET0_RGMII_TXD3 | MUX_PAD_CTRL(ENET_NORMAL_PAD_CTRL),
    SC_P_ENET0_RGMII_TXC | MUX_PAD_CTRL(ENET_NORMAL_PAD_CTRL),

    /* Shared MDIO */
    SC_P_ENET0_MDC | MUX_PAD_CTRL(ENET_NORMAL_PAD_CTRL),
    SC_P_ENET0_MDIO | MUX_PAD_CTRL(ENET_NORMAL_PAD_CTRL),
};
static iomux_cfg_t pad_rst_enet[] = {
    /*Rst enet*/
    SC_P_SPI3_SDO | MUX_MODE_ALT(4) | MUX_PAD_CTRL(GPIO_PAD_CTRL),
};

#define RST_ENET   IMX_GPIO_NR(0, 14)

static void setup_iomux_fec(void)
{
    if (0 == CONFIG_FEC_ENET_DEV)
        imx8_iomux_setup_multiple_pads(pad_enet0, ARRAY_SIZE(pad_enet0));
    else
        imx8_iomux_setup_multiple_pads(pad_enet1, ARRAY_SIZE(pad_enet1));
}

static void enet_device_phy_reset(void)
{
    int ret = 0;

    imx8_iomux_setup_multiple_pads(pad_rst_enet, ARRAY_SIZE(pad_rst_enet));
    ret = gpio_request(RST_ENET, "rst_gpio-gpio0-io14");
    if(ret)
        printf("Cannot request rst_gpio-gpio0-io14 \n");
    
    gpio_direction_output(RST_ENET, 1);
    udelay(100);
    gpio_direction_output(RST_ENET, 0);

    /* The board has a long delay for this reset to become stable */
    mdelay(200);
}

int board_eth_init(bd_t *bis)
{
    int ret;
    struct power_domain pd;

    printf("[%s] %d\n", __func__, __LINE__);

    /* Reset ENET PHY */
    enet_device_phy_reset();

    if (CONFIG_FEC_ENET_DEV) {
        if (!power_domain_lookup_name("conn_enet1", &pd))
            power_domain_on(&pd);
    } else {
        if (!power_domain_lookup_name("conn_enet0", &pd))
            power_domain_on(&pd);
    }

    setup_iomux_fec();

    ret = fecmxc_initialize_multi(bis, CONFIG_FEC_ENET_DEV,
        CONFIG_FEC_MXC_PHYADDR, IMX_FEC_BASE);
    if (ret)
        printf("FEC1 MXC: %s:failed\n", __func__);

    return ret;
}

int board_phy_config(struct phy_device *phydev)
{	
    int val, ret;

    /*Configure Status Led for enabled Fec device*/
    ret = phy_write(phydev,CONFIG_FEC_ENET_DEV, DP83867_LEDCR1, 0xbb58);
    if (ret)
       return ret;

    val = phy_read(phydev, CONFIG_FEC_ENET_DEV, DP83867_LEDCR2);
    val = val & ~(0x444);
    ret = phy_write(phydev, CONFIG_FEC_ENET_DEV, DP83867_LEDCR2,val);
    if (ret)
       return ret;

    if (phydev->drv->config)
        phydev->drv->config(phydev);

    return 0;
}
#endif
#endif

int checkboard(void)
{
    puts("Board: SECO iMX8QXP C57\n");

    strap_get_board_cfg( );
    strap_show();
    print_bootinfo();

    return 0;
}

#ifdef CONFIG_FSL_HSIO

#define PCIE_PAD_CTRL	((SC_PAD_CONFIG_OD_IN << PADRING_CONFIG_SHIFT))
static iomux_cfg_t board_pcie_pins[] = {
    SC_P_PCIE_CTRL0_CLKREQ_B | MUX_MODE_ALT(0) | MUX_PAD_CTRL(PCIE_PAD_CTRL),
    SC_P_PCIE_CTRL0_WAKE_B | MUX_MODE_ALT(0) | MUX_PAD_CTRL(PCIE_PAD_CTRL),
    SC_P_PCIE_CTRL0_PERST_B | MUX_MODE_ALT(0) | MUX_PAD_CTRL(PCIE_PAD_CTRL),
};

static void imx8qxp_hsio_initialize(void)
{
    struct power_domain pd;
    int ret;

    if (!power_domain_lookup_name("hsio_pcie1", &pd)) {
        ret = power_domain_on(&pd);
        if (ret)
            printf("hsio_pcie1 Power up failed! (error = %d)\n", ret);
    }

    if (!power_domain_lookup_name("hsio_gpio", &pd)) {
        ret = power_domain_on(&pd);
        if (ret)
             printf("hsio_gpio Power up failed! (error = %d)\n", ret);
    }

    lpcg_all_clock_on(HSIO_PCIE_X1_LPCG);
    lpcg_all_clock_on(HSIO_PHY_X1_LPCG);
    lpcg_all_clock_on(HSIO_PHY_X1_CRR1_LPCG);
    lpcg_all_clock_on(HSIO_PCIE_X1_CRR3_LPCG);
    lpcg_all_clock_on(HSIO_MISC_LPCG);
    lpcg_all_clock_on(HSIO_GPIO_LPCG);

    imx8_iomux_setup_multiple_pads(board_pcie_pins, ARRAY_SIZE(board_pcie_pins));
}

void pci_init_board(void)
{
    imx8qxp_hsio_initialize();

    /* test the 1 lane mode of the PCIe A controller */
    mx8qxp_pcie_init();
}

#endif


#ifdef CONFIG_USB

#define USB1_PWR_EN_GPIO IMX_GPIO_NR(4, 3)
#define HUB_USB_RST_GPIO IMX_GPIO_NR(3, 17)

static iomux_cfg_t ss_rst_hub_gpio[] = {
    SC_P_USB_SS3_TC0 | MUX_MODE_ALT(4) | MUX_PAD_CTRL(GPIO_PAD_CTRL),
    SC_P_QSPI0B_SCLK | MUX_MODE_ALT(4) | MUX_PAD_CTRL(GPIO_PAD_CTRL),
};

void hub_rst(void){

    imx8_iomux_setup_multiple_pads(ss_rst_hub_gpio, ARRAY_SIZE(ss_rst_hub_gpio));

    gpio_request(USB1_PWR_EN_GPIO, "usb1_pwr_en");
    gpio_request(HUB_USB_RST_GPIO, "hub_usb_rst");

    gpio_direction_output(USB1_PWR_EN_GPIO, 0);
    gpio_direction_output(HUB_USB_RST_GPIO, 0);
}


int board_usb_init(int index, enum usb_init_type init)
{
    int ret = 0;

    if (index == 1) {
        if (init == USB_INIT_HOST) {

#ifdef CONFIG_USB_CDNS3_GADGET
        } else {

#endif
        }
    }

    return ret;

}

int board_usb_cleanup(int index, enum usb_init_type init)
{
    int ret = 0;

    if (index == 1) {
        if (init == USB_INIT_HOST) {

        }
    }

    return ret;
}
#endif

/*
 * Work-around: to get both eth1/eth0 work
 * at the same time just change CONFIG_FEC_ENET_DEV
 * from 0 to 1 into seco_imx8qxp_c57.c
 */

#define FEC2_ETH1_RST IMX_GPIO_NR(3,13)

static iomux_cfg_t ss_rst_fec2_gpio[] = {
    SC_P_QSPI0A_DQS | MUX_MODE_ALT(4) | MUX_PAD_CTRL(GPIO_PAD_CTRL),
};

void fec2_eth1_rst(void)
{
    imx8_iomux_setup_multiple_pads(ss_rst_fec2_gpio, ARRAY_SIZE(ss_rst_fec2_gpio));

    gpio_request(FEC2_ETH1_RST, "fec2_eth1_rst_gpio");

    gpio_direction_output(FEC2_ETH1_RST, 0);
    udelay(100);
    gpio_direction_output(FEC2_ETH1_RST, 1);
    udelay(100);
    gpio_direction_output(FEC2_ETH1_RST, 0);

    udelay(200);

}

int board_init(void)
{
    board_gpio_init();
    hub_rst();
    fec2_eth1_rst();

    return 0;
}

void board_quiesce_devices(void)
{
    const char *power_on_devices[] = {
        "dma_lpuart2",
        "PD_UART2_RX",
        "PD_UART2_TX"
    };

    imx8_power_off_pd_devices(power_on_devices, ARRAY_SIZE(power_on_devices));
}

/*
 * Board specific reset that is system reset.
 */
void reset_cpu(ulong addr)
{
    sc_pm_reboot(-1, SC_PM_RESET_TYPE_COLD);
    while(1);

}

/* Overload of weak definition for C57 to handle all the DDR sizes */
void board_mem_get_layout(u64 *phys_sdram_1_start,
                 u64 *phys_sdram_1_size,
                 u64 *phys_sdram_2_start,
                 u64 *phys_sdram_2_size)
{
    int ram_size = RAM_UNKNOWN;

    strap_conf_t *strap_conf = (strap_conf_t *)gd->strap_configuration;
    if ( strap_conf != NULL ) {
        ram_size = strap_conf->ram_size;
    }

    if (RAM_1GB == ram_size){
        *phys_sdram_1_start = PHYS_SDRAM_1;
        *phys_sdram_1_size = PHYS_SDRAM_1_SIZE_1GB;
        *phys_sdram_2_start = 0;
        *phys_sdram_2_size = 0;
    }
    else if (RAM_2GB == ram_size){
        *phys_sdram_1_start = PHYS_SDRAM_1;
        *phys_sdram_1_size = PHYS_SDRAM_1_SIZE;
        *phys_sdram_2_start = 0;
        *phys_sdram_2_size = 0;
    }
    else if (RAM_4GB == ram_size){
        *phys_sdram_1_start = PHYS_SDRAM_1;
        *phys_sdram_1_size = PHYS_SDRAM_1_SIZE;
        *phys_sdram_2_start = PHYS_SDRAM_2;
        *phys_sdram_2_size = PHYS_SDRAM_2_SIZE;
    }
    else
    {
        printf("No known strap detected: run with default settings...\n");
        *phys_sdram_1_start = PHYS_SDRAM_1;
        *phys_sdram_1_size = PHYS_SDRAM_1_SIZE;
        *phys_sdram_2_start = PHYS_SDRAM_2;
        *phys_sdram_2_size = PHYS_SDRAM_2_SIZE;
    }
}

#ifdef CONFIG_OF_BOARD_SETUP
int ft_board_setup(void *blob, struct bd_info *bd)
{
    c57_fdt_rev_setup(blob, bd);
    return 0;
}
#endif

bool check_m4_parts_boot(void)
{
    sc_rm_pt_t m4_parts[2];
    int err;

    err = sc_rm_get_resource_owner(-1, SC_R_M4_0_PID0, &m4_parts[0]);
    if (err != SC_ERR_NONE) {
        printf("%s get resource [%d] owner error: %d\n", __func__, SC_R_M4_0_PID0, err);
        return false;
    }

    if (sc_pm_is_partition_started(-1, m4_parts[0]))
        return true;

    if (is_imx8qm()) {
        err = sc_rm_get_resource_owner(-1, SC_R_M4_1_PID0, &m4_parts[1]);
        if (err != SC_ERR_NONE) {
            printf("%s get resource [%d] owner error: %d\n", __func__, SC_R_M4_1_PID0, err);
            return false;
        }

        if (sc_pm_is_partition_started(-1, m4_parts[1]))
            return true;
    }

    return false;
}

#if CONFIG_IS_ENABLED(CMD_NET)
static void setup_macaddr(void)
{
    u8 mac_addr[ARP_HLEN];

    /* retrieve macaddress from eeprom */
    if(seco_eeprom_get_macaddr(&mac_addr[0]) < 0)
    {
        printf("failed to get eeprom macaddress\n");
        return;
    }
    printf("setting mac address  : %pM\n", mac_addr);

    if(!(mac_addr[0] == 0x0 && mac_addr[1] == 0xc0 && mac_addr[2] == 0x8))
        printf("eeprom macaddress is not a SECO macaddress\n");

    if(eth_env_set_enetaddr("ethaddr", mac_addr))
        printf("setting eeprom macaddress failed\n");
}
#endif

int board_late_init(void)
{
    char *fdt_file;
    bool m4_booted;

    build_info();

#ifdef CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG
    env_set("board_name", "C57");
    env_set("board_rev", "iMX8QXP");
#endif

    env_set("sec_boot", "no");
#ifdef CONFIG_AHAB_BOOT
    env_set("sec_boot", "yes");
#endif

    fdt_file = env_get("fdt_file");
    m4_booted = check_m4_parts_boot();

    if (fdt_file && !strcmp(fdt_file, "undefined")) {
        if (m4_booted)
            env_set("fdt_file", "imx8qxp-c57-rpmsg.dtb");
        else
            env_set("fdt_file", "seco-imx8qxp-c57.dtb");
    }

/* seco_config variables */
#ifdef CONFIG_SECO_ENV_MANAGER
        gd->bsp_sources.kern_dev_list            = &kern_dev_imx8_list[0];
        gd->bsp_sources.kern_dev_num             = kern_dev_imx8_size;
        gd->bsp_sources.fdt_dev_list             = &fdt_dev_imx8_list[0];
        gd->bsp_sources.fdt_dev_num              = fdt_dev_imx8_size;
        gd->bsp_sources.fw_dev_list              = firmware_dev_imx8_list;
        gd->bsp_sources.fw_dev_num               = firmware_dev_imx8_size;
#ifdef CONFIG_OF_LIBFDT_OVERLAY
        gd->bsp_sources.fdt_overlay_dev_list     = fdt_overlay_dev_imx8_list;
        gd->bsp_sources.fdt_overlay_dev_num      = fdt_overlay_dev_imx8_size;
#endif
        gd->bsp_sources.ramfs_dev_list           = &ramfs_dev_imx8_list[0];
        gd->bsp_sources.ramfs_dev_num            = ramfs_dev_imx8_size;
        gd->bsp_sources.filesystem_dev_list      = &filesystem_dev_imx8_list[0];
        gd->bsp_sources.filesystem_dev_num       = filesystem_dev_imx8_size;

#ifdef CONFIG_OF_LIBFDT_OVERLAY
        gd->boot_setup.video_mode_list           = video_mode_list_cfg;
        gd->boot_setup.video_mode_num            = video_mode_size_cfg;

        // gd->boot_setup.overlay_peripheral_list   = overlay_peripheral_list;
        // gd->boot_setup.overlay_peripheral_num    = overlay_peripheral_size;
#endif
#endif

#ifdef CONFIG_ENV_IS_IN_MMC
    board_late_mmc_env_init();
#endif

#ifdef CONFIG_SECO_EEPROM_MANAGER
    seco_eeprom_init(I2C_EEPROM_BUS, I2C_EEPROM_ADDR);
    seco_eeprom_print_all();
#if CONFIG_IS_ENABLED(CMD_NET)
    setup_macaddr();
#endif
#endif

    return 0;
}

#ifdef CONFIG_FSL_FASTBOOT
#ifdef CONFIG_ANDROID_RECOVERY
int is_recovery_key_pressing(void)
{
    return 0; /* TODO */
}
#endif /* CONFIG_ANDROID_RECOVERY */
#endif /* CONFIG_FSL_FASTBOOT */

#if defined(CONFIG_VIDEO_IMXDPUV1)
static void enable_lvds(struct display_info_t const *dev)
{
    struct gpio_desc desc;
    int ret;

    /* MIPI_DSI0_EN on IOEXP 0x1a port 6, MIPI_DSI1_EN on IOEXP 0x1d port 7 */
    ret = dm_gpio_lookup_name("gpio@1a_6", &desc);
    if (ret)
        return;

    ret = dm_gpio_request(&desc, "lvds0_en");
    if (ret)
        return;

    dm_gpio_set_dir_flags(&desc, GPIOD_IS_OUT | GPIOD_IS_OUT_ACTIVE);

    display_controller_setup((PS2KHZ(dev->mode.pixclock) * 1000));
    lvds_soc_setup(dev->bus, (PS2KHZ(dev->mode.pixclock) * 1000));
    lvds_configure(dev->bus);
    lvds2hdmi_setup(13);
}

struct display_info_t const displays[] = {{
    .bus	= 0, /* LVDS0 */
    .addr	= 0, /* LVDS0 */
    .pixfmt	= IMXDPUV1_PIX_FMT_BGRA32,
    .detect	= NULL,
    .enable	= enable_lvds,
    .mode	= {
        .name           = "IT6263", /* 720P60 */
        .refresh        = 60,
        .xres           = 1280,
        .yres           = 720,
        .pixclock       = 13468, /* 74250000 */
        .left_margin    = 110,
        .right_margin   = 220,
        .upper_margin   = 5,
        .lower_margin   = 20,
        .hsync_len      = 40,
        .vsync_len      = 5,
        .sync           = FB_SYNC_EXT,
        .vmode          = FB_VMODE_NONINTERLACED
} } };
size_t display_count = ARRAY_SIZE(displays);

#endif /* CONFIG_VIDEO_IMXDPUV1 */
