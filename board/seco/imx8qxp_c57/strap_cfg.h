/*
 *
 * Copyright SECO S.p.A.
 * Mantained by marco.sandrelli@seco.com
 *
 */

/*
 *	This typedef must be matched with:
 *		overlay_video_mode_list[]
 *		overlay_addons_list[]
 *	in env_conf.c files
 *
*/

typedef enum {      
      LVDS_S = 0,
      LVDS_D,
      eDP,
      NUM_VIDEO_MODES,
} VIDEO_MODES;

typedef enum  {
      NOADDONS = 0,
      OV5640_CSI0,
      OV5640_CSI1,
      NUM_ADDONS,
} ADDONS;

#define BOOT_DEV_ID_EMMC      __stringify(BOOT_ID_EMMC)"\0"
#define BOOT_DEV_ID_SPI       "0"
#define BOOT_DEV_ID_SATA      "0"
#define BOOT_DEV_ID_USB       "0"

#define ROOT_DEV_ID_EMMC      __stringify(ROOT_ID_EMMC)"\0"

int c57_get_board_configuration (void);
int c57_get_board_hw_rev_configuration ( void );
void c57_fdt_rev_setup(void *blob, struct bd_info *bdev);

int strap_get_board_cfg(void);
void strap_show(void);

typedef enum {
      RAM_1GB = 0,
      RAM_2GB = 0x1,
      RAM_4GB = 0x2,
      RAM_UNKNOWN = 0xF
} RAM_STRAPS;

typedef enum {
      HW_REVA = 0x0,
      HW_REVB = 0x1,
      HW_REVC = 0x2,
      HW_REVD = 0x3,
      HW_REVE = 0x4,
      HW_REVF = 0x5,
      HW_UNKNOWN = 0xF
} HW_REV_STRAPS;

typedef struct strap_conf {
	int ram_size;
	int hw_rev;
} strap_conf_t;

#define GET_C57_STRAPS	(c57_get_board_configuration())

#define C57_IS_1GB 	(GET_C57_STRAPS == RAM_1GB)
#define C57_IS_2GB      (GET_C57_STRAPS == RAM_2GB)
#define C57_IS_4GB      (GET_C57_STRAPS == RAM_4GB)

#define GET_C57_HW_REV	(c57_get_board_hw_rev_configuration())

#define C57_IS_REVA 	(GET_C57_HW_REV == HW_REVA)
#define C57_IS_REVB     (GET_C57_HW_REV == HW_REVB)
#define C57_IS_REVC     (GET_C57_HW_REV == HW_REVC)
#define C57_IS_REVD     (GET_C57_HW_REV == HW_REVD)
#define C57_IS_REVE     (GET_C57_HW_REV == HW_REVE)
#define C57_IS_REVF     (GET_C57_HW_REV == HW_REVF)
