/*
 *
 *
 * Reading C57 Board type
 *
 * tommaso.merciai@seco.com
 *
 *
 *
 */

#include <common.h>
#include <errno.h>

#include <asm/io.h>
#include <asm/gpio.h>
#include <asm/arch/clock.h>
#include <asm/arch/sci/sci.h>
#include <asm/arch/iomux.h>
#include <asm/arch/imx8-pins.h>
#include <fsl_esdhc.h>
#include <mmc.h>
#include <asm/arch/sys_proto.h>
#include <stdlib.h>
#include "strap_cfg.h"

#define PULLUP_PAD_CTRL (PAD_CTL_PUE)

/* ____________________________________________________________________________
  |                                                                            |
  |                                  C57 REVISION                              |
  |____________________________________________________________________________|
*/

#define GPIO_PAD_CTRL ((SC_PAD_CONFIG_OD_IN << PADRING_CONFIG_SHIFT) | (SC_PAD_ISO_OFF << PADRING_LPCONFIG_SHIFT))
#define GPIO_PAD_CTRL_PU	(GPIO_PAD_CTRL | (SC_PAD_28FDSOI_PS_PU << PADRING_PULL_SHIFT))

const iomux_cfg_t board_hw_rev_pads[] = {
    SC_P_QSPI0B_DATA0   | MUX_MODE_ALT(4) | MUX_PAD_CTRL(GPIO_PAD_CTRL), /* SECO_CODE_0 - GPIO3_IO18 */
    SC_P_QSPI0B_DATA1   | MUX_MODE_ALT(4) | MUX_PAD_CTRL(GPIO_PAD_CTRL), /* SECO_CODE_1 - GPIO3_IO19 */
    SC_P_QSPI0B_DATA2   | MUX_MODE_ALT(4) | MUX_PAD_CTRL(GPIO_PAD_CTRL), /* SECO_CODE_2 - GPIO3_IO20 */
    SC_P_QSPI0B_DATA3   | MUX_MODE_ALT(4) | MUX_PAD_CTRL(GPIO_PAD_CTRL)  /* SECO_CODE_3 - GPIO3_IO21 */
};

const iomux_cfg_t board_conf_pads_revA_revB[] = {
    SC_P_USDHC1_RESET_B | MUX_MODE_ALT(4) | MUX_PAD_CTRL(GPIO_PAD_CTRL_PU), /* SECO_CODE_4 - GPIO4_IO19 */
    SC_P_CSI_EN         | MUX_MODE_ALT(4) | MUX_PAD_CTRL(GPIO_PAD_CTRL), /* SECO_CODE_5 - GPIO3_IO02 */
    SC_P_CSI_RESET      | MUX_MODE_ALT(4) | MUX_PAD_CTRL(GPIO_PAD_CTRL)  /* SECO_CODE_6 - GPIO3_IO03 */
};

const iomux_cfg_t board_conf_pads_revC[] = {
    SC_P_USDHC1_RESET_B | MUX_MODE_ALT(4) | MUX_PAD_CTRL(GPIO_PAD_CTRL_PU), /* SECO_CODE_4 - GPIO4_IO19 */
    SC_P_CSI_EN         | MUX_MODE_ALT(4) | MUX_PAD_CTRL(GPIO_PAD_CTRL), /* SECO_CODE_5 - GPIO3_IO02 */
    SC_P_MCLK_IN0       | MUX_MODE_ALT(4) | MUX_PAD_CTRL(GPIO_PAD_CTRL)  /* SECO_CODE_6 - GPIO0_IO19 */
};

int c57_get_board_configuration(void)
{

    int value = RAM_UNKNOWN;
    struct gpio_desc seco_code4;
    struct gpio_desc seco_code5;
    struct gpio_desc seco_code6;

    int hw_rev = c57_get_board_hw_rev_configuration();

    /*
     *
     * DDR_CFG code is composed in this way:
     * SECO_CODE_4 -> first bit   X
     * SECO_CODE_5 -> second bit  X
     * SECO_CODE_6 -> third bit   X
     *
     */
    if (hw_rev < 0)
    {
        printf("Failed to get DDR configuration.\n");
        return -1;
    }
    else if (hw_rev <= HW_REVB)
    {
        imx8_iomux_setup_multiple_pads(board_conf_pads_revA_revB, ARRAY_SIZE(board_conf_pads_revA_revB));

        if (dm_gpio_lookup_name("gpio3_3", &seco_code6))
        {
            printf("Lookup GPIO3_3 failed.\n");
            return -1;
        }
    }
    else
    {
        imx8_iomux_setup_multiple_pads(board_conf_pads_revC, ARRAY_SIZE(board_conf_pads_revC));

        if (dm_gpio_lookup_name("gpio0_19", &seco_code6))
        {
            printf("Lookup GPIO0_19 failed.\n");
            return -1;
        }
    }

    if (dm_gpio_lookup_name("gpio4_19", &seco_code4))
    {
        printf("Lookup GPIO4_19 failed.\n");
        return -1;
    }

    if (dm_gpio_lookup_name("gpio3_2", &seco_code5))
    {
        printf("Lookup GPIO3_2 failed.\n");
        return -1;
    }

    if (dm_gpio_request(&seco_code4, "seco_code4"))
    {
        printf("Request seco_code4 failed.\n");
        return -1;
    }

    if (dm_gpio_request(&seco_code5, "seco_code5"))
    {
        printf("Request seco_code5 failed.\n");
        return -1;
    }

    if (dm_gpio_request(&seco_code6, "seco_code6"))
    {
        printf("Request seco_code6 failed.\n");
        return -1;
    }

    dm_gpio_set_dir_flags(&seco_code4, GPIOD_IS_IN);
    dm_gpio_set_dir_flags(&seco_code5, GPIOD_IS_IN);
    dm_gpio_set_dir_flags(&seco_code6, GPIOD_IS_IN);

    value = dm_gpio_get_value(&seco_code4) |
            dm_gpio_get_value(&seco_code5) << 1 |
            dm_gpio_get_value(&seco_code6) << 2;

    if (dm_gpio_free((seco_code4.dev), &seco_code4))
        return -1;

    if (dm_gpio_free((seco_code5.dev), &seco_code5))
        return -1;

    if (dm_gpio_free((seco_code6.dev), &seco_code6))
        return -1;
        
    return value;
}

int c57_get_board_hw_rev_configuration(void)
{
    int value = HW_UNKNOWN;
    struct gpio_desc seco_code0;
    struct gpio_desc seco_code1;
    struct gpio_desc seco_code2;
    struct gpio_desc seco_code3;

    /*
     *
     * HW_REV code is composed in this way:
     * SECO_CODE_0 -> first bit   X
     * SECO_CODE_1 -> second bit  X
     * SECO_CODE_2 -> third bit   X
     * SECO_CODE_3 -> fourth bit  X
     *
     */

    imx8_iomux_setup_multiple_pads(board_hw_rev_pads, ARRAY_SIZE(board_hw_rev_pads));

    if (dm_gpio_lookup_name("gpio3_18", &seco_code0))
    {
        printf("Lookup GPIO3_18 failed.\n");
        return -1;
    }

    if (dm_gpio_lookup_name("gpio3_19", &seco_code1))
    {
        printf("Lookup GPIO3_19 failed.\n");
        return -1;
    }

    if (dm_gpio_lookup_name("gpio3_20", &seco_code2))
    {
        printf("Lookup GPIO3_20 failed.\n");
        return -1;
    }

    if (dm_gpio_lookup_name("gpio3_21", &seco_code3))
    {
        printf("Lookup GPIO3_21 failed.\n");
        return -1;
    }

    if (dm_gpio_request(&seco_code0, "seco_code0"))
    {
        printf("Request seco_code0 failed.\n");
        return -1;
    }

    if (dm_gpio_request(&seco_code1, "seco_code1"))
    {
        printf("Request seco_code1 failed.\n");
        return -1;
    }

    if (dm_gpio_request(&seco_code2, "seco_code2"))
    {
        printf("Request seco_code2 failed.\n");
        return -1;
    }

    if (dm_gpio_request(&seco_code3, "seco_code3"))
    {
        printf("Request seco_code3 failed.\n");
        return -1;
    }

    dm_gpio_set_dir_flags(&seco_code0, GPIOD_IS_IN);
    dm_gpio_set_dir_flags(&seco_code1, GPIOD_IS_IN);
    dm_gpio_set_dir_flags(&seco_code2, GPIOD_IS_IN);
    dm_gpio_set_dir_flags(&seco_code3, GPIOD_IS_IN);

    value = dm_gpio_get_value(&seco_code0) |
            dm_gpio_get_value(&seco_code1) << 1 |
            dm_gpio_get_value(&seco_code2) << 2 |
            dm_gpio_get_value(&seco_code3) << 3;

    if (dm_gpio_free((seco_code0.dev), &seco_code0))
        return -1;

    if (dm_gpio_free((seco_code1.dev), &seco_code1))
        return -1;

    if (dm_gpio_free((seco_code2.dev), &seco_code2))
        return -1;

    if (dm_gpio_free((seco_code3.dev), &seco_code3))
        return -1;

    return value;
}

//Enables the proper GPIO pin to startup the backlight regulator
void c57_fdt_rev_setup(void *blob, struct bd_info *bdev)
{

    uint32_t phandle;
    int phandle_offs;
    uint8_t gpio_pin;
    int offset, ret;

    u8 gpio_prop_bytes[12] = {0x00};

    offset = fdt_path_offset(blob, "/regulators/backlight_vcc_bkl_sw/");
    if (offset < 0)
    {
        printf("ERROR: find node /: %s.\n", fdt_strerror(offset));
        return;
    }

    if (C57_IS_REVC)
    {
        printf("Overlay dts: C57 RevC /regulators/backlight_vcc_bkl_sw - csi_reset\n");

        // GPIO3_PIN3
        phandle_offs = fdt_path_offset(blob, "/bus@5d000000/gpio@5d0b0000/");
        gpio_pin = 3U;
    }
    else
    {
        printf("Overlay dts: C57 RevB /regulators/backlight_vcc_bkl_sw - mclk_in0\n");

        // GPIO0_PIN19
        phandle_offs = fdt_path_offset(blob, "/bus@5d000000/gpio@5d080000/");
        gpio_pin = 19U;
    }

    // Dynamically retrieve phandle
    phandle = fdt_get_phandle(blob, phandle_offs);

    if (0U == phandle)
    {
        printf("ERROR: couldn't find GPIO's phandle.\n");
        return;
    }

    gpio_prop_bytes[3] = phandle;
    gpio_prop_bytes[7] = gpio_pin;

    ret = fdt_setprop(blob, offset, "gpio", gpio_prop_bytes, sizeof(gpio_prop_bytes));

    if (ret < 0)
        printf("ERROR: could not update revision property %s.\n", fdt_strerror(ret));

    return;
}

int strap_get_board_cfg( void ) {
    strap_conf_t c57_strap_conf;    
    c57_strap_conf.ram_size = c57_get_board_configuration( );
    c57_strap_conf.hw_rev = c57_get_board_hw_rev_configuration( );
    gd->strap_configuration = malloc( sizeof( strap_conf_t ) );
    
    if ( gd->strap_configuration != NULL ) {
        memcpy( gd->strap_configuration, &c57_strap_conf, sizeof( strap_conf_t ) );
        return 0;
    } else
        return -1;
}

void strap_show( void ) {
    strap_conf_t *c57_strap_conf = (strap_conf_t *)gd->strap_configuration;
    if ( gd->strap_configuration == NULL )
        return;
    printf( "Strap configuration: \n" );
    
    printf( "	RAM code: %d (", c57_strap_conf->ram_size );
    switch ( c57_strap_conf->ram_size ) {
        case RAM_1GB:
            printf( "1GB" );
            break;
        case RAM_2GB:
            printf( "2GB" );
            break;
        case RAM_4GB:
            printf( "4GB" );
            break;
        default:
            printf( "Unknown" );
            break;
    }
    printf ( ")\n" );
    
    printf( "	Revision code: %d (", c57_strap_conf->hw_rev );
    switch ( c57_strap_conf->hw_rev ) {
        case HW_REVA:
            printf( "HW_REVA" );
            break;
        case HW_REVB:
            printf( "HW_REVB" );
            break;
        case HW_REVC:
            printf( "HW_REVC" );
            break;
        case HW_REVD:
            printf( "HW_REVD" );
            break;
        case HW_REVE:
            printf( "HW_REVE" );
            break;
        case HW_REVF:
            printf( "HW_REVF" );
            break;
        default:
            printf( "Unknown" );
            break;
    }
    printf ( ")\n" );
}