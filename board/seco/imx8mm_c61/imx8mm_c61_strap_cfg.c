/*
 * 
 * Reading C61 Board type
 *
 * sam.crossman@seco.com
 *
 */

#include <common.h>
#include <command.h>
#include <malloc.h>
#include <errno.h>
#include <asm/io.h>
#include <miiphy.h>
#include <netdev.h>
#include <asm/mach-imx/iomux-v3.h>
#include <asm-generic/gpio.h>
#include <fsl_esdhc.h>
#include <mmc.h>
#include <asm/arch/imx8mm_pins.h>
#include <asm/arch/sys_proto.h>
#include <asm/mach-imx/gpio.h>
#include "imx8mm_c61_strap_cfg.h"
#include <env.h>
#include <seco/env_common.h>
#include <configs/seco_mx8_dtbo.h>


#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

DECLARE_GLOBAL_DATA_PTR;

typedef enum  {
	port1_setup = 0,
	port2_setup,
	modem_setup,
	camera_setup,
} seco_config_option_t;

int strap_get_port1_cfg ( void ) {

	struct gpio_desc desc_enet8;
	struct gpio_desc desc_enet9;
	int ret, value_sum;

	ret = dm_gpio_lookup_name("gpio@21_8", &desc_enet8);
	if (ret)
		return -1;

	ret = dm_gpio_lookup_name("gpio@21_9", &desc_enet9);
	if (ret)
		return -1;

	ret = dm_gpio_request(&desc_enet8, "gpio21_cfg0");
	if (ret)
		return -1;

	ret = dm_gpio_request(&desc_enet9, "gpio21_cfg1");
	if (ret)
		return -1;

	dm_gpio_set_dir_flags(&desc_enet8, GPIOD_IS_IN);
	dm_gpio_set_dir_flags(&desc_enet9, GPIOD_IS_IN);

	value_sum = dm_gpio_get_value(&desc_enet8)  | dm_gpio_get_value(&desc_enet9) << 1;

	ret = dm_gpio_free((desc_enet8.dev),&desc_enet8);
	if (ret)
		return -1;

	ret = dm_gpio_free((desc_enet9.dev), &desc_enet9);
	if (ret)
		return -1;

	return value_sum;
}

int strap_get_port2_cfg ( void ) {

	struct gpio_desc desc_enet10;
	struct gpio_desc desc_enet11;
	int ret, value_sum;

	ret = dm_gpio_lookup_name("gpio@21_10", &desc_enet10);
	if (ret)
		return -1;

	ret = dm_gpio_lookup_name("gpio@21_11", &desc_enet11);
	if (ret)
		return -1;

	ret = dm_gpio_request(&desc_enet10, "gpio21_cfg2");
	if (ret)
		return -1;

	ret = dm_gpio_request(&desc_enet11, "gpio21_cfg3");
	if (ret)
		return -1;

	dm_gpio_set_dir_flags(&desc_enet10, GPIOD_IS_IN);
	dm_gpio_set_dir_flags(&desc_enet11, GPIOD_IS_IN);

	value_sum = dm_gpio_get_value(&desc_enet10)  | dm_gpio_get_value(&desc_enet11) << 1;

	ret = dm_gpio_free((desc_enet10.dev), &desc_enet10);
	if (ret)
		return -1;

	ret = dm_gpio_free((desc_enet11.dev), &desc_enet11);
	if (ret)
		return -1;

	return value_sum;
}

int strap_get_video_cfg ( void ) {

	struct gpio_desc desc_enet3;
	struct gpio_desc desc_enet4;
	int ret, value_sum;

	ret = dm_gpio_lookup_name("gpio@21_3", &desc_enet3);
	if (ret)
		return -1;

	ret = dm_gpio_lookup_name("gpio@21_4", &desc_enet4);
	if (ret)
		return -1;

	ret = dm_gpio_request(&desc_enet4, "gpio21_cfg13");
	if (ret)
		return -1;

	ret = dm_gpio_request(&desc_enet3, "gpio21_cfg14");
	if (ret)
		return -1;

	dm_gpio_set_dir_flags(&desc_enet3, GPIOD_IS_IN);
	dm_gpio_set_dir_flags(&desc_enet4, GPIOD_IS_IN);

	value_sum = dm_gpio_get_value(&desc_enet4)  | dm_gpio_get_value(&desc_enet3) << 1;

	ret = dm_gpio_free((desc_enet4.dev), &desc_enet4);
	if (ret)
		return -1;

	ret = dm_gpio_free((desc_enet3.dev), &desc_enet3);
	if (ret)
		return -1;

	return value_sum;
}

int strap_get_modem_cfg ( void ) {

	struct gpio_desc desc_enet6;
	int ret, value_sum;

	ret = dm_gpio_lookup_name("gpio@21_6", &desc_enet6);
	if (ret)
		return -1;

	ret = dm_gpio_request(&desc_enet6, "gpio21_cfg11");
	if (ret)
		return -1;

	dm_gpio_set_dir_flags(&desc_enet6, GPIOD_IS_IN);

	value_sum = dm_gpio_get_value(&desc_enet6);

	ret = dm_gpio_free((desc_enet6.dev), &desc_enet6);
	if (ret)
		return -1;

	return value_sum;
}



void strap_get_board_cfg(void){

	int sel;
	char per_options[256];
	char video_options[256];
	char *video_sel_dtbo = "";
	char *port1_sel_dtbo = "";
	char *port2_sel_dtbo = "";
	char *modem_sel_dtbo = "";

	/* check video strap cfg  and select right dtbo */
	sel = strap_get_video_cfg();
	switch(sel)
	{
		case strap_edp_video_cfg:
			video_sel_dtbo = STR(ENV_DTBO_C61_EDP);
			break;
		case strap_lvds_video_cfg:
			video_sel_dtbo = STR(ENV_DTBO_C61_LVDS);
			break;
		case strap_lvds_led_driver_video_cfg:
			video_sel_dtbo = STR(ENV_DTBO_C61_LVDS);
			break;
	}

	/* check port1 strap cfg  and select right dtbo*/
	sel = strap_get_port1_cfg();

	switch(sel)
	{
		case strap_gpio_cfg:
			port1_sel_dtbo = STR(ENV_DTBO_C61_P1_GPIOS);
			break;
		case strap_rs232_cfg:
			port1_sel_dtbo = STR(ENV_DTBO_C61_P1_RS232);
			break;
		case strap_rs485_cfg:
			port1_sel_dtbo = STR(ENV_DTBO_C61_P1_RS485);
			break;
		case strap_can_cfg:
			port1_sel_dtbo = STR(ENV_DTBO_C61_P1_CAN);
			break;
	}

	/* check port2 strap cfg  and select right dtbo*/
	sel = strap_get_port2_cfg();

	switch(sel)
	{
		case strap_gpio_cfg:
			port2_sel_dtbo = STR(ENV_DTBO_C61_P2_GPIOS);
			break;
		case strap_rs232_cfg:
			port2_sel_dtbo = STR(ENV_DTBO_C61_P2_RS232);
			break;
		case strap_rs485_cfg:
			port2_sel_dtbo = STR(ENV_DTBO_C61_P2_RS485);
			break;
		case strap_can_cfg:
			port2_sel_dtbo = STR(ENV_DTBO_C61_P2_CAN);
			break;
	}

	/* check modem strap cfg  and select right dtbo*/
	sel = strap_get_modem_cfg();

    if(sel == strap_modem_cfg){
		modem_sel_dtbo = STR(ENV_DTBO_C61_MODEM);
    }

	/* append peripheral/video selected dtbo */
	snprintf(per_options, sizeof(per_options), "%s %s %s", port1_sel_dtbo, port2_sel_dtbo, modem_sel_dtbo);
	snprintf(video_options, sizeof(video_options), "%s", video_sel_dtbo);

	env_set("fdt_overlay_per_dynamic_list", per_options);
	env_set("fdt_overlay_video_dynamic_list", video_options);
}
