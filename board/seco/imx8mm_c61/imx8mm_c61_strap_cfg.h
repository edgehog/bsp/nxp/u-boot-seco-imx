/*
 *
 * Reading C61 Board type
 *
 * sam.crossman@seco.com
 *
 */

#define strap_cfg_modem 0x01

int strap_get_modem_cfg (void);
int strap_get_video_cfg (void);
int strap_get_port1_cfg (void);
int strap_get_port2_cfg (void);
void strap_get_board_cfg(void);

typedef enum {
	strap_no_video_cfg = 0,
	strap_edp_video_cfg,
	strap_lvds_video_cfg,
	strap_lvds_led_driver_video_cfg,
} strap_video_cfg_t;

typedef enum  {
	strap_gpio_cfg = 0,
	strap_rs232_cfg,
	strap_rs485_cfg,
	strap_can_cfg,
} strap_per_cfg_t;

typedef enum  {
	strap_modem_cfg = 1,
} strap_modem_cfg_t;
