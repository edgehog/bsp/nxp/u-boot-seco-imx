// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright 2018 NXP
 */
#include <common.h>
#include <init.h>
#include <env.h>
#include <asm/io.h>
#include <asm/global_data.h>
#include <asm/arch/imx8mm_pins.h>
#include <asm/arch/clock.h>
#include <asm/arch/sys_proto.h>
#include <asm/mach-imx/gpio.h>
#include <asm-generic/gpio.h>
#include <linux/delay.h>
#include <miiphy.h>
#include <netdev.h>
#include <power/pmic.h>
#include <power/bd71837.h>
#include <usb.h>

#include "fuse_cfg.h"

#ifdef CONFIG_SECO_ENV_MANAGER
#include <seco/env_common.h>
#include "env_conf.h"
#endif

// Built-in ethernet (FEC) is disbaled by default as it is unused 
// in the configuration Myon 2 + ConXM. Uncomment if needed.
// #define USE_ETHERNET

/* map the usdhc controller id to the devno given to the board device */
int usdhc_devno[2] = { BOARD_BOOT_ID_EMMC, BOARD_BOOT_ID_SD_EXT };

DECLARE_GLOBAL_DATA_PTR;

// clang-format off
#define UART_PAD_CTRL		(PAD_CTL_DSE6 | PAD_CTL_FSEL1)
#define WDOG_PAD_CTRL		(PAD_CTL_DSE6 | PAD_CTL_ODE | PAD_CTL_PUE | PAD_CTL_PE)
#define GPIO_PAD_CTRL		(PAD_CTL_DSE6 | PAD_CTL_FSEL1)

#define GPIO_PAD_PU_CTRL	(PAD_CTL_DSE6 | PAD_CTL_PUE | PAD_CTL_PE)
#define GPIO_PAD_PD_CTRL	(PAD_CTL_DSE6 |               PAD_CTL_PE)

static iomux_v3_cfg_t const uart_pads[] = {
	IMX8MM_PAD_UART1_RXD_UART1_RX | MUX_PAD_CTRL(UART_PAD_CTRL),
	IMX8MM_PAD_UART1_TXD_UART1_TX | MUX_PAD_CTRL(UART_PAD_CTRL),
};

#define PCIE_CLKREQ		IMX_GPIO_NR(5, 20)
#define PCIE_WL_POWERDOWN	IMX_GPIO_NR(3, 5)
#define PCIE_W_DISABLE_GPIO	IMX_GPIO_NR(3, 17)
#define PCIE_RESET		IMX_GPIO_NR(2, 19)
#define PCIE_WAKE		IMX_GPIO_NR(3, 2)

#define PCIE_W_DISABLE_GPIO_EXT	IMX_GPIO_NR(4, 15)
#define PCIE_RESET_EXT		IMX_GPIO_NR(4, 17)
#define PCIE_WAKE_EXT		IMX_GPIO_NR(3, 13)

static iomux_v3_cfg_t const pcie_wifi_pads[] = {
	IMX8MM_PAD_I2C4_SCL_PCIE1_CLKREQ_B | MUX_PAD_CTRL(0x61),             // CLKREQ_B
//	IMX8MM_PAD_I2C4_SCL_GPIO5_IO20     | MUX_PAD_CTRL(GPIO_PAD_CTRL),    // CLKREQ_B
	IMX8MM_PAD_NAND_WE_B_GPIO3_IO17    | MUX_PAD_CTRL(GPIO_PAD_CTRL),    // PCIE_W_DISABLE_GPIO
	IMX8MM_PAD_SD2_RESET_B_GPIO2_IO19  | MUX_PAD_CTRL(GPIO_PAD_CTRL),    // PCIE_RESET
	IMX8MM_PAD_NAND_CLE_GPIO3_IO5      | MUX_PAD_CTRL(GPIO_PAD_CTRL),    // PCIE_WL_POWERDOWN
	IMX8MM_PAD_NAND_CE1_B_GPIO3_IO2    | MUX_PAD_CTRL(GPIO_PAD_CTRL),    // PCIE_WAKE
};

static iomux_v3_cfg_t const pcie_ext_pads[] = {
	IMX8MM_PAD_SAI1_TXD5_GPIO4_IO17    | MUX_PAD_CTRL(GPIO_PAD_PU_CTRL), // PCIE_RESET
	IMX8MM_PAD_I2C4_SCL_PCIE1_CLKREQ_B | MUX_PAD_CTRL(0x61),             // CLKREQ_B
	IMX8MM_PAD_SAI1_TXD3_GPIO4_IO15    | MUX_PAD_CTRL(GPIO_PAD_CTRL),    // PCIE_W_DISABLE_GPIO
	IMX8MM_PAD_NAND_CLE_GPIO3_IO5      | MUX_PAD_CTRL(GPIO_PAD_CTRL),    // PCIE_WL_POWERDOWN
	IMX8MM_PAD_NAND_DATA07_GPIO3_IO13  | MUX_PAD_CTRL(GPIO_PAD_CTRL),    // PCIE_WAKE
};

#define USBH_PWR_GPIO		IMX_GPIO_NR(1,14)
#define OTG_PWR_GPIO		IMX_GPIO_NR(1,12)
#define USBH_SWSEL_HOST_GPIO	IMX_GPIO_NR(3,15)

static iomux_v3_cfg_t const usb_pads[] = {
	IMX8MM_PAD_GPIO1_IO14_GPIO1_IO14 | MUX_PAD_CTRL(GPIO_PAD_CTRL),
	IMX8MM_PAD_GPIO1_IO12_GPIO1_IO12 | MUX_PAD_CTRL(GPIO_PAD_CTRL),
	IMX8MM_PAD_NAND_RE_B_GPIO3_IO15  | MUX_PAD_CTRL(GPIO_PAD_CTRL),
};

static iomux_v3_cfg_t const wdog_pads[] = {
	IMX8MM_PAD_GPIO1_IO02_WDOG1_WDOG_B | MUX_PAD_CTRL(WDOG_PAD_CTRL),
};

#define GPIO_RESET_OUT_V1R1	IMX_GPIO_NR(1, 2)
#define GPIO_RESET_OUT_V1R2	IMX_GPIO_NR(3,14)

static iomux_v3_cfg_t const reset_out_pads_v1r1[] = {
	IMX8MM_PAD_GPIO1_IO02_GPIO1_IO2 | MUX_PAD_CTRL(GPIO_PAD_PU_CTRL),
};
static iomux_v3_cfg_t const reset_out_pads_v1r2[] = {
	IMX8MM_PAD_NAND_DQS_GPIO3_IO14 | MUX_PAD_CTRL(GPIO_PAD_PU_CTRL),
};
// clang-format on

int board_early_init_f(void)
{
	struct wdog_regs *wdog = (struct wdog_regs *)WDOG1_BASE_ADDR;
	imx_iomux_v3_setup_multiple_pads(wdog_pads, ARRAY_SIZE(wdog_pads));
	set_wdog_reset(wdog);

#ifdef CONFIG_DISABLE_CONSOLE
	/* fully disable U-Boot console if CONFIG_DISABLE_CONSOLE is set */
	gd->flags |= (GD_FLG_DISABLE_CONSOLE);
#endif
	imx_iomux_v3_setup_multiple_pads(usb_pads, ARRAY_SIZE(usb_pads));
	imx_iomux_v3_setup_multiple_pads(uart_pads, ARRAY_SIZE(uart_pads));

	init_uart_clk(0);

	return 0;
}

#ifdef CONFIG_BOARD_POSTCLK_INIT
int board_postclk_init(void)
{
	return 0;
}
#endif

int board_phys_sdram_size(phys_size_t *size)
{
	u64 ram_size;

	switch (GET_MYON2_RAM_FUSES) {
	case RAM_512MB:
		ram_size = (512 * 1024 * 1024UL);
		break;
	case RAM_1GB:
		ram_size = (1024 * 1024 * 1024UL);
		break;
	case RAM_2GB:
		ram_size = (2048 * 1024 * 1024UL);
		break;
	case RAM_3GB: // reduced 4GB
		ram_size = (3072 * 1024 * 1024UL);
		break; 
	case RAM_4GB:
		ram_size = (4096 * 1024 * 1024UL);
		break;
	case RAM_8GB:
		ram_size = (8192 * 1024 * 1024UL);
		break;
	default:
		ram_size = PHYS_SDRAM_SIZE;
		break;
	}

	*size = ram_size;
	return 0;
}

ulong board_get_usable_ram_top(ulong total_size)
{
	if (gd->ram_top > 0x100000000)
		gd->ram_top = 0x100000000;

	return gd->ram_top;
}

#ifdef CONFIG_OF_BOARD_SETUP
int ft_board_setup(void *blob, struct bd_info *bd)
{
	return 0;
}
#endif

#ifdef CONFIG_FEC_MXC

static iomux_v3_cfg_t const fec1_nrst_pads[] = {
	IMX8MM_PAD_ENET_RX_CTL_ENET1_RGMII_RX_CTL | MUX_PAD_CTRL(0x91),
	IMX8MM_PAD_ENET_RD2_ENET1_RGMII_RD2 | MUX_PAD_CTRL(0x91),
	IMX8MM_PAD_ENET_RXC_ENET1_RGMII_RXC | MUX_PAD_CTRL(0x91),
	IMX8MM_PAD_ENET_RD3_ENET1_RGMII_RD3 | MUX_PAD_CTRL(0x91),
	IMX8MM_PAD_ENET_RD0_ENET1_RGMII_RD0 | MUX_PAD_CTRL(0x91),
	IMX8MM_PAD_ENET_RD1_ENET1_RGMII_RD1 | MUX_PAD_CTRL(0x91),
};

#define FEC_RST_PAD IMX_GPIO_NR(1, 9)
#define FEC_MODE0 IMX_GPIO_NR(1, 24)
#define FEC_MODE1 IMX_GPIO_NR(1, 28)
#define FEC_MODE2 IMX_GPIO_NR(1, 25)
#define FEC_MODE3 IMX_GPIO_NR(1, 29)

// for V2R1
#define FEC_PHY_ADDR0 IMX_GPIO_NR(1, 29)
#define FEC_PHY_ADDR1 IMX_GPIO_NR(1, 25)
#define FEC_PHY_ADDR2 IMX_GPIO_NR(1, 24)

static iomux_v3_cfg_t const fec1_rst_pads[] = {
	IMX8MM_PAD_GPIO1_IO09_GPIO1_IO9 | MUX_PAD_CTRL(GPIO_PAD_PD_CTRL),
	IMX8MM_PAD_ENET_RX_CTL_GPIO1_IO24 | MUX_PAD_CTRL(GPIO_PAD_PD_CTRL),
	IMX8MM_PAD_ENET_RD2_GPIO1_IO28 | MUX_PAD_CTRL(GPIO_PAD_PD_CTRL),
	IMX8MM_PAD_ENET_RXC_GPIO1_IO25 | MUX_PAD_CTRL(GPIO_PAD_PD_CTRL),
	IMX8MM_PAD_ENET_RD3_GPIO1_IO29 | MUX_PAD_CTRL(GPIO_PAD_PD_CTRL),
	IMX8MM_PAD_ENET_RD0_GPIO1_IO26 | MUX_PAD_CTRL(GPIO_PAD_PD_CTRL),
	IMX8MM_PAD_ENET_RD1_GPIO1_IO27 | MUX_PAD_CTRL(GPIO_PAD_PD_CTRL),
};

static void setup_iomux_fec(void)
{
	imx_iomux_v3_setup_multiple_pads(fec1_rst_pads, ARRAY_SIZE(fec1_rst_pads));
	gpio_request(FEC_RST_PAD, "fec1_rst");
	gpio_direction_output(FEC_RST_PAD, 0);
	udelay(1000);
	if (GET_MYON2_REV_FUSES >= 3) {
		gpio_request(FEC_PHY_ADDR0, "fec_phy_addr0");
		gpio_direction_output(FEC_PHY_ADDR0, 0);
		gpio_request(FEC_PHY_ADDR1, "fec_phy_addr1");
		gpio_direction_output(FEC_PHY_ADDR1, 0);
		gpio_request(FEC_PHY_ADDR2, "fec_phy_addr2");
		gpio_direction_output(FEC_PHY_ADDR2, 1);
		mdelay(15);
		gpio_direction_output(FEC_RST_PAD, 1);
		mdelay(100);
	} else {
#if 1
		gpio_request(FEC_MODE0, "fec_mode0");
		gpio_direction_output(FEC_MODE0, 0);
		gpio_request(FEC_MODE1, "fec_mode1");
		gpio_direction_output(FEC_MODE1, 0);
		gpio_request(FEC_MODE2, "fec_mode2");
		gpio_direction_output(FEC_MODE2, 0);
		gpio_request(FEC_MODE3, "fec_mode3");
		gpio_direction_output(FEC_MODE3, 0);
#endif
		gpio_direction_output(FEC_RST_PAD, 1);
		udelay(50);
		gpio_direction_output(FEC_RST_PAD, 0);
		udelay(1000);
		gpio_direction_output(FEC_RST_PAD, 1);
	}

	imx_iomux_v3_setup_multiple_pads(fec1_nrst_pads, ARRAY_SIZE(fec1_nrst_pads));
	udelay(1000);
}

static int setup_fec(void)
{
	struct iomuxc_gpr_base_regs *const iomuxc_gpr_regs =
		(struct iomuxc_gpr_base_regs *)IOMUXC_GPR_BASE_ADDR;

	/* Use 125M anatop REF_CLK1 for ENET1, not from external */
	// printf("%s: Enable FEC CLK\n", __func__);
	clrsetbits_le32(&iomuxc_gpr_regs->gpr[1], IOMUXC_GPR_GPR1_GPR_ENET1_TX_CLK_SEL_SHIFT, 0);
	set_clk_enet(ENET_125MHZ);
	setup_iomux_fec();
	return 0;
}

int ethernet_1GB(void)
{
	char *s;
	int i;
	s = env_get("ethspeed");
	i = strncmp(s, "1G", 2);
	if (i == 0) {
		printf("1Gb (ethspeed)\n");
		return (1);
	} else {
		printf("100Mb (ethspeed)\n");
		return (0);
	}
}

// clang-format off
#define AR803x_PHY_DEBUG_ADDR_REG	0x1d
#define AR803x_PHY_DEBUG_DATA_REG	0x1e
#define AR803x_PHY_MMD_ADDR_REG		0x0d
#define AR803x_PHY_MMD_OFFSET_REG	0x0e
// clang-format on

void ar8031_write_debug_reg(struct phy_device *phydev, int reg, int value)
{
	phy_write(phydev, CONFIG_FEC_MXC_PHYADDR, AR803x_PHY_DEBUG_ADDR_REG, reg);
	phy_write(phydev, CONFIG_FEC_MXC_PHYADDR, AR803x_PHY_DEBUG_DATA_REG, value);
}

int ar8031_write_mmd_reg(struct phy_device *phydev, int mmd, int reg, int value)
{
	int regval;
	phy_write(phydev, CONFIG_FEC_MXC_PHYADDR, AR803x_PHY_MMD_ADDR_REG, mmd);
	phy_write(phydev, CONFIG_FEC_MXC_PHYADDR, AR803x_PHY_MMD_OFFSET_REG, reg);
	phy_write(phydev, CONFIG_FEC_MXC_PHYADDR, AR803x_PHY_MMD_ADDR_REG, (0x4000 | mmd));
	regval = phy_read(phydev, CONFIG_FEC_MXC_PHYADDR, AR803x_PHY_MMD_OFFSET_REG);
	phy_write(phydev, CONFIG_FEC_MXC_PHYADDR, AR803x_PHY_MMD_OFFSET_REG, value);
	return (regval);
}

int ar8031_read_mmd_reg(struct phy_device *phydev, int mmd, int reg)
{
	int regval;
	phy_write(phydev, CONFIG_FEC_MXC_PHYADDR, AR803x_PHY_MMD_ADDR_REG, mmd);
	phy_write(phydev, CONFIG_FEC_MXC_PHYADDR, AR803x_PHY_MMD_OFFSET_REG, reg);
	phy_write(phydev, CONFIG_FEC_MXC_PHYADDR, AR803x_PHY_MMD_ADDR_REG, (0x4000 | mmd));
	regval = phy_read(phydev, CONFIG_FEC_MXC_PHYADDR, AR803x_PHY_MMD_OFFSET_REG);
	return (regval & 0xffff);
}

// clang-format off
#define MMD7			0x07
#define MMD3			0x03
#define SGMII_Control_Register	0x8011
#define EEE_Advertisement	0x3C
#define EEE_Control		0x805D
#define VDIFF_900mV		0x8000
#define VDIFF_800mV		0x6000
#define VDIFF_700mV		0x4000
#define VDIFF_600mV		0x2000
// clang-format on

int board_phy_config(struct phy_device *phydev)
{
	int j, i = 100;

#ifndef USE_ETHERNET
	printf("none (disabled)\n");
	return 0;
#else
	/* Enable RGMII RXC skew and PHY mode select to RGMII copper */
	if (phydev->drv->config)
		phydev->drv->config(phydev);

	phy_write(phydev, CONFIG_FEC_MXC_PHYADDR, 0x1F, 0xF500);

	i = ar8031_read_mmd_reg(phydev, MMD7, EEE_Advertisement);
	if (ethernet_1GB() == 0) {
		/* Do not use 1GBit */
		phy_write(phydev, CONFIG_FEC_MXC_PHYADDR, 0x09, 0x0);
		j = i & ~4;
		j &= ~2;
		/* Write MMD7 EEE no EEE mode */
		ar8031_write_mmd_reg(phydev, MMD7, EEE_Advertisement, i);
	} else {
		/* Use 1GBit */
		phy_write(phydev, CONFIG_FEC_MXC_PHYADDR, 0x09, 0x300);
	}

	i = ar8031_read_mmd_reg(phydev, MMD3, EEE_Control);
	j = i & ~0x100;
	// printf("%s: Disable EEE Control    0x%04x->0x%04x\n", __func__, i, j);
	i = ar8031_write_mmd_reg(phydev, MMD3, EEE_Control, j);

	i = phy_read(phydev, CONFIG_FEC_MXC_PHYADDR, 0x14);
	j = 0x2C;
	// printf("%s: Smart Speed Register   0x%04x->0x%04x\n", __func__, i, j);
	phy_write(phydev, CONFIG_FEC_MXC_PHYADDR, 0x14, j);

	i = ar8031_read_mmd_reg(phydev, MMD7, SGMII_Control_Register);
	j = (i & 0xfff) | VDIFF_900mV;

	// printf("%s: SGMII_Control_Register 0x%04x->0x%04x\n", __func__, i, j);

	/* Write MMD7 8011 900mV */
	ar8031_write_mmd_reg(phydev, MMD7, SGMII_Control_Register, j);
	/* 100MBit Enable AutoNeg. DuplexMode */
	phy_write(phydev, CONFIG_FEC_MXC_PHYADDR, 0x00, 0x3100);
	/* restart AN */
	phy_write(phydev, CONFIG_FEC_MXC_PHYADDR, 0x00, 0x3300);
	return (0);
#endif
}
#endif

#define GPC_IPS_BASE_ADDR 0x303A0000

static void setup_pcie(void)
{
	char *s;
	int internal_wifi = 0, pcie_ext = 0;

	s = env_get("pcie");
	if (s) {
		internal_wifi = (strncmp(s, "wifionboard", 2) == 0) ? 1 : 0;
		pcie_ext = (strncmp(s, "extern", 2) == 0) ? 1 : 0;
	} else {
		printk("%s: Environment variable pcie not set. Skip Init PCIe gpios\n", __func__);
	}

	// if (seco_fuse_GetPeripheral(SECO_FUSE_PERIPHERAL_WIRELESS) == SECO_FUSE_WIRELESS_NONE) {
	if (true) {

		internal_wifi = 0;
	}

	// clang-format off
	if (internal_wifi) {
		printk("PCIE:  Configure for onboard Wifi Card\n");
		imx_iomux_v3_setup_multiple_pads(pcie_wifi_pads, ARRAY_SIZE(pcie_wifi_pads));
		gpio_request(PCIE_CLKREQ,                  "PCIE_CLKREQ");
		gpio_request(PCIE_WL_POWERDOWN,            "PCIE_WL_POWERDOWN");
		gpio_request(PCIE_WAKE,                    "PCIE_WAKE");
		gpio_request(PCIE_RESET,                   "PCIE_RESET");
		gpio_request(PCIE_W_DISABLE_GPIO,          "PCIE_W_DISABLE_GPIO");

		gpio_direction_output(PCIE_CLKREQ,          0); // Switch on PCIE CLKREQ
		gpio_direction_output(PCIE_WL_POWERDOWN,    0); // Switch on Wifi Module
		gpio_direction_output(PCIE_W_DISABLE_GPIO,  0); // Do not disable PCIe
		gpio_direction_input( PCIE_WAKE);               // Take Wake as input
		gpio_direction_output(PCIE_RESET,           0); // Activate Reset
		udelay(1000);
		gpio_direction_output(PCIE_WL_POWERDOWN,    1); // Switch on Wifi Module
		udelay(1000);
		gpio_direction_output(PCIE_W_DISABLE_GPIO,  1); // Do not disable PCIe
		gpio_direction_output(PCIE_RESET,           1);
		udelay(500);
		gpio_direction_output(PCIE_RESET,           0); // Activate Reset again
		udelay(500);
		gpio_direction_output(PCIE_RESET,           1);
	} else if (pcie_ext) {
		printk("PCIE:  Configure for external PCIe Slot\n");
		imx_iomux_v3_setup_multiple_pads(pcie_ext_pads, ARRAY_SIZE(pcie_ext_pads));
		gpio_request(PCIE_WAKE_EXT,                     "PCIE_WAKE_EXT");
		gpio_request(PCIE_RESET_EXT,                    "PCIE_RESET_EXT");
		gpio_request(PCIE_W_DISABLE_GPIO_EXT,           "PCIE_W_DISABLE_GPIO_EXT");
		gpio_direction_output(PCIE_W_DISABLE_GPIO_EXT,  1); // Do not disable PCIe
		gpio_direction_input(PCIE_WAKE_EXT);                // Take Wake as input
		gpio_direction_output(PCIE_RESET_EXT,           0); // Activate Reset
		udelay(1000);
		gpio_direction_output(PCIE_RESET_EXT,           1);
		udelay(500);
		gpio_direction_output(PCIE_RESET_EXT,           0); // Activate Reset again
		udelay(500);
		gpio_direction_output(PCIE_RESET_EXT,           1);
	}
	// clang-format on
}

/* Enable Uart4/Uart2 pre-set in imx8mm_bl31_setup.c bl31_early_platform_setup2() */

void setup_periph2mcu(void)
{
	char *s;
	volatile unsigned long uart4m4 = 0xff;
	volatile unsigned long uart2m4 = 0xff;

	s = env_get("uart4-access");
	if (s) {
		if (strncmp(s, "both", 2) == 0)
			uart4m4 = 0xff;
		if (strncmp(s, "a53", 2) == 0)
			uart4m4 = 0xf3;
		if (strncmp(s, "m4", 2) == 0)
			uart4m4 = 0xfc;
		*(volatile unsigned long *)0x303D0518 = uart4m4;
	} else {
		*(volatile unsigned long *)0x303D0518 = uart4m4;
	}

	s = env_get("uart2-access");
	if (s) {
		if (strncmp(s, "both", 2) == 0)
			uart2m4 = 0xff;
		if (strncmp(s, "a53", 2) == 0)
			uart2m4 = 0xf3;
		if (strncmp(s, "m4", 2) == 0)
			uart2m4 = 0xfc;
		*(volatile unsigned long *)0x303D05A4 = uart2m4;
	} else {
		*(volatile unsigned long *)0x303D05A4 = uart2m4;
	}
}

void pci_init_board(void)
{
	/* test the 1 lane mode of the PCIe A controller */
	printf("%s:***************************************************\n", __func__);
}
static int resout = 0;

void reset_out(int val)
{
	if (GET_MYON2_REV_FUSES > REV_V1R1) {

		if (resout == 0) {
			imx_iomux_v3_setup_multiple_pads(reset_out_pads_v1r2, 1);
			gpio_request(GPIO_RESET_OUT_V1R2, "RESET_OUT");
			resout = 1;
		}
		gpio_direction_output(GPIO_RESET_OUT_V1R2, val);
	} else {
		if (resout == 0) {
			imx_iomux_v3_setup_multiple_pads(reset_out_pads_v1r1, 1);
			gpio_request(GPIO_RESET_OUT_V1R1, "RESET_OUT");
			resout = 1;
		}
		gpio_direction_output(GPIO_RESET_OUT_V1R1, val);
	}
}

int board_init(void)
{
	reset_out(1);

#ifdef CONFIG_FEC_MXC
#ifdef USE_ETHERNET
	setup_fec();
#endif
#endif
#if defined(ENABLE_I2C_TRIZEPS8MINI) && ENABLE_I2C_TRIZEPS8MINI
	setup_pcie(); /* environment not read here */
#ifdef CONFIG_SYS_I2C_MXC
	setup_i2c(0, CONFIG_SYS_I2C_SPEED, 0x7f, &i2c_pad_info1);
#endif
#endif
	/* Re-Configure nRESET_OUT on MCU as input */
	uint8_t mcu_reset_out_data[2] = { 0x57, 0x01 };
	struct udevice *bus, *mcudev;
	int ret = uclass_get_device_by_seq(UCLASS_I2C, 2, &bus);
	if (!ret)
		ret = dm_i2c_probe(bus, 0x10, 0, &mcudev);
	if (!ret)
		dm_i2c_write(mcudev, 0x04, mcu_reset_out_data, 2);

	/* Enable Uart4/Uart2 pre-set in imx8mm_bl31_setup.c bl31_early_platform_setup2() */
	setup_periph2mcu();

	return 0;
}

int board_late_init(void)
{

	fuse_show();

#ifdef CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG
	env_set("board_name", "MYON2_IMX8MM");
	
	switch (GET_MYON2_REV_FUSES) {
		case REV_V1R1:
			env_set("board_rev", "v1r1");
			break;
		case REV_V1R2:
			env_set("board_rev", "v1r2");
			break;
		case REV_V1R3:
			env_set("board_rev", "v1r3");
			break;
		case REV_V1R4:
			env_set("board_rev", "v1r4");
			break;
		case REV_V2R1:
			env_set("board_rev", "v2r1");
			break;
		default:
			env_set("board_rev", "unknown");
			break;
	}
#endif

#ifdef CONFIG_ENV_IS_IN_MMC
	board_late_mmc_env_init();
#endif
	// clang-format off
#ifdef CONFIG_SECO_ENV_MANAGER
	gd->bsp_sources.kern_dev_list            = &kern_dev_imx8_list[0];
	gd->bsp_sources.kern_dev_num             = kern_dev_imx8_size;
	gd->bsp_sources.fdt_dev_list             = &fdt_dev_imx8_list[0];
	gd->bsp_sources.fdt_dev_num              = fdt_dev_imx8_size;
	gd->bsp_sources.ramfs_dev_list           = &ramfs_dev_imx8_list[0];
	gd->bsp_sources.ramfs_dev_num            = ramfs_dev_imx8_size;
	gd->bsp_sources.filesystem_dev_list      = &filesystem_dev_imx8_list[0];
	gd->bsp_sources.filesystem_dev_num       = filesystem_dev_imx8_size;
#ifdef CONFIG_OF_LIBFDT_OVERLAY
	gd->bsp_sources.fdt_overlay_dev_list     = fdt_overlay_dev_imx8_list;
	gd->bsp_sources.fdt_overlay_dev_num      = fdt_overlay_dev_imx8_size;
	gd->boot_setup.video_mode_list           = video_mode_list_cfg;
	gd->boot_setup.video_mode_num            = ARRAY_SIZE(video_mode_list_cfg);
	gd->boot_setup.overlay_peripheral_list   = overlay_peripheral_list;
	gd->boot_setup.overlay_peripheral_num    = overlay_peripheral_size;
#endif
#endif
	// clang-format on

	setup_pcie();
	return 0;
}

int board_ehci_usb_phy_mode(struct udevice *dev)
{
	if (dev_seq(dev) == 0) {
		printf("%s: USB_INIT_DEVICE\n", __func__);
		return USB_INIT_DEVICE;
	} else {
		printf("%s: USB_INIT_HOST\n", __func__);
		return USB_INIT_HOST;
	}
}

int board_usb_init(int index, enum usb_init_type init)
{
#ifndef CONFIG_SPL_BUILD
	int off;
	const char *mode_string;

	off = fdt_path_offset(gd->fdt_blob, "/soc@0/bus@32c00000/usb@32e40000");
	mode_string = fdt_getprop(gd->fdt_blob, off, "dr_mode", NULL);
	if (mode_string && strncmp(mode_string, "host", 4) == 0) {
		gpio_request(USBH_SWSEL_HOST_GPIO, "USBH_SWSEL_HOST");
		gpio_direction_output(USBH_SWSEL_HOST_GPIO, 1);
		gpio_free(USBH_SWSEL_HOST_GPIO);
	}
#endif

	gpio_request(OTG_PWR_GPIO, "OTG_PWR");
	gpio_direction_output(OTG_PWR_GPIO, 0);
	gpio_free(OTG_PWR_GPIO);

	gpio_request(USBH_PWR_GPIO, "USBH_PWR");
	gpio_direction_output(USBH_PWR_GPIO, 0);
	gpio_free(USBH_PWR_GPIO);

	imx8m_usb_power(index, true);

	return (0);
}

int board_usb_cleanup(int index, enum usb_init_type init)
{
	imx8m_usb_power(index, false);
	gpio_request(OTG_PWR_GPIO, "otgpwr");
	gpio_direction_output(OTG_PWR_GPIO, 1);
	gpio_free(OTG_PWR_GPIO);

	gpio_request(USBH_PWR_GPIO, "usbhpwr");
	gpio_direction_output(USBH_PWR_GPIO, 1);
	gpio_free(USBH_PWR_GPIO);
	return (0);
}
