#ifndef _MYON2_REVISION_H_
#define _MYON2_REVISION_H_

int fuse_get_boot_cfg (void);
int fuse_get_module_cfg (void);
int fuse_get_ram_cfg (void);
int fuse_get_revision_cfg (void);
int fuse_get_temp_cfg (void);
int fuse_get_io_volt_cfg (void);

void fuse_show (void);

typedef enum {
    BOOT_SD   = 0x1,
    BOOT_EMMC = 0x2,
} BOOT_FUSES;

typedef enum {
	MODULE_TRIZEPS = 0x1,
    MODULE_MYON    = 0x2,
} MODULE_FUSES;

typedef enum {
	REV_V1R1 = 0x0,
	REV_V1R2 = 0x1,
	REV_V1R3 = 0x2,
    REV_V1R4 = 0x3,
    REV_V2R1 = 0x4,
} BOARD_REV_FUSES;

typedef enum {
	RAM_UNDEF       = 0x0,
    RAM_1GB         = 0x1,	
    RAM_2GB         = 0x2,
    RAM_3GB         = 0x3,
    RAM_4GB         = 0x4,
    RAM_6GB         = 0x6,
    RAM_8GB         = 0x8,
    RAM_2GB_DUALDIE = 0xA,
    RAM_512MB       = 0xE,
} RAM_FUSES;

typedef enum {
    TEMP_UNDEFINED  = 0x0,
    TEMP_CONSUMER   = 0x1,
    TEMP_EXTENDED   = 0x2,
    TEMP_INDUSTRIAL = 0x3,
} TEMP_FUSES;

typedef enum {
    IO_VOLT_UNDEFINED = 0x0,
    IO_VOLT_1V8       = 0x1,
    IO_VOLT_3V3       = 0x2,
    IO_VOLT_CUSTOM    = 0x3,
} IO_VOLT_FUSES;

typedef struct {
    u8 module;
	u8 ram_size;
	u8 hw_rev;
    u8 temp;
    u8 io_volt;
} fuse_conf_t;

#define GET_BOOT_FUSES              (fuse_get_boot_cfg())
#define BOOT_IS_SD                  (GET_BOOT_FUSES == BOOT_SD)
#define BOOT_IS_EMMC                (GET_BOOT_FUSES == BOOT_EMMC)

#define GET_MODULE_FUSES            (fuse_get_module_cfg())
#define MODULE_IS_MYON              (GET_MODULE_FUSES == MODULE_MYON)

#define GET_MYON2_RAM_FUSES         (fuse_get_ram_cfg())
#define MYON2_IS_512MB              (GET_MYON2_RAM_FUSES == RAM_512MB)
#define MYON2_IS_1GB                (GET_MYON2_RAM_FUSES == RAM_1GB)
#define MYON2_IS_3GB                (GET_MYON2_RAM_FUSES == RAM_3GB)
#define MYON2_IS_4GB                (GET_MYON2_RAM_FUSES == RAM_4GB)
#define MYON2_IS_6GB                (GET_MYON2_RAM_FUSES == RAM_6GB)
#define MYON2_IS_8GB                (GET_MYON2_RAM_FUSES == RAM_8GB)
#define MYON2_IS_2GB                (GET_MYON2_RAM_FUSES == RAM_2GB)

#define GET_MYON2_REV_FUSES         (fuse_get_revision_cfg())
#define MYON2_IS_REV_V1R1           (GET_MYON2_REV_FUSES == REV_V1R1)
#define MYON2_IS_REV_V1R2           (GET_MYON2_REV_FUSES == REV_V1R2)
#define MYON2_IS_REV_V1R3           (GET_MYON2_REV_FUSES == REV_V1R3)
#define MYON2_IS_REV_V1R4           (GET_MYON2_REV_FUSES == REV_V1R4)
#define MYON2_IS_REV_V2R1           (GET_MYON2_REV_FUSES == REV_V2R1)

#define GET_MYON2_TEMP_FUSES        (fuse_get_temp_cfg())
#define MYON2_IS_TEMP_UNDEFINED     (GET_MYON2_TEMP_FUSES == TEMP_UNDEFINED)
#define MYON2_IS_TEMP_CONSUMER      (GET_MYON2_TEMP_FUSES == TEMP_CONSUMER)
#define MYON2_IS_TEMP_EXTENDED      (GET_MYON2_TEMP_FUSES == TEMP_EXTENDED)
#define MYON2_IS_TEMP_INDUSTRIAL    (GET_MYON2_TEMP_FUSES == TEMP_INDUSTRIAL)

#define GET_MYON2_IO_VOLT_FUSES     (fuse_get_io_volt_cfg())
#define MYON2_IS_IO_VOLT_UNDEFINED  (GET_MYON2_IO_VOLT_FUSES == IO_VOLT_UNDEFINED)
#define MYON2_IS_IO_VOLT_1V8        (GET_MYON2_IO_VOLT_FUSES == IO_VOLT_1V8)
#define MYON2_IS_IO_VOLT_3V3        (GET_MYON2_IO_VOLT_FUSES == IO_VOLT_3V3)

#endif
