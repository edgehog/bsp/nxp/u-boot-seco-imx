/*
 *
 * Reading MYON2 Board type
 *
 * Copyright 2025 SECO
 * 
 * oleksii.kutuzov@seco.com
 *
 */

#include <asm/arch/sys_proto.h>
#include <asm/arch/imx-regs.h> 
#include "fuse_cfg.h"

/* ____________________________________________________________________________
  |                                                                            |
  |                              MYON2 REVISION                                |
  |____________________________________________________________________________|
*/

static int fuse_get_boot_fuse(void)
{
    /*
     | 0x1....... = uSD
     | 0x2....... = eMMC
     */

    struct ocotp_regs *ocotp = (struct ocotp_regs *)OCOTP_BASE_ADDR;
    struct fuse_bank *bank1 = &ocotp->bank[1];
    struct fuse_bank1_regs *fuse1 = (struct fuse_bank1_regs *)bank1->fuse_regs;

#ifdef DEBUG
    printf("FUSE1:  0x%08X\n", fuse1->cfg0);
#endif

    return fuse1->cfg0;
}

static u32 fuse_get_seco_cfg( void )
{
    /*
	|....|                  Module  (0xF.......=next , 0x1.......=Trizeps,0x2.......=Myon)
	|    |....|             RAM     (0x.F......)
	|    |    |....|        PCB-Rev (0x..F.....)
	|    |    |    |..  |   Temp    (0x...0....=undef, 0x...4....= Con  , 0x...8....= Ext   , 0x...C....= Industrial)
	|    |    |    |  ..|   Extra   (0x...0....=undef, 0x...1....= 1V8  , 0x...2....= 3V3   , 0x...3....= Custom)

	i.e.
	Myon II V1R1 with 1GB RAM (K4F8E304HB,K4F8E3S4HB):          fuse prog 14 0 0x21000000
	Myon II V1R1 with 2GB RAM (Dual-Die K4F6E304HB):            fuse prog 14 0 0x2A000000   // Engineering sample, typ. no fuse set
	Myon II V1R1 with 2GB RAM (Mono-Die K4F6E3S4HM):            fuse prog 14 0 0x22000000
	Myon II V1R1 with 4GB RAM (Dual-Die K4FBE3D4HM):            fuse prog 14 0 0x24000000
    */

    struct ocotp_regs *ocotp = (struct ocotp_regs *)OCOTP_BASE_ADDR;
	struct fuse_bank *bank14 = &ocotp->bank[14]; // GP10

#ifdef DEBUG
	u32 fuse14;

	fuse14 = bank14->fuse_regs[0];

	printf("FUSE14:  0x%08X\n", fuse14);
	printf("Module:  %d\n", (fuse14 & 0xF0000000) >> 28);
	printf("RAM:     %d\n", (fuse14 & 0x0F000000) >> 24);
	printf("PCB-Rev: %d\n", (fuse14 & 0x00F00000) >> 20);
	printf("Temp:    %d\n", (fuse14 & 0x000C0000) >> 18);
	printf("Extra:   %d\n", (fuse14 & 0x00030000) >> 16);
#endif

	return bank14->fuse_regs[0];
}

int fuse_get_boot_cfg (void) {
	return (fuse_get_boot_fuse() & 0x7000) >> 12;
};

int fuse_get_module_cfg (void) {
	return (fuse_get_seco_cfg() & 0xF0000000) >> 28;
};

int fuse_get_ram_cfg (void) {
	return (fuse_get_seco_cfg() & 0x0F000000) >> 24;
};

int fuse_get_revision_cfg (void) {
	return (fuse_get_seco_cfg() & 0x00F00000) >> 20;
};

int fuse_get_temp_cfg (void) {
	return (fuse_get_seco_cfg() & 0x000C0000) >> 18;
};

int fuse_get_io_volt_cfg (void) {
	return (fuse_get_seco_cfg() & 0x00030000) >> 16;
};

void fuse_show( void ) {
	fuse_conf_t myon2_fuses_conf;
	myon2_fuses_conf.module = GET_MODULE_FUSES;
	myon2_fuses_conf.ram_size = GET_MYON2_RAM_FUSES;
	myon2_fuses_conf.hw_rev = GET_MYON2_REV_FUSES;
	myon2_fuses_conf.temp = GET_MYON2_TEMP_FUSES;
	myon2_fuses_conf.io_volt = GET_MYON2_IO_VOLT_FUSES;
	u8 myon2_boot_fuse = GET_BOOT_FUSES;

	printf( "Fuses:\n" );

	printf( " - Boot code: %d (", myon2_boot_fuse );
	switch ( myon2_boot_fuse ) {
		case BOOT_SD:
			printf( "uSD" );
			break;
		case BOOT_EMMC:
			printf( "eMMC" );
			break;
		default:
			printf( "Unknown" );
			break;
	}
	printf ( ")\n" );

	printf( " - Module code: %d (", myon2_fuses_conf.module );
	switch ( myon2_fuses_conf.module ) {
		case MODULE_TRIZEPS:
			printf( "Trizeps" );
			break;
		case MODULE_MYON:
			printf( "Myon" );
			break;
		default:
			printf( "Unknown" );
			break;
	}
	printf ( ")\n" );

	printf( " - RAM code: %d (", myon2_fuses_conf.ram_size );
	switch ( myon2_fuses_conf.ram_size ) {
		case RAM_512MB:
			printf( "512MB" );
			break;
		case RAM_1GB:
			printf( "1GB" );
			break;
		case RAM_2GB:
			printf( "2GB" );
			break;
		case RAM_3GB:
			printf( "3GB" );
			break;
		case RAM_4GB:
			printf( "4GB" );
			break;
		case RAM_6GB:
			printf( "6GB" );
			break;
		case RAM_8GB:
			printf( "8GB" );
			break;
		default:
			printf( "Unknown" );
			break;
	}
	printf ( ")\n" );

	printf( " - Revision code: %d (", myon2_fuses_conf.hw_rev );
	switch ( myon2_fuses_conf.hw_rev ) {
		case REV_V1R1:
			printf( "V1R1" );
			break;
		case REV_V1R2:
			printf( "V1R2" );
			break;
		case REV_V1R3:
			printf( "V1R3" );
			break;
		case REV_V1R4:
			printf( "V1R4" );
			break;
		case REV_V2R1:
			printf( "V2R1" );
			break;
		default:
			printf( "Unknown" );
			break;
	}
	printf ( ")\n" );

	printf( " - Temp code: %d (", myon2_fuses_conf.temp );
	switch ( myon2_fuses_conf.temp ) {
		case TEMP_UNDEFINED:
			printf( "Undefined" );
			break;
		case TEMP_CONSUMER:
			printf( "Consumer" );
			break;
		case TEMP_EXTENDED:
			printf( "Extended" );
			break;
		case TEMP_INDUSTRIAL:
			printf( "Industrial" );
			break;
		default:
			printf( "Unknown" );
			break;
	}
	printf ( ")\n" );

	printf( " - IO Voltage code: %d (", myon2_fuses_conf.io_volt );
	switch ( myon2_fuses_conf.io_volt ) {
		case IO_VOLT_UNDEFINED:
			printf( "Undefined" );
			break;
		case IO_VOLT_1V8:
			printf( "1V8" );
			break;
		case IO_VOLT_3V3:
			printf( "3V3" );
			break;
		case IO_VOLT_CUSTOM:
			printf( "Custom" );
			break;
		default:
			printf( "Unknown" );
			break;
	}
	printf ( ")\n" );
}
