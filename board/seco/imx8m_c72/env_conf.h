#include <common.h>
#include <command.h>
#include <env.h>


#include <seco/env_common.h>
#include <configs/seco_mx8_dtbo.h>


#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

 
/* *********************************** IMX8 *********************************** */

data_boot_dev_t kern_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_EMMC,     "eMMC onboard",   STR(MACRO_ENV_KERNEL_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_SD,   "external SD",    STR(MACRO_ENV_KERNEL_SRC_USDHCI),   SCFG_BOOT_DEV_ID_EXT_SD,  LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     "TFTP",           STR(MACRO_ENV_KERNEL_SRC_TFTP),     "",                       LOAD_ADDR_KERNEL_REMOTE_DEV, SCFG_KERNEL_FILENAME },
	{ SECO_DEV_TYPE_USB,      "USB",            STR(MACRO_ENV_KERNEL_SRC_USB),      SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_KERNEL_LOCAL_DEV,  SCFG_KERNEL_FILENAME },
};

size_t kern_dev_imx8_size = sizeof( kern_dev_imx8_list ) / sizeof( kern_dev_imx8_list[0] );


data_boot_dev_t fdt_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_EMMC,     "eMMC onboard",   STR(MACRO_ENV_FDT_SRC_USDHCI),    SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX8_FILE },
	{ SECO_DEV_TYPE_SD,   "external SD",    STR(MACRO_ENV_FDT_SRC_USDHCI),    SCFG_BOOT_DEV_ID_EXT_SD,  LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX8_FILE },
	{ SECO_DEV_TYPE_TFTP,     "TFTP",           STR(MACRO_ENV_FDT_SRC_TFTP),      "",                       LOAD_ADDR_FDT_REMOTE_DEV, SCFG_DEFAULT_FDT_IMX8_FILE },
	{ SECO_DEV_TYPE_USB,      "USB",            STR(MACRO_ENV_FDT_SRC_USB),       SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_FDT_LOCAL_DEV,  SCFG_DEFAULT_FDT_IMX8_FILE },
};

size_t fdt_dev_imx8_size = sizeof( fdt_dev_imx8_list ) / sizeof( fdt_dev_imx8_list[0] );


#ifdef CONFIG_OF_LIBFDT_OVERLAY
data_boot_dev_t fdt_overlay_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_EMMC,     "eMMC onboard",   STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),    SCFG_BOOT_DEV_ID_EMMC,    "" },
	{ SECO_DEV_TYPE_SD,   "external SD",    STR(MACRO_ENV_FDT_OVERLAY_SRC_USDHCI),    SCFG_BOOT_DEV_ID_EXT_SD,  "" },
	{ SECO_DEV_TYPE_TFTP,     "TFTP",           STR(MACRO_ENV_FDT_OVERLAY_SRC_TFTP),      "",                       "" },
	{ SECO_DEV_TYPE_USB,      "USB",            STR(MACRO_ENV_FDT_OVERLAY_SRC_USB),       SCFG_BOOT_DEV_ID_USB,     "" },
};

size_t fdt_overlay_dev_imx8_size = sizeof( fdt_overlay_dev_imx8_list ) / sizeof( fdt_overlay_dev_imx8_list[0] );
#endif  /* CONFIG_OF_LIBFDT_OVERLAY */


data_boot_dev_t ramfs_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_NONE,     "Not use",        "0x0",                            "0",                      LOAD_ADDR_RAMFS_LOCAL_DEV,  ""          },
	{ SECO_DEV_TYPE_EMMC,     "eMMC onboard",   STR(MACRO_ENV_RAMFS_SRC_USDHCI),  SCFG_BOOT_DEV_ID_EMMC,    LOAD_ADDR_RAMFS_LOCAL_DEV,  SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_SD,   "external SD",    STR(MACRO_ENV_RAMFS_SRC_USDHCI),  SCFG_BOOT_DEV_ID_EXT_SD,  LOAD_ADDR_RAMFS_LOCAL_DEV,  SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_TFTP,     "TFTP",           STR(MACRO_ENV_RAMFS_SRC_TFTP),    "",                       LOAD_ADDR_RAMFS_REMOTE_DEV, SCFG_RAMFS_FILENAME },
	{ SECO_DEV_TYPE_USB,      "USB",            STR(MACRO_ENV_RAMFS_SRC_USB),     SCFG_BOOT_DEV_ID_USB,     LOAD_ADDR_RAMFS_LOCAL_DEV,  SCFG_RAMFS_FILENAME },
};

size_t ramfs_dev_imx8_size = sizeof( ramfs_dev_imx8_list ) / sizeof( ramfs_dev_imx8_list[0] );


data_boot_dev_t filesystem_dev_imx8_list [] = {
	{ SECO_DEV_TYPE_EMMC,     "eMMC onboard",    STR(MACRO_ENV_FS_SRC_USDHCI),  SCFG_ROOT_DEV_ID_EMMC,    0,  "" },
	{ SECO_DEV_TYPE_SD,   "external SD",     STR(MACRO_ENV_FS_SRC_USDHCI),  SCFG_ROOT_DEV_ID_EXT_SD,  0,  "" },
	{ SECO_DEV_TYPE_NFS,      "NFS",             __stringify(MACRO_ENV_FS_SRC_NFS),     "",                       0,  "" },
	{ SECO_DEV_TYPE_USB,      "USB",             STR(MACRO_ENV_FS_SRC_USB),     "",                       0,  "" },
};

size_t filesystem_dev_imx8_size = sizeof( filesystem_dev_imx8_list ) / sizeof( filesystem_dev_imx8_list[0] );

video_mode_t video_mode_list_lvds [] = {
    {   
/* NO DISPLAY */
        .label    = "no display",
        .video = {
            { VIDEO_NOT_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = NULL,
        .use_bootargs = 0,
    }, {
/* DSI-to-LVDS */
        .label    = "LVDS 21.5",
        .video = {
            { VIDEO_USED,     VIDEO_LVDS, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = STR(ENV_DTBO_C72_LVDS_215),
        .use_bootargs = 0,
    }, {
/* DSI-to-LVDS */
        .label    = "LVDS 15.6",
        .video = { 
            { VIDEO_USED,     VIDEO_LVDS, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = STR(ENV_DTBO_C72_LVDS_156),
        .use_bootargs = 0,
    }, {
/* DSI-to-LVDS */
        .label    = "LVDS 7",
        .video = {
            { VIDEO_USED,     VIDEO_LVDS, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = STR(ENV_DTBO_C72_LVDS_7),
        .use_bootargs = 0,
    },
};

video_mode_t video_mode_list_edp [] = {
    {
/* NO DISPLAY */
        .label    = "no display",
        .video = {
            { VIDEO_NOT_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = NULL,
        .use_bootargs = 0,
    }, {
/* DSI-to-eDP */
        .label    = "eDP",
        .video = { 
			{ VIDEO_USED, 	  VIDEO_EDP, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = STR(ENV_DTBO_C72_EDP),
        .use_bootargs = 0,
    },
};

video_mode_t video_mode_list_novideo [] = {
    {
/* NO DISPLAY */
        .label    = "no display",
        .video = {
            { VIDEO_NOT_USED, VIDEO_HDMI, NO_VIDEO_ARGS },
        },
        .panel_name = "none",
        .dtbo_conf_file = NULL,
        .use_bootargs = 0,
    },
};

#ifdef CONFIG_OF_LIBFDT_OVERLAY
/* *********************************** FDT OVERLAY *********************************** */

overlay_list_t overlay_peripheral_list [] = {
};

size_t overlay_peripheral_size = sizeof( overlay_peripheral_list ) / sizeof( overlay_peripheral_list[0] );

#endif  /* CONFIG_OF_LIBFDT_OVERLAY */

