// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright 2024 SECO
 */
#include <common.h>
#include <miiphy.h>
#include <netdev.h>
#include <asm/mach-imx/iomux-v3.h>
#include <asm-generic/gpio.h>
#include <asm/arch/imx8mm_pins.h>
#include <asm/arch/clock.h>
#include <asm/arch/sys_proto.h>
#include <asm/mach-imx/gpio.h>
#include <asm/mach-imx/mxc_i2c.h>
#include <i2c.h>
#include <asm/io.h>
#include "../common/tcpc.h"
#include <usb.h>
#include <watchdog.h>
#include "strap_cfg.h"

#ifdef CONFIG_SECO_ENV_MANAGER
        #include <seco/env_common.h>
        #include "env_conf.h"
#endif

/* map the usdhc controller id to the devno given to the board device */
int usdhc_devno[4] = { -1, 1, 0, -1};

DECLARE_GLOBAL_DATA_PTR;

#define UART_PAD_CTRL	(PAD_CTL_DSE6 | PAD_CTL_FSEL1)
#define WDOG_PAD_CTRL	(PAD_CTL_DSE6 | PAD_CTL_ODE | PAD_CTL_PUE | PAD_CTL_PE)

static iomux_v3_cfg_t const uart_pads[] = {
	IMX8MM_PAD_UART2_RXD_UART2_RX | MUX_PAD_CTRL(UART_PAD_CTRL),
	IMX8MM_PAD_UART2_TXD_UART2_TX | MUX_PAD_CTRL(UART_PAD_CTRL),
};

static iomux_v3_cfg_t const wdog_pads[] = {
	IMX8MM_PAD_GPIO1_IO02_WDOG1_WDOG_B	| MUX_PAD_CTRL(WDOG_PAD_CTRL),
	IMX8MM_PAD_SAI5_RXD0_GPIO3_IO21		| MUX_PAD_CTRL(WDOG_PAD_CTRL),
	IMX8MM_PAD_SAI5_RXD1_GPIO3_IO22		| MUX_PAD_CTRL(WDOG_PAD_CTRL),
};


static iomux_v3_cfg_t const usb_hub_pads[] = {
        IMX8MM_PAD_SAI1_RXD4_GPIO4_IO6 | MUX_PAD_CTRL(PAD_CTL_PE | PAD_CTL_PUE),
        IMX8MM_PAD_SAI1_RXD0_GPIO4_IO2 | MUX_PAD_CTRL(PAD_CTL_PE | PAD_CTL_PUE),
        IMX8MM_PAD_SAI5_RXD3_GPIO3_IO24 | MUX_PAD_CTRL(PAD_CTL_PE | PAD_CTL_PUE),
};


void usb_hub_init ( void ) {
	
	imx_iomux_v3_setup_multiple_pads(usb_hub_pads, ARRAY_SIZE(usb_hub_pads));

	gpio_request(IMX_GPIO_NR(3, 24), "USB_HUB_RST");	

	gpio_direction_output(IMX_GPIO_NR(3, 24), 1);

}


static iomux_v3_cfg_t const wifi_pwr_down_pads[] = {
        IMX8MM_PAD_SD1_DATA5_GPIO2_IO7 | MUX_PAD_CTRL(PAD_CTL_PE | PAD_CTL_PUE),
};


void wifi_pwr_down_init ( void ) {
	
	imx_iomux_v3_setup_multiple_pads(wifi_pwr_down_pads, ARRAY_SIZE(wifi_pwr_down_pads));

	gpio_request(IMX_GPIO_NR(2, 7), "WIFI PWR DOWN");	

	gpio_direction_output(IMX_GPIO_NR(2, 7), 0);

}

void video_bridge_disable ( void ) {

}

#ifdef CONFIG_FSL_FSPI
#define QSPI_PAD_CTRL	(PAD_CTL_DSE2 | PAD_CTL_HYS)
static iomux_v3_cfg_t const qspi_pads[] = {
	IMX8MM_PAD_NAND_ALE_QSPI_A_SCLK | MUX_PAD_CTRL(QSPI_PAD_CTRL | PAD_CTL_PE | PAD_CTL_PUE),
	IMX8MM_PAD_NAND_CE0_B_QSPI_A_SS0_B | MUX_PAD_CTRL(QSPI_PAD_CTRL),

	IMX8MM_PAD_NAND_DATA00_QSPI_A_DATA0 | MUX_PAD_CTRL(QSPI_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA01_QSPI_A_DATA1 | MUX_PAD_CTRL(QSPI_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA02_QSPI_A_DATA2 | MUX_PAD_CTRL(QSPI_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA03_QSPI_A_DATA3 | MUX_PAD_CTRL(QSPI_PAD_CTRL),
};

int board_qspi_init(void)
{
	imx_iomux_v3_setup_multiple_pads(qspi_pads, ARRAY_SIZE(qspi_pads));

	set_clk_qspi();

	return 0;
}
#endif

#ifdef CONFIG_MXC_SPI
#define SPI_PAD_CTRL	(PAD_CTL_DSE2 | PAD_CTL_HYS)
static iomux_v3_cfg_t const ecspi1_pads[] = {
	IMX8MM_PAD_ECSPI1_SCLK_ECSPI1_SCLK | MUX_PAD_CTRL(SPI_PAD_CTRL),
	IMX8MM_PAD_ECSPI1_MOSI_ECSPI1_MOSI | MUX_PAD_CTRL(SPI_PAD_CTRL),
	IMX8MM_PAD_ECSPI1_MISO_ECSPI1_MISO | MUX_PAD_CTRL(SPI_PAD_CTRL),
	IMX8MM_PAD_ECSPI1_SS0_GPIO5_IO9 | MUX_PAD_CTRL(NO_PAD_CTRL),
};

static iomux_v3_cfg_t const ecspi2_pads[] = {
	IMX8MM_PAD_ECSPI2_SCLK_ECSPI2_SCLK | MUX_PAD_CTRL(SPI_PAD_CTRL),
	IMX8MM_PAD_ECSPI2_MOSI_ECSPI2_MOSI | MUX_PAD_CTRL(SPI_PAD_CTRL),
	IMX8MM_PAD_ECSPI2_MISO_ECSPI2_MISO | MUX_PAD_CTRL(SPI_PAD_CTRL),
	IMX8MM_PAD_ECSPI2_SS0_GPIO5_IO13 | MUX_PAD_CTRL(NO_PAD_CTRL),
};

static void setup_spi(void)
{
	imx_iomux_v3_setup_multiple_pads(ecspi1_pads, ARRAY_SIZE(ecspi1_pads));
	imx_iomux_v3_setup_multiple_pads(ecspi2_pads, ARRAY_SIZE(ecspi2_pads));
	gpio_request(IMX_GPIO_NR(5, 9), "ECSPI1 CS");
	gpio_request(IMX_GPIO_NR(5, 13), "ECSPI2 CS");
}

int board_spi_cs_gpio(unsigned bus, unsigned cs)
{
	if (bus == 0)
		return IMX_GPIO_NR(5, 9);
	else
		return IMX_GPIO_NR(5, 13);
}
#endif

#ifdef CONFIG_NAND_MXS
#ifdef CONFIG_SPL_BUILD
#define NAND_PAD_CTRL	(PAD_CTL_DSE6 | PAD_CTL_FSEL2 | PAD_CTL_HYS)
#define NAND_PAD_READY0_CTRL (PAD_CTL_DSE6 | PAD_CTL_FSEL2 | PAD_CTL_PUE)
static iomux_v3_cfg_t const gpmi_pads[] = {
	IMX8MM_PAD_NAND_ALE_RAWNAND_ALE | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_CE0_B_RAWNAND_CE0_B | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_CE1_B_RAWNAND_CE1_B | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_CLE_RAWNAND_CLE | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA00_RAWNAND_DATA00 | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA01_RAWNAND_DATA01 | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA02_RAWNAND_DATA02 | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA03_RAWNAND_DATA03 | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA04_RAWNAND_DATA04 | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA05_RAWNAND_DATA05	| MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA06_RAWNAND_DATA06	| MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA07_RAWNAND_DATA07	| MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_RE_B_RAWNAND_RE_B | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_READY_B_RAWNAND_READY_B | MUX_PAD_CTRL(NAND_PAD_READY0_CTRL),
	IMX8MM_PAD_NAND_WE_B_RAWNAND_WE_B | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_WP_B_RAWNAND_WP_B | MUX_PAD_CTRL(NAND_PAD_CTRL),
};
#endif

static void setup_gpmi_nand(void)
{
#ifdef CONFIG_SPL_BUILD
	imx_iomux_v3_setup_multiple_pads(gpmi_pads, ARRAY_SIZE(gpmi_pads));
#endif

	init_nand_clk();
}
#endif

int board_early_init_f(void)
{
	struct wdog_regs *wdog = (struct wdog_regs *)WDOG1_BASE_ADDR;

	imx_iomux_v3_setup_multiple_pads(wdog_pads, ARRAY_SIZE(wdog_pads));

	set_wdog_reset(wdog);
	
	hw_watchdog_init();

	imx_iomux_v3_setup_multiple_pads(uart_pads, ARRAY_SIZE(uart_pads));

	init_uart_clk(1);

#ifdef CONFIG_NAND_MXS
	setup_gpmi_nand(); /* SPL will call the board_early_init_f */
#endif

	usb_hub_init();

	wifi_pwr_down_init();

	return 0;
}

int dram_init(void)
{
        unsigned long long sdram_size;
        if(C72_IS_1GB)
                sdram_size = PHYS_DRAM_IS_1GB ;
        if(C72_IS_2GB)
                sdram_size = PHYS_DRAM_IS_2GB ;
        if(C72_IS_4GB)
                sdram_size = PHYS_DRAM_IS_3GB ;
        /* rom_pointer[1] contains the size of TEE occupies */
        if (rom_pointer[1])
                gd->ram_size = sdram_size - rom_pointer[1];
        else
                gd->ram_size = sdram_size;

#if CONFIG_NR_DRAM_BANKS > 1
	if(C72_IS_4GB)
		gd->ram_size += PHYS_SDRAM_2_SIZE;
#endif

	return 0;
}

int dram_init_banksize(void)
{

	unsigned long long sdram_size;
        if(C72_IS_1GB)
                sdram_size = PHYS_DRAM_IS_1GB ;
        if(C72_IS_2GB)
                sdram_size = PHYS_DRAM_IS_2GB ;
        if(C72_IS_4GB)
                sdram_size = PHYS_DRAM_IS_3GB ; /* Actually the Kernel supports MAX 3GB of RAM */

        gd->bd->bi_dram[0].start = PHYS_SDRAM;
        if (rom_pointer[1])
                gd->bd->bi_dram[0].size = sdram_size -rom_pointer[1];
        else
                gd->bd->bi_dram[0].size = sdram_size;

#if CONFIG_NR_DRAM_BANKS > 1
	if(C72_IS_4GB) {
		gd->bd->bi_dram[1].start = PHYS_SDRAM_2;
		gd->bd->bi_dram[1].size = PHYS_SDRAM_2_SIZE;
	}
#endif

	return 0;
}

#ifdef CONFIG_OF_BOARD_SETUP
int ft_board_setup(void *blob, bd_t *bd)
{
	return 0;
}
#endif

#ifdef CONFIG_FEC_MXC

static void setup_iomux_fec(void)
{

}

static int setup_fec(void)
{
	struct iomuxc_gpr_base_regs *gpr =
		(struct iomuxc_gpr_base_regs *)IOMUXC_GPR_BASE_ADDR;

	setup_iomux_fec();

	/* Use 125M anatop REF_CLK1 for ENET1, not from external */
	clrsetbits_le32(&gpr->gpr[1], 0x2000, 0);

	return 0;
}

int board_phy_config(struct phy_device *phydev)
{

	if (phydev->drv->config)
		phydev->drv->config(phydev);
	return 0;
}
#endif

#ifdef CONFIG_USB_EHCI_HCD
int board_usb_init(int index, enum usb_init_type init)
{
	int ret = 0;

	debug("board_usb_init %d, type %d\n", index, init);

	imx8m_usb_power(index, true);

	return ret;
}

int board_usb_cleanup(int index, enum usb_init_type init)
{
	int ret = 0;

	debug("board_usb_cleanup %d, type %d\n", index, init);

	imx8m_usb_power(index, false);
	return ret;
}
#endif

int board_init(void)
{
#ifdef CONFIG_USB_TCPC
	setup_typec();
#endif

#ifdef CONFIG_MXC_SPI
	setup_spi();
#endif

#ifdef CONFIG_FEC_MXC
	setup_fec();
#endif

#ifdef CONFIG_FSL_FSPI
	board_qspi_init();
#endif

	video_bridge_disable();

	return 0;
}

int board_late_init(void)
{
#ifdef CONFIG_ENV_IS_IN_MMC
	board_late_mmc_env_init();
#endif

#ifdef CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG
	env_set("board_name", "SECO DDR4 C72");
	env_set("board_rev", "iMX8MM");
#endif

/* seco_config variables */

#ifdef CONFIG_SECO_ENV_MANAGER
        gd->bsp_sources.kern_dev_list            = &kern_dev_imx8_list[0];
        gd->bsp_sources.kern_dev_num             = kern_dev_imx8_size;
        gd->bsp_sources.fdt_dev_list             = &fdt_dev_imx8_list[0];
        gd->bsp_sources.fdt_dev_num              = fdt_dev_imx8_size;
#ifdef CONFIG_OF_LIBFDT_OVERLAY
        gd->bsp_sources.fdt_overlay_dev_list     = fdt_overlay_dev_imx8_list;
        gd->bsp_sources.fdt_overlay_dev_num      = fdt_overlay_dev_imx8_size;
#endif
        gd->bsp_sources.ramfs_dev_list           = &ramfs_dev_imx8_list[0];
        gd->bsp_sources.ramfs_dev_num            = ramfs_dev_imx8_size;
        gd->bsp_sources.filesystem_dev_list      = &filesystem_dev_imx8_list[0];
        gd->bsp_sources.filesystem_dev_num       = filesystem_dev_imx8_size;

        int c72_video= c72_get_video();
        if (c72_video == DSI_LVDS_BRIDGE){
            gd->boot_setup.video_mode_list           = video_mode_list_lvds;
            gd->boot_setup.video_mode_num            = ARRAY_SIZE(video_mode_list_lvds);
        }
        else if(c72_video == DSI_eDP_BRIDGE){
            gd->boot_setup.video_mode_list           = video_mode_list_edp;
            gd->boot_setup.video_mode_num            = ARRAY_SIZE(video_mode_list_edp);
        }else{
            gd->boot_setup.video_mode_list           = video_mode_list_novideo;
            gd->boot_setup.video_mode_num            = ARRAY_SIZE(video_mode_list_novideo);
            printf("DSI bridge NOT found!\n");
        }

#ifdef CONFIG_OF_LIBFDT_OVERLAY
        gd->boot_setup.overlay_peripheral_list   = overlay_peripheral_list;
        gd->boot_setup.overlay_peripheral_num    = overlay_peripheral_size;
#endif
#endif

	return 0;
}

phys_size_t get_effective_memsize(void)
{
	unsigned long long sdram_size;
        if(C72_IS_1GB)
                sdram_size = PHYS_DRAM_IS_1GB ;
        if(C72_IS_2GB)
                sdram_size = PHYS_DRAM_IS_2GB ;
        if(C72_IS_4GB)
                sdram_size = PHYS_DRAM_IS_3GB ;
        if (rom_pointer[1])
                return (sdram_size - rom_pointer[1]);
        else
                return sdram_size;
}

#ifdef CONFIG_FSL_FASTBOOT
#ifdef CONFIG_ANDROID_RECOVERY
int is_recovery_key_pressing(void)
{
	return 0; /*TODO*/
}
#endif /*CONFIG_ANDROID_RECOVERY*/
#endif /*CONFIG_FSL_FASTBOOT*/
