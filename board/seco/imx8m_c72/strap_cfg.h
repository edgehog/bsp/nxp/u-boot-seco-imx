#ifndef _C72_REVISION_H_
#define _C72_REVISION_H_

int c72_get_board_configuration (void);
void c72n_fdt_ram_setup(void *blob, struct bd_info *bd);
int c72_get_video ( void );

#define DSI_LVDS_BRIDGE 1
#define DSI_eDP_BRIDGE 2

typedef enum {
        RAM_x32_1GB   = 0,
        RAM_x32_2GB   = 0x1,
        RAM_x32_4GB   = 0x2,
        RAM_x16_512MB = 0x4,
        RAM_x16_1GB   = 0x5,
        RAM_x16_2GB   = 0x6,
} RAM_STRAPS;

#define GET_C72_STRAPS  (c72_get_board_configuration() & 0x7)

#define C72_IS_512MB      (GET_C72_STRAPS == RAM_x16_512MB)
#define C72_IS_1GB        ((GET_C72_STRAPS == RAM_x32_1GB) || (GET_C72_STRAPS == RAM_x16_1GB))
#define C72_IS_2GB        ((GET_C72_STRAPS == RAM_x32_2GB) || (GET_C72_STRAPS == RAM_x16_2GB))
#define C72_IS_4GB        (GET_C72_STRAPS == RAM_x32_4GB)

#endif
