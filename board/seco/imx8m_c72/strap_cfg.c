/*
 * Reading C72 Board type
 */

#include <common.h>
#include <malloc.h>
#include <errno.h>
#include <asm/io.h>
#include <miiphy.h>
#include <netdev.h>
#include <asm/mach-imx/iomux-v3.h>
#include <asm-generic/gpio.h>
#include <fsl_esdhc.h>
#include <mmc.h>
#include <i2c.h>
#ifdef TARGET_SECO_IMX8MN_C72
    #include <asm/arch/imx8mn_pins.h>
#else
    #include <asm/arch/imx8mm_pins.h>
#endif
#include <asm/arch/sys_proto.h>
#include <asm/mach-imx/gpio.h>
#include <asm/mach-imx/mxc_i2c.h>
#include "strap_cfg.h"

#define PULLUP_PAD_CTRL  ((PAD_CTL_PUE | PAD_CTL_PE | PAD_CTL_DSE0))
#define PAD_CTL_DSE0            (0x0 << 0)
/* ____________________________________________________________________________
  |                                                                            |
  |                                  C72 REVISION                              |
  |____________________________________________________________________________|
*/


#define C72_IOMUX_REG(x)        ((x))
#define C72_PADCTRL_REG(x)      ((x))
#define C72_GDIR_REG_IN(x,n)    writel((readl(x + 0x4)) & ~(1<<n), x + 0x4 )
#define C72_PSR_REG(x,n)	(readl(x + 0x8) & (1<<n))

#define shift_0			0
#define shift_1			1
#define shift_2			2
#define shift_3			3

#define GPIO5_PAD_BASE		0x30240000		
/* RAM straps */
#define GPIO_CFG_0		22
#define GPIO_CFG_1              23
#define GPIO_CFG_2              26


struct sizes {
	u32 s0;
	u32 s1;
};

DECLARE_GLOBAL_DATA_PTR;

iomux_v3_cfg_t const board_conf_pads[] = {

#ifdef TARGET_SECO_IMX8MN_C72
        /* RAM CONFIG */
        IMX8MN_PAD_UART1_RXD__GPIO5_IO22  	| MUX_PAD_CTRL(PULLUP_PAD_CTRL),   /* cfg_0 */
        IMX8MN_PAD_UART1_TXD__GPIO5_IO23 	| MUX_PAD_CTRL(PULLUP_PAD_CTRL),   /* cfg_1 */
	IMX8MN_PAD_UART3_RXD__GPIO5_IO26	| MUX_PAD_CTRL(PULLUP_PAD_CTRL),   /* cfg_2 */
#else
        /* RAM CONFIG */
    IMX8MM_PAD_UART1_RXD_GPIO5_IO22     | MUX_PAD_CTRL(PULLUP_PAD_CTRL),   /* cfg_0 */
    IMX8MM_PAD_UART1_TXD_GPIO5_IO23     | MUX_PAD_CTRL(PULLUP_PAD_CTRL),   /* cfg_1 */
    IMX8MM_PAD_UART3_RXD_GPIO5_IO26     | MUX_PAD_CTRL(PULLUP_PAD_CTRL),   /* cfg_2 */
#endif
};


int c72_get_board_configuration ( void ) {

        ulong value = 0;

	/* 
         *
         * CFG code is composed in this way:
         * cfg_0 -> first bit   X 
         * cfg_1 -> second bit  X
         * cfg_2 -> third bit   X
         *  
         */
	
	imx_iomux_v3_setup_multiple_pads(board_conf_pads, ARRAY_SIZE(board_conf_pads));
//	gpio_request(IMX_GPIO_NR(5, 22), "ram_cfg0");
//	gpio_request(IMX_GPIO_NR(5, 23), "ram_cfg1");
//	gpio_request(IMX_GPIO_NR(5, 26), "ram_cfg2");

	/* Mux as Input */
	C72_GDIR_REG_IN(GPIO5_PAD_BASE,GPIO_CFG_0);
	C72_GDIR_REG_IN(GPIO5_PAD_BASE,GPIO_CFG_1);
	C72_GDIR_REG_IN(GPIO5_PAD_BASE,GPIO_CFG_2);

	/* Read Conf value */
	value = (C72_PSR_REG(GPIO5_PAD_BASE,GPIO_CFG_0) >> GPIO_CFG_0) << shift_0 | 
		(C72_PSR_REG(GPIO5_PAD_BASE,GPIO_CFG_1) >> GPIO_CFG_1) << shift_1 | 
		(C72_PSR_REG(GPIO5_PAD_BASE,GPIO_CFG_2) >> GPIO_CFG_2) << shift_2 ; 

//	printf("Value 0x%x\n",readl(GPIO5_PAD_BASE + 0x8));
        return value;

}

/* Substitute in the fdt kernel file the right dram setup */
void c72n_fdt_ram_setup(void *blob, struct bd_info *bd) {

        int offset, ret;
        struct sizes ssize;


        printf("Overlay dts /reserved-memory/linux,cma/: size = ");
        offset = fdt_path_offset(blob, "/reserved-memory/linux,cma/");
        if (offset < 0) {
                printf("ERROR: find node /: %s.\n", fdt_strerror(offset));
                return;
        }
        if(C72_IS_512MB) {
                ssize.s0 = cpu_to_fdt32(0x0);
                ssize.s1 = cpu_to_fdt32(0x8000000);
                printf("<0x0 0x8000000>\n");
        }
        if(C72_IS_1GB) {
                ssize.s0 = cpu_to_fdt32(0x0);
                ssize.s1 = cpu_to_fdt32(0x14000000);
                printf("<0x0 0x14000000>\n");
        }
        if(C72_IS_2GB) {
                ssize.s0 = cpu_to_fdt32(0x0);
                ssize.s1 = cpu_to_fdt32(0x20000000);
                printf("<0x0 0x20000000>\n");
        }

        ret = fdt_setprop(blob, offset, "size", &ssize, sizeof(ssize));
        if (ret < 0)
                printf("ERROR: could not update revision property %s.\n",
                        fdt_strerror(ret));

        return;
}

#define DSI_BRIDGE_I2C_BUS 1
#define DSI_LVDS_BRIDGE_I2C_ADD 0x2D
#define DSI_eDP_BRIDGE_I2C_ADD 0x2C
#define DSI_BRIDGE_GPIO_EN IMX_GPIO_NR(2, 11)
static iomux_v3_cfg_t dsi_bridge_en_gpio [] = {
    #ifdef TARGET_SECO_IMX8MN_C72
        IMX8MN_PAD_SD1_STROBE__GPIO2_IO11 | MUX_PAD_CTRL(NO_PAD_CTRL),
    #else
        IMX8MM_PAD_SD1_STROBE_GPIO2_IO11 | MUX_PAD_CTRL(NO_PAD_CTRL),
    #endif
};

int c72_get_video ( void ) {
        struct udevice *bus;
	struct udevice *i2c_dev = NULL;
	int ret;
        int c72_video = -1;

        imx_iomux_v3_setup_multiple_pads(dsi_bridge_en_gpio,ARRAY_SIZE(dsi_bridge_en_gpio));
        gpio_request(DSI_BRIDGE_GPIO_EN, "dsi bridge enable");
        gpio_direction_output ( DSI_BRIDGE_GPIO_EN, 1 );

	ret = uclass_get_device_by_seq(UCLASS_I2C, DSI_BRIDGE_I2C_BUS, &bus);
	if (ret) {
		printf("%s: Can't find bus\n", __func__);
		return -EINVAL;
	}

	ret = dm_i2c_probe(bus, DSI_LVDS_BRIDGE_I2C_ADD, 0, &i2c_dev);
	if (ret==0) {
                printf("DSI to LVDS bridge found!\n");
                c72_video = DSI_LVDS_BRIDGE;
	}

        ret = dm_i2c_probe(bus, DSI_eDP_BRIDGE_I2C_ADD, 0, &i2c_dev);
	if (ret==0) {
                printf("DSI to eDP bridge found!\n");
                c72_video = DSI_eDP_BRIDGE;
	}

        gpio_direction_output ( DSI_BRIDGE_GPIO_EN, 0 );
        return c72_video;
}
