/*
 * (C) Copyright 2015 Seco
 *
 * Author: Davide Cardillo <davide.cardillo@seco.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */
#ifndef _SECO_SBC_A75_DDR_CONFIG_1GB_2x512_H_
#define _SECO_SBC_A75_DDR_CONFIG_1GB_2x512_H_

#include "../common/ddr_conf.h"


#if defined( CONFIG_SECOMX6_1GB_2x512 )

__maybe_unused static int mx6dl_2x512_dcd_table[] = {
	/* Write Leveling */
	0x021b080c, 0x002D0038,  // MX6_MMDC_P0_MPWLDECTRL0
	0x021b0810, 0x00350032,  // MX6_MMDC_P0_MPWLDECTRL1
	/* DQS gating, read delay, write delay calibration values */
	0x021b083c, 0x4244023C,  // MX6_MMDC_P0_MPDGCTRL0
	0x021b0840, 0x0234023C,  // MX6_MMDC_P0_MPDGCTRL1
	/* Read calibration */
	0x021b0848, 0x42444A46,  // MX6_MMDC_P0_MPRDDLCTL
	/* write calibrttion */
	0x021b0850, 0x3A342E34,  // MX6_MMDC_P0_MPWRDLCTL
	/* Complete calibration by forced measurement */
	0x021b08b8, 0x00000800,  // MX6_MMDC_P0_MPMUR0
	/* ========== MMDC init ========== */
	/* in DDR3, 64-bit mode, only MMDC0 is init */
	0x021b0004, 0x0002002D,  // MX6_MMDC_P0_MDPDC
	0x021b0008, 0x1B333030,  // MX6_MMDC_P0_MDOTC
	0x021b000c, 0x3F435333,  // MX6_MMDC_P0_MDCFG0
	0x021b0010, 0xB68E0B63,  // MX6_MMDC_P0_MDCFG1
	0x021b0014, 0x01FF00DB,  // MX6_MMDC_P0_MDCFG2
	0x021b0018, 0x00001740,  // MX6_MMDC_P0_MDMISC
	0x021b001c, 0x00008000,  // MX6_MMDC_P0_MDSCR
	0x021b002c, 0x000026d2,  // MX6_MMDC_P0_MDRWD
	0x021b0030, 0x00431023,  // MX6_MMDC_P0_MDOR
	/* CS0_END = 1280MB (1024 + 256)  in step da 256Mb -> [(768*8/256) - 1] */
	0x021b0040, 0x00000027,  // MX6_MMDC_P0_MDASP
	/* SDE_1=0; ROW=3; BL=1; DSIZ=1 -> 32 bit */
	0x021b0000, 0x84190000,  // MX6_MMDC_P0_MDCTL
	/* Initialize DDR3 on CS_0 */
	0x021b001c, 0x04008032,  // MX6_MMDC_P0_MDSCR
	0x021b001c, 0x00008033,  // MX6_MMDC_P0_MDSCR
	0x021b001c, 0x00048031,  // MX6_MMDC_P0_MDSCR
	/* write 0x0940 to MR0 bank_0 (Burst Type=1 (Interlived)) */
	0x021b001c, 0x15208030,  // MX6_MMDC_P0_MDSCR
	/* ZQ - Calibration */
	0x021b0800, 0xa1390003,  // MX6_MMDC_P0_MPZQHWCTRL
	0x021b001c, 0x04008040,  // MX6_MMDC_P0_MDSCR
	0x021b0020, 0x00005800,  // MX6_MMDC_P0_MDREF
	0x021b0818, 0x00011117,  // MX6_MMDC_P0_MPODTCTRL
	0x021b0004, 0x0002556D,  // MX6_MMDC_P0_MDPDC
	0x021b0404, 0x00011006,  // MX6_MMDC_P0_MAPSR
	0x021b001c, 0x00000000,  // MX6_MMDC_P0_MDSCR
};

#endif    /*  CONFIG_SECOMX6_1GB_2x512  */

#endif    /*  _SECO_SBC_A75_DDR_CONFIG_1GB_2x512_H_  */

