 /* (C) Copyright 2021 Seco
 *
 * Author: Davide Cardillo <davide.cardillo@seco.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */
#ifndef _BOARD_MX8QM_SECO_C26_H__
#define _BOARD_MX8QM_SECO_C26_H__

#include "../common/muxing_mx8.h"

/*  __________________________________________________________________________
 * |                                                                          |
 * |                                   UART                                   |
 * |__________________________________________________________________________|
 */
static iomux_cfg_t uart0_pads[] = {
	SC_P_UART0_RX | MUX_PAD_CTRL(UART_PAD_CTRL),
	SC_P_UART0_TX | MUX_PAD_CTRL(UART_PAD_CTRL),
};
/*  __________________________________________________________________________
 * |__________________________________________________________________________|
 */

#endif    /*  _BOARD_MX8QM_SECO_C26_H__  */