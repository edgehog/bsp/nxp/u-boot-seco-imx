/*
 * (C) Copyright 2022 Seco
 *
 * Author: Davide Cardillo <davide.cardillo@seco.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

#ifndef _C26_REVISION_H_
#define _C26_REVISION_H_

int strap_get_ram_cfg (void);
int strap_get_video_cfg (void);
int strap_get_revision_cfg (void);
int strap_get_board_cfg(void);
void strap_show(void);

typedef enum {
	RAM_RESERVED = 0x0,
	RAM_2GB      = 0x1,
	RAM_4GB      = 0x2,
	RAM_8GB      = 0x3,
} RAM_STRAPS;

typedef enum {
	VIDEO_CFG_A1 = 0x0,
	VIDEO_CFG_A2 = 0x1,
} VIDEO_STRAPS;

typedef enum {
	BEFORE_REV_C = 0x0,
	REV_C        = 0x1,
} BOARD_REV;

typedef struct {
	u8 ram_size;
	u8 video;
	u8 hw_rev;
} strap_conf_t;

#endif
