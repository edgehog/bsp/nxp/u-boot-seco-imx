/*
 * (C) Copyright 2022 Seco
 *
 * Author: Davide Cardillo <davide.cardillo@seco.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

#include <common.h>
#include <command.h>
#include <malloc.h>
#include <errno.h>
#include <asm/io.h>
#include <asm/mach-imx/iomux-v3.h>
#include <asm-generic/gpio.h>
#include <asm/arch/sys_proto.h>
#include <asm/mach-imx/gpio.h>

#include "strap_cfg.h"


int strap_get_ram_cfg( void ) {
    struct gpio_desc seco_code0, seco_code1;
    int ret;
    ulong value = 0;

    ret = sc_pm_set_resource_power_mode( -1, SC_R_GPIO_0, SC_PM_PW_MODE_ON );
    if (ret) {
        printf("%s unabled to power resource SC_R_GPIO_0, ret = %d\n", __func__, ret);
        return 0;
    }

    ret = dm_gpio_lookup_name("gpio0_0", &seco_code0);
    if (ret) {
        printf("%s lookup GPIO0_0 failed ret = %d\n", __func__, ret);
        return 0;
    }

    ret = dm_gpio_lookup_name("gpio0_2", &seco_code1);
    if (ret) {
        printf("%s lookup GPIO0_2 failed ret = %d\n", __func__, ret);
        return 0;
    }

    ret = dm_gpio_request(&seco_code0, "seco_code0");
    if (ret) {
        printf("%s request seco_code0 failed ret = %d\n", __func__, ret);
        return 0;
    }

    ret = dm_gpio_request(&seco_code1, "seco_code1");
    if (ret) {
        dm_gpio_free((seco_code0.dev), &seco_code0);
        printf("%s request seco_code1 failed ret = %d\n", __func__, ret);
        return 0;
    }

    dm_gpio_set_dir_flags(&seco_code0, GPIOD_IS_IN);
    dm_gpio_set_dir_flags(&seco_code1, GPIOD_IS_IN);

    /* Read Conf value */
    value = ((dm_gpio_get_value(&seco_code1))<<1) | dm_gpio_get_value(&seco_code0) ; 

    ret = dm_gpio_free((seco_code0.dev), &seco_code0);
    if (ret)
        return 0;

    ret = dm_gpio_free((seco_code1.dev), &seco_code1);
    if (ret)
        return 0;

    return value;
}


int strap_get_video_cfg( void ) {
    struct gpio_desc seco_code2;
    int ret;
    ulong value = 0;

    ret = sc_pm_set_resource_power_mode( -1, SC_R_GPIO_0, SC_PM_PW_MODE_ON );
    if (ret)
			return 0;

    ret = dm_gpio_lookup_name("gpio0_1", &seco_code2);
    if (ret) {
        printf("%s lookup GPIO0_1 failed ret = %d\n", __func__, ret);
        return 0;
    }

    ret = dm_gpio_request(&seco_code2, "seco_code2");
    if (ret) {
        printf("%s request seco_code2 failed ret = %d\n", __func__, ret);
        return 0;
    }

    dm_gpio_set_dir_flags(&seco_code2, GPIOD_IS_IN);

    /* Read Conf value */
    value = 0x1 & dm_gpio_get_value(&seco_code2) ; 

    ret = dm_gpio_free((seco_code2.dev), &seco_code2);
    if (ret)
        return 0;



    return value;
}


int strap_get_revision_cfg( void ) {
    struct gpio_desc seco_code3, seco_code4, seco_code5;
    int ret;
    ulong value = 0;

    ret = sc_pm_set_resource_power_mode( -1, SC_R_GPIO_0, SC_PM_PW_MODE_ON );
    if (ret)
			return 0;
    ret = sc_pm_set_resource_power_mode( -1, SC_R_GPIO_2, SC_PM_PW_MODE_ON );
    if (ret)
			return 0;

    ret = dm_gpio_lookup_name("gpio0_5", &seco_code3);
    if (ret) {
        printf("%s lookup GPIO0_5 failed ret = %d\n", __func__, ret);
        return 0;
    }

    ret = dm_gpio_lookup_name("gpio2_23", &seco_code4);
    if (ret) {
        printf("%s lookup GPIO2_23 failed ret = %d\n", __func__, ret);
        return 0;
    }

    ret = dm_gpio_lookup_name("gpio2_24", &seco_code5);
    if (ret) {
        printf("%s lookup GPIO2_24 failed ret = %d\n", __func__, ret);
        return 0;
    }

    ret = dm_gpio_request(&seco_code3, "seco_code3");
    if (ret) {
        printf("%s request seco_code3 failed ret = %d\n", __func__, ret);
        return 0;
    }

    ret = dm_gpio_request(&seco_code4, "seco_code4");
    if (ret) {
        dm_gpio_free((seco_code3.dev), &seco_code3);
        printf("%s request seco_code4 failed ret = %d\n", __func__, ret);
        return 0;
    }

    ret = dm_gpio_request(&seco_code5, "seco_code5");
    if (ret) {
        dm_gpio_free((seco_code3.dev), &seco_code3);
        dm_gpio_free((seco_code4.dev), &seco_code4);
        printf("%s request seco_code5 failed ret = %d\n", __func__, ret);
        return 0;
    }

    dm_gpio_set_dir_flags(&seco_code3, GPIOD_IS_IN);
    dm_gpio_set_dir_flags(&seco_code4, GPIOD_IS_IN);
    dm_gpio_set_dir_flags(&seco_code5, GPIOD_IS_IN);

    /* Read Conf value */
    value = ((dm_gpio_get_value(&seco_code5))<<2) | ((dm_gpio_get_value(&seco_code4))<<1) | dm_gpio_get_value(&seco_code3); 

    ret = dm_gpio_free((seco_code3.dev), &seco_code3);
    if (ret)
        return 0;

    ret = dm_gpio_free((seco_code4.dev), &seco_code4);
    if (ret)
        return 0;

    ret = dm_gpio_free((seco_code5.dev), &seco_code5);
    if (ret)
        return 0;

    return value;
}


int strap_get_board_cfg( void ) {
    strap_conf_t c26_strap_conf;
    c26_strap_conf.ram_size = strap_get_ram_cfg( );
    c26_strap_conf.video = strap_get_video_cfg( );
    c26_strap_conf.hw_rev = strap_get_revision_cfg( );

    gd->strap_configuration = malloc( sizeof( strap_conf_t ) );
    if ( gd->strap_configuration != NULL ) {
        memcpy( gd->strap_configuration, &c26_strap_conf, sizeof( strap_conf_t ) );
        return 0;
    } else
        return -1;
}


void strap_show( void ) {
    strap_conf_t *c26_strap_conf = (strap_conf_t *)gd->strap_configuration;
    if ( gd->strap_configuration == NULL )
        return;
    printf( "Strap configuration: \n" );
    
    printf( "	RAM code: %d (", c26_strap_conf->ram_size );
    switch ( c26_strap_conf->ram_size ) {
        case RAM_RESERVED:
            printf( "RESERVED" );
            break;
        case RAM_2GB:
            printf( "2GB" );
            break;
        case RAM_4GB:
            printf( "4GB" );
            break;
        case RAM_8GB:
            printf( "8GB" );
            break;
        default:
            printf( "Unknown" );
            break;
    }
    printf ( ")\n" );

    printf( "	Video code: %d (", c26_strap_conf->video );
    switch ( c26_strap_conf->video ) {
        case VIDEO_CFG_A1:
            printf( "HDMI/DP + LVDS dual" );
            break;
        case VIDEO_CFG_A2:
            printf( "eDP + LVDS single" );
            break;
        default:
            printf( "Unknown" );
            break;
    }
    printf ( ")\n" );
    
    printf( "	Revision code: %d (", c26_strap_conf->hw_rev );
    switch ( c26_strap_conf->hw_rev ) {
        case BEFORE_REV_C:
            printf( "BEFORE REVC" );
            break;
        case REV_C:
            printf( "REVC" );
            break;
        default:
            printf( "Unknown" );
            break;
    }
    printf ( ")\n" );

}
