// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright 2018 NXP
 */

#include <common.h>
#include <cpu_func.h>
#include <env.h>
#include <errno.h>
#include <init.h>
#include <asm/global_data.h>
#include <linux/libfdt.h>
#include <fdt_support.h>
#include <asm/io.h>
#include <asm/gpio.h>
#include <asm/arch/clock.h>
#include <asm/arch/sci/sci.h>
#include <asm/arch/imx8-pins.h>
#include <asm/arch/snvs_security_sc.h>
#include <usb.h>
#include <asm/arch/iomux.h>
#include <asm/arch/sys_proto.h>
#include "../common/tcpc.h"
#include "command.h"
#include "../common/proto_seco.h"
#include "seco/seco_env_gd.h"
#include "imx8qm_c26.h"
#include "strap_cfg.h"
#include "asm/arch-imx8/imx8qm_lpcg.h"
#include "asm/arch-imx8/lpcg.h"
#include "dt-bindings/soc/imx8_pd.h"
#include <linux/delay.h>

DECLARE_GLOBAL_DATA_PTR;

#if IS_ENABLED(CONFIG_FEC_MXC)
#include <miiphy.h>
#endif

#ifdef CONFIG_SECO_ENV_MANAGER
	#include <seco/env_common.h>
	#include "env_conf.h"
#endif 


/*  __________________________________________________________________________
 * |                                                                          |
 * |                                   UART                                   |
 * |__________________________________________________________________________|
 */
static void setup_iomux_uart( void ) {
	imx8_iomux_setup_multiple_pads( uart0_pads, ARRAY_SIZE( uart0_pads ) );
}
/*  __________________________________________________________________________
 * |__________________________________________________________________________|
 */

/*  __________________________________________________________________________
 * |                                                                          |
 * |                               BOOT VALIDATE                              |
 * |__________________________________________________________________________|
 */
#define BOOT_VALIDATE	IMX_GPIO_NR(2,31)


static void send_boot_validate( void ) {
	boot_validate (BOOT_VALIDATE, SC_P_ESAI0_TX5_RX0 | MUX_MODE_ALT(3) | MUX_PAD_CTRL(GPIO_PAD_CTRL));
}
/*  __________________________________________________________________________
 * |__________________________________________________________________________|
 */


#define PHYS_DRAM_IS_1GB                0x40000000
#define PHYS_DRAM_IS_2GB                0x80000000
#define PHYS_DRAM_IS_3GB                0xc0000000
#define PHYS_DRAM_IS_4GB                0x100000000
#define PHYS_DRAM_IS_6GB                0x180000000

void board_mem_get_layout(u64 *phys_sdram_1_start,
			  u64 *phys_sdram_1_size,
			  u64 *phys_sdram_2_start,
			  u64 *phys_sdram_2_size)
{
	static int ram_size = 0;
	u64 sdram_size = 0x0;

	strap_conf_t *strap_conf = (strap_conf_t *)gd->strap_configuration;
	if ( strap_conf != NULL ) {
		ram_size = strap_conf->ram_size;
	}
	
	switch( ram_size ) {
		case RAM_RESERVED:
			sdram_size = 0x0;
			break;
		case RAM_2GB:
			sdram_size = 0x0;
			break;
		case RAM_4GB:
			sdram_size = PHYS_DRAM_IS_2GB;
			break;
		case RAM_8GB:
			sdram_size = PHYS_DRAM_IS_6GB;
			break;
		default:
			sdram_size = 0x0;
			
			break;
	}
	
	*phys_sdram_1_start = PHYS_SDRAM_1;
	*phys_sdram_1_size = PHYS_SDRAM_1_SIZE;
	*phys_sdram_2_start = PHYS_SDRAM_2;
	*phys_sdram_2_size = sdram_size;
}



/*  __________________________________________________________________________
 * |                                                                          |
 * |                               BOOT DEVICE                                |
 * |__________________________________________________________________________|
 */
boot_mem_dev_t boot_mem_dev_list[SECO_NUM_BOOT_DEV] = {
	{ SD1_BOOT, SECO_DEV_LABEL_EMMC },
	{ SD2_BOOT, SECO_DEV_LABEL_SD },
	{ SD3_BOOT, SECO_DEV_LABEL_SD_EXT },
};

int usdhc_devno[4] = { BOARD_BOOT_ID_EMMC, BOARD_BOOT_ID_SD, BOARD_BOOT_ID_SD_EXT, -1};  
/*  __________________________________________________________________________
 * |__________________________________________________________________________|
 */


int board_early_init_f(void)
{
	sc_pm_clock_rate_t rate = SC_80MHZ;
	int ret;

	/* When start u-boot in XEN VM, directly return */
	if (IS_ENABLED(CONFIG_XEN)) {
		writel(0xF53535F5, (void __iomem *)0x80000000);
		return 0;
	}

#ifdef CONFIG_TARGET_IMX8QM_MEK_A72_ONLY
	/* Set UART2 clock root to 80 MHz */
	ret = sc_pm_setup_uart(SC_R_UART_2, rate);
	if (ret)
		return ret;
#else
	/* Set UART0 clock root to 80 MHz */
	ret = sc_pm_setup_uart(SC_R_UART_0, rate);
	if (ret)
		return ret;
#endif	/* CONFIG_TARGET_IMX8QM_MEK_A72_ONLY */

	setup_iomux_uart();

	return 0;
}




#define BB_GPIO_3V3_1 IMX_GPIO_NR(4, 20)
#define BB_GPIO_3V3_2 IMX_GPIO_NR(4, 24)
#define BB_GPIO_3V3_3 IMX_GPIO_NR(4, 23)
#define IMX8_RST_HUB    IMX_GPIO_NR(4, 3)
#define USB_HC_SEL      IMX_GPIO_NR(4, 6)

#define PCIE_PAD_CTRL   ((SC_PAD_CONFIG_OD_IN << PADRING_CONFIG_SHIFT))
static iomux_cfg_t board_pcie_pins[] = {
        SC_P_PCIE_CTRL0_CLKREQ_B | MUX_MODE_ALT(0) | MUX_PAD_CTRL(PCIE_PAD_CTRL),
        SC_P_PCIE_CTRL0_WAKE_B | MUX_MODE_ALT(0) | MUX_PAD_CTRL(PCIE_PAD_CTRL),
        SC_P_PCIE_CTRL0_PERST_B | MUX_MODE_ALT(0) | MUX_PAD_CTRL(PCIE_PAD_CTRL),
};

static iomux_cfg_t board_gpios[] = {
    SC_P_USB_SS3_TC0    | MUX_MODE_ALT(3) | MUX_PAD_CTRL(GPIO_PAD_CTRL), // IMX8_RST_HUB  - GPIO 455
};


static void board_gpio_init(void)
{

	int ret;
	struct gpio_desc desc;
	struct power_domain pd;
    
    //imx8_iomux_setup_multiple_pads(board_gpios, ARRAY_SIZE(board_gpios));
    
    sc_pm_set_resource_power_mode( -1, SC_R_GPIO_4, SC_PM_PW_MODE_ON );
    
    ret = dm_gpio_lookup_name("gpio4_3", &desc);
	if (ret) {
		printf("%s lookup GPIO@4_3 failed ret = %d\n", __func__, ret);
		return;
	}

	ret = dm_gpio_request(&desc, "imx8_rst_hub");
	if (ret) {
		printf("%s request imx8_rst_hub failed ret = %d\n", __func__, ret);
		return;
	}
	
    dm_gpio_set_dir_flags(&desc, GPIOD_IS_OUT);
	dm_gpio_set_value(&desc, 0);

	ret = dm_gpio_lookup_name("gpio4_6", &desc);
	if (ret) {
		printf("%s lookup GPIO@4_6 failed ret = %d\n", __func__, ret);
		return;
	}

	ret = dm_gpio_request(&desc, "usb_hc_sel");
	if (ret) {
		printf("%s request usb_hc_sel failed ret = %d\n", __func__, ret);
		return;
	}
	
	dm_gpio_set_dir_flags(&desc, GPIOD_IS_OUT);
	dm_gpio_set_value(&desc, 0);

	ret = dm_gpio_lookup_name("gpio4_20", &desc);
	if (ret) {
		printf("%s lookup GPIO@4_20 failed ret = %d\n", __func__, ret);
		return;
	}

	ret = dm_gpio_request(&desc, "bb_3v3_1");
	if (ret) {
		printf("%s request bb_3v3_1 failed ret = %d\n", __func__, ret);
		return;
	}

	dm_gpio_set_dir_flags(&desc, GPIOD_IS_OUT | GPIOD_IS_OUT_ACTIVE);

	ret = dm_gpio_lookup_name("gpio4_24", &desc);
	if (ret) {
		printf("%s lookup GPIO@4_24 failed ret = %d\n", __func__, ret);
		return;
	}

	ret = dm_gpio_request(&desc, "bb_3v3_2");
	if (ret) {
		printf("%s request bb_3v3_2 failed ret = %d\n", __func__, ret);
		return;
	}

	dm_gpio_set_dir_flags(&desc, GPIOD_IS_OUT | GPIOD_IS_OUT_ACTIVE);

	ret = dm_gpio_lookup_name("gpio4_23", &desc);
	if (ret) {
		printf("%s lookup GPIO@4_23 failed ret = %d\n", __func__, ret);
		return;
	}

	ret = dm_gpio_request(&desc, "bb_3v3_3");
	if (ret) {
		printf("%s request bb_3v3_3 failed ret = %d\n", __func__, ret);
		return;
	}

	dm_gpio_set_dir_flags(&desc, GPIOD_IS_OUT | GPIOD_IS_OUT_ACTIVE);

	/* enable LVDS SAS boards */
	ret = dm_gpio_lookup_name("gpio1_6", &desc);
	if (ret) {
		printf("%s lookup GPIO1_6 failed ret = %d\n", __func__, ret);
		return;
	}

	ret = dm_gpio_request(&desc, "lvds_enable");
	if (ret) {
		printf("%s request lvds_enable failed ret = %d\n", __func__, ret);
		return;
	}

	dm_gpio_set_dir_flags(&desc, GPIOD_IS_OUT | GPIOD_IS_OUT_ACTIVE);

	/* enable MIPI SAS boards */
	ret = dm_gpio_lookup_name("gpio1_7", &desc);
	if (ret) {
		printf("%s lookup GPIO1_7 failed ret = %d\n", __func__, ret);
		return;
	}

	ret = dm_gpio_request(&desc, "mipi_enable");
	if (ret) {
		printf("%s request mipi_enable failed ret = %d\n", __func__, ret);
		return;
	}

	dm_gpio_set_dir_flags(&desc, GPIOD_IS_OUT | GPIOD_IS_OUT_ACTIVE);
	
	if (!power_domain_lookup_name("hsio_sata0", &pd)) {
            ret = power_domain_on(&pd);
            if (ret)
                    printf("hsio_sata0 Power up failed! (error = %d)\n", ret);
    }

    if (!power_domain_lookup_name("hsio_pcie0", &pd)) {
            ret = power_domain_on(&pd);
            if (ret)
                    printf("hsio_pcie0 Power up failed! (error = %d)\n", ret);
    }

    if (!power_domain_lookup_name("hsio_pcie1", &pd)) {
            ret = power_domain_on(&pd);
            if (ret)
                    printf("hsio_pcie1 Power up failed! (error = %d)\n", ret);
    }

    if (!power_domain_lookup_name("hsio_gpio", &pd)) {
            ret = power_domain_on(&pd);
            if (ret)
                     printf("hsio_gpio Power up failed! (error = %d)\n", ret);
    }
    
    lpcg_all_clock_on(HSIO_PCIE_X2_LPCG);
    lpcg_all_clock_on(HSIO_PCIE_X1_LPCG);
    lpcg_all_clock_on(HSIO_SATA_LPCG);
    lpcg_all_clock_on(HSIO_PHY_X2_LPCG);
    lpcg_all_clock_on(HSIO_PHY_X1_LPCG);
    lpcg_all_clock_on(HSIO_PHY_X2_CRR0_LPCG);
    lpcg_all_clock_on(HSIO_PHY_X1_CRR1_LPCG);
    lpcg_all_clock_on(HSIO_PCIE_X2_CRR2_LPCG);
    lpcg_all_clock_on(HSIO_PCIE_X1_CRR3_LPCG);
    lpcg_all_clock_on(HSIO_SATA_CRR4_LPCG);
    lpcg_all_clock_on(HSIO_MISC_LPCG);
    lpcg_all_clock_on(HSIO_GPIO_LPCG);

    imx8_iomux_setup_multiple_pads(board_pcie_pins, ARRAY_SIZE(board_pcie_pins));

}





#ifdef CONFIG_USB

// #ifdef CONFIG_USB_TCPC
// struct gpio_desc type_sel_desc;

// static iomux_cfg_t ss_mux_gpio[] = {
// 	SC_P_USB_SS3_TC3 | MUX_MODE_ALT(3) | MUX_PAD_CTRL(GPIO_PAD_CTRL),
// 	SC_P_QSPI1A_SS0_B | MUX_MODE_ALT(3) | MUX_PAD_CTRL(GPIO_PAD_CTRL),
// };

// struct tcpc_port port;
// struct tcpc_port_config port_config = {
// 	.i2c_bus = 0,
// 	.addr = 0x51,
// 	.port_type = TYPEC_PORT_DFP,
// };

// void ss_mux_select(enum typec_cc_polarity pol)
// {
// 	if (pol == TYPEC_POLARITY_CC1)
// 		dm_gpio_set_value(&type_sel_desc, 0);
// 	else
// 		dm_gpio_set_value(&type_sel_desc, 1);
// }

// static void setup_typec(void)
// {
// 	int ret;
// 	struct gpio_desc typec_en_desc;

// 	imx8_iomux_setup_multiple_pads(ss_mux_gpio, ARRAY_SIZE(ss_mux_gpio));
// 	ret = dm_gpio_lookup_name("GPIO4_6", &type_sel_desc);
// 	if (ret) {
// 		printf("%s lookup GPIO4_6 failed ret = %d\n", __func__, ret);
// 		return;
// 	}

// 	ret = dm_gpio_request(&type_sel_desc, "typec_sel");
// 	if (ret) {
// 		printf("%s request typec_sel failed ret = %d\n", __func__, ret);
// 		return;
// 	}

// 	dm_gpio_set_dir_flags(&type_sel_desc, GPIOD_IS_OUT);

// 	ret = dm_gpio_lookup_name("GPIO4_19", &typec_en_desc);
// 	if (ret) {
// 		printf("%s lookup GPIO4_19 failed ret = %d\n", __func__, ret);
// 		return;
// 	}

// 	ret = dm_gpio_request(&typec_en_desc, "typec_en");
// 	if (ret) {
// 		printf("%s request typec_en failed ret = %d\n", __func__, ret);
// 		return;
// 	}

// 	/* Enable SS MUX */
// 	dm_gpio_set_dir_flags(&typec_en_desc, GPIOD_IS_OUT | GPIOD_IS_OUT_ACTIVE);

// 	tcpc_init(&port, port_config, &ss_mux_select);
// }
// #endif

/*int board_usb_init(int index, enum usb_init_type init)
{
	int ret = 0;
	if (index == 1) {
		if (init == USB_INIT_HOST) {
#ifdef CONFIG_USB_TCPC
			ret = tcpc_setup_dfp_mode(&port);
#endif
#ifdef CONFIG_USB_CDNS3_GADGET
		} else {
#ifdef CONFIG_USB_TCPC
			ret = tcpc_setup_ufp_mode(&port);
			printf("%d setufp mode %d\n", index, ret);
#endif
#endif
		}
	}

	return ret;

}

int board_usb_cleanup(int index, enum usb_init_type init)
{
	int ret = 0;

	if (index == 1) {
		if (init == USB_INIT_HOST) {
#ifdef CONFIG_USB_TCPC
			ret = tcpc_disable_src_vbus(&port);
#endif
		}
	}

	return ret;
}*/

/*
INIT
We just want one USB, HSIC, which is tested as second port.
As a consequence:
- the first call to board_usb_init must fail to speed up boot (but it could work, if needed)
- the second call to board_usb_init moves HUB RESET
For this, we cannot use index, as this is incremented only for
correctly initialized ports, so we use a different counter.

Note that this order is independent on the presence of JP3, which only influences USB0 initialization result.


CLEANUP
Instead, board_usb_cleanup is called just once, for the initialized port, so
there we reset to zero the local counter and move HUB RESET.
*/

int board_usb_init(int index, enum usb_init_type init)
{
	int ret;
	struct gpio_desc desc;
	sc_pm_set_resource_power_mode( -1, SC_R_GPIO_4, SC_PM_PW_MODE_ON );
	
    dm_gpio_lookup_name("gpio4_3", &desc);
	dm_gpio_request(&desc, "imx8_rst_hub");
	dm_gpio_set_dir_flags(&desc, GPIOD_IS_OUT);
	dm_gpio_set_value(&desc,1);

    mdelay(100);

	return 0;
}

int board_usb_cleanup(int index, enum usb_init_type init)
{
	int ret;
	struct gpio_desc desc;
    /* RESET just once */
    sc_pm_set_resource_power_mode( -1, SC_R_GPIO_4, SC_PM_PW_MODE_ON );

    dm_gpio_lookup_name("gpio4_3", &desc);
	dm_gpio_request(&desc, "imx8_rst_hub");
	dm_gpio_set_dir_flags(&desc, GPIOD_IS_OUT);
	dm_gpio_set_value(&desc,0);
	return 0;
}


#endif




/*  __________________________________________________________________________
 * |__________________________________________________________________________|
 */

int checkboard( void ) {
	print_bootinfo();

	strap_get_board_cfg( );
	strap_show( );
	
	return 0;
}


int board_init( void ) {
	if (IS_ENABLED(CONFIG_XEN))
		return 0;
	
	board_gpio_init();
		
#ifdef CONFIG_IMX_SNVS_SEC_SC_AUTO
	{
		int ret = snvs_security_sc_init();

		if (ret)
			return ret;
	}
#endif

	send_boot_validate( );
	return 0;
}

int board_late_init(void)
{
	char *fdt_file;
#if !defined(CONFIG_TARGET_IMX8QM_MEK_A53_ONLY) && !defined(CONFIG_TARGET_IMX8QM_MEK_A72_ONLY)
	bool m4_booted;
#endif

#if defined(CONFIG_SECO_ENV_MANAGER) && defined(CONFIG_OF_LIBFDT_OVERLAY)
	strap_conf_t *c26_strap_conf = (strap_conf_t *)gd->strap_configuration;
#endif

	build_info();

#ifdef CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG
	env_set("board_name", "C26");
	env_set("board_rev", "iMX8QM");
#endif

	env_set("sec_boot", "no");
#ifdef CONFIG_AHAB_BOOT
	env_set("sec_boot", "yes");
#endif

	fdt_file = env_get("fdt_file");

	if (fdt_file && !strcmp(fdt_file, "undefined")) {
#if defined(CONFIG_TARGET_IMX8QM_MEK_A53_ONLY)
		env_set("fdt_file", "imx8qm-mek-cockpit-ca53.dtb");
#elif defined(CONFIG_TARGET_IMX8QM_MEK_A72_ONLY)
		env_set("fdt_file", "imx8qm-mek-cockpit-ca72.dtb");
#else
		m4_booted = m4_parts_booted();
		if (m4_booted)
			env_set("fdt_file", "imx8qm-mek-rpmsg.dtb");
		else
			env_set("fdt_file", "imx8qm-mek.dtb");
#endif
	}

/* seco_config variables */
#ifdef CONFIG_SECO_ENV_MANAGER
        gd->bsp_sources.kern_dev_list            = &kern_dev_imx8_list[0];
        gd->bsp_sources.kern_dev_num             = kern_dev_imx8_size;
        gd->bsp_sources.fdt_dev_list             = &fdt_dev_imx8_list[0];
        gd->bsp_sources.fdt_dev_num              = fdt_dev_imx8_size;
        gd->bsp_sources.fw_dev_list				 = firmware_dev_imx8_list;
        gd->bsp_sources.fw_dev_num               = firmware_dev_imx8_size;
#ifdef CONFIG_OF_LIBFDT_OVERLAY
        gd->bsp_sources.fdt_overlay_dev_list     = fdt_overlay_dev_imx8_list;
        gd->bsp_sources.fdt_overlay_dev_num      = fdt_overlay_dev_imx8_size;
#endif
        gd->bsp_sources.ramfs_dev_list           = &ramfs_dev_imx8_list[0];
        gd->bsp_sources.ramfs_dev_num            = ramfs_dev_imx8_size;
        gd->bsp_sources.filesystem_dev_list      = &filesystem_dev_imx8_list[0];
        gd->bsp_sources.filesystem_dev_num       = filesystem_dev_imx8_size;

#ifdef CONFIG_OF_LIBFDT_OVERLAY
		if ( (VIDEO_STRAPS)c26_strap_conf->video == VIDEO_CFG_A1 ) {
			gd->boot_setup.video_mode_list           = video_mode_list_cfg_a1;
        	gd->boot_setup.video_mode_num            = video_mode_size_cfg_a1;
		} else if ( (VIDEO_STRAPS)c26_strap_conf->video == VIDEO_CFG_A2 ) {
			gd->boot_setup.video_mode_list           = video_mode_list_cfg_a2;
        	gd->boot_setup.video_mode_num            = video_mode_size_cfg_a2;
		} else {
			gd->boot_setup.video_mode_list           = NULL;
        	gd->boot_setup.video_mode_num            = 0;
		}

        gd->boot_setup.overlay_peripheral_list   = overlay_peripheral_list;
        gd->boot_setup.overlay_peripheral_num    = overlay_peripheral_size;
#endif
#endif

#ifdef CONFIG_ENV_IS_IN_MMC
	board_late_mmc_env_init();
#endif

	return 0;
}





void board_quiesce_devices(void)
{
	const char *power_on_devices[] = {
#ifdef CONFIG_TARGET_IMX8QM_MEK_A72_ONLY
		"dma_lpuart2",
		"PD_UART2_TX",
		"PD_UART2_RX",
#else
		"dma_lpuart0",
#endif
	};

	if (IS_ENABLED(CONFIG_XEN)) {
		/* Clear magic number to let xen know uboot is over */
		writel(0x0, (void __iomem *)0x80000000);
		return;
	}

	imx8_power_off_pd_devices(power_on_devices, ARRAY_SIZE(power_on_devices));
}

/*
 * Board specific reset that is system reset.
 */
void reset_cpu(ulong addr)
{
	sc_pm_reboot(-1, SC_PM_RESET_TYPE_COLD);
	while(1);
}

#ifdef CONFIG_OF_BOARD_SETUP
int ft_board_setup(void *blob, struct bd_info *bd)
{
	return 0;
}
#endif

// int board_mmc_get_env_dev(int devno)
// {
// 	/* Use EMMC */
// 	if (IS_ENABLED(CONFIG_XEN))
// 		return 0;

// 	return devno;
// }

// int mmc_map_to_kernel_blk(int dev_no)
// {
// 	/* Use EMMC */
// 	if (IS_ENABLED(CONFIG_XEN))
// 		return 0;

// 	return dev_no;
// }


#ifdef CONFIG_ANDROID_SUPPORT
bool is_power_key_pressed(void) {
	sc_bool_t status = SC_FALSE;

	sc_misc_get_button_status(-1, &status);
	return (bool)status;
}
#endif

#ifdef CONFIG_FSL_FASTBOOT
#ifdef CONFIG_ANDROID_RECOVERY
int is_recovery_key_pressing(void)
{
	return 0; /* TODO */
}
#endif /* CONFIG_ANDROID_RECOVERY */
#endif /* CONFIG_FSL_FASTBOOT */
