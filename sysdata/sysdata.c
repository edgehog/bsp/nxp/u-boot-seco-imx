

#ifndef USE_HOSTCC

#include <memalign.h>
#include <env.h> 

#else

#include <compiler.h>
#include <fcntl.h>
#include <libgen.h>
#include <linux/fs.h>
#include <linux/stringify.h>
#include <linux/stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <stdint.h>
#include <u-boot/crc.h>

#endif

#include <malloc.h>
#include <errno.h>
#include <sysdata.h>
#include <u-boot/crc.h>



#ifndef USE_HOSTCC

/* __________________________________________________________________________________________
  |                                                                                          |
  |                                  BASIC INTERNAL FUNCTION                                 |
  |__________________________________________________________________________________________|
*/

/** 
 * _sysdata_driver_find( ) - search device to use
 * 
 * This gets the most suitable device to use
 * 
 * @return data structure of the device to use
*/
struct sysdata_driver *_sysdata_driver_find( void ) {
	static struct sysdata_driver *drv = NULL;
	const int             n_ents = ll_entry_count( struct sysdata_driver, sysdata_driver );
	struct sysdata_driver *entry;

	if ( drv != NULL )
		return drv;
		
	drv = ll_entry_start( struct sysdata_driver, sysdata_driver );
	for ( entry = drv ; entry != drv + n_ents ; entry++ ) {
		return entry;
	}

	/* Not found */
	return NULL;
}


/** 
 * _sysdata_wr( ) - simple write operation 
 * 
 * This uses the device selected at compilation time
 * 
 * @table: main data structure to save
 * @return 0 if ok, negative value if operation fails
*/
int _sysdata_wr( sysdata_t *table ) {
	struct sysdata_driver *drv = _sysdata_driver_find( );

	if ( drv == NULL )
		return -ENODEV;

	if ( drv->save )
		return drv->save( table );

	return -ENOSYS;
}


/** 
 * _sysdata_rd( ) - simple read operation 
 * 
 * This uses the device selected at compilation time
 * 
 * @table: main data structure in which to load stored data
 * @return 0 if ok, negative value if operation fails
*/
int _sysdata_rd( sysdata_t *table ) {
	struct sysdata_driver *drv = _sysdata_driver_find( );

	if ( drv == NULL )
		return -ENODEV;

	if ( drv->load )
		return drv->load( table );

	return -ENOSYS;
}


/** 
 * get_sysdata_device_name( ) - name of device in using

 * This gets the name of the device the device have been found
 * 
 * @return the string with the device name
*/
const char *get_sysdata_device_name( ) {
	struct sysdata_driver *drv = _sysdata_driver_find( );

	if ( drv == NULL )
		return NULL;

	return  drv->get_device_name( );
}
#endif    /*  USE_HOSTCC  */


/** 
 * _get_data_crc32( ) - checksum evaluation
 * 
 * This gets the CRC value of the sensitive data
 * 
 * @table: sensitive data of the sysdata structure 
 * @return crc32 value
*/
inline int _get_data_crc32( sysdata_data_t *data ) {
	return crc32( 0, (u_char *)data, SYSTEMDATA_CONTENT_SIZE );
} 


/** 
 * _sysdata_check_with_read( ) - stored data integration check
 * 
 * This reads data from the device and check that the
 * CRC of the sensitive data match with the stored CRC
 * 
 * @return 0 for data integration, -1 for corrupted data
*/
int _sysdata_check_with_read( void ) {
	int       ret;
	int       crc;
	
#ifndef USE_HOSTCC
	ALLOC_CACHE_ALIGN_BUFFER( sysdata_t, tester, 1 );
#else
	sysdata_t *tester;
	tester = malloc( sizeof( sysdata_t ) );
#endif

	if ( tester == NULL )
		return -ENOMEM;

	ret = _sysdata_rd( tester );
	if ( ret ) {
		return ret;
	}

	crc = _get_data_crc32( &tester->data );

	if ( crc == tester->header.crc)
		return 0;
	else
		return -1;
}


/** 
 * _sysdata_read( ) read operation with data integration check
 * 
 * This uses the device selected at compilation time
 * and provides redundance data management
 * 
 * @table: main data structure in which to load stored data
 * @return 0 if ok, negative value if operation fails
*/
int _sysdata_read( sysdata_t *table ) { 
	int ret;
	int crc;

	if ( table == NULL )
		return -ENOMEM;

	ret = _sysdata_rd( table );
	if ( ret ) {
		return ret;
	}

	crc = _get_data_crc32( &table->data );

	if ( crc == table->header.crc)
		return 0;
	else
		return -1;
}


/** 
 * _sysdata_write( ) write operation with data integration check
 * 
 * This uses the device selected at compilation time
 * and provides redundance data management. 
 * the CRC is evaluated before the writing operation
 * A read follows the write operation in order to provide data integration.
 * 
 * @table: main data structure to store
 * @return 0 if ok, negative value if operation fails
*/
int _sysdata_write( sysdata_t *sysdata ) { 
	int ret;

	if ( sysdata == NULL )
		return -ENOMEM;

	sysdata->header.crc = _get_data_crc32( &sysdata->data );

	ret = _sysdata_wr( sysdata );
	if ( ret ) {
		return ret;
	}

	return _sysdata_check_with_read( );
}

/* __________________________________________________________________________________________
  |__________________________________________________________________________________________|
*/

/* __________________________________________________________________________________________
  |                                                                                          |
  |                                     HEADER MANAGEMENT                                    |
  |__________________________________________________________________________________________|
*/
/** 
 * _sysdata_header_init( ) - sysdata header initialization

 * This initializes the metadata of the sysdata structure
 * (the CRC field is invalid and not evaluated)
 * 
 * @return 0 if the pointer is not NULL, negative value else
*/
int _sysdata_header_init( sysdata_header_t *header ) {
	if ( header == NULL )
		return -EINVAL;

	header->version_major = VERSION_MAJOR;
	header->version_minor = VERSION_MINOR;
	header->crc           = 0; // not valid crc

	return 0;
}
/* __________________________________________________________________________________________
  |__________________________________________________________________________________________|
*/


/* __________________________________________________________________________________________
  |                                                                                          |
  |                                      TABLE MANAGEMENT                                    |
  |__________________________________________________________________________________________|
*/
/** 
 * _sysdata_switching_table_init( ) - swiching data initialization

 * This initializes the data relative to the switching partition structure
 * 
 * @return 0 if the pointer is not NULL, negative value else
*/
int _sysdata_switching_table_init( switching_table_t *table ) {
	int i = 0;
    if ( table == NULL ) {
        return -EINVAL;
	}

	for ( i = 0 ; i < SYSDATA_MAX_SLOTS ; i++ ) {
    	table->boot_slot_counter[i] = 0;
		table->boot_fail_counter[i] = 0;
		table->boot_slot_order[i] = i + 1;
	}
	
    table->current_boot_slot = 1;
	table->previous_boot_slot = SLOT_NULL;	
	table->enable_flags = ( SYSDATA_GLOBAL_BOOT_COUNT_ENABLE << GLOBAL_BOOT_COUNT_ENABLE_SHIFT );

	return 0;
}


/** 
 * _sysdata_data_init( ) - all data initialization

 * This initializes all sensitive data of the sysdata structure
 * (calls sequentially all sub data initialization functions )
 * 
 * @return 0 if the pointer is not NULL, negative value else
*/
int _sysdata_data_init( sysdata_data_t *data ) {
	int ret = 0;

	if ( data == NULL )
		return -EINVAL;

	ret = _sysdata_switching_table_init( &data->switch_table );
	if ( ret ) return ret;

	return 0;
}


/** 
 * _sysdata_table_init( ) - sysdata structure initialization (global function)

 * This initializes the whole sysdata structure (both meta-data and sensitive data)
 * (calls sequentially all sub data initialization functions )
 * 
 * @return 0 if the pointer is not NULL, negative value else
*/
int _sysdata_table_init( sysdata_t *table ) {
	int ret = 0;

	if ( table == NULL )
		return -EINVAL;

	ret = _sysdata_header_init( &table->header );
	if ( ret ) return ret;

	ret = _sysdata_data_init( &table->data );
	if ( ret ) return ret;

	return 0;
}


/** 
 * sysdata_table_init( ) - sysdata structure initialization (global function)

 * This initializes the whole sysdata structure (both meta-data and sensitive data)
 * (calls sequentially all sub data initialization functions )
 * 
 * @return 0 if the pointer is not NULL, negative value else
*/
int sysdata_init ( void ) {
	int       ret;
	
#ifndef USE_HOSTCC
	ALLOC_CACHE_ALIGN_BUFFER( sysdata_t, sysdata, 1 );
#else
	sysdata_t *sysdata;
	sysdata = malloc( sizeof( sysdata_t ) );
#endif
	
	if ( sysdata == NULL )
		return -ENOMEM;

	ret = _sysdata_table_init( sysdata );
	if ( ret ) {
		return ret;
	}

	return _sysdata_write( sysdata );
}


/** 
 * sysdata_integration_check( ) - check integration of whole sysdata table
 *
 * This check the state of system data. If needed, this initializes the table.
 * 
 * @return 0 if the data are ok, 1 if was needed to initialize the data
 * (may be is the first boot), -1 if the initialization procedure returns an error
*/
int sysdata_integration_check( void ) {
	int ret;

	ret = _sysdata_check_with_read( );
	if ( ret ) {
		// data not valid.. initialize it
		ret = sysdata_init( );
		if ( ret ){
			// impossible to initialize it
			return -1;
		} else {
			return 1;
		}
	}

	return 0;
}
/* __________________________________________________________________________________________
  |__________________________________________________________________________________________|
*/


/* __________________________________________________________________________________________
  |                                                                                          |
  |                                          GENERIC DATA                                    |
  |__________________________________________________________________________________________|
*/
/** 
 * get_sysdata_version( ) - version of the sysdata structure
 *
 * This gets string including the version of the sysdata structure
 * in the form of <major version>.<minor version>
 * 
 * @return the string with sysdata version
*/
int get_sysdata_version( char **version ) {
	sysdata_t *table;
	int       ret;

	*version = malloc( sizeof( char ) * SYSDATA_VERSION_LEN );
	if ( ! version )
		return -1;

	table = malloc( sizeof( sysdata_t ) );
	if ( !table ) {
		free( version );
		return -1;
	}
	 
	ret = _sysdata_read( table );
	if ( ret ) {
		free( version );
		free( table );
		return -1;
	}

	sprintf( *version, "%03d.%03d", table->header.version_major, 
				table->header.version_minor );

	free( table );

	return 0;
	//return (const char *)version;
}
/* __________________________________________________________________________________________
  |__________________________________________________________________________________________|
*/


/* __________________________________________________________________________________________
  |                                                                                          |
  |                                       SWITCHSLOT DATA                                    |
  |__________________________________________________________________________________________|
*/


/** 
 * get_switchslot_max_fail_boot( ) - maximum allowed consecutive boot fail
 * 
 * @return the maximum allowed consecutive boot fail
*/
inline int get_switchslot_max_fail_boot( void ) {
	return (int)SYSDATA_MAX_FAIL_BOOT;
}

/** 
 * get_switchslot_data( ) - all data about switching partition system
 *
 * This gets the data structure containing the actual switching  
 * partition system's state
 * 
 * @return the current switching_table_t data
*/
 int get_switchslot_data( switching_table_t *switch_data ) {
	sysdata_t *table;
	int       ret;
	 
	if ( switch_data == NULL )
		return -EINVAL;

	table = malloc( sizeof( sysdata_t ) );
	if ( !table ) {
		return -ENOMEM;
	}

	ret = _sysdata_read( table );
	if ( ret ) {
		free( table );
		return -ENOSYS;
	}

	if ( memcpy( switch_data, &table->data.switch_table, sizeof( switching_table_t ) ) == NULL )
		return -1;

	return 0;
 }


/** 
 * sysdata_switchslot_init( ) - sysdata switchslot structure initialization

 * This initializes the sysdata switchslot structure 
 * 
 * @return 0 if the pointer is not NULL, negative value else
*/
int sysdata_switchslot_init ( void ) {
	
	switching_table_t switch_data;
	int       ret;
	

	ret = _sysdata_switching_table_init( &switch_data );
	if ( ret ) {
		return ret;
	}


#ifndef USE_HOSTCC
	ALLOC_CACHE_ALIGN_BUFFER( sysdata_t, sysdata, 1 );
#else
	sysdata_t *sysdata;
	sysdata = malloc( sizeof( sysdata_t ) );
#endif
	if ( !sysdata ) {
		return -ENOMEM;
	}

	ret = _sysdata_read( sysdata );
	if ( ret ) {
		free( sysdata );
		return -ENOSYS;
	}

	if ( memcpy( &sysdata->data.switch_table, &switch_data, sizeof( switching_table_t ) ) == NULL )
		return -1;

	return 0;
}


/** 
 * sysdata_switchslot_set_enable_global_counter( ) - set the enable for the global boot counter
 *
 * This set the global boot counter enable
 * 
 * @return 0 if ok, error number otherwise
*/
int sysdata_switchslot_set_enable_global_counter( int en ) {
	int               ret = 0;
	sysdata_t         table;

	ret = _sysdata_read( &table );
	if ( ret < 0 )
		return ret;

	if ( GET_GLOBAL_BOOT_COUNT_ENABLE(table.data.switch_table.enable_flags) == !!en )
		return 0;

	table.data.switch_table.enable_flags = !!en ? table.data.switch_table.enable_flags | GLOBAL_BOOT_COUNT_ENABLE_MASK :
						table.data.switch_table.enable_flags &  ~GLOBAL_BOOT_COUNT_ENABLE_MASK;
	
	ret = _sysdata_write( &table );
	if ( ret < 0 )
		return ret;

	return 0;
}


/** 
 * sysdata_switchslot_set_enable_fail_counter( ) - set the enable for the fail boot counter
 *
 * This set the fail boot counter enable
 * 
 * @return 0 if ok, error number otherwise
*/
int sysdata_switchslot_set_enable_fail_counter( int en ) {
	int               ret = 0;
	int               i;
	sysdata_t         table;

	ret = _sysdata_read( &table );
	if ( ret < 0 )
		return ret;

	if ( GET_FAIL_BOOT_COUNT_ENABLE(table.data.switch_table.enable_flags) == !!en )
		return 0;

	table.data.switch_table.enable_flags = !!en ? table.data.switch_table.enable_flags | FAIL_BOOT_COUNT_ENABLE_MASK :
						table.data.switch_table.enable_flags &  ~FAIL_BOOT_COUNT_ENABLE_MASK;

	if ( !!en ) {
		for ( i = 0 ; i < SYSDATA_MAX_SLOTS ; i++ ) {
			table.data.switch_table.boot_fail_counter[i] = 0;
		}
	}

	ret = _sysdata_write( &table );
	if ( ret < 0 )
		return ret;

	return 0;
}


int sysdata_switchslot_get_primary_slot( int *primary ) {
	int               ret = 0;
	switching_table_t switch_data;

	ret = get_switchslot_data( &switch_data );
	if ( ret < 0 )
		*primary = SLOT_NULL;
	else
		*primary =  switch_data.boot_slot_order[0];

	return 0;
}


int sysdata_switchslot_get_valid_slot( int *curr_slot ) {
	int               ret = 0;
	int               i;
	switching_table_t switch_data;
	int               current = SLOT_NULL;
	int               slot;

	ret = get_switchslot_data( &switch_data );
	if ( ret >= 0 ) {
		for ( i = 0 ; i < SYSDATA_MAX_SLOTS ; i++ ) {
			slot = switch_data.boot_slot_order[i];
			if ( slot == SLOT_NULL )
				continue;
			if ( switch_data.boot_fail_counter[slot - 1] <= get_switchslot_max_fail_boot( ) ) {
				current = slot;
				break;
			}
		}
	}

	*curr_slot = current;
	return ret;
}


/** 
 * sysdata_swtchslot_update_current_slot( ) - update the current slot to use
 *
 * This evaluate and eventually update the current slot to use
 * 
 * @return updated current slot if ok, error number otherwise
*/
int sysdata_swtchslot_update_current_slot( void ) {
	int               ret = 0;
	int               valid = SLOT_NULL;
	sysdata_t         table;

	ret = _sysdata_read( &table );
	if ( ret < 0 )
		return ret;

	ret = sysdata_switchslot_get_valid_slot( &valid );
	if ( ret < 0 )
		return ret;

	if ( valid != SLOT_NULL ) {
		if ( table.data.switch_table.current_boot_slot != valid ) {
			
			table.data.switch_table.current_boot_slot = valid;
			ret = _sysdata_write( &table );
			if ( ret < 0 )
				return ret;

		}
	} 	

	return valid;
}


/** 
 * sysdata_switchslot_get_current_slot( ) - get current slot
 * 
 * This get the current slot stored in media storage
 * 
 * @return 0 if ok, error number otherwise
*/
int sysdata_switchslot_get_current_slot( int *current ) {
	int               ret = 0;
	sysdata_t         table;

	ret = _sysdata_read( &table );
	if ( ret < 0 )
		return ret;

	*current = table.data.switch_table.current_boot_slot;
	return 0;
}


/** 
 * sysdata_switchslot_get_previous_slot( ) - get previous slot
 * 
 * This get the previous slot stored in media storage
 * 
 * @return 0 if ok, error number otherwise
*/
int sysdata_switchslot_get_previous_slot( int *previous ) {
	int               ret = 0;
	sysdata_t         table;

	ret = _sysdata_read( &table );
	if ( ret < 0 )
		return ret;

	*previous = table.data.switch_table.previous_boot_slot;
	return 0;
}


#ifndef USE_HOSTCC
/** 
 * sysdata_switchslot_current_env( ) - create a environment variable for valid slot
 *
 * This creates a environment variable for valid slot
 * 
 * @return void
*/
void sysdata_switchslot_current_env( void ) {
	int               ret;
	int               valid = SLOT_NULL;
	char              part[5];

	ret = sysdata_switchslot_get_valid_slot( &valid );
	if ( ret < 0 )
		return;

	sprintf ( part, "%c", SLOT_NUM2ID( valid ) );
	env_set ( ENV_SYSDATA_GROUP_PART, (const char *)&part );
}
#endif    /*  USE_HOSTCC  */


/** 
 * sysdata_switchslot_boot_counter_update( ) - increment the number of boot for current slot
 *
 * This increments the number of boot for current slot
 * 
 * @return 0 if ok, error number otherwise
*/
int sysdata_switchslot_boot_counter_update( void ) {
	int               ret = 0;
	sysdata_t         table;
	int               changed = 0;

	ret = _sysdata_read( &table );
	if ( ret < 0 )
		return ret;

	if ( GET_GLOBAL_BOOT_COUNT_ENABLE(table.data.switch_table.enable_flags) ) {
		table.data.switch_table.boot_slot_counter[table.data.switch_table.current_boot_slot - 1]++;
		changed = 1;
	}

	if ( GET_FAIL_BOOT_COUNT_ENABLE(table.data.switch_table.enable_flags) ) {
		table.data.switch_table.boot_fail_counter[table.data.switch_table.current_boot_slot - 1]++;
		changed = 1;
	}

	if ( changed ) {
		ret = _sysdata_write( &table );
		if ( ret < 0 )
			return ret;
	}
			
	return 0;
}


/** 
 * sysdata_switchslot_boot_ok( ) - initialize the fail boot counter
 *
 * This initializes the fail boot counter in order to sign a valid boot.
 * There is also an updade of the previous_boot_slot feld
 * 
 * @return 0 if ok, error number otherwise
*/
int sysdata_switchslot_boot_ok( void ) {
	int               ret = 0;
	int               changed = 0;
	sysdata_t         table;

	ret = _sysdata_read( &table );
	if ( ret < 0 )
		return ret;

	if ( GET_FAIL_BOOT_COUNT_ENABLE(table.data.switch_table.enable_flags) ) {
		changed = 1;
	} else {
		if ( table.data.switch_table.boot_fail_counter[table.data.switch_table.current_boot_slot - 1] != 0 ) {
			changed = 1;
		}
	}

	if ( table.data.switch_table.previous_boot_slot != table.data.switch_table.current_boot_slot ) {
		changed = 1;
	}

	if ( changed ) {
		table.data.switch_table.previous_boot_slot = table.data.switch_table.current_boot_slot;
		table.data.switch_table.boot_fail_counter[table.data.switch_table.current_boot_slot - 1] = 0;
		ret = _sysdata_write( &table );
		if ( ret < 0 )
			return ret;
	}
			
	return 0;
}


/** 
 * sysdata_switchslot_set_primary( ) - set the primary slot with the passed slot ID
 *
 * This set the primary slot with the passed slot ID. The passed slot is placed into
 * the head of boot order list and the other positions are filled with the previous
 * order.
 * 
 * @return 0 if ok, error number otherwise
*/
int sysdata_switchslot_set_primary( char slot_id ) {
	int               ret = 0;
	int               i, j;
	sysdata_t         table;
	int               slot = SLOT_ID2NUM(slot_id);
	uint8_t           new_boot_slot_order[SYSDATA_MAX_SLOTS];

	if ( !SLOT_IS_VALID( slot ) )
		return -1;

	ret = _sysdata_read( &table );
	if ( ret < 0 )
		return ret;

	new_boot_slot_order[0] = slot;
	for ( i = 0, j = 1 ; i < SYSDATA_MAX_SLOTS ; i++ ) {
		if ( table.data.switch_table.boot_slot_order[i] == slot )
			continue;
		new_boot_slot_order[j] = table.data.switch_table.boot_slot_order[i];
		j++;
	}
	if ( memcpy( &table.data.switch_table.boot_slot_order[0],
				 &new_boot_slot_order[0], sizeof( new_boot_slot_order  ) ) == NULL )
		return -1;

	table.data.switch_table.boot_fail_counter[slot - 1] = 0;

	ret = _sysdata_write( &table );
		if ( ret < 0 )
			return ret;

	return 0;
}


/** 
 * sysdata_switchslot_remote_from_boot_order( ) - remove slot from boot order list
 *
 * This remove slot from boot order list.
 * 
 * @return 0 if ok, error number otherwise
*/
int sysdata_switchslot_remote_from_boot_order( char slot_id ) {
	int               ret = 0;
	int               i, j;
	sysdata_t         table;
	int               slot = SLOT_ID2NUM(slot_id);
	uint8_t           new_boot_slot_order[SYSDATA_MAX_SLOTS];
	
	if ( !SLOT_IS_VALID( slot ) )
		return -1;

	ret = _sysdata_read( &table );
	if ( ret < 0 )
		return ret;

	for ( i = 0, j = 0 ; i < SYSDATA_MAX_SLOTS ; i++ ) {
		if ( table.data.switch_table.boot_slot_order[i] == slot )
			continue;

		new_boot_slot_order[j] = table.data.switch_table.boot_slot_order[i];
		j++;
	}

	for ( ; j < SYSDATA_MAX_SLOTS ; j++ ) {
		new_boot_slot_order[j] = SLOT_NULL;
	}

	if ( memcpy( &table.data.switch_table.boot_slot_order[0],
				 &new_boot_slot_order[0], sizeof( new_boot_slot_order  ) ) == NULL )
		return -1;

	ret = _sysdata_write( &table );
		if ( ret < 0 )
			return ret;

	return 0;
}


/** 
 * sysdata_switchslot_invalidate_slot( ) - invalidate slot
 *
 * This invalidate slot:
 * - remote it from the boot order list
 * - set its fail boot counter as maximum allowed plus 1
 * 
 * @return 0 if ok, error number otherwise
*/
int sysdata_switchslot_invalidate_slot( char slot_id ) {
	int               ret = 0;
	sysdata_t         table;
	int               i, j;
	uint8_t           new_boot_slot_order[SYSDATA_MAX_SLOTS];
	int               slot = SLOT_ID2NUM(slot_id);

	if ( !SLOT_IS_VALID( slot ) )
		return -1;

	ret = _sysdata_read( &table );
	if ( ret < 0 )
		return ret;

	for ( i = 0, j = 0 ; i < SYSDATA_MAX_SLOTS ; i++ ) {
		if ( table.data.switch_table.boot_slot_order[i] == slot )
			continue;

		new_boot_slot_order[j] = table.data.switch_table.boot_slot_order[i];
		j++;
	}

	for ( ; j < SYSDATA_MAX_SLOTS ; j++ ) {
		new_boot_slot_order[j] = SLOT_NULL;
	}

	if ( memcpy( &table.data.switch_table.boot_slot_order[0],
				 &new_boot_slot_order[0], sizeof( new_boot_slot_order  ) ) == NULL )
		return -1;

	table.data.switch_table.boot_fail_counter[slot - 1] = get_switchslot_max_fail_boot( ) + 1;

	ret = _sysdata_write( &table );
	if ( ret < 0 )
		return ret;
	
	return 0;
}


/** 
 * sysdata_switchslot_reset_fail_counter( ) - reset fail boot counter for passed slot ID
 *
 * This reset fail boot counter for passed slot ID
 * 
 * @return 0 if ok, error number otherwise
*/
int sysdata_switchslot_reset_fail_counter( char slot_id ) {
	int               ret = 0;
	sysdata_t         table;
	int               slot = SLOT_ID2NUM(slot_id);

	if ( !SLOT_IS_VALID( slot ) )
		return -1;

	ret = _sysdata_read( &table );
	if ( ret < 0 )
		return ret;

	table.data.switch_table.boot_fail_counter[slot - 1] = 0;

	ret = _sysdata_write( &table );
		if ( ret < 0 )
			return ret;

	return 0;
}
/* __________________________________________________________________________________________
  |__________________________________________________________________________________________|
*/
