/*
 * (C) Copyright 2015 Seco
 *
 * Author: Davide Cardillo <davide.cardillo@seco.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 *
 * This code provide a tool to properly configure the boot environment
 * for the Seco Boards.
 *
 */


/*
 * Boot support
 */
#include <common.h>
#include <command.h>
#include <env.h> 
#include <stdio_dev.h>
#include <linux/ctype.h>

#include <common.h>
#include <command.h>
#include <malloc.h>
#include <stdio_dev.h>
#include <timestamp.h>
#include <version.h>


#include "seco_config_lib.h"



static int do_seco_config (struct cmd_tbl *cmdtp, int flag, int argc, char * const argv[]) {
	int set_kernel = 0;
    int set_fdt = 0;
	int set_ramfs = 0;
	int set_filesystem = 0;
	int set_video = 0;
	int set_peripheral = 0;
	int set_fdt_overlay_load = 0;

	int selected_video = 0;

    int use_tftp = 0;

   	if (argc > 2)
		return cmd_usage (cmdtp);


	if (argc == 2 && strcmp(argv[1], "help") == 0)
		return cmd_usage (cmdtp);


	if (argc == 2) {

		if (strcmp(argv[1], "default") == 0) {
			env_set_default ("## Resetting to default environment", 0);
		}

		if (strcmp(argv[1], "ksrc") == 0) {
			set_kernel = 1;
		}

		if (strcmp(argv[1], "fdtsrc") == 0) {
			set_fdt = 1;
		}

		if (strcmp(argv[1], "ramfssrc") == 0) {
			set_ramfs = 1;
		}

		if (strcmp(argv[1], "fssrc") == 0) {
			set_filesystem = 1;
		}

		if (strcmp(argv[1], "video") == 0) {
			set_video = 1;
		}

		if (strcmp(argv[1], "peripheral") == 0) {
			set_peripheral = 1;
		}

		if (strcmp(argv[1], "fdtoverlay") == 0) {
			set_fdt_overlay_load = 1;
		}

	}


    if (argc == 1) {
		set_kernel = 1;
        set_fdt = 1;
		set_ramfs = 1;
		set_filesystem = 1;
		set_video = 1;
		set_peripheral = 1;
	}


/*  __________________________________________________________________________
 * |________________________________ KERNEL __________________________________|
 */
	if ( set_kernel) {
        set_kernel_source( &use_tftp );
    }

/*  __________________________________________________________________________
 * |__________________________________ FDT ___________________________________|
*/
	if ( set_fdt ) {
        set_fdt_source( &use_tftp );
    }

/*  __________________________________________________________________________
 * |_________________________________ RAMFS __________________________________|
*/
	if ( set_ramfs ) {
        set_ramfs_source( &use_tftp );
    }

/*  __________________________________________________________________________
 * |__________________________________ TFTP __________________________________|
*/
	if ( set_kernel || set_fdt || set_ramfs ) {
        set_for_tftp( use_tftp );
	}
/*  __________________________________________________________________________
 * |______________________________ FILE SYSTEM _______________________________|
 */
	if ( set_filesystem ) {
        set_filesystem_source( );
    }

/*  __________________________________________________________________________
 * |______________________________ VIDEO OUTPUT ______________________________|
 */
 	if ( set_video ) {
        selected_video = set_video_mode( -1 );
		set_video_specification( selected_video );
	}

/*  __________________________________________________________________________
 * |___________________________ PERIPHERAL OUTPUT ____________________________|
 */
 	if ( set_peripheral ) {
        set_peripherals( -1 );
    }
	
	if ( set_fdt_overlay_load ) {
		create_overlay_per_dynamic_load_cmd( );
		create_overlay_video_dynamic_load_cmd( );
	}

	if ( set_fdt || set_ramfs || set_kernel || set_fdt || set_ramfs ||
			set_filesystem || set_peripheral 
			|| set_video
		) 
	{
    	printf ("\n\n");
    	env_save ();
   		printf ("\n\n");
	}

    return 0;
}

U_BOOT_CMD(
	seco_config, 3, 1, do_seco_config,
	"Interactive setup for seco configuration.",
	"                           - set whole environment\n"
	"seco_config [default]      - resetting to default environment\n"
	"seco_config [memory]       - set kernel RAM size.\n"
	"seco_config [ksrc]         - set Kernel source device.\n"
	"seco_config [fdtsrc]       - set FDT source device.\n"
	"seco_config [fssrc]        - set FileSystem source device.\n"
	"seco_config [video]        - set video usage mode\n"
	"seco_config [fdtoverlay]   - set fdt overlay load command\n"
);
