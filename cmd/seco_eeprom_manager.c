// SPDX-License-Identifier: GPL-2.0+

/*
 * (C) Copyright 2024 SECO
 *
 * Tobias Kahlki <tobias.kahlki@seco.com>
 */

#include <common.h>
#include <command.h>
#include <stdlib.h>
#include <errno.h>

#include "../board/seco/common/seco_eeprom_manager.h"

#define DEBUG 0

#define ENV_PANEL_ID "devconf_panel_id"
#define ENV_TOUCH_ID "devconf_touch_id"

/*
 * Command Functions
 */

/*
 * Gets the supported UIDs from the EEPROM manager library and writes
 * them to the environment and the stdout.
 *
 * Note: The panel_id and touch_id are currently stored in ASCII format.
 * For example ID 1 has the numerical value of 0x31, 2 has the value of
 * 0x32 and so on.
 * For better handling, we subtract 0x30 (= '0') from the ID to get the
 * numerical value.
 */
static void do_seco_eeprom_manager_get(const u8 uid, const bool print)
{
	u8 panel_id = 0, touch_id = 0;

	switch(uid)
	{
		case UID_PANEL_ID:
			if (seco_eeprom_get_panel_id(&panel_id) == 0 && panel_id != 0)
			{
				panel_id -= '0';
				env_set_ulong(ENV_PANEL_ID, panel_id);

				if (print)
					printf("panel_id: %d\n", panel_id);
			}
			break;

		case UID_TOUCH_ID:
			if(seco_eeprom_get_touch_id(&touch_id) == 0 && touch_id != 0)
			{
				touch_id -= '0';
				env_set_ulong(ENV_TOUCH_ID, touch_id);

				if (print)
					printf("touch_id: %d\n", touch_id);
			}
			break;

		default:
			if (print)
				printf("Error: Unknown/Unsupported UID \"0x%02X\"\n", uid);
	}
}

static void do_seco_eeprom_manager_env(void)
{
	u8 uid = 0;

	for (uid = 0; uid < UID_NONE; ++uid)
		do_seco_eeprom_manager_get(uid, false);
}

static void do_seco_eeprom_manager_print(void)
{
	seco_eeprom_print_all();
}

static int do_seco_eeprom_manager(struct cmd_tbl *cmdtp, int flag, int argc, char *const argv[])
{
	unsigned long i2c_bus = 0, i2c_addr = 0, uid = 0;
	char *end = NULL;

#if DEBUG
	printf("argc: %d\n", argc);

	for(int i = 0; i < argc; ++i)
		printf("argv[%d]: %s\n", i, argv[i]);
#endif

	if (argc < 4)
		return CMD_RET_USAGE;

	i2c_bus = simple_strtoul(argv[2], &end, 0);
	i2c_addr = simple_strtoul(argv[3], &end, 0);

	seco_eeprom_init(i2c_bus, i2c_addr);

	if (!strcmp(argv[1], "get") && argc == 5)
	{
		uid = simple_strtoul(argv[4], &end, 0);
		do_seco_eeprom_manager_get(uid, true);
	}
	else if (!strcmp(argv[1], "env"))
		do_seco_eeprom_manager_env();
	else if (!strcmp(argv[1], "print"))
		do_seco_eeprom_manager_print();
	else
		return CMD_RET_USAGE;

	return 0;
}

#define CMD_DESC "SECO EEPROM Manager"

#define CMD_HELP "get <I2C_BUS> <I2C_ADDR> <UID>\n" \
		 "                    env <I2C_BUS> <I2C_ADDR>\n" \
		 "                    print <I2C_BUS> <I2C_ADDR>\n"

U_BOOT_CMD(
	seco_eeprom_manager, 6, 0, do_seco_eeprom_manager,
	CMD_DESC,
	CMD_HELP
);
